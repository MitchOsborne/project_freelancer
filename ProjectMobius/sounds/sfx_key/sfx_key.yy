{
  "$GMSound":"",
  "%Name":"sfx_key",
  "audioGroupId":{
    "name":"audiogroup_default",
    "path":"audiogroups/audiogroup_default",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.344694,
  "name":"sfx_key",
  "parent":{
    "name":"SFX",
    "path":"folders/Sounds/SFX.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sfx_key.wav",
  "type":0,
  "volume":1.0,
}