/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(sprite_index != -1)
{
	image_speed = owner.timeScale * global.timeScale;
	depth = baseDepth - moveSpeed;
	if(isPsiTouched) DrawOutline(id, c_psiLPurple, 4);
	else draw_self();
}