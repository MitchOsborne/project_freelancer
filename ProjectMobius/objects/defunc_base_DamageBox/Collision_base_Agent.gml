/// @description Insert description here
// You can write your code in this editor
event_inherited();

if(!isHeld)
{
	if(friendlyFire || friendlyOnly)
	{
		if(teamID == other.teamID)
		{
			//forceDir = GetDirection(x,y,other.x,other.y,false, true);
			forceDir = direction - 90;
			OnProjHit(id,other);
		}
	}else
	{
		if(teamID != other.teamID)
		{
			//forceDir = GetDirection(x,y,other.x,other.y,false, true);
			forceDir = direction - 90;
			
			OnProjHit(id,other);
		}
	}
}