/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

damageType = weapon.abl_DmgType;
moveSpeed = baseSpeed;


for(var i = 0; i < array_length(dmg_modArr); i += 1)
{
	dmg_modArr[i].mod_PreStep();
	dmg_modArr[i].mod_Update();
	dmg_modArr[i].mod_EndStep();
}

for(var i = 0; i < array_length(dmg_Components); i += 1)
{
	dmg_Components[i].dmg_comp_Update();	
}


//Because image index is stored as a decimal, and I need to convert it to whole image indexes, then convert it to an array index
var tFrame = floor(image_index);
var tI = clamp(tFrame, 0, array_length(dmg_activeFrames)-1);	//So the frames dont go beyond the boundaries of the activeFrames array

dmg_canDamage = dmg_activeFrames[tI];
