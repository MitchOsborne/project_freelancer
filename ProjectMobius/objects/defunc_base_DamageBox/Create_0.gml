/// @description Insert description here
// You can write your code in this editor
event_inherited();

weapon = undefined;
teamID = TEAMID.Neutral;

dmg_EnemyProj = id;

leadProjectile = undefined;
damageType = DMG_TYPE.HARD;

mx = 0;
my = 0;

baseSpeed = 0;
moveSpeed = baseSpeed;
speedMod = 1;
dmg_speedCurve = undefined;
range = 0;
distTravelled = 0;
distLerp = 0;
altSpeed = false;

damage = 0;

dmg_hitSlow = 0;



dmg_activeFrames = [true];
dmg_Components = [];
dmg_canDamage = false;
friendlyOnly = false;
friendlyFire = false;
objectsDamaged = ds_list_create();	//Keeps track of damaged objects
damageCooldown = 10;	//Number of frames before it can damage a previously damaged object

dmg_ambLight = instance_create_layer(x,y,"Projectiles", light_AmbProj);
dmg_ambLight.owner = id;


hp = infinity;

dmg_maxBounces = 0;
maxPierces = 0;

ignoreObstacles = false;
lifeTime = 10;
lifeTimer = 0;
dmg_persists = false;	
dmg_unblockable = false;


spawnParticle = undefined;
spawnRate = .05;
spawnTimer = 0;

isStasis = false;
isHeld = false;
isPsiTouched = false;

in_Released = false;


forceStrength = 2;

forceDir = 0;
forceDur = 0;
stunDur = 0;

locomotorType = ploco_1;
locomotor = undefined;
dmg_modArr = [];


baseDepth = depth;


ShakeScreen(5);









