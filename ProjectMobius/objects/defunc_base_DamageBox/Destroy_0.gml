/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(weapon != undefined)
{
	ds_list_delete(weapon.abl_Instances, ds_list_find_index(weapon.abl_Instances,id));
}

if(teamID = TEAMID.Ally)
{
	array_delete(global.allyProjArr, array_find(global.allyProjArr,id),1);	
	ds_list_delete(global.l_allyProjArr, ds_list_find_index(global.l_allyProjArr,id));
}else
{
	array_delete(global.enmProjArr, array_find(global.enmProjArr,id),1);	
	ds_list_delete(global.l_enmProjArr, ds_list_find_index(global.l_enmProjArr,id));
}

if(CheckExistance(dmg_ambLight)) instance_destroy(dmg_ambLight);

ShakeScreen(5);
ds_list_destroy(objectsDamaged);
