/// @description Insert description here
// You can write your code in this editor
event_inherited();
enabled = true;


targetingAngle = 45;
maxRotSpeed = 4.5;

retargetTime = 10;
retargetTimer = retargetTime/2;

target = undefined;

homingComp = new dmg_comp_Homing(id);
array_push(dmg_Components, homingComp);