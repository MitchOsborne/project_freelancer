/// @description Insert description here
// You can write your code in this editor

if(global.gameRunning)
{
	global.EventRunning = false;
	for(var i = 0; i < array_length(global.currentRoom.rd_events); i += 1)
	{
		var tEv = global.currentRoom.rd_events[i];
		if(tEv.rmEvent_IsActive)
		{
			if(global.gameState <= GAME_STATE.CUSTOM)
			{
				global.EventRunning = true;
				tEv.rmEvent_EventActive();
				break;
			}
		}
		else
		{
			tEv.rmEvent_EventInactive();
			if(tEv.rmEvent_Trigger())
			{
				tEv.rmEvent_OnTrigger();
			}
		}
	}
	
}

