/// @description Insert description here
// You can write your code in this editor
event_inherited();

var tLvl = uiEl_StatSlot.stat_ABLLvl;
var tCost = 1 + tLvl;
uiEl_text = "Upgrade ABL Stat: Lvl " + string(tLvl);
uiEl_tooltipTxt = "Ability Cores: " + string(tCost) + "/" + string(global.inv_upgradeParts.ABL);