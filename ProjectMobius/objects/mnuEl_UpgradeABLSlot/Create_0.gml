/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


	uiEl_slotIndex = 0;
	uiEl_StatCard = UI_GetCharCard(global.editChar);
	uiEl_StatSlot = -1;
	uiEl_text = "Upgrade ABL Stat: ";
	uiEl_tooltipTxt = "";
	uiEl_sprite = spr_None;
	
	uiEl_AltStat = false;
		
	uiEl_Function = function()
	{
		var tLvl = uiEl_StatSlot.stat_ABLLvl;
		if(tLvl < 5)
		{
			var tCost = 1;// + tLvl;
		
			if(tCost <= global.inv_upgradeParts.ABL)
			{
				uiEl_StatSlot.stat_ABLLvl += 1;
				global.inv_upgradeParts.ABL -= tCost;
			}
		}
	}
	
	uiEl_Init = function()
	{
		uiEl_StatSlot = global.StatCardUnlocks.stat_UnlockArr[uiEl_parentMenu.ui_slotIndex];
		//uiEl_text = string(uiEl_NewMod.stat_currLvl);
		//uiEl_tooltipTxt = uiEl_NewMod.stat_mainTxt + "-" + uiEl_NewMod.stat_altTxt;
		uiEl_hasInit = true;
	
	}