/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_dragonboss_idle_f;

anim_idle_front = spr_dragonboss_idle_f;
anim_idle_back = spr_dragonboss_idle_f;

anim_walk_front = spr_dragonboss_walk_f;
anim_walk_back = spr_dragonboss_walk_f;

anim_dash_front = spr_dragonboss_walk_f;
anim_dash_back = spr_dragonboss_walk_f;

anim_incap = spr_dragonboss_incap;
anim_stun_front = spr_dragonboss_attack_f;
anim_stun_back = spr_dragonboss_attack_f;