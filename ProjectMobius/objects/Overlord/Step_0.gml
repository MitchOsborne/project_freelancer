/// @description Insert description here
// You can write your code in this editor
event_inherited();

if(keyboard_check_pressed(ord("O"))) 
{
	editTimeScale = true;
	global.timeScale -= 0.1;
}
if(keyboard_check_pressed(ord("P")))
{
	editTimeScale = true;
	global.timeScale += 0.1;
}
if(keyboard_check_pressed(ord("L"))) editTimeScale = false;

global.timeScale = clamp(global.timeScale, 0, 2);

if(global.gameRunning)
{

	//Ensures a parent character isn't deleted when room transitioning while it is disabled
	if(global.exitRoom)
	{
		for(var i = 0; i < instance_number(base_Character); i += 1)
		{
			var tChar = instance_find(base_Character, i);
			if(tChar.teamID == TEAMID.Ally)
			{
				if(tChar.smn_Master != undefined) instance_activate_object(tChar.smn_Master);
			}
		}	
	}

	if(global.exitRoom)
	{
		global.timeScale = 0;
		roomSwitchTmr = roomSwitchTime;
		endRoom = true;	
		global.exitRoom = false;
		if(global.currentRoom.rd_isCleared)
		{
			global.cp_Room = global.currentRoom;
			global.cp_ExitSide = global.entryPoint;
			global.cp_ExitIndex = global.entryIndex;
		}
	}

	if(endRoom || startRoom)
	{
		if(endRoom)
		{
			if(roomSwitchTmr <= 0)
			{
				instance_activate_object(global.Player1Char);
				roomCycle = true;
				endRoom = false;
			}
			else
			{
				fadeToBlack = true;
				roomSwitchTmr -= GetFixedDeltaTime();
			}
		}
		else if(startRoom)
		{
			if(roomSwitchTmr >= roomSwitchTime)
			{
				startRoom = false;
				fadeToBlack = false;
				global.timeScale = 1;
			}
			else 
			{
				fadeToBlack = true;
				roomSwitchTmr += GetFixedDeltaTime();
			}
		}
		roomSwitchTmr = clamp(roomSwitchTmr, 0, roomSwitchTime);
		var scrBlur = roomSwitchTmr/roomSwitchTime;
		clamp (scrBlur, 0, 1);
		if(scrBlur != 1) global.camBlurScale = 0.3 * scrBlur;
		else global.camBlurScale = 1;
	
	
	}

	if(global.rm != undefined)
	{
		room_Update();	
		if(roomCycle)
		{
			roomCycle = false;
			TransitionRoom();
		}
	}

	if(global.bm != undefined)
	{
		Update_Boss();	
	}


	for(var i = 0; i < ds_list_size(global.pkup_List); i += 1)
	{
		var tPkup = global.pkup_List[|i];
		script_execute(tPkup.func_UpdateStep, tPkup);
		script_execute(tPkup.func_OnPickup, tPkup);
		script_execute(tPkup.func_EndUpdateStep, tPkup);
		if(tPkup.lifeTimer >= tPkup.lifeTime)
		{
			delete(tPkup);
			ds_list_delete(global.pkup_List, i);
			i -= 1;
		}
	}
	var tPause = 0;
	for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
	{
		var tCont = global.PlayerIDList[|i].inputSource;	
		if(tCont.in_PAUSE == 1) 
		{
			if(!global.uiFocus)
			{
				var tPlayer = global.PlayerIDList[|i];
				CreateMenu(new Vector2(100, 100),mnu_CharMenu, tPlayer);
			}	
		}
	}
	
}