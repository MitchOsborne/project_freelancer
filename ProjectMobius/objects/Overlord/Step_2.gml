/// @description Insert description here
// You can write your code in this editor

for(var n = 0; n < array_length(global.TombstoneArr); n += 1)
{
	var tN = global.TombstoneArr[n];
	tN.tmb_Update();
}
if(global.gameRunning)
{
	for(var n = 0; n < array_length(global.currentRoom.rd_TombstoneArr); n += 1)
	{
		var tN = global.currentRoom.rd_TombstoneArr[n];
		if(tN.tmb_Delete)
		{
			array_delete(global.currentRoom.rd_TombstoneArr, n, 1);
			array_delete(global.TombstoneArr, array_find(global.TombstoneArr, tN), 1);
			var tInst = SpawnCharacter(tN.tmb_charData.charData_Object, tN.tmb_pos, undefined, TEAMID.Enemy);
			AttachMod(debuff_incap, tInst, tInst, 1, 1, false);
			n -= 1;
		}
	}
}