/// @description Insert description here
// You can write your code in this editor

roomSwitchTmr = 0;
startRoom = true;
SanityCheckLayers();

for(var i = 0; i < nyuk; i += 1)
{
	Debug_Log("Controller Connected: " + string(i) + " - ", gamepad_is_connected(i),true);
}
if(global.gameRunning)
{
	
	if(global.initPlayers)
	{
		if(global.Player1ID == undefined) AddPlayer(global.sv_Player1Char, global.p1Cont);
		
		if(global.sv_LoadGame)
		{
			sv_UpdateLoadout(global.Player1Char, global.sv_Player1Data);
			RefreshLoadout(global.Player1Char);
		}
		
		global.worldLvl = global.sv_PlayerLvl;
		global.inv_StatMods = global.sv_StatInv;
		
		global.initPlayers = false;
	}
	global.currRegion = GetRegion(global.currentRoom.rd_region);
	
	if(global.currentRoom.rd_EnemyLevel != -1) global.enemyLevel = global.currentRoom.rd_EnemyLevel;


	global.enemyIntensity = global.difficulty/5;

	global.roomClear = global.currentRoom.rd_isCleared;


	array_clear(global.spawnPool);
	for(var n = 0; n < array_length(global.currentRoom.rd_TombstoneArr); n += 1)
	{
		var tN = global.currentRoom.rd_TombstoneArr[n];
		if(tN.tmb_Delete)
		{
			array_delete(global.currentRoom.rd_TombstoneArr, n, 1);
			array_delete(global.TombstoneArr, array_find(global.TombstoneArr, tN), 1);
			n -= 1;
		}
	}
							

	global.rm = new RoomManager();


	global.cp_Respawn = false;

	if(instance_number(region_EnemySpawn) == 0)
	{
		global.roomClear = true;
		global.currentRoom.rd_isCleared = true;
	}
}
//global.spawnEnm = true;