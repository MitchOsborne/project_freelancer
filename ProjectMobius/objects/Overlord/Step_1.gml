/// @description Insert description here
// You can write your code in this editor
if(CheckExistance(global.Dummy) == false) global.Dummy = SpawnCharacter(Dummy, new Vector2(-100,-100), undefined, TEAMID.Enemy);


global.screen_middle.x = global.window_width/2;
global.screen_middle.y = global.window_height/2;

if(global.p1Cont != undefined) global.p1Cont.func_InputUpdate(global.p1Cont);
if(global.p2Cont != undefined) global.p2Cont.func_InputUpdate(global.p2Cont);


if(global.uiFocus) global.gameState = GAME_STATE.PAUSED;
else if(startRoom || endRoom) global.gameState = GAME_STATE.LOADING;
else if editTimeScale global.gameState = GAME_STATE.CUSTOM;
else global.gameState = GAME_STATE.NONE;

switch (global.gameState)
{
	case GAME_STATE.NONE:
		global.timeScale = 1;
		break;
	case GAME_STATE.PAUSED:
		global.timeScale = 0;
		break;
	case GAME_STATE.LOADING:
		global.timeScale = 0;
		break;
	case GAME_STATE.CUSTOM:
		break;
	default:
		global.timeScale = 1;
		break;		
}

//diffTimer += GetDeltaTime(global.timeScale);
if(diffTimer >= diffTime) 
{
	global.difficulty += diffScale;
	diffTimer = 0;
	lvlIndex += 1;
	
	if(lvlIndex < array_length(levelScaling)) global.enemyLevel = levelScaling[lvlIndex];
	else global.enemyLevel += 20;

}

if(global.BGM != global.currBGM)
{
	PlayBGM();	
}

for(var i = 0; i < array_length(global.SequenceArr); i += 1)
{
	var tSeq = global.SequenceArr[i];
	if(tSeq != undefined) layer_sequence_speedscale(tSeq, global.timeScale);
	else
	{
		array_delete(global.SequenceArr, i, 1);
		i -= 1;
	}
}

if(global.gameRunning && global.gameState != GAME_STATE.LOADING)
{
	
	var	xp = GetExpPool();
	global.worldLvl = floor(global.LevelCap*(1 - (xp/global.WorldExpPool)));
	var hasWiped = true;
	for(var i = 0; i < ds_list_size(global.coreAllyList); i += 1)
	{
		var tChar = global.coreAllyList[|i];
		if(CheckExistance(tChar))
		{
			if(tChar.isIncap == false) hasWiped = false;
		}else if(tChar.PlayerControlled == false)
		{
			ds_list_delete(global.coreAllyList,i);
			i -= 1;
		}
	}
	
	for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
	{
		var tChar = global.PlayerIDList[|i].slavedObj;
		if(tChar.isIncap == false) hasWiped = false;
	}
	
	if(hasWiped)
	{
		cpTimer += GetFixedDeltaTime();
	}else cpTimer = 0;
	if(cpTimer >= cpTime)
	{
		cpTimer = 0;
		global.cp_Respawn = true;
		if(global.cp_Room != undefined && global.cp_ExitSide != -1 && global.cp_ExitIndex != -1) 
		{
			global.entryPoint = room_GetExitCheckpoint(global.cp_ExitSide);
			global.entryIndex = global.cp_ExitIndex;
			global.nextRoom =  global.cp_Room;
		}else
		{
			global.cp_Room = global.currentRoom;	
			global.nextRoom =  global.currentRoom;
		}
		global.exitRoom = true;
		HealParty(50);
	}
}


if(global.scoreScreen)
{
	if(global.p1Cont.in_PAUSE)
	{	
		global.scoreScreen = false;
		game_restart();
	}
}

