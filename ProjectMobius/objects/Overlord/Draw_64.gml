/// @description Insert description here
// You can write your code in this editor

if(fadeToBlack)
{
	var fadeLrp = roomSwitchTmr/roomSwitchTime;
	draw_sprite_ext(spr_SolidBlack, 0, 0,0,global.window_width, global.window_height, 0, 0, 1-fadeLrp);	
}

if(global.initPlayers)
{
	
	draw_set_color(c_black);
	draw_set_alpha(0.8);
	draw_rectangle(0,0,global.view_width, global.view_height, false);
	
	draw_set_alpha(1);
	draw_set_color(c_white);
	
}else if(global.scoreScreen)
{	
	
	draw_set_color(c_black);
	draw_set_alpha(0.8);
	draw_rectangle(0,0,global.view_width, global.view_height, false);
	
	draw_set_alpha(1);
	draw_set_color(c_white);
	
	var levelReached = floor(global.enemyLevel/10)+1;
	draw_text(200,250, "Game Over");
	draw_text(200,270, "You Reached: Level " + string(levelReached));
	draw_text(200,290, "Combined Score: " + string(score));
	draw_text(200,330, "Player 1, Press (Start) or Enter Key to Restart");	
	
	
	
}else
{
	if(global.dm != undefined) DrawMiniMap();
	if(global.bm != undefined) DrawBossHP();
	draw_set_font(fnt_default);
	draw_set_color(c_white);
	DrawTextOutline(10,20,1, "Score: " + string(score), c_white, c_black);
	DrawTextOutline(10,30,1, "Enemy Level: " + string(global.enemyLevel + global.worldLvl), c_white, c_black);
	var tCol = c_white;
	if(global.alertMode == ALERT_STATE.Danger) tCol = c_red;
	else if (global.alertMode == ALERT_STATE.Warning) tCol = c_orange;
	DrawTextOutline(10,40,1, "Alert State: " + string(global.alertTxt), tCol, c_black);
	DrawTextOutline(10,50,1,"difficulty Increases in: " + string(diffTime - diffTimer), c_white, c_black);
	DrawTextOutline(10,60,1,"difficulty Level: " + string(global.difficulty), c_white, c_black);
	DrawTextOutline(10,70,1,"Global Timescale: " + string(global.timeScale), c_white, c_black);
	DrawTextOutline(10,80,1, "Room Cleared: " + string(global.currentRoom.rd_isCleared), tCol, c_black);
	
}	