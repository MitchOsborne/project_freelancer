/// @description Contains any resources that need ot be persistant and accessible from anywhere
// You can write your code in this editor

global.gameRunning = false;

nyuk = gamepad_get_device_count();

global.cp_Respawn = false;

diffTime = 45;
diffTimer = 0;
diffScale = 0.1;

roomSwitchTime = 0.5;
roomSwitchTmr = 0;
fadeToBlack = false;


roomCycle = false;
endRoom = false;
startRoom = false;

editTimeScale = false;

cpTimer = 0;
cpTime = 3;

p1CharIndex = 0;
p1ChosenChar = undefined;
p2CharIndex = 0;
p2ChosenChar = undefined;

completeInit = false;

lvlIndex = 0;
levelScaling = [1,3,5,10,15,20,25,30,35,40,50,65,80,100];

p1InReset = false;
p2InReset = false;

charArr = [char_Shepherd, char_Kamui];

roomArr = [];

arch_Init();


//draw options
draw_set_font(fnt_default);
draw_set_valign(fa_top);
draw_set_halign(fa_left);






