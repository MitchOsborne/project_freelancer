/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_list_add(modsToApply,  debuff_stunned);
applyModOnHit = true;

forceStrength = 75;

baseSpeed = 7.5;
damage = 1;
hp = infinity;
maxPierces = infinity;
lifeTime = 6;
dmg_persists = true;
dmg_unblockable = true;

stunDur = 0.3;


image_angle = direction;