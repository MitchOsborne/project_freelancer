{
  "$GMObject":"",
  "%Name":"tBladeWave",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":{"name":"base_Projectile","path":"objects/base_Projectile/base_Projectile.yy",},"eventNum":0,"eventType":4,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"tBladeWave",
  "overriddenProperties":[],
  "parent":{
    "name":"Ranged",
    "path":"folders/Objects/defunc_Damagers/Ranged.yy",
  },
  "parentObjectId":{
    "name":"base_Projectile",
    "path":"objects/base_Projectile/base_Projectile.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_bladeWave",
    "path":"sprites/spr_bladeWave/spr_bladeWave.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}