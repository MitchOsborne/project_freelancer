/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
baseSpeed = 2;
damage = 1;
hp = 1;

targetingAngle = 360;
maxRotSpeed = 12;

drunkMod = 5;

retargetTime = 0.3;
retargetTimer = retargetTime/2;

target = undefined;

array_push(dmg_Components, new dmg_comp_SpawnParticlePassive(id, tGravitonParticle, 0.005, 3));