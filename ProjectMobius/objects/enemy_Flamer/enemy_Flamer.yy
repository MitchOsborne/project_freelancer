{
  "$GMObject":"",
  "%Name":"enemy_Flamer",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"enemy_Flamer",
  "overriddenProperties":[],
  "parent":{
    "name":"Bosses",
    "path":"folders/Objects/Agents/Characters/Enemies/Bosses.yy",
  },
  "parentObjectId":{
    "name":"base_Character",
    "path":"objects/base_Character/base_Character.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[
    {"x":14.0,"y":7.0,},
    {"x":36.0,"y":7.0,},
    {"x":36.0,"y":42.0,},
    {"x":14.0,"y":42.0,},
  ],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_FlamerTurret_Idle",
    "path":"sprites/spr_FlamerTurret_Idle/spr_FlamerTurret_Idle.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}