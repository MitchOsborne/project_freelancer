/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_FlamerBoss;
ai_combatPriority = 3;

ai_defaultOperator = ai_Wildlife;

baseSpeed = 3;
moveSpeed = baseSpeed;

char_hasLight = false;

char_baseHealth = 20;
char_pushRes = 1;
char_baseStunRes = 1;
char_KORes = 0;

autoAimCap = 0;
focusAimCap = 0;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_FlamerTurret, id);