/// @description Insert description here
// You can write your code in this editor
event_inherited();

direction = irandom_range(0,359);

active_operator = undefined;
player_operator = undefined;
ai_operator = undefined;
sequence_operator = undefined;
seqControl = false;

ai_defaultOperator = ai_BasicAgent;
char_patrolPath = undefined;
ai_combatPriority = 1;
ai_priorityMod = 1;
ai_fightScore = 1;
ai_atkArr = [];
ai_canEvade = false;
char_RezTime = 10;
char_RezTimer = 0;
char_rezBeacon = undefined;
char_TargetIntr = undefined;
char_Elite = false;
char_SmnOwner = undefined;
charData = undefined;

char_hasLight = true;
char_ambLight = undefined;

inFocus	= 0;
inLockDir = 0;
inWeapon1 = 0;
inWeapon2 = 0;
inUtility1 = 0;
inUtility2 = 0;
inAbility1 = 0;
inAbility2 = 0;
inCore = 0;
inInteract = 0;

globalCD = 0;

rollDirection = 0;

char_AC = instance_create_layer(x,y,"Instances", base_AbilityController);
char_AC.slavedObj = id;
char_ldKeywords = [];


char_ldWeightBonus = 1;
char_AC.ac_bpLd = new Loadout(
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Evade,
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Empty,
	global.bp_Abilities.bp_Empty
);


char_hasAltLoadout = false;


char_activeAbility = undefined; //Used for allowing ability usage whilst ability locked

char_StatCard = new StatCard();
char_UpdateCard = true;	//Used to call the resource intensive "Update_StatCard" logic whenever the card is editted instead of being run every frame,




shoppingArr = [];

passiveArr = [];


autoAimCap = 45;
focusAimCap = 15;
target = undefined;
retargetTime = 0.1;
retargetTimer = 0;

isLocked = false;



//Character Stat Info

char_Ranged = false;
char_InitHeal = true;

char_Name = "Unnamed Char (Derp)";
char_StatArch = global.statArch_Shooter;
char_StatArchCard = new crd_Default();
char_StatGrowths = animcurve_get_channel(crv_StatGrowths,"HPMed");
char_animScale = 1;

char_tipArr = [];

charLvl = 1;
classLvl = 0;
xp = 0;

char_baseDamage = 10;
char_damageLvl = 1;
char_damagePerLvl = 1;
char_damageStat = char_baseDamage + (char_damageLvl*char_damagePerLvl);

char_baseHealth = 40;
char_healthLvl = 0;
char_healthPerLvl = 0;
char_maxHP = char_baseHealth + (char_healthLvl*char_healthPerLvl);
hp = char_maxHP;
char_woundLvl = 0;
char_KO = 0;

char_healCap = 0;	//Max amount of HP that can be healed to (used for calculating hp amounts with wounds)

char_dmgCap = 0.25; //Max damage that can be taken from an attack, excess damage is converted into wounds

maxVit = char_maxHP/2;
vitStat = 0;

staminaStat = 100;
maxStamina = 100;
staminaRegen = 50;
staminaRegenDelay = 1;
staminaRegenTimer = 0;


char_baseShdLvl = 0;
char_shieldLvl = 0;
char_shieldPerLvl = 10; //char_maxHP Percent
char_maxShd = -1;
char_shdStat = 0;
char_shdBuffer = 0;
char_shdBufferMax = char_maxShd/2;
char_shdDelayRate = 12;
char_shdDelayTimer = 0;
char_shdChargeRate = char_maxHP/10;

char_armLvl = 0;
char_maxArm = 0;
char_armStat = 0;
char_armTimer = 0;
char_armTime = 5;

totalHP = 0;
totalchar_maxHP = 0;

char_spdLvl = 1;
char_spdStat = 1;

char_durLvl = 1;
char_durStat = 1;

char_CDLvl = 1;
char_CDStat = 1;

char_critLvl = 1;
char_critPerLvl = 2;
char_procLvl = 1;
char_procPerLvl = 2;

char_baseStunRes = 0;
char_stunRes = 0;
char_pushRes = 0;
char_KORes = 1;	//Multiplier for KO damage calculations, 0 = complete resistence to KO.

inDamage = 0;
inHitArr = [];	//Incoming Damage Reports
wasHit = false;

char_hitReports = [];
char_killReports = [];


baseSightAngle = 140;
baseSightRadius = 80;
sightAngle = baseSightAngle;
sightRadius = baseSightRadius;


baseSpeed = 1.6;
moveSpeed = baseSpeed;

char_modArr = [];
regionList = ds_list_create();

threatProj = 0;
threatMod = 1;
	
navDir = 0;

canSubscribe = true;
canRead = false;
canWrite = true;
heatMapRange = 180;	//Range to collect information from the heatMap
heatMapAllyRange = 360; //Range it can collect information from allies heatMap Range

droppedLoot = false;
lootDrops = 1;

despawnTime = 10;
despawnTimer = 0;
fadeTime = 3;

char_offset = new Vector2(0,0);
diffScaling = 1;
forceMult = 1;

isPrepAttacking = false;
char_preppingAttack = false;
char_isAttacking = false;
char_atkCharge = 0;

smn_Master = undefined;
smn_Minion = undefined;
smn_SummonAbility = undefined;


dbf_taggedObjects = [];
abl_ActiveAbility = undefined;

locoOffset = new Vector2(0,8);
locoBBoxTL = new Vector2(0,0);
locoBBoxBR = new Vector2(0,0);


validX = 0;
validY = 0;