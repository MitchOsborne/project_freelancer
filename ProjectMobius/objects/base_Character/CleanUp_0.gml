/// @description Insert description here
// You can write your code in this editor
event_inherited()

delete active_operator;
delete ai_operator;
delete player_operator;

instance_destroy(char_AC);

for(var i = 0; i < array_length(char_modArr); i++)
{
	delete(char_modArr[i]);
	array_delete(char_modArr, i, 1);
	i -= 1;
}
