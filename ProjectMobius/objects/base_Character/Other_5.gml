/// @description Insert description here
// You can write your code in this editor
if(active_operator == global.Player1ID || teamID == TEAMID.Ally)
{
	persistent = true;	
}else
{
	persistent = false;	
}

for(var i = 0; i < array_length(char_modArr); i += 1)
{
	var tMod = char_modArr[i];
	if(tMod.clearOnExit) tMod.durTimer = tMod.duration;
}
