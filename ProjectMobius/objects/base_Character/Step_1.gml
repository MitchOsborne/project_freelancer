/// @description Insert description here
// You can write your code in this editor
event_inherited();

	Char_InitLocomotor(id);
	Char_InitLight(id);
	
	if(charData != undefined) charData.charData_Animator = animator;
	SanityCheckOperator(id);
	SanityCheckLoadout(id);
	if(global.gameState != GAME_STATE.PAUSED) SanityCheckCharStats(id);
		
	char_offset = new Vector2(x,y);
		
if(global.gameState <= GAME_STATE.CUSTOM)
{	
	
	//Heals the character if they have been direct spawned
	if(char_InitHeal)
	{
		hp = char_maxHP;
		char_InitHeal = false;
	}

	if(CheckExistance(smn_Master))
	{
		UpdateSummonStats(id);
	}


	char_preppingAttack = false;
	char_isAttacking = false;

	if(instance_find(locomotorType, 0) != noone) locomotor = instance_find(locomotorType,0);
	else
	{
		var tInst = instance_create_layer(0,0,"Instances", locomotorType);
		locomotor = tInst;
	}
	
	Char_UpdateOperator(id);
	//char_offset = new Vector2(x,y - (sprite_height/4));
	
	Char_ProcessOutgoingHitReports(id);

	//---------------------------------------------- Process Incoming Hit Reports
	wasHit = false;
	Char_ProcessIncomingHitReports(id);
	//Char_ProcessLethalDamage(id);
	//Char_ProcessNonLethalDamage(id);
	
	Char_ProcessHitFX(id);
	
	inDamage = 0;
	inKO = 0;

	Char_CalculateIncap(id);
	
	Char_ProcessHealthStat(id)
	
	retargetTimer += GetDeltaTime(global.timeScale);
	
	Char_ProcessTarget(id);

}
