/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(global.gameState <= GAME_STATE.CUSTOM)
{
	Char_ProcessAbilityInputs(id);
	
			//------- Fix this code \/ --------------------------
	Char_ProcessLookDir(id);
	
	
	//Reset Stats in preparation for Modifier Updating
	
	moveSpeed = baseSpeed;
	char_KO = 0;
	
	//Stun resistance decays while not stunned, yes yes...
	if(!isStunned) char_stunRes -= GetDeltaTime(timeScale)/7.5;
	if(char_stunRes < char_baseStunRes) char_stunRes = char_baseStunRes;
	isStunned = false;
	
	isAbilityLocked = false;
	isPhased = false;
	isInvis = false;
	isInvul = false;
	isSprinting = false;
	isDashing = false;	
	timeScale = 1;
	
	Char_ProcessModifiers(id);
}
