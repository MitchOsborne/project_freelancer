/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

script_execute(active_operator.func_DrawStep, active_operator);

for(var i = 0; i < array_length(char_modArr); i += 1)
{
	var tMod = char_modArr[i];
	tMod.mod_Draw();
}

if(!PlayerControlled)
{
	var posOffset = new Vector2(x, y + sprite_height/2);
		
	if(hp > 0)// && teamID == TEAMID.Ally)
	{
		if(teamID != TEAMID.Enemy || global.drawEnmHP)
		DrawHPBar(id,posOffset, new Vector2(12,2), false);
	}
	
	if(!isIncap)
	{
		var slotHeight = 6;
		if(active_operator.atkTimer > 0)
		{
			var atkVal = (active_operator.atkTimer / active_operator.ai_atkDelay)*100;
			var atkWidth = 16;
			var atkHeight = 2;
			var barSlot = 1;
			var atkOffset = new Vector2(x, y + (sprite_height/2));
			draw_healthbar(atkOffset.x - atkWidth/2, atkOffset.y - atkHeight/2 + (slotHeight * barSlot), atkOffset.x + atkWidth/2, atkOffset.y + atkHeight/2 + (slotHeight * barSlot), atkVal, c_black, c_fuchsia, c_fuchsia, 0,true, true);
			DrawOutline(id, ownerColor, 0.5);
		}
		
	}
}else
{
	if(CheckExistance(char_TargetIntr))
	{
		draw_line(x,y,char_TargetIntr.x, char_TargetIntr.y);	
	}
}

/*
for(var i = 0; i < array_length(ai_atkArr); i += 1)
{
	if(i < global.cmd_combatWidth)
	{
		var tEnm = ai_atkArr[i];
		draw_line_color(x,y, tEnm.x, tEnm.y,c_red, c_red);
	}
}
DrawTextOutline(x+5, y+5, 0.5, string(ai_fightScore), c_white, c_black, 32,false);
*/

if(isInvis) image_alpha = 0;
else if(isPhased) image_alpha = clamp(draw_get_alpha(),0,0.5);
else image_alpha = 1;



if(global.debug == DEBUG_MODES.PHYSICS)
{
	draw_set_color(c_fuchsia);
	draw_rectangle(locoBBoxTL.x, locoBBoxTL.y, locoBBoxBR.x, locoBBoxBR.y,true);

	//Draw Line of Sight circles
	draw_circle_color(x,y,sightRadius, c_yellow, c_yellow, true);
	DrawCone(new Vector2(x,y), direction+90, sightRadius, sightAngle, 8, c_orange, 0.5, false);

	if(ai_operator != undefined) draw_path(ai_operator.ai_Path, x, y, true);
}