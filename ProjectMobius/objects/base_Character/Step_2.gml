/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(global.gameState <= GAME_STATE.CUSTOM)
{
	Char_ProcessIncap(id);
	Agent_Locomote(id, locomotor);

	if(CheckExistance(char_TargetIntr))
	{
		if(point_distance(x,y, char_TargetIntr.x, char_TargetIntr.y) > char_TargetIntr.intr_range) char_TargetIntr = undefined;
	}
	
	Char_ProcessAnimationState(id);

	Char_ProcessCombatWidth(id);

}