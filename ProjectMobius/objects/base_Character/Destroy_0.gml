/// @description Insert description here
// You can write your code in this editor

event_inherited();

if(teamID == TEAMID.Enemy)
{
	ds_list_delete(global.enmList,ds_list_find_index(global.enmList, id));
}else
{
	ds_list_delete(global.allyList,ds_list_find_index(global.allyList, id));	
}

if(CheckExistance(char_ambLight)) instance_destroy(char_ambLight);

