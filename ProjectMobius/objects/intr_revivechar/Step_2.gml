/// @description Insert description here
// You can write your code in this editor
event_inherited();

if(CheckExistance(intr_resTarget))
{
	x = intr_resTarget.x;
	y = intr_resTarget.y;
	if(intr_resTarget.isIncap == false || (intr_resTarget.hp - intr_resTarget.char_KO) > 0)
	{
		instance_destroy(id);
	}
}else instance_destroy(id);