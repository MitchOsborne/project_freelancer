/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();
intr_weight = 5;
intr_holdTime = 1;
intr_resTarget = undefined;
func_OnInteract = function()
{
	if(!is_undefined(intr_resTarget))
	{
		intr_resTarget.hp = intr_resTarget.char_maxHP;
		intr_resTarget.char_KO = 0;
		var tMod = FindMod(intr_resTarget.char_modArr,"Knockout")
		if(tMod != undefined) tMod.stacks = 0;
		instance_destroy(id);
	}
}