/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

soldSlot = irandom_range(0,array_length(ShopSlots)-1);

soldItem = undefined;
instItem = undefined;
itemStock = 0;
itemCost = 0;
itemName = "undefined";
itemSprite = spr_None;

face_icon = spr_gunShop_face;