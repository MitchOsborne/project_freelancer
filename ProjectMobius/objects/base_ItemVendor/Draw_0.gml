/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(open_shop)
{
	if(soldItem != undefined) 
	{
		draw_sprite_ext(itemSprite, -1, x, y-16, 1, 1, -90, c_white, 1);
		draw_set_font(fnt_default);
		draw_set_color(c_white);
		DrawTextOutline(x,y,1, itemCost, c_white, c_black);
		var buyVal = (buyTimer / buyTime) * 100
		DrawBar(buyVal, 32,8,0,new Vector2(x,y-32), c_white);
	}
}
