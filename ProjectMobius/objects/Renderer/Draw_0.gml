/// @description Insert description here
// You can write your code in this editor

if(global.gameRunning)
{
	for(var i = 0; i < array_length(global.currentRoom.rd_TombstoneArr); i += 1)
	{
		var rCall = global.currentRoom.rd_TombstoneArr[i];
	
		var tSpr = rCall.tmb_charData.charData_Animator.anim_incap;
		draw_sprite(tSpr, sprite_get_number(tSpr)-1, rCall.tmb_pos.x, rCall.tmb_pos.y); 
		
		if(rCall.tmb_type == DMG_TYPE.SOFT)
		{
			tSpr = spr_KO_3;
			if(rCall.tmb_Tmr > rCall.tmb_KODur*0.5) tSpr = spr_KO_2;
			if(rCall.tmb_Tmr > rCall.tmb_KODur*0.8) tSpr = spr_KO_1;
			var tFr = (rndr_animTmr * sprite_get_speed(tSpr)) mod sprite_get_number(tSpr);
			draw_sprite(tSpr, tFr, rCall.tmb_pos.x, rCall.tmb_pos.y); 
		}
	}
	
	for(var i = 0; i < array_length(rndr_ParticleArr); i += 1)
	{
		var tDel = false;
		var tCall = rndr_ParticleArr[i];
		tDel = tCall.Particle_Update();
		if(tCall.ptl_Text != undefined) DrawTextOutline(tCall.x, tCall.y, 1, tCall.ptl_Text, c_white, c_black, 64, true);
		else draw_sprite_ext(tCall.ptl_Sprite, tCall.animCtr, tCall.x, tCall.y, tCall.ptl_Scale, tCall.ptl_Scale, tCall.rot, c_white, tCall.ptl_Alpha);	
		if(tDel)
		{
			delete(rndr_ParticleArr[i]);
			array_delete(rndr_ParticleArr, i, 1);
			i -= 1;
		}
	}
}
