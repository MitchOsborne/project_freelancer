/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

	ui_opWidth = 1;
	ui_opXSpacing = 128;
	ui_opYSpacing = 32;
	ui_slotIndex = -1;
	ui_CharCard = UI_GetCharCard(global.editChar);
	ui_RivenEnabled = false;
	ui_Init = function()
	{
		var tCore = global.StatCardUnlocks.stat_UnlockArr[ui_slotIndex];
		var tSlot = ui_CharCard.crd_StatSlotArr[ui_slotIndex];
		array_push(ui_Options, mnuEl_EditMainStat);
		if(tCore.stat_CanRiven)
		{
			array_push(ui_Options, mnuEl_ToggleRiven);
			array_push(ui_Options, mnuEl_EditAltStat);
		}
		if(ui_slotIndex < global.SlotRange.ATK || tCore.stat_CanRiven) array_push(ui_Options, mnuEl_UpgradeATKSlot);
		else if(ui_slotIndex < global.SlotRange.DEF || tCore.stat_CanRiven) array_push(ui_Options, mnuEl_UpgradeDEFSlot);
		else if(ui_slotIndex < global.SlotRange.ABL || tCore.stat_CanRiven) array_push(ui_Options, mnuEl_UpgradeABLSlot);
		else if(ui_slotIndex < global.SlotRange.LUCK || tCore.stat_CanRiven) array_push(ui_Options, mnuEl_UpgradeLUCKSlot);
	}

