/// @description Insert description here
// You can write your code in this editor


if(array_length(global.inv_StatMods) != modInvSize)
{
	array_sort(global.inv_StatMods, func_SortStatMods);
	modInvSize = array_length(global.inv_StatMods);
}

for(var i = 0; i < array_length(global.ui_MenuStack); i += 1)
{
	var tMenu = global.ui_MenuStack[i];
	if(CheckExistance(tMenu) == false)
	{
		array_delete(global.ui_MenuStack, i, 1);
		i -= 1;
	}else tMenu.depth = -i;
}

if(array_length(global.ui_MenuStack) > 0) 
{
	global.activeUI = global.ui_MenuStack[array_length(global.ui_MenuStack)-1];
	global.uiFocus = true;
}
else 
{
	global.activeUI = undefined;
	global.uiFocus = false;
}

