/// @description Insert description here
// You can write your code in this editor

if(global.activeUI != undefined)
{
	global.uiFocus = true;
	var tCont = undefined;
	if(!is_undefined(global.menuUser))tCont = global.menuUser.inputSource;
	else tCont = global.p1Cont;
	
	if(global.uiUP == 1) global.uiUP = 2;
	if(global.uiDOWN == 1) global.uiDOWN = 2;
	if(global.uiLEFT == 1) global.uiLEFT = 2;
	if(global.uiRIGHT == 1) global.uiRIGHT = 2;
	if(global.uiYES == 1) global.uiYES = 2;
	if(global.uiNO == 1) global.uiNO = 2;
	
	//if(global.activeUI.ui_PlayerIndex == 2) tCont = global.p2Cont;
	//else tCont = global.p1Cont;
	if(tCont != undefined)	//Some checks to make sure buttons need to be released before it recognises additional input
	{
		if(global.uiLEFT == 2 && !round(abs(tCont.in_LLEFT))) global.uiLEFT = 0;
		if(global.uiRIGHT == 2 && !round(abs(tCont.in_LRIGHT))) global.uiRIGHT = 0;
		if(global.uiUP == 2 && !round(abs(tCont.in_LUP))) global.uiUP = 0;
		if(global.uiDOWN == 2 && !round(abs(tCont.in_LDOWN))) global.uiDOWN = 0;
		if(global.uiYES == 2 && !tCont.in_ACCEPT)
		{
			global.uiYES = 0;
		}
		if(global.uiNO == 2 && !tCont.in_CANCEL) global.uiNO = 0;
		
		if(global.uiLEFT == 0) global.uiLEFT = round(abs(tCont.in_LLEFT));
		if(global.uiRIGHT == 0) global.uiRIGHT = round(abs(tCont.in_LRIGHT));
		if(global.uiUP == 0) global.uiUP = round(abs(tCont.in_LUP));
		if(global.uiDOWN == 0) global.uiDOWN = round(abs(tCont.in_LDOWN));
		if(global.uiYES == 0) global.uiYES = tCont.in_ACCEPT;
		if(global.uiNO == 0) global.uiNO = tCont.in_CANCEL;
	}
	
		//global.activeUI.ui_UpdateFunc(global.activeUI);

	
}
else
{
	//global.uiFocus = false;
}