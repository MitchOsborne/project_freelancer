/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_Ranged = true;
char_StatArch = global.statArch_Kamui;
char_StatArchCard = new crd_Pilot_Flight();

timeScale = 1;

char_AC.ac_bpLd.ld_slots.ld_wep1  = global.bp_Abilities.bp_Storm;
char_AC.ac_bpLd.ld_slots.ld_wep2  = global.bp_Abilities.bp_Drake;
char_AC.ac_bpLd.ld_slots.ld_util1 = global.bp_Abilities.bp_BlitzEvade;


char_tipArr = [
"You can unequip a Pilots temporary weapon (like the Minigun) by holding down the equip ability button", 
"The Pilot's temporary weapons will continue to reload whilst unequipped",
"You can Pilot the Mech by re-casting the Summon Ability while the mech is alive",
"The Mech cannot evade, but it can sprint, its claw attacks can destroy projectiles, and it comes with an Armour Repair ability"
]


char_baseDamage = 5;
char_damageLvl = 1;
char_damagePerLvl =  char_StatArch.dmg;

char_baseHealth = 20;
char_healthLvl = 1;
char_healthPerLvl = char_StatArch.HP;

maxVit = char_maxHP/2;
vitStat = 0;

char_shieldLvl = 1;
char_maxShd = char_maxHP * ((char_shieldLvl*char_shieldPerLvl)/100);
char_shdBufferMax = char_maxShd/2;
char_shdChargeRate = char_maxShd/20;

char_armStat = 0;

baseSpeed = 1.1;
moveSpeed = baseSpeed;

baseSightRadius = 160;

canSubscribe = true;
canRead = true;
canWrite = true;


ai_defaultOperator = ai_Adaptive;
animator = InitAnimator(anim_Pilot,id);
teamID = TEAMID.Ally;
