/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Runner;
char_Elite = true;
ai_combatPriority = 4;

baseSpeed = 1.8;
moveSpeed = baseSpeed;

char_hasLight = false;


char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_NezumiBlade;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_NezumiLeader, id);