/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ui_opWidth = 1;
ui_opXSpacing = ui_Size.x*ui_Scale;
ui_opYSpacing = 32
ui_HeaderText = "";

ui_Options = [mnuEl_HealParty, mnuEl_UpRKLight, mnuEl_UpRKRifle, mnuEl_UpSolTech, mnuEl_BuyStatMods, mnuEl_OpenUpgradeMods];

ui_CloseParent = true;