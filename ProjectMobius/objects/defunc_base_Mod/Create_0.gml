/// @description Insert description here
// You can write your code in this editor


owner = undefined;			//Object the applied the Mod
subject = undefined;		//Object the Mod is attached to


stacks = 1;					//Number of stacks this Mod has
stackLimit = 1;
stackType = false;		//Whether other objects applying the same Mod cause it to stack
duration = 1;
durTimer = 0;

isDebuff = false;	//Indicates whether it is a Buff or Debuff

timeScale = 1;

