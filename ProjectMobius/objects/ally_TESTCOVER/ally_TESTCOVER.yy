{
  "$GMObject":"",
  "%Name":"ally_TESTCOVER",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":3,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"ally_TESTCOVER",
  "overriddenProperties":[],
  "parent":{
    "name":"EngineerUnits",
    "path":"folders/Objects/Agents/Characters/Allies/Deprecated/EngineerUnits.yy",
  },
  "parentObjectId":{
    "name":"t_baseTurret",
    "path":"objects/t_baseTurret/t_baseTurret.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[
    {"x":14.0,"y":7.0,},
    {"x":36.0,"y":7.0,},
    {"x":36.0,"y":42.0,},
    {"x":14.0,"y":42.0,},
  ],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_Box",
    "path":"sprites/spr_Box/spr_Box.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}