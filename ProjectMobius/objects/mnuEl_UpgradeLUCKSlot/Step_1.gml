/// @description Insert description here
// You can write your code in this editor
event_inherited();

var tLvl = uiEl_StatSlot.stat_LUCKLvl;
var tCost = 1 + tLvl;
uiEl_text = "Upgrade LUCK Stat: Lvl " + string(tLvl);
uiEl_tooltipTxt = "Luck Cores: " + string(tCost) + "/" + string(global.inv_upgradeParts.LUCK);