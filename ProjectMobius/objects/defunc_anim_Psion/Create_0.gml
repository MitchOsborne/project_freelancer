/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_psion_idle;

anim_idle_front = spr_psion_idle;
anim_idle_back = spr_psion_idle;

anim_walk_front = spr_psion_walk;
anim_walk_back = spr_psion_walk;

anim_dash_front = spr_psion_dash;
anim_dash_back = spr_psion_dash;

anim_incap = spr_psion_idle;
anim_stun_front = spr_psion_dash;
anim_stun_back = spr_psion_dash;