/// @description Insert description here
// You can write your code in this editor
enum SHOPSTATS
{
	HEALTH = 0,
	DAMAGE = 1,
	SHIELD = 2,
	ARMOUR = 3,
}

// Inherit the parent event
event_inherited();

soldItem = undefined;
soldStat = choose(SHOPSTATS.HEALTH, SHOPSTATS.DAMAGE, SHOPSTATS.SHIELD, SHOPSTATS.ARMOUR);
itemName = "Undefined";
face_icon = spr_medShop_face;