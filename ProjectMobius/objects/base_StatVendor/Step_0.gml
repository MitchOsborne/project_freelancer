/// @description Insert description here
// You can write your code in this editor
event_inherited();


if(open_shop)
{
	switch (soldStat)
	{
		case SHOPSTATS.HEALTH:
		itemName = "Max Health Up";
		itemCost = (shopper.slavedObj.upgradeCost * (shopper.slavedObj.char_healthLvl+1))*50;
		break;
		case SHOPSTATS.DAMAGE:
		itemName = "Damage Up";
		itemCost = (shopper.slavedObj.upgradeCost * (shopper.slavedObj.char_damageLvl+1))*50;
		break;
		case SHOPSTATS.SHIELD:
		itemName = "Shields Up";
		itemCost = (shopper.slavedObj.upgradeCost * (shopper.slavedObj.char_shieldLvl+1))*150;
		break;
		case SHOPSTATS.ARMOUR:
		itemName = "Armour Refill";
		itemCost = (shopper.slavedObj.upgradeCost * (shopper.slavedObj.char_healthLvl+1))*10;
		break;
		default:
		itemName = "Undefined";
		break;
	}
	
	if !textbox_exists() {
			textbox_create("Buy the " + itemName + "?");
			textbox_set(noone,c_dkgray,c_yellow,c_yellow,1,example_font,face_icon,true);
			textbox_add_options(c_blue,0,"Yes (-" + string(itemCost) + ")","No");
	}
	
	if (chooses_option() = 1) {
		if(itemCost <= shopper.global.credits)
		{
			switch (soldStat)
			{
				case SHOPSTATS.HEALTH:
				
				shopper.global.credits	-= itemCost;
				shopper.slavedObj.char_healthLvl += 5;
				SanityCheckCharStats(shopper.slavedObj);
				SanityCheckCharStats(shopper.slavedObj);
				shopper.slavedObj.hp = shopper.slavedObj.char_maxHP;
				break;
				
				case SHOPSTATS.DAMAGE:
				shopper.global.credits	-= itemCost;
				shopper.slavedObj.char_damageLvl += 5;
				SanityCheckCharStats(shopper.slavedObj);
				break;
				
				case SHOPSTATS.SHIELD:
				shopper.global.credits	-= itemCost;
				shopper.slavedObj.char_shieldLvl += 1;
				SanityCheckCharStats(shopper.slavedObj);
				break;
				
				case SHOPSTATS.ARMOUR:
				
				if(shopper.slavedObj.char_armStat < shopper.slavedObj.char_maxArm)
				{
					shopper.global.credits	-= itemCost;
					shopper.slavedObj.char_armStat += (shopper.slavedObj.char_maxArm/4);
					
					SanityCheckCharStats(shopper.slavedObj);
				}
				break;
				default:
				break;
			}
			
		
		}
		
		open_shop = false;
	}else if chooses_option() = 2 {
		open_shop = false;		
	}
	
}