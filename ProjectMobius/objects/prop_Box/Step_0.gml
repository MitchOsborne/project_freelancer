/// @description Update Loop
// You can write your code in this editor
event_inherited();

if(hp <= 0)
{
	if(!lootSpawned)
	{
		instance_create_layer(x,y,"Instances", base_pickupSpawner);
		lootSpawned = true;
	}
}



if (hp <= 0)
{
	sprite_index = spr_BoxDestroyed;	
}else if(hp <= maxHP*0.5)
{
	sprite_index = spr_BoxDamaged;
}else 
{
	sprite_index = spr_Box;	
}