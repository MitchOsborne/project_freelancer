/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(!lootSpawned)
{
	instance_create_layer(x,y,"Instances", base_pickupSpawner);
	lootSpawned = true;
}