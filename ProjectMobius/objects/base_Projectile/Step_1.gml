/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

distLerp = distTravelled/range;
if(!altSpeed)
{
	var t_speedMod = animcurve_channel_evaluate(dmg_speedCurve, distLerp) * randSpeedMod;
	moveSpeed = baseSpeed * t_speedMod
	if(distLerp > 1 && !dmg_persists)
	{
		lifeTimer = lifeTime;	
	}
}else
{
	moveSpeed = baseSpeed * speedMod;
}

if(isStasis)
{
	moveSpeed = 0;
	lifeTimer = 0;
}