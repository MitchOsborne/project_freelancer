/// @description Insert description here
// You can write your code in this editor

if(global.activeUI == id)
{
	if(ui_delayTime < ui_delayTmr)	//Ignores inputs on the first update step, to prevent cascading inputs on menu creation
	{
		if(global.uiUP == 1) 
		{
			ui_OptionIndex -= ui_opWidth;
			
		}
		else if(global.uiDOWN == 1) 
		{
			ui_OptionIndex += ui_opWidth;
		}
		else if(global.uiLEFT == 1) 
		{
			ui_OptionIndex -= 1;
		}
		else if(global.uiRIGHT == 1) 
		{
			ui_OptionIndex += 1;
		}

		ui_OptionIndex = clamp(ui_OptionIndex, 0, array_length(ui_Options)-1);
		
		
		if(global.uiYES == 1)
		{
			if(ui_OptionIndex >= 0)ui_Options[ui_OptionIndex].uiEl_Function();
		}
		else if(global.uiNO == 1) 
		{
			instance_destroy(id);
			if(ui_CloseParent == false)	global.uiNO = 2;
		}
	}
}
		