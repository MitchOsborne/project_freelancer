/// @description Insert description here
// You can write your code in this editor

if(!ui_hasInit)
{
	x = (global.window_width/2) - (ui_Size.x/2)*ui_Scale;
	y = (global.window_height/2) - (ui_Size.y/2)*ui_Scale;
	
	for(var i = 0; i < array_length(ui_Options); i += 1)
	{
		var tInst = instance_create_layer(x+ui_opBorder + (ui_opXSpacing*(i mod ui_opWidth)), y+ui_opBorder + (ui_opYSpacing*(floor(i/ui_opWidth))), "Menu", ui_Options[i])
		tInst.depth -= 1 + i;
		tInst.uiEl_parentMenu = id;
		tInst.uiEl_Init();
		ui_hasInit = true;
	}
}

for(var i = 0; i < array_length(ui_Options); i += 1)
{
	var tInst = ui_Options[i];
	if(i == ui_OptionIndex)tInst.uiEl_isSelected = true;
	else ui_Options[i].uiEl_isSelected = false;
}

x = (global.window_width/2) - (ui_Size.x/2)*ui_Scale;
y = (global.window_height/2) - (ui_Size.y/2)*ui_Scale;

ui_delayTmr += GetFixedDeltaTime();