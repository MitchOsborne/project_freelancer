/// @description Insert description here
// You can write your code in this editor

ui_animOpen = true;
ui_animYTmr = 0;
ui_animXTmr = 0;
ui_animTime = 0.3;

ui_PlayerIndex = 1;
ui_Size = new Vector2(360,360);
ui_Scale = 1;

ui_CloseParent = false;	//if true, closes the parent UI when this ui closes;

ui_HeaderText = "Header Text Goes Here";

ui_opWidth = 2;
ui_opXSpacing = 128;
ui_opYSpacing = 32;
ui_opBorder = 8;

ui_OptionIndex = 0;

ui_Options = [];

ui_OptionFunc = [];

ui_hasInit = false;

ui_delayTime = 0.05;
ui_delayTmr = 0;

ui_Init = function()
{
	
}

x = (global.window_width/2) - (ui_Size.x/2)*ui_Scale;
y = (global.window_height/2) - (ui_Size.y/2)*ui_Scale;
