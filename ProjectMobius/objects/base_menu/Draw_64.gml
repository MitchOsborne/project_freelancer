/// @description Insert description here
// You can write your code in this editor


if(ui_hasInit)
{
	var uiScale = new Vector2(ui_Size.x/sprite_width, ui_Size.y/sprite_height);
	var tAnimScale = new Vector2(1,1);
	if(ui_animOpen)
	{
		tAnimScale.x = 0.1;
		ui_animYTmr += GetFixedDeltaTime();
		tAnimScale.y = clamp(ui_animYTmr/(ui_animTime/2),0.1,1);
		if(tAnimScale.y >= 1)
		{
			ui_animXTmr += GetFixedDeltaTime();
			tAnimScale.x = clamp(ui_animXTmr/(ui_animTime/2), 0.1, 1);
		}
		
	}
	if(ui_animYTmr >= ui_animTime) ui_animOpen = false;
	draw_sprite_ext(sprite_index, image_index, x+(ui_Size.x/2), y + (ui_Size.y/2), (uiScale.x*tAnimScale.x)*ui_Scale, (uiScale.y*tAnimScale.y)*ui_Scale, 0, c_white, 1);
}