/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Runner;
ai_combatPriority = 3;

ai_defaultOperator = ai_Wildlife;

baseSpeed = 3;
moveSpeed = baseSpeed;

char_hasLight = false;



char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_LungeBite;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_Doggo, id);