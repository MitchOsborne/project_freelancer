/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Heavy;

baseSpeed = 1.2;
moveSpeed = baseSpeed;


char_baseDamage = 5;
char_damageLvl = 1;
char_damagePerLvl =  char_StatArch.dmg;

char_baseHealth = 20;
char_healthLvl = 1;
char_healthPerLvl = char_StatArch.HP;

char_AC.ac_bpLd.ld_slots.ld_wep1  = new PKG_SinglePack(wep_ShieldBash);
char_AC.ac_bpLd.ld_slots.ld_core  = new PKG_LP_TankProtect();

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_LP_Tank, id);