/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_Shepherd_idle_2;

anim_idle_front = spr_Shepherd_idle_2;
anim_idle_back = spr_Shepherd_idle_2;

anim_walk_front = spr_Shepherd_walk_2;
anim_walk_back = spr_Shepherd_walk_2;

anim_dash_front = spr_Shepherd_dash_2;
anim_dash_back = spr_Shepherd_dash_2;

anim_incap = spr_Shepherd_idle_2;
anim_stun_front = spr_Shepherd_dash_2;
anim_stun_back = spr_Shepherd_dash_2;