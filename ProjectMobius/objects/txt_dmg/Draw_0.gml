/// @description Insert description here
// You can write your code in this editor
lifeTimer += GetDeltaTime(1);

if(lifeTimer >= lifeTime)
{
	instance_destroy(id);	
}else
{
	if(is_numeric(damageVal))
	{
		if(damageVal != 0)
		{
			if(damageVal < 0)
			{
				draw_set_color(c_green);
			}else
			{
				draw_set_color(c_red);
			}
			if((damageVal mod 1) == 0)
			{
				damageVal = string_format(damageVal,1,0);
			}else
			{
				damageVal = string_format(damageVal,1,1);
			}
		}
	}
		draw_set_font(fnt_dmg);
		DrawTextOutline(x,y,0.5,string_replace(damageVal, "-", ""), colA, colB);
		draw_set_font(fnt_default);
		draw_set_color(c_white);
}
