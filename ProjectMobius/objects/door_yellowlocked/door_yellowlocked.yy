{
  "$GMObject":"",
  "%Name":"door_YellowLocked",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"door_YellowLocked",
  "overriddenProperties":[],
  "parent":{
    "name":"Doors",
    "path":"folders/Objects/Misc/Management/Interactables/TriggerVolumes/Doors.yy",
  },
  "parentObjectId":{
    "name":"base_LockedDoor",
    "path":"objects/base_LockedDoor/base_LockedDoor.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_door_yellow",
    "path":"sprites/spr_door_yellow/spr_door_yellow.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}