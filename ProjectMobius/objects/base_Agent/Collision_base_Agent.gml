/// @description Insert description here
// You can write your code in this editor


if(global.timeScale > 0)
{
	var forceDir = (point_direction(x,y,other.x,other.y)+90) + 180;
	forceDir += irandom_range(-20,20);
	if(!other.isIncap && !isIncap)
	{
		AttachLinearForce(other, id, 0,forceDir, 0.5 + irandom_range(-0.1,0.1));
	}
}