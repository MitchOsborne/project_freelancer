/// @description Insert description here
// You can write your code in this editor
event_inherited();


enum ANIM_STATE
{
	IDLE,
	WALK,
	DASH,
	INCAP,
	REVIVE,
	ATTACK,
	STUNNED, 
}

PlayerControlled = false;

baseSpeed = 0;
moveSpeed = 0;	//Speed controlled by self (movement, rolling etc)

lookDir = 0;
moveDir = 0;

in_mx = 0;
in_my = 0;
in_lx = 0;
in_ly = 0;
mx = 0;
my = 0;
lx = 0;
ly = 0;

canMove = true;

lastX = entry_default;
lastY = entry_default;

spawnX = 0;
spawnY = 0;


physWidth = abs(bbox_left - bbox_right) / 2;
physHeight = abs(bbox_top - bbox_bottom) / 2;

locomotorType = loco_4;
locomotor = undefined;

velX = 0;
velY = 0;
velLerp = 0.5;

debugColour = c_green;

animState = ANIM_STATE.IDLE;
