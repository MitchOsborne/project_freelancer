/// @description Insert description here
// You can write your code in this editor
event_inherited();
baseSpeed = 6;
damage = 1;
hp = 1;

targetingAngle = 360;
maxRotSpeed = 2;

drunkMod = 7;

retargetTime = 0.3;
retargetTimer = retargetTime/2;

target = undefined;

image_angle = direction;

spawnParticle = tSmokeParticle;
spawnRate = .05;
spawnTimer = 0;