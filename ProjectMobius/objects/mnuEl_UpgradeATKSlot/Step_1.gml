/// @description Insert description here
// You can write your code in this editor
event_inherited();

var tLvl = uiEl_StatSlot.stat_ATKLvl;
var tCost = 1 + tLvl;
uiEl_text = "Upgrade ATK Stat: Lvl " + string(tLvl);
uiEl_tooltipTxt = "Attack Cores: " + string(tCost) + "/" + string(global.inv_upgradeParts.ATK);