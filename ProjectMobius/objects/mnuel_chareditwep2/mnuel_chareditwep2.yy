{
  "$GMObject":"",
  "%Name":"mnuEl_CharEditWep2",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"mnuEl_CharEditWep2",
  "overriddenProperties":[],
  "parent":{
    "name":"AbilityEdit",
    "path":"folders/Objects/Misc/Menu/MenuElements/CharacterEdit/Editor/AbilityEdit.yy",
  },
  "parentObjectId":{
    "name":"mnuEl_CharEdit",
    "path":"objects/mnuEl_CharEdit/mnuEl_CharEdit.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":null,
  "spriteMaskId":null,
  "visible":true,
}