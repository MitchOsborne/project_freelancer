/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

func_OnCharTouch = function(a_other)
{
	AttachLinearForce(a_other, a_other, 0, image_angle - 90, 1);
	AttachLinearForce(a_other, a_other, 0, point_direction(a_other.x, a_other.y,x,y)-90, 0.2);
}