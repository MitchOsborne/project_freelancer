/// @description Insert description here
// You can write your code in this editor
event_inherited();


if(open_shop)
{
	
	
	
	
	if(ifStats)
	{
		var hpCost = (shopper.slavedObj.upgradeCost * shopper.slavedObj.char_healthLvl)*5;
		var dmgCost = (shopper.slavedObj.upgradeCost * shopper.slavedObj.char_damageLvl)*5;
			
		if (textbox_exists()) {
			textbox_set(noone,c_dkgray,c_yellow,c_yellow,1,example_font,face_icon,true);
			textbox_add_options(c_blue,0,"Health: " + string(hpCost) + " global.credits","Damage: " + string(dmgCost) + " global.credits","Nevermind");
		}
		
		if (chooses_option() = 1) {
			if(hpCost <= shopper.global.credits)
			{
				shopper.global.credits	-= hpCost;
				shopper.slavedObj.char_healthLvl += 5;
				SanityCheckCharStats(shopper.slavedObj);
				
				}
		
		}
		else if (chooses_option() = 2) {
			if(dmgCost <= shopper.global.credits)
			{
				shopper.global.credits	-= dmgCost;
				shopper.slavedObj.char_damageLvl += 5;
				SanityCheckCharStats(shopper.slavedObj);
				
				}
		
		}
		else if (chooses_option() = 3) {
			open_shop = false;
			ifStats = false;
			ifWeapons = false;
		}
	}
	
	if(ifWeapons)
	{
		if (textbox_exists()) {
			textbox_set(noone,c_dkgray,c_yellow,c_yellow,1,example_font,face_icon,true);
			textbox_add_options(c_blue,0,"SMG: 100 global.credits" ,"Shotgun: 250 global.credits","Nevermind");
		}
		
		if (chooses_option() = 1) {
			if(100 <= shopper.global.credits)
			{
				shopper.global.credits	-= 100;
				shopper.slavedObj.char_AC.ac_bpLd.ld_slots.ld_wep1 = wep_MP5;
				RefreshLoadout(shopper.slavedObj);
				
				open_shop = false;
				ifStats = false;
				ifWeapons = false;
			}
		
		}
		else if (chooses_option() = 2) {
			if(250 <= shopper.global.credits)
			{
				shopper.global.credits	-= 250;
				shopper.slavedObj.char_AC.ac_bpLd.ld_slots.ld_wep1 = wep_SPAS12;
				RefreshLoadout(shopper.slavedObj);
				
				open_shop = false;
				ifStats = false;
				ifWeapons = false;
			}
		
		}
		else if (chooses_option() = 3) {
			open_shop = false;
			ifStats = false;
			ifWeapons = false;	
		}
	}
}