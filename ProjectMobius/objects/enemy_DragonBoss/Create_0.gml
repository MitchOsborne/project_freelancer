/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

baseSpeed = 0.1;
moveSpeed = baseSpeed;


char_baseDamage = 10;
char_damageLvl = 1;
char_damagePerLvl =  char_StatArch.dmg;

char_baseHealth = 800;
char_healthLvl = 1;
char_healthPerLvl =  char_StatArch.HP;

char_AC.ac_bpLd.ld_slots.ld_wep1  = new PKG_SinglePack(wep_DragonWaveBoss);
char_AC.ac_bpLd.ld_slots.ld_abl1  = new PKG_SinglePack(wep_DragonFire);
char_AC.ac_bpLd.ld_slots.ld_util1  = new PKG_SinglePack(wep_DragonField);

lootDrops = 10;

teamID = TEAMID.Enemy;
ai_defaultOperator = defunc_ai_DragonBoss;
animator = InitAnimator(anim_DragonBoss, id);

forceMult = 0;