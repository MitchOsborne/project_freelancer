/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
trig_weapon = global.bp_Abilities.bp_Empty;
trig_ChestID = -1;
openCheck = true;

func_OnCharTouch = function(a_other)
{
	if(a_other.PlayerControlled)
	{
		UnlockBlueprint(trig_weapon);
		array_push(Renderer.rndr_ParticleArr, new Ptl_Text(x, y, random_range(-0.2, 0.2), -0.5, trig_weapon.bp_Name));
		array_push(global.currentRoom.rd_ChestArr, trig_ChestID);
		audio_play_sound(sfx_collect, 1, false);
		instance_destroy(self);

	}
}