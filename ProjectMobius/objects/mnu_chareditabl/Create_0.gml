/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ui_slotArr = [];
ui_targetChar = global.editChar;
ui_targetSlot = LOADOUT_SLOT.NONE;

ui_Init = function()
{
	//UpdateSlotRequirements(ui_targetChar);
	//ui_GetSlotArr(ui_targetSlot, id);
	var ablArr = GetValidAbilities(ui_targetSlot, GetUnlockedAbilities());

	for(var i = 0; i < array_length(ablArr); i += 1)
	{
		var tInst = instance_create_layer(x+ui_opBorder + (ui_opXSpacing*(i mod ui_opWidth)), y+ui_opBorder + (ui_opYSpacing*(floor(i/ui_opWidth))), "Menu", mnuEl_AblOption);
		tInst.uiEl_TargetChar = ui_targetChar;
		tInst.uiEl_TargetSlot = ui_targetSlot;
		tInst.uiEl_Blueprint = ablArr[i];
		tInst.uiEl_parentMenu = id;
		tInst.depth -= 1 + i;
	
		array_push(ui_Options,tInst);
	}
	ui_hasInit = true;	
}
