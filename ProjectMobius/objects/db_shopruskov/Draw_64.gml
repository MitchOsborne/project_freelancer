/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();


if(db_charFace != undefined) draw_sprite_ext(db_charFace, -1, x + db_facePos.x, y + db_facePos.y, ui_Scale, ui_Scale, 0, c_white, 1);
draw_text_ext_transformed(x + db_textPos.x + 10, y + db_textPos.y + 10, db_text, font_get_size(0)+4, 256-42, ui_Scale, ui_Scale, 0);
