/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ui_Sprite = spr_MenuBkg;

ui_Size = new Vector2(256,34);
ui_Scale = 4;
db_charFace = spr_RuskovShop;
db_text = "Welcome to Ruskov, where gun carries You!!!";

db_facePos = new Vector2(4,4);
db_textPos = new Vector2(128,10);

db_initShop = false;

ui_Init = function()
{
	CreateMenu(new Vector2(x,y + (ui_Size.y*ui_Scale)), mnu_RuskovShop, global.menuUser);
	ui_hasInit = true;
}