/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

debuff_ctr += GetDeltaTime(timeScale);

if(debuff_ctr >= debuff_res)
{
	debuff_ctr = 0;
	for(var i = 0; i < instance_number(base_Character); i += 1)
	{
		var tInst = instance_find(base_Character, i);
		if(tInst.teamID != teamID)
		{
			if(point_distance(x,y,tInst.x,tInst.y) <= maxPullRange)
			{
				AttachPositionalForce(tInst, owner, 1, id, pullStrength, minPullRange, maxPullRange);	
			}
		}
	}

	for(var i = 0; i < instance_number(base_Projectile); i += 1)
	{
		var tInst = instance_find(base_Projectile, i);
		if(tInst.teamID != teamID)
		{
			if(point_distance(x,y,tInst.x,tInst.y) <= maxPullRange)
			{
				tInst = AttachPositionalForce(tInst, owner, 1, id, pullStrength/5, minPullRange, maxPullRange);	
			}
		}
	}
}