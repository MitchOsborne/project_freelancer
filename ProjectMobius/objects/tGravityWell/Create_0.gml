/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
baseSpeed = 1;
damage = 1;
hp = infinity;

lifeTime = infinity;
maxPullRange = 300;
minPullRange = 100;
pullStrength = 3;
debuff_res = 0.25;
debuff_ctr = 0;

ds_list_add(modsToApply,force_positional);

image_angle = direction;