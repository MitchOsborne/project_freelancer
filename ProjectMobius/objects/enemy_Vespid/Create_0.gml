/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Swarm;
ai_combatPriority = 4;

ai_defaultOperator = ai_Swarm;

baseSpeed = 2;
moveSpeed = baseSpeed;
char_hasLight = false;

char_baseHealth = 1;



char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_LungeBite;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_Vespid, id);