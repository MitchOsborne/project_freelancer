/// @description Insert description here
// You can write your code in this editor

if(global.currentRoom.rd_isCleared)
{
	if(!am_WaveClear) 
	{
		am_WaveClear = true;
		am_Round += 1;
		instance_activate_object(am_IntrStartObj);
		am_IntrStartObj.visible = true;
		if(am_Round == 5)
		{
			Overlord.diffTimer = Overlord.diffTime;
			am_Round = 0;
		}
	}
}

if(am_StartWave)
{
	am_StartWave = false;
	am_WaveClear = false;
	global.alertMode = ALERT_STATE.Danger;
	random_set_seed(current_time);
	for(var i = 0; i < ds_list_size(global.enmList); i += 1)
	{
		instance_destroy(global.enmList[|i]);
	}
	
	am_CurrSquad = global.squadArr[irandom_range(0, array_length(global.squadArr)-1)];
	global.currentRoom.rd_isCleared = false;
	array_clear(global.spawnPool);
	array_copy(global.spawnPool, 0, am_CurrSquad.sqd_Members, 0, array_length(am_CurrSquad.sqd_Members));
}