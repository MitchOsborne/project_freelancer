/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


	uiEl_slotIndex = 0;
	uiEl_StatCard = UI_GetCharCard(global.editChar);
	uiEl_StatSlot = -1;
	uiEl_text = "Set Main Stat:";
	uiEl_tooltipTxt = "";
	uiEl_sprite = spr_None;
	
	uiEl_AltStat = false;
	
	uiEl_Function = function()
	{
		var rivenMode = uiEl_parentMenu.ui_RivenEnabled;
		
		var validStat = false;
		var validSlot = false;
		
		var tStat = -1;
		if(uiEl_AltStat) tStat = uiEl_StatSlot.stat_AltStat;
		else tStat = uiEl_StatSlot.stat_MainStat;
		
		if(rivenMode && uiEl_AltStat || !uiEl_AltStat)	//Prevent editting of AltStat when Riven Mode is disabled
		{
			//So I can edit AltStat and MainStat with same code using inheritance
			while(!validStat)
			{
				validSlot = false;
				tStat += 1;
				var tVal = GetStatValByType(tStat, uiEl_slotIndex);
				if(rivenMode)
				{
					if(uiEl_AltStat)
					{
						if(GetStatInValidSlot(tStat, uiEl_slotIndex) || GetStatInValidSlot(uiEl_StatSlot.stat_MainStat, uiEl_slotIndex)) validSlot = true;	
					}else if(GetStatInValidSlot(tStat, uiEl_slotIndex) || GetStatInValidSlot(uiEl_StatSlot.stat_AltStat, uiEl_slotIndex)) validSlot = true;	
				}
				if(tVal > 0)
				{
					if(!rivenMode && GetStatInValidSlot(tStat, uiEl_slotIndex))
					{
						validStat = true;
					}else if(rivenMode && validSlot)
					{
						validStat = true;
					}
				}
				else if(tVal == -1)
				{
					tStat = 0
					validStat = true;
				}
			}
		}
		
		if(uiEl_AltStat) uiEl_StatSlot.stat_AltStat = tStat;
		else uiEl_StatSlot.stat_MainStat = tStat;
		
		if(!rivenMode) uiEl_StatSlot.stat_AltStat = uiEl_StatSlot.stat_MainStat;
		
		Init_StatSlot(uiEl_StatSlot, uiEl_slotIndex);
		
	}
	
	uiEl_Init = function()
	{
		uiEl_StatCard = UI_GetCharCard(global.editChar);
		uiEl_StatSlot = uiEl_StatCard.crd_StatSlotArr[uiEl_slotIndex];
		//uiEl_text = string(uiEl_NewMod.stat_currLvl);
		//uiEl_tooltipTxt = uiEl_NewMod.stat_mainTxt + "-" + uiEl_NewMod.stat_altTxt;
		uiEl_hasInit = true;
	
	}