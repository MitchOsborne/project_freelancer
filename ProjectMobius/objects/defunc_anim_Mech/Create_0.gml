/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_mech_idle_f;

anim_idle_front = spr_mech_idle_f;
anim_idle_back = spr_mech_idle_b;

anim_walk_front = spr_mech_walk_f;
anim_walk_back = spr_mech_walk_b;

anim_dash_front = spr_mech_dash_f;
anim_dash_back = spr_mech_dash_b;

anim_incap = spr_mech_incap;
anim_stun_front = spr_mech_stun_f;
anim_stun_back = spr_mech_stun_b;