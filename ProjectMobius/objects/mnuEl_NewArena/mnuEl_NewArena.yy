{
  "$GMObject":"",
  "%Name":"mnuEl_NewArena",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"mnuEl_NewArena",
  "overriddenProperties":[],
  "parent":{
    "name":"StartMenu",
    "path":"folders/Objects/Misc/Menu/MenuElements/StartMenu.yy",
  },
  "parentObjectId":{
    "name":"base_MenuElement",
    "path":"objects/base_MenuElement/base_MenuElement.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":null,
  "spriteMaskId":null,
  "visible":true,
}