/// @description Insert description here
// You can write your code in this editor
event_inherited();
baseSpeed = 1.5;
damage = 1;
hp = 1;

targetingAngle = 360;
maxRotSpeed = 12;

drunkMod = 30;

retargetTime = 0.3;
retargetTimer = retargetTime/2;

target = undefined;

image_angle = direction;

spawnParticle = tGravitonParticle;
spawnRate = .005;
spawnTimer = 0;