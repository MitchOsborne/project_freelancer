/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Runner;

baseSpeed = 0.2;
moveSpeed = baseSpeed;


char_baseDamage = 1;
char_damageLvl = 1;
char_damagePerLvl = char_StatArch.dmg;

char_baseHealth = 2400;
char_healthLvl = 10;
char_healthPerLvl = char_StatArch.HP;

char_AC.ac_bpLd.ld_slots.ld_wep1 = bp_Dragonwave;
char_AC.ac_bpLd.ld_slots.ld_wep2 = bp_Dragonfire;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_Dragon, id);