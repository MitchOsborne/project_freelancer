/// @description Insert description here
// You can write your code in this editor

spawnTimer += GetDeltaTime(global.timeScale);
if(spawnTimer > spawnDelay)
{
	//Spawn Creds
	spawnTimer = 0;
	if(totalCredPool/100 > 1)
	{
		var tVal = 100;
		if(totalCredPool/4 > 100) tVal = totalCredPool/4;
		var tInst = new Pkup_lCredit();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);
		tInst.credAmount = tVal;
		totalCredPool -= tVal;
	}

	if(totalCredPool/10 > 1)
	{
		var tInst = new Pkup_mCredit();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);	
		totalCredPool -= 10;
	}

	if(totalCredPool > 0)
	{
		var tInst = new Pkup_sCredit();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);
		totalCredPool -= 1;
	}
	
	//Spawn EXP
	if(totalXPPool/225 > 1)
	{
		var tVal = 100;
		if(totalXPPool/4 > 100) tVal = totalXPPool/4;
		var tInst = new Pkup_lXP();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);
		tInst.xpAmount = tVal;
		totalXPPool -= tVal;
	}

	if(totalXPPool/25 > 1)
	{
		var tInst = new Pkup_mXP();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);
		totalXPPool -= 25;
	}

	if(totalXPPool > 0)
	{
		var tInst = new Pkup_sXP();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);
		totalXPPool -= 1;
	}
}