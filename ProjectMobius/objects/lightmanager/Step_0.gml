/// @description Insert description here
// You can write your code in this editor


if(global.gameRunning)
{
	
		lt_CycleTmr += GetDeltaTime(global.timeScale);
	
		if(lt_CycleTmr >= lt_CycleTime)
		{
			lt_CycleIndex += 1;
			lt_CycleTmr = 0;
		}
		
		var lerpVal = clamp(lt_CycleTmr/lt_CycleTime, 0, 1);
	
		if(lt_CycleIndex > array_length(lt_DayCycle)-1) lt_CycleIndex = 0;
		lt_NextCycle = lt_CycleIndex + 1;
		if(lt_CycleIndex >= array_length(lt_DayCycle)-1) lt_NextCycle = 0;
		
		var colA = lt_DayCycle[lt_CycleIndex].ltCyc_Col;
		var colB = lt_DayCycle[lt_NextCycle].ltCyc_Col;
	
		var alphA = lt_DayCycle[lt_CycleIndex].ltCyc_Alpha;
		var alphB = lt_DayCycle[lt_NextCycle].ltCyc_Alpha;
		
		var strA = lt_DayCycle[lt_CycleIndex].ltCyc_LightStr;
		var strB = lt_DayCycle[lt_NextCycle].ltCyc_LightStr;
	
		global.lt_extColour = merge_color(colA, colB, lerpVal);
		
	if(global.currentRoom.rd_Outdoors)
	{
		global.lt_ambColour = global.lt_extColour;
		global.lt_ambAlpha = lerp(alphA, alphB, lerpVal);
		global.lt_lightStr = lerp(strA, strB, lerpVal);

	}else
	{
		global.lt_ambAlpha = global.currentRoom.rd_ambAlpha;	
		global.lt_ambColour = global.currentRoom.rd_ambColour;
		global.lt_lightStr = 0.5;
	}
}
