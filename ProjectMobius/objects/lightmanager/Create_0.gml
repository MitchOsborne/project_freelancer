/// @description Insert description here
// You can write your code in this editor

global.lightSurface = 0;

global.shdr_ambAlpha = shader_get_uniform(shdr_lighting, "ambAlpha");

global.shdr_lightStr = shader_get_uniform(shdr_lighting, "lightStr");

if(!layer_exists("Lighting")) layer_create(layer_get_depth("Abilites")-100, "Lighting");
lt_LightArr = [];
lt_ArrSize = 0;

lt_CycleTime = 60;
lt_CycleTmr = 0;
lt_CycleIndex = 0;
lt_NextCycle = 0;
lt_CycleDurTmr = 0;

lt_DayCycle = 
[
	//Pre-Noon : 
	{
		ltCyc_Alpha : 0.5,
		ltCyc_Col : c_white,
		ltCyc_LightStr : 0,
	},
	//Noon : 
	{
		ltCyc_Alpha : 0.5,
		ltCyc_Col : c_white,
		ltCyc_LightStr : 0,
	},
	//Twilight :
	{
		ltCyc_Alpha : 0.5,
		ltCyc_Col : c_orange,
		ltCyc_LightStr : 0,
	},
	//Pre-Night :
	{
		ltCyc_Alpha : 1,
		ltCyc_Col : c_navy,
		ltCyc_LightStr : 0.5,
	},
	//Night :
	{
		ltCyc_Alpha : 1,
		ltCyc_Col : c_black,
		ltCyc_LightStr : 0.5,
	},
	//Night-Ext :
	{
		ltCyc_Alpha : 1,
		ltCyc_Col : c_black,
		ltCyc_LightStr : 0.5,
	},
	//Dawn :
	{
		ltCyc_Alpha : 0,
		ltCyc_Col : make_color_rgb(200,200,255),
		ltCyc_LightStr : 0,
	},

];