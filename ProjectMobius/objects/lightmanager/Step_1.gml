/// @description Insert description here
// You can write your code in this editor


if(global.gameRunning)
{
	if(!surface_exists(global.lightSurface))
	{
		global.lightSurface = surface_create(global.view_width, global.view_height);
	}
	
	for(var i = 0; i < array_length(lt_LightArr); i += 1)
	{
		if(!instance_exists(lt_LightArr[i]))
		{
			array_delete(lt_LightArr, i, 1);
			i -= 1;
		}
	}
	
	if(array_length(lt_LightArr) != lt_ArrSize)
	{
		array_sort(lt_LightArr, func_SortLightByAlpha);	
		lt_ArrSize = array_length(lt_LightArr);
	}
}