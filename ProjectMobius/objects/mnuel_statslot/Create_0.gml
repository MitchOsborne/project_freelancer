/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

	uiEl_slotIndex = 0;
	uiEl_StatCard = UI_GetCharCard(global.editChar);
	uiEl_StatSlot = -1;
	uiEl_text = "nyuk";
	uiEl_tooltipTxt = "nyku nyku";
	uiEl_sprite = spr_None;
	
	uiEl_Function = function()
	{
		var tMenu = CreateMenu(new Vector2(x,y), mnu_CharEditStatSlot, global.menuUser);
		tMenu.ui_slotIndex = uiEl_slotIndex;
		tMenu.ui_Init();
		array_push(global.ui_MenuStack, tMenu);
	}
	
	uiEl_Init = function()
	{
		uiEl_StatCard = UI_GetCharCard(global.editChar);
		uiEl_StatSlot = uiEl_StatCard.crd_StatSlotArr[uiEl_slotIndex];
		uiEl_text = "";//string(uiEl_StatSlot.stat_currLvl);
		uiEl_tooltipTxt = uiEl_StatSlot.stat_mainTxt + "-" + uiEl_StatSlot.stat_altTxt;
		uiEl_sprite = uiEl_StatSlot.stat_icon;
	}