/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

uiEl_TargetSlot = LOADOUT_SLOT.NONE;
uiEl_TargetChar = global.editChar;
uiEl_Blueprint = undefined;
uiEl_text = "Undefined Ability";

uiEl_Function = function()
{
	ui_SetLoadoutSlot(uiEl_TargetSlot, uiEl_TargetChar, uiEl_Blueprint);
	RefreshLoadout(uiEl_TargetChar);
	SanityCheckLoadout(uiEl_TargetChar);
	instance_destroy(uiEl_parentMenu);
	global.uiYES = 2;
}

