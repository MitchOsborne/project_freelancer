/// @description Insert description here
// You can write your code in this editor
event_inherited();

image_angle = direction - 90;

if(hitComplete == false)
{
	drawRange = range;
	
	hitComplete = true;
	var targetPos = GetCoord(direction);
	targetPos.x *= range;
	targetPos.y *= range;

	var hitList = ds_list_create();
	var aX = x+targetPos.x;
	var aY = y+targetPos.y;
	hitList = Collision_Line_List(x, y, aX, aY, base_Object, false, true);

	//Check to ensure objects are hit
	if(hitList != noone)
	{
		/* Delete all items that are on the same team, to prevent calculations with 
		friendly units/projectiles */
		for(var i = 0; i < ds_list_size(hitList); i++)
		{
			
			var tObj = hitList[|i];
			if(!is_undefined(tObj))
			{
				if(tObj.teamID == teamID)
				{
					ds_list_delete(hitList,i);
					i -= 1;
				}
			}
			
		}
	
		//Perform hit calculations on remaining objects
		var pierceCount = 0;
		var lastHitObj = hitList[|0];	// Used for getting the last object hit
		for(var i = 0; i < ds_list_size(hitList); i++)
		{
			//Continue so long as hasn't exceeded maximum objects pierced
			if(pierceCount < pierceStr)
			{
				var tObj = hitList[|i];
				//if enemy, perform damage calculation, otherwise if wall, increase Pierce Counter
				if(object_is_ancestor(tObj.object_index, base_Character))
				{
					var targetHit = CheckDamageProc(id,tObj,friendlyFire)
					if (targetHit  == true)
					{
						lastHitObj = hitList[|i];
						pierceCount += 1;
						ds_map_add(objectsDamaged,tObj,damageCooldown);
						DamageAgent(tObj,damage);
	
	
					}
				}else if(tObj == base_PhysObstacle)
				{
					if(ignoreObstacles == false) pierceCount += 1;
				}
			}
		}
		if(CheckExistance(lastHitObj) == false) lastHitObj = base_PhysObstacle;
		var hitPoint = GetCollisionPoint(x,y,aX,aY,lastHitObj,true,true);
		var tRange = point_distance(x,y,hitPoint.x,hitPoint.y);
		if(tRange != 0) drawRange = tRange;
	}
}
image_yscale = (drawRange/sprite_height);