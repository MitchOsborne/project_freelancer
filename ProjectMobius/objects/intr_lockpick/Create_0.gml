/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();
intr_weight = 5;
intr_holdTime = 1;
intr_door = undefined;
func_OnInteract = function()
{
	if(!is_undefined(intr_door))
	{
		intr_door.isLocked = false;
		instance_destroy(self);
	}
}