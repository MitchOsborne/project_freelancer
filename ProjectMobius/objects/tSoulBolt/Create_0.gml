/// @description Insert description here
// You can write your code in this editor
event_inherited();

targetingAngle = 30;
maxRotSpeed = 0.2;

retargetTime = 0.3;
retargetTimer = retargetTime/2;

target = undefined;

direction += random_range(-5,5);
image_angle = direction;

hp = infinity;
damage = 1;
spd = 4+random_range(-0.2,0.2);
speed = spd;

ignoreObstacles = true;