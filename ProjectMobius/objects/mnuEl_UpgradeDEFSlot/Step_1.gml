/// @description Insert description here
// You can write your code in this editor
event_inherited();

var tLvl = uiEl_StatSlot.stat_DEFLvl;
var tCost = 1 + tLvl;
uiEl_text = "Upgrade DEF Stat: Lvl " + string(tLvl);
uiEl_tooltipTxt = "Defence Cores: " + string(tCost) + "/" + string(global.inv_upgradeParts.DEF);