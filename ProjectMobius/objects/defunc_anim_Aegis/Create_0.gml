/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_Aegis_idle;

anim_idle_front = spr_Aegis_idle;
anim_idle_back = spr_Aegis_idle;

anim_walk_front = spr_Aegis_walk;
anim_walk_back = spr_Aegis_walk;

anim_dash_front = spr_Aegis_dash_slow;
anim_dash_back = spr_Aegis_dash_slow;

anim_incap = spr_Aegis_idle;
anim_stun_front = spr_Aegis_dash_slow;
anim_stun_back = spr_Aegis_dash_slow;