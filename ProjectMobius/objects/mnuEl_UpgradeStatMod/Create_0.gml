/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


	uiEl_slotIndex = 0;
	uiEl_text = "nyuk";
	uiEl_tooltipTxt = "nyku nyku";
	uiEl_sprite = spr_None;
	uiEl_upgradeCost = 5;
	uiEl_StatSlot = undefined;

	uiEl_Function = function()
	{
		if(uiEl_StatSlot.mainStat == uiEl_StatSlot.altStat)
		{
			switch uiEl_StatSlot.stat_MainType
			{
				case STAT_CAT.ATTACK:
				if(global.inv_upgradeParts.ATK >= uiEl_upgradeCost)
				{
					uiEl_StatSlot.stat_currLvl += 1;
					global.inv_upgradeParts.ATK -= uiEl_upgradeCost
				}
				break;
				case STAT_CAT.DEFENSE:
				if(global.inv_upgradeParts.DEF >= uiEl_upgradeCost)
				{
					uiEl_StatSlot.stat_currLvl += 1;
					global.inv_upgradeParts.DEF -= uiEl_upgradeCost
				}
				break;
				case STAT_CAT.ABILITY:
				if(global.inv_upgradeParts.ABL >= uiEl_upgradeCost)
				{
					uiEl_StatSlot.stat_currLvl += 1;
					global.inv_upgradeParts.ABL -= uiEl_upgradeCost
				}
				break;
				case STAT_CAT.LUCK:
				if(global.inv_upgradeParts.LUCK >= uiEl_upgradeCost)
				{
					uiEl_StatSlot.stat_currLvl += 1;
					global.inv_upgradeParts.LUCK -= uiEl_upgradeCost
				}
				break;
			}
		}else
		{
			if(global.inv_upgradeParts.RIVEN >= uiEl_upgradeCost)
			{
				uiEl_StatSlot.stat_currLvl += 1;
				global.inv_upgradeParts.RIVEN -= uiEl_upgradeCost
			}
		}
		global.uiYES = 2;
	}
	
	uiEl_Init = function()
	{
		uiEl_StatSlot = global.inv_StatMods[uiEl_slotIndex];
		uiEl_text = string(uiEl_StatSlot.stat_currLvl);
		uiEl_sprite = uiEl_StatSlot.stat_icon;	
	}