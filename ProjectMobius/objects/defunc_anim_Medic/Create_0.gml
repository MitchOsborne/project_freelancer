/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_medic_idle_fl;

anim_idle_front = spr_medic_idle_fl;
anim_idle_back = spr_medic_idle_bl;

anim_walk_front = spr_medic_walk_fl;
anim_walk_back = spr_medic_walk_bl;

anim_dash_front = spr_medic_dash_fl;
anim_dash_back = spr_medic_dash_bl;

anim_incap = spr_medic_incap;
anim_stun_front = anim_default;
anim_stun_back = anim_default;