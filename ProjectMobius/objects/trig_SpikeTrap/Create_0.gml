/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

func_OnCharTouch = function(a_other)
{
	if(!(a_other.isPhased || a_other.isInvul))
	{
		var tDir = point_direction(x+(sprite_width/2), y + (sprite_height/2), a_other.x, a_other.y);
		ReportHit(Dummy.DummyProj, Dummy.DummyWep, a_other, Dummy.char_damageStat, tDir, new DMGReport(0,1,0,0)); 
	}
}