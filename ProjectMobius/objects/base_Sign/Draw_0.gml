/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(show_sign)
{
	var tText = dispText;
	array_delete(txtArr, 0, array_length(txtArr));
	while (string_length(tText) > 0)
	{
		if(string_length(tText) < txtLineLimit)
		{
			array_push(txtArr, tText);
			tText = string_delete(tText, 1, txtLineLimit);
		}else
		{
			var tStr = string_copy(tText,1,txtLineLimit);
			var tInd = string_last_pos(" ", tStr);
			var tStr2 = string_copy(tText, 1, tInd-1);
			tText = string_delete(tText, 1, tInd);
			array_push(txtArr, tStr2);
		}
	}
	drawPos = new Vector2(x - ((txtLineLimit/2)*charWidth), y - (32 + (charWidth * array_length(txtArr))));
	for(var i = 0; i < array_length(txtArr); i += 1)
	{
		
		DrawTextOutline(drawPos.x,drawPos.y + (10*i),1,txtArr[i], c_white, c_black);
	}
}