/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

lifeTime = 0.3;

baseDamage = 5;
damage = 5;

ignoreObstacles = true;

audio_play_sound(sfx_explosion, 5,false);