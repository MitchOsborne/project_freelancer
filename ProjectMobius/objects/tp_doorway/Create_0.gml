/// @description Insert description here
// You can write your code in this editor

event_inherited();

doorIndex = 0;

func_OnCharTouch = function(a_other)
{
	if(a_other.PlayerControlled)
	{
		if(!hasEntered)
		{
			hasEntered = true;
			global.exitRoom = true;
			global.entryIndex = tp_entIndex;
			global.targetRoom = tp_targetRoom;
		}
	}
}