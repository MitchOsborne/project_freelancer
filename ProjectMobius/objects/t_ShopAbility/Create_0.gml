/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

itemStock = 1;
seed = date_get_second(date_current_datetime());


soldSlot = choose("Ability1", "Ability2", "Core");
soldItem = undefined;

itemCost = -1;
itemName = -1;

face_icon = spr_gunShop_face;