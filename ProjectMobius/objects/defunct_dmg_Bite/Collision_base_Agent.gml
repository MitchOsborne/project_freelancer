/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(weapon.onHit != 0)
{
	with (weapon)
	{
		AttachLinearForce(force_linear, owner, owner, melee_currAtk.atk_lungeDur/2, owner.direction + 180, melee_currAtk.atk_lungeForce);
	}
}