/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

uiEl_HealCost = -1;
uiEl_ArchText = "Create Stat Mods: ";
uiEl_ModType = STAT_TYPE.NONE;
uiEl_ModCost = 1;
uiEl_tooltipTxt = "";

uiEl_Function = function()
{
	if(CheckExistance(global.Player1Char))
	{
		switch uiEl_ModType
		{
			case STAT_TYPE.DAMAGE:
			if(global.inv_upgradeParts.ATK >= uiEl_ModCost)
			{
				var tInst = new Stat_DMGSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.ATK -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.ATKSPEED:
			if(global.inv_upgradeParts.ATK >= uiEl_ModCost)
			{
				var tInst = new Stat_SPDSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.ATK -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.HEALTH:
			if(global.inv_upgradeParts.DEF >= uiEl_ModCost)
			{
				var tInst = new Stat_HPSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.DEF -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.SHIELDS:
			if(global.inv_upgradeParts.DEF >= uiEl_ModCost)
			{
				var tInst = new Stat_SHDSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.DEF -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.COOLDOWN:
			if(global.inv_upgradeParts.ABL >= uiEl_ModCost)
			{
				var tInst = new Stat_CDSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.ABL -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.DURATION:
			if(global.inv_upgradeParts.ABL >= uiEl_ModCost)
			{
				var tInst = new Stat_DURSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.ABL -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.CRITRATE:
			if(global.inv_upgradeParts.LUCK >= uiEl_ModCost)
			{
				var tInst = new Stat_CRITSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.LUCK -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.PROCRATE:
			if(global.inv_upgradeParts.LUCK >= uiEl_ModCost)
			{
				var tInst = new Stat_PROCSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.LUCK -= uiEl_ModCost;
			};
			break;
			case STAT_TYPE.NONE:
			if(global.inv_upgradeParts.RIVEN >= uiEl_ModCost)
			{
				var tInst = new Stat_RivenSlot();
				tInst.stat_currLvl = uiEl_ModCost/5;
				array_push(global.inv_StatMods, tInst);
				global.inv_upgradeParts.RIVEN -= uiEl_ModCost;
			};
			break;
			default:
			break;
		}
	}
}

