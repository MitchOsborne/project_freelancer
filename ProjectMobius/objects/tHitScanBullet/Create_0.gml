/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

event_inherited();
teamID = TEAMID.Neutral

spread = 0;
randSpread = false;
range = 1000;
pierceStr = 1;

spd = 1;
speedMod = 1;

ignoreObstacles = false;
lifeTime = 0.2;
lifeTimer = 0;

baseDamage = 2;
damage = 2;
weapon = undefined;

drawRange = range;
