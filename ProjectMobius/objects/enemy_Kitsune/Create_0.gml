/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();


char_StatArch = global.statArch_Heavy;
ai_combatPriority = 4;

ai_defaultOperator = ai_AdvMeleeAgent;

ai_canEvade = true;

baseSpeed = 2;
moveSpeed = baseSpeed;

char_hasLight = false;

char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_Katana;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_KitsuneSamurai, id);