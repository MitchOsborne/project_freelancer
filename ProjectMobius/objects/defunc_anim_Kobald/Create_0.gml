/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_kobald_idle;

anim_idle_front = spr_kobald_idle;
anim_idle_back = spr_kobald_idle;

anim_walk_front = spr_kobald_walk;
anim_walk_back = spr_kobald_walk;

anim_dash_front = spr_kobald_dash;
anim_dash_back = spr_kobald_dash;

anim_incap = spr_kobald_idle;
anim_stun_front = spr_kobald_dash;
anim_stun_back = spr_kobald_dash;