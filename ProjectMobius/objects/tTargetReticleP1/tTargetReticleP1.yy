{
  "$GMObject":"",
  "%Name":"tTargetReticleP1",
  "eventList":[],
  "managed":true,
  "name":"tTargetReticleP1",
  "overriddenProperties":[],
  "parent":{
    "name":"Physics",
    "path":"folders/Objects/Misc/Physics.yy",
  },
  "parentObjectId":null,
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"spr_tTargetP1",
    "path":"sprites/spr_tTargetP1/spr_tTargetP1.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}