/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();
intr_weight = 10;
intr_holdTime = 1;
intr_stat = STAT_CAT.ATTACK;
intr_ChestID = -1;

openCheck = true;
func_OnInteract = function()
{
	var tTxt = "NONE";
	
	switch intr_stat
	{
		case STAT_CAT.ATTACK:
		global.inv_upgradeParts.ATK += 1;
		tTxt = "ATK Mod +1";
		break;
		case STAT_CAT.DEFENSE:
		global.inv_upgradeParts.DEF += 1;
		tTxt = "DEF Mod +1";
		break;
		case STAT_CAT.ABILITY:
		global.inv_upgradeParts.ABL += 1;
		tTxt = "ABL Mod +1";
		break;
		case STAT_CAT.LUCK:
		global.inv_upgradeParts.LUCK += 1;
		tTxt = "LUCK Mod +1";
		break;
	}
	
	array_push(Renderer.rndr_ParticleArr, new Ptl_Text(x, y, random_range(-0.2, 0.2), -0.5, tTxt));
	array_push(global.currentRoom.rd_ChestArr, intr_ChestID);
	instance_destroy(self);
	
}