/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(openCheck)
{
	for(var i = 0; i < array_length(global.currentRoom.rd_ChestArr); i += 1)
	{
		if(global.currentRoom.rd_ChestArr[i] == intr_ChestID)
		{
			instance_destroy(id);
		}
	}
	openCheck = false;
}
