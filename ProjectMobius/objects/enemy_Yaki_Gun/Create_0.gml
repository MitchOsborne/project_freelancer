/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Shooter;

ai_combatPriority = 2;

char_armLvl = 0;

delete(char_AC.ac_bpLd.ld_slots.ld_wep1);
char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_Rook443;

instance_destroy(animator);
animator = InitAnimator(anim_YakiHat, id);