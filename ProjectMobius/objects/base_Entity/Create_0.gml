/// @description Insert description here
// You can write your code in this editor
event_inherited();

shdr_color = shader_get_uniform(shdr_ownerColor, "ownerColor");

baseHp = -1;
hp = -1;

isPhased = false;
isInvis = false;
isInvul = false;
isSprinting = false;
isDashing = false;

isPsiTouched = false;
isStunned = false;
isIncap = false;
isAbilityLocked = false;

locomotor = undefined;
animator = undefined;

teamID = TEAMID.Neutral;