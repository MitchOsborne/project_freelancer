/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

teamID = TEAMID.Enemy;

animator = InitAnimator(base_Animator,id);

DummyBP = global.bp_Abilities.bp_Empty;
DummyWep = InitAbility(DummyBP, id);
DummyProj = CreateLiteProjectile(base_DummyProj, DummyWep, 0, new Vector2(x,y), 0, undefined);