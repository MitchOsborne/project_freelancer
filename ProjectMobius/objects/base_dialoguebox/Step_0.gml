/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();


var tLen = string_length(db_text)
if(db_charDispl < tLen)
{
	db_charDispl += GetFixedDeltaTime()*db_charPerSecond;
	db_charDispl = clamp(db_charDispl, 0, tLen);
}
if(db_charDispl == tLen) db_showFullText = true;
