/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ui_Sprite = spr_MenuBkg;

db_DialogueChain = undefined;

ui_Size = new Vector2(256,34);
ui_Scale = 2;
db_charFace = spr_None;
db_text = "Text Goes Here";

db_charPerSecond = 20;
db_charDispl = 0;
db_showFullText = false;

db_facePos = new Vector2(4,4);
db_textPos = new Vector2(128,10);

ui_Options = [mnuEl_DialogueChain];