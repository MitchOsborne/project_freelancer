/// @description Insert description here
// You can write your code in this editor
if(global.debug)
{
	draw_circle_color(x,y,intr_range, c_Gold, c_Gold,true);
	draw_rectangle_color(bbox_left, bbox_top, bbox_right, bbox_bottom, c_aqua, c_aqua, c_aqua, c_aqua, true);
}

draw_sprite(sprite_index, image_index, x,y);

if(intr_holdTmr > 0)
{
	var tmrPercent = (intr_holdTmr/intr_holdTime) * 100;
	draw_healthbar(bbox_left, bbox_top, bbox_right, bbox_top+4, tmrPercent, c_black, c_white, c_Gold, 0, true, true);
}