/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
teamID = TEAMID.Neutral

bounceCount = 0;
dmg_maxBounces = 3;

baseSpeed = 2;
speedMod = 1;

travelTime = 1;
lifeTime = 1.5;

hp = 1;

damageMultiplier = 1;
damage = 0;
weapon = undefined;

explosionProj = tExplosion;
ignoreObstacles = true;