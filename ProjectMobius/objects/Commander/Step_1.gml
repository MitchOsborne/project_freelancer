/// @description Insert description here
// You can write your code in this editor

if(array_length(global.cmd_combatantArr) != ds_list_size(global.enmList))
{
	array_clear(global.cmd_combatantArr);
	for(var i = 0; i < ds_list_size(global.enmList); i += 1)
	{
		array_push(global.cmd_combatantArr, global.enmList[|i]);
	}
	array_sort(global.cmd_combatantArr, func_SortCombatWidth);
	
}

cmd_UpdateTmr += GetDeltaTime(global.timeScale);

if(cmd_UpdateTmr >= cmd_UpdateRes)
{
	cmd_UpdateTmr = 0;
	
	for(var i = 0; i < ds_list_size(global.enmList); i += 1)
	{
		var tEnm = global.enmList[|i];
		var tTarg = undefined;
		if(tEnm.ai_operator != undefined)
		{
			tTarg = tEnm.ai_operator.enemyTarget;
		}
		tEnm.ai_fightScore = infinity;
		if(CheckExistance(tTarg))
		{
			tEnm.ai_fightScore = cmd_CalcFightScore(tEnm, tTarg);
			if(array_find(tTarg.ai_atkArr, tEnm) == -1) array_push(tTarg.ai_atkArr, tEnm);
		}
	}
	
	for(var i = 0; i < ds_list_size(global.allyList); i += 1)
	{
		var tChar = global.allyList[|i];
		array_sort(tChar.ai_atkArr, func_SortCombatWidth);
	}
}
