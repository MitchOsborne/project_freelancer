/// @description Insert description here
// You can write your code in this editor

switch (global.alertMode)
{
	case ALERT_STATE.Clear:
	{
		global.alertTxt = "Clear";
		for(var i = 0; i < array_length(global.cmd_combatantArr); i += 1)
		{
			var tChar = global.cmd_combatantArr[i];
			if(tChar.ai_operator.ai_CurrentState <= AI_STATE.SEARCH)
			{
				//tChar.ai_operator.ai_CurrentState = AI_STATE.IDLE;
			}
		}
	}break;
	case ALERT_STATE.Warning:
	{
		global.alertTxt = "Warning";
		
		for(var i = 0; i < array_length(global.cmd_combatantArr); i += 1)
		{
			var tChar = global.cmd_combatantArr[i];
				
			//Makes sure the combatant isnt already fighting the enemy
			if(tChar.ai_operator.ai_CurrentState == AI_STATE.IDLE)
			{
				if(i <= 1) tChar.ai_operator.ai_CurrentState = AI_STATE.INVESTIGATE;
				else
				{
					tChar.ai_operator.ai_CurrentState = AI_STATE.INVESTIGATE;
					tChar.ai_operator.allyTarget = global.cmd_combatantArr[i mod 2] //Gets the combatants to follow one of the 2 top Combat Priority characters
				}
			}
		}
	}break;
	case ALERT_STATE.Danger:
	{
		global.alertTxt = "Danger";
		
		for(var i = 0; i < array_length(global.cmd_combatantArr); i += 1)
		{
			var tChar = global.cmd_combatantArr[i];
			var tTarg = tChar.ai_operator.enemyTarget;
			if(CheckExistance(tTarg))
			{
				var tI = array_find(tTarg.ai_atkArr, tChar);
				if(tI != -1 && tI < global.cmd_combatWidth)
				{
					tChar.ai_operator.ai_CurrentState = AI_STATE.FIGHT;
				}
				else 
				{
					tChar.ai_operator.ai_CurrentState = AI_STATE.ENGAGE;
				}
			}
		}
	}
	
}

for(var i = 0; i < ds_list_size(global.allyList); i += 1)
{
	var tChar = global.allyList[|i];
	if(!tChar.PlayerControlled)
	{
		switch global.alertMode
		{
			case ALERT_STATE.Clear:
			{
				tChar.ai_operator.ai_CurrentState = AI_STATE.IDLE;
			}break;
			case ALERT_STATE.Warning:
			{
				if(CheckExistance(tChar.ai_operator.enemyTarget)) tChar.ai_operator.ai_CurrentState = AI_STATE.ENGAGE;
				else tChar.ai_operator.ai_CurrentState = AI_STATE.INVESTIGATE;	
			}break;
			case ALERT_STATE.Danger:
			{
				if(CheckExistance(tChar.ai_operator.enemyTarget)) tChar.ai_operator.ai_CurrentState = AI_STATE.FIGHT;
				else tChar.ai_operator.ai_CurrentState = AI_STATE.INVESTIGATE;	
			}
		}
		
	}
}