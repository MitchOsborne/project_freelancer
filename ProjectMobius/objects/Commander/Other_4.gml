/// @description Insert description here
// You can write your code in this editor

CreateNavGrids(global.gridSize);

ds_list_clear(global.enmList);
array_clear(global.cmd_combatantArr);
ds_list_clear(global.allyList);

for(var i = 0; i < instance_number(base_Character); i+= 1)
{
	var tInst = instance_find(base_Character,i);
	if(tInst.teamID == TEAMID.Enemy)
	{
		if(!object_is_ancestor(tInst.object_index, defunc_base_Trap))
		{
			ds_list_add(global.enmList, tInst);
		}
	}else if(tInst.teamID == TEAMID.Ally)
	{
		ds_list_add(global.allyList, tInst);
	}
}

