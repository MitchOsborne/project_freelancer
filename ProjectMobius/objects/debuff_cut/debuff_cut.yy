{
  "$GMObject":"",
  "%Name":"debuff_cut",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":1,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":1,"eventType":3,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"debuff_cut",
  "overriddenProperties":[],
  "parent":{
    "name":"Debuffs",
    "path":"folders/Objects/Agents/Components/defunc_Modifiers/Debuffs.yy",
  },
  "parentObjectId":{
    "name":"base_dbf_CutDebuff",
    "path":"objects/base_dbf_CutDebuff/base_dbf_CutDebuff.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"t_LaserTrail",
    "path":"sprites/t_LaserTrail/t_LaserTrail.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}