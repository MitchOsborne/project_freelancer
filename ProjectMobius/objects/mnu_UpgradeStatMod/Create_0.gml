/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

	ui_ModArr = [];
	ui_opWidth = 5;
	ui_opXSpacing = 64;
	ui_opYSpacing = 64;
	ui_slotIndex = -1;
	ui_Init = function()
	{
		array_clear(ui_ModArr);
		//Gets all the valid mods that can be socketed
		for(var i = 0; i < array_length(global.inv_StatMods); i += 1)
		{
			var invMod = global.inv_StatMods[i];
			var canSlot = true;
			//Makes sure non-empty mods aren't already being used
			if(invMod.stat_MainType == STAT_TYPE.NONE)canSlot = false;
			if(canSlot) array_push(ui_ModArr, invMod);
		}
		
		
		//Creates a UI element for each valid stat mod that can be equipped
		for(var z = 0; z < array_length(ui_ModArr); z += 1)
		{
			var tMod = ui_ModArr[z];
			var tInst = instance_create_layer(x+ui_opBorder + (ui_opXSpacing*(z mod ui_opWidth)), y+ui_opBorder + (ui_opYSpacing*(floor(z/ui_opWidth))), "Menu", mnuEl_UpgradeStatMod);
			tInst.uiEl_slotIndex = array_find(global.inv_StatMods, tMod);
			tInst.uiEl_parentMenu = id;
			tInst.uiEl_Init();
			array_push(ui_Options,tInst);
		}
		ui_hasInit = true;
	}

