/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

uiEl_HealCost = -1;
uiEl_ArchText = "Heal Wounds: ";

uiEl_tooltipTxt = "";

uiEl_Function = function()
{
	if(CheckExistance(global.Player1Char))
	{
		if(global.credits >= uiEl_HealCost)
		{
			HealParty();
			global.credits -= uiEl_HealCost;
			audio_play_sound(sfx_lvlup, 5, false);
		}
	}
}

