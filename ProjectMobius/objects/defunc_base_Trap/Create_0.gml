/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

baseSpeed = 0;
moveSpeed = baseSpeed;


char_baseDamage = 1;
char_damageLvl = 0;
char_damagePerLvl = char_StatArch.dmg;

char_baseHealth = 50;
char_healthLvl = 3;
char_healthPerLvl = char_StatArch.HP;

isPhased = true;
isInvul = true;

char_AC.ac_bpLd.ld_slots.ld_wep1 = new PKG_SinglePack(wep_AK);

ai_defaultOperator = ai_Trap;
teamID = TEAMID.Enemy;
animator = undefined;

direction = image_angle;