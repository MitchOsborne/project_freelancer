/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

baseSpeed = 0;
moveSpeed = baseSpeed;


isPhased = true;

char_AC.ac_bpLd.ld_slots.ld_wep1  = new PKG_SinglePack(wep_TurnTrap);

animator = undefined;
ai_defaultOperator = ai_TurnTrap;