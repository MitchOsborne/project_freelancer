/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Shooter;
ai_combatPriority = 1;

ai_defaultOperator = ai_AdvRangedAgent;

baseSpeed = 1.6;
moveSpeed = baseSpeed;

char_hasLight = false;

char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_Soulfire;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_KitsuneOracle, id);