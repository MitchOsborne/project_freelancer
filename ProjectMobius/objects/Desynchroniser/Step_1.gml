/// @description Insert description here
// You can write your code in this editor

var qSize = ds_list_size(threadQueue);
for(var q = 0; q < qSize; q += 1)
{
	var it = 0;
	for(var l = 1; l < ds_list_size(threadList); l += 1)
	{
		var iThread = threadList[|it];
		var tThread = threadList[|l];
		var tSize = ds_list_size(tThread);
		if(tSize < ds_list_size(iThread)) it = l;
	}
	
	ds_list_add(threadList[|it],threadQueue[|0]);
	ds_list_delete(threadQueue, 0);
	qSize = ds_list_size(threadQueue);
	q -= 1;
}
