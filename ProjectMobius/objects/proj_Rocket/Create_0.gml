/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
baseSpeed = 15;
damage = 1;
hp = 1;

explosive = tExplosion;

image_angle = direction;

spawnParticle = tSmokeParticle;
spawnRate = .01;
spawnTimer = 0;