/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Shepherd;
char_StatArchCard = new crd_Ronin_Iaido();

char_AC.ac_bpLd.ld_slots.ld_wep1  = global.bp_Abilities.bp_Katana;

char_OnKillProcs = [Proc_RefreshStamina];

char_tipArr = [
"You can extend your dodge duration by chaining Triple Slash and Finishing Blow with Dashes", 
"The Ronin doesn't need a sword equipped to use its Sword based abilities",
"Landing a Finishing Blow will refund stamina, killing an enemy with it will refund extra"
]

char_baseDamage = 5;
char_damageLvl = 1;
char_damagePerLvl =  char_StatArch.dmg;

char_baseHealth = 20;
char_healthLvl = 1;
char_healthPerLvl =  char_StatArch.HP;

maxVit = char_maxHP/2;
vitStat = 0;

char_shieldLvl = 0;
char_maxShd = char_maxHP * ((char_shieldLvl*char_shieldPerLvl)/100);
char_shdBufferMax = char_maxShd/2;
char_shdChargeRate = char_maxShd/20;

char_armStat = 0;

baseSpeed = 1;
moveSpeed = baseSpeed;

baseSightRadius = 160;

canSubscribe = true;
canRead = true;
canWrite = true;


ai_defaultOperator = ai_Adaptive;
animator = InitAnimator(anim_Ronin,id);
teamID = TEAMID.Ally;
