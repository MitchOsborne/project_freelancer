/// @description Insert description here
// You can write your code in this editor


event_inherited();
teamID = TEAMID.Neutral

dmg_activeFrames = [false, true, false, false];

dmg_hitSlow = 0.2;

spread = 0;
randSpread = false;
ignoreObstacles = true;

hp = 999

spd = 0;
speedMod = 1;
