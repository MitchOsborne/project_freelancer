/// @description Insert description here
// You can write your code in this editor
event_inherited();
//Reflects projectile back at target of weapon owner, otherwise weapon direction

if(other.teamID != teamID)
{
	var tOwner = weapon.owner;
	if(weapon.abl_AbilityType == ABILITY_TYPE.MELEE) 
	{
		if(other.weapon.abl_AbilityType == ABILITY_TYPE.MELEE)
		{
			if(other.image_index > image_index || (other.image_index == image_index && teamID == TEAMID.Ally))
			{
				var tDir = point_direction(other.x, other.y, x,y)-90;
				AttachMod(debuff_stunned, other.weapon.owner, weapon.owner, 0.5, 1, false);
				AttachLinearForce(other.weapon.owner, weapon.owner, 0.1, tDir, 2);
				AttachLinearForce(other.weapon.owner, weapon.owner, 0.1, tDir-180, 2);
				array_push(Renderer.rndr_ParticleArr, new Ptl_SwordHit(other.x, other.y, 0,0));
				audio_play_sound(sfx_coin_l,1,false);
				instance_destroy(other);
			}
		}else ReflectProjectile(id, other);
	}
}