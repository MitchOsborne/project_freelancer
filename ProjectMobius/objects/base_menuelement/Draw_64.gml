/// @description Insert description here
// You can write your code in this editor


if(uiEl_parentMenu == global.activeUI && uiEl_parentMenu.ui_hasInit && !uiEl_parentMenu.ui_animOpen)
{
	var tC1 = uiEl_color;
	var tC2 = c_black;
	if(uiEl_isSelected)
	{
		tC1 = c_teal;
		tC2 = c_dkgrey;
	}
	if(!uiEl_isValid) tC2 = c_red;
	if(uiEl_sprite != spr_None) draw_sprite_ext(uiEl_sprite, -1, x, y, 1, 1, 0, tC1, 1);
	DrawTextOutline(x, y, 1, uiEl_text, tC1, tC2, uiEl_parentMenu.ui_opXSpacing, false);
	if(uiEl_isSelected) draw_text(global.activeUI.x, global.activeUI.y - 20, uiEl_tooltipTxt);
}