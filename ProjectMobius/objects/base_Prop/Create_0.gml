/// @description Insert description here
// You can write your code in this editor

enum PSI_PROJTYPE
{
	SMALL,
	MEDIUM,
	LARGE,
}

// Inherit the parent event
event_inherited();

maxHP = 10;
hp = maxHP;


cboxType = phys_Prop;
cbox = undefined;

psi_ProjectileType = PSI_PROJTYPE.SMALL;

