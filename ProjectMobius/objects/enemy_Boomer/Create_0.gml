/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

baseSpeed = 2;
moveSpeed = baseSpeed;

ai_defaultOperator = ai_Wildlife;

char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_SelfDestruct;

AttachMod(buff_BoomerBomb, id, id, infinity, 1);

teamID = TEAMID.Enemy;

animator = InitAnimator(anim_Boomer, id);