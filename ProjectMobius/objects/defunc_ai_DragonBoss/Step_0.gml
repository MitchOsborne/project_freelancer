/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(slavedObj.locomotor != undefined && instance_exists(slavedObj.locomotor))
{
	object_set_mask(id,slavedObj.locomotor.mask_index);
}
//slavedObj.inAbility1 = false;
//Debug_AiState("State Info: ", currentState, true); 
if(CheckExistance(enemyTarget))
{
	reqPath = false;
	var movPos = new Vector2(x,y);
	var engageRange = slavedObj.weapon1.ai_engageRange*0.5;
	engageRange = 500;
	var cont = ai_task_MoveIntoRange(self, enemyTarget, engageRange);
	
	if(cont)
	{
		var negMod = 180 * (slavedObj mod 2);
		if(waitTimer > waitTime)
		{
			if(attackPhase == 0)
			{
				if(slavedObj.utility1.ammoCount > 0)
				{
					slavedObj.direction += 36 * GetDeltaTime(timeScale);
					slavedObj.inUtility1 = true;
					slavedObj.inAbility1 = false;
					slavedObj.inWeapon1 = false;
				}else
				{
					attackPhase += 1;
					waitTimer = 0;
				}
			}
			else if(attackPhase == 1)
			{
				if(slavedObj.ability1.ammoCount > 0)
				{
					sinTimer += 2 * GetDeltaTime(timeScale);
					if(sinTimer >= (pi * 2)) sinTimer = 0;
					var tDir = 70 * (sin(sinTimer)-0.5);
					slavedObj.inUtility1 = false
					slavedObj.inAbility1 = true;
					slavedObj.inWeapon1 = false;
					slavedObj.direction = tDir + point_direction(slavedObj.x,slavedObj.y,enemyTarget.x, enemyTarget.y) - 90;
				}else
				{
					attackPhase += 1;
					waitTimer = 0;
				}
			}
			else if(attackPhase == 2)
			{
				if(slavedObj.weapon1.ammoCount > 0)
				{
					
					slavedObj.direction += 36 * GetDeltaTime(timeScale);
					slavedObj.inUtility1 = false;
					slavedObj.inAbility1 = false;
					slavedObj.inWeapon1 = true;			
				}else
				{
					attackPhase += 1;
					waitTimer = 0;
				}
			}else attackPhase = 0;
		}else waitTimer += GetDeltaTime(timeScale);
		//slavedObj.direction = point_direction(global.Player1ID.x, global.Player1ID.y,slavedObj.x,slavedObj.y);
		//var movVec = GetCoord(slavedObj.direction + (90 - negMod));
		//slavedObj.mx = movVec.x/2;
		//slavedObj.my = movVec.y/2;
	}else
	{
		
		slavedObj.inWeapon1 = false;
	}
}
