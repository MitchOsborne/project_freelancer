/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

spd = 10;
damage = 2;
hp = infinity;
ignoreObstacles = true;

isReversed = false;

lifeTime = 3;

baseRotSpeed = 0.3;
targetingAngle = 45;
maxRotSpeed = baseRotSpeed;

retargetTime = 0.3;
retargetTimer = retargetTime/2;

target = undefined;