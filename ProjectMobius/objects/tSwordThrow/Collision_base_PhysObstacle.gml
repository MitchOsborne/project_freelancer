/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var wallNormal = GetCollisionNormal(x,y,other);
if(wallNormal != -1)
{
	isReversed = true;
	direction = (2*wallNormal) - direction - 180;
}
