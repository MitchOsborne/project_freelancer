/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();
intr_weight = 10;
intr_holdTime = 1;
intr_weapon = global.ablArchUnlocks.RKRifle;
intr_ChestID = -1;

openCheck = true;
func_OnInteract = function()
{
	intr_weapon.arch_Level += 1;
	array_push(Renderer.rndr_ParticleArr, new Ptl_Text(x, y, random_range(-0.2, 0.2), -0.5, intr_weapon.arch_Name));
	array_push(global.currentRoom.rd_ChestArr, intr_ChestID);
	instance_destroy(self);
	
}