/// @description Insert description here
// You can write your code in this editor
event_inherited();
var tInst = SpawnCharacter(spn_charData, id, undefined, teamID);
if(spn_PatrolPath != undefined) 
{
	tInst.ai_operator.ai_patrolPath = spn_PatrolPath;
	tInst.ai_operator.ai_patrolID = spn_PatrolPath.ptrl_ID;
}
instance_destroy(self)