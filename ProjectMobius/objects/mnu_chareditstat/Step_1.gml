/// @description Insert description here
// You can write your code in this editor



// Inherit the parent event
event_inherited();

//Repopulates the mod array when the menu regains focus
if(global.activeUI == id && ui_refocus)
{
	ui_Init();
	ui_refocus = false;
}else if(global.activeUI != id) ui_refocus = true;