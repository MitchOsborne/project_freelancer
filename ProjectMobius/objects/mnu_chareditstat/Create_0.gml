/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ui_opWidth = 5;
ui_opXSpacing = 64;
ui_opYSpacing = 64;
ui_CharCard = undefined;
ui_refocus = false;

ui_Init = function()
{
	
	
	for(var z = 0; z < array_length(ui_Options); z += 1)
	{
		if(CheckExistance(ui_Options[z])) instance_destroy(ui_Options[z]);
		array_delete(ui_Options, z, 1);
		z -= 1;
	}
	
	ui_CharCard = UI_GetCharCard(global.editChar);
	for(var i = 0; i < array_length(ui_CharCard.crd_StatSlotArr); i += 1)
	{
		var tInst = instance_create_layer(x+ui_opBorder + (ui_opXSpacing*(i mod ui_opWidth)), y+ui_opBorder + (ui_opYSpacing*(floor(i/ui_opWidth))), "Menu", mnuEl_StatSlot);
		tInst.uiEl_slotIndex = i;
		tInst.uiEl_parentMenu = id;
		tInst.uiEl_Init();
		tInst.depth = depth - 1;
		array_push(ui_Options, tInst);
	}
	ui_hasInit = true;
}
	
