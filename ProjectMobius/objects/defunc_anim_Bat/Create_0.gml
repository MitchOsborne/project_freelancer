/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_bat;

anim_idle_front = spr_bat;
anim_idle_back = spr_bat;

anim_walk_front = spr_bat;
anim_walk_back = spr_bat;

anim_dash_front = spr_bat;
anim_dash_back = spr_bat;

anim_incap = spr_bat;
anim_stun_front = spr_bat;
anim_stun_back = spr_bat;