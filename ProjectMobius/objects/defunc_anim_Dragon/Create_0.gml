/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_dragon_idle_f;

anim_idle_front = spr_dragon_idle_f;
anim_idle_back = spr_dragon_idle_f;

anim_walk_front = spr_dragon_walk_f;
anim_walk_back = spr_dragon_walk_f;

anim_dash_front = spr_dragon_walk_f;
anim_dash_back = spr_dragon_walk_f;

anim_incap = spr_dragon_incap;
anim_stun_front = spr_dragon_stun;
anim_stun_back = spr_dragon_stun;