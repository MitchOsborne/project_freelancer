/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

uiEl_text = "Buy Stat Mods";
uiEl_ModType = STAT_TYPE.NONE;
uiEl_tooltipTxt = "";

uiEl_Function = function()
{
	var tMenu = CreateMenu(new Vector2(x,y), mnu_BuyStatMods, global.menuUser);
	tMenu.ui_Init();
	array_push(global.ui_MenuStack, tMenu);
}

