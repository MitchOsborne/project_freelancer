/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(CheckExistance(subject))
{
	subject.isPhased = false;
	subject.isDashing = false;
}