/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_Spool_Idle;

anim_idle_front = spr_Spool_Idle;
anim_idle_back = spr_Spool_Idle;

anim_walk_front = spr_Spool_Idle;
anim_walk_back = spr_Spool_Idle;

anim_dash_front = spr_Spool_Idle;
anim_dash_back = spr_Spool_Idle;

anim_incap = spr_Spool_Idle;
anim_stun_front = anim_default;
anim_stun_back = anim_default;