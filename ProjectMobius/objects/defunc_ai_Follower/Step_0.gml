/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(CheckExistance(slavedObj.locomotor))
{
	object_set_mask(id,slavedObj.locomotor.mask_index);
}
//slavedObj.inAbility1 = false;
//Debug_AiState("State Info: ", currentState, true); 
if(CheckExistance(global.Player1ID))
{
	reqPath = false;
	var movPos = new Vector2(x,y);
	var followRange = 24;
	var cont = ai_task_MoveIntoRange(self, global.Player1ID, followRange);
	
	
	hitCtr += GetDeltaTime(global.timeScale);
	var avoidProj = false;
	if(hitCtr >= hitRes)
	{
		hitCtr = 0;
		ds_list_clear(hitList);
		collision_circle_list(x,y,dodgeRad, base_Projectile,false, true,hitList,false);
	}
	for(var i = 0; i < ds_list_size(hitList); i += 1)
	{
		if(CheckExistance(hitList[|i]))
		{
			if(hitList[|i].teamID != slavedObj.teamID) avoidProj = true;
		}
	}
	
	if(avoidProj)
	{
		dodgeCtr += GetDeltaTime(global.timeScale);
		
		if(dodgeCtr >= dodgeRes)
		{
			dodgeCtr = 0;
			var physArr = [base_Projectile];
			array_copy(physArr, array_length(physArr), global.AgentCollisionClass, 0, array_length(global.AgentCollisionClass));
			var tNorm = Collision_Normal_Combined(x,y,physArr,global.AgentTileCollisionClass,dodgeRad, 4);
			dodgeVec = GetCoord(tNorm - 70);
		}
		slavedObj.mx = dodgeVec.x;
		slavedObj.my = dodgeVec.y;
	}
	
	
	if(cont)
	{
		//var negMod = 180 * (slavedObj mod 2);
		//if(slavedObj.weapon1.ammoCount > 0) slavedObj.inWeapon1 = cont;
		
		//var movVec = GetCoord(slavedObj.direction + (90 - negMod));
		//slavedObj.mx = movVec.x/2;
		//slavedObj.my = movVec.y/2;
		if(CheckExistance(enemyTarget))
		{
			slavedObj.direction = GetDirection(x,y,enemyTarget.x, enemyTarget.y, false, true);
			slavedObj.inWeapon1 = true;
		}
	}else
	{
		
		slavedObj.inWeapon1 = false;
	}
}

/*
switch(currentState)
{
	case AISTATE.IDLE:
		idleTimer += GetDeltaTime(timeScale);
	
		slavedObj.direction += 1;
		if(collision_line(x,y,enemyTarget.x, enemyTarget.y, base_PhysWall, false, true) == noone)
		{
			nextState = AISTATE.ENGAGE;
			canTransition = true;
		}else
		{
			if(idleTimer > idleTime)
			{
				idleTimer = 0;
				nextState = AISTATE.ROAM;
				canTransition = true;
			}
		}
	break;
	case AISTATE.ROAM:
	
		if(roamPos.x == -infinity || roamPos.y == -infinity)
		{
			//Will make up to 30 attempts to find a valid location to roam to, otherwise IDLE on current position
			roamPos = new Vector2(x,y);
			for(var i = 0; i < 30; i++)
			{
			
				var tPos = new Vector2(x + random_range(-320, 320), y + random_range(-320, 320));
				tPos.x = clamp(tPos.x, 0, room_width);
				tPos.y = clamp(tPos.y, 0, room_height);
			
				var tI = random_range(0, ds_list_size(hotEnmNodes));
				if(tI != 0)
				{
					var node = hotEnmNodes[|tI];
					tPos = new Vector2(node.position.x, node.position.y);
				}
				var tX = tPos.x;
				var tY = tPos.y;
				//tPos = new Vector2(enemyTarget.x, enemyTarget.y);
				var tCellX = abs(floor(tPos.x/32));
				var tCellY = abs(floor(tPos.y/32));
				if(mp_grid_get_cell(global.walkNavGrid,tCellX,tCellY) == 0)
				{
					canPath = mp_grid_path(global.walkNavGrid, navPath, x, y, tCellX*global.gridSize, tCellY * global.gridSize, true);
					if(canPath)
					{
						roamPos = new Vector2(tCellX*global.gridSize+ (global.gridSize/2), tCellY * global.gridSize + (global.gridSize/2));
						break;
					}
				}
			}
		}
	
		reqPath = true;
		navPos = roamPos;
	
	
		if(CheckExistance(enemyTarget))
		{
			if(collision_line(x,y,enemyTarget.x, enemyTarget.y, base_PhysWall, false, true) == noone)
			{
				nextState = AISTATE.ENGAGE;
				canTransition = true;
			}
		}
		if(point_distance(x,y,navPos.x,navPos.y) < 16)
		{
			nextState = AISTATE.IDLE;
			canTransition = true;
			roamPos = new Vector2(-infinity, -infinity);
		}
	break;
	case AISTATE.SEEK:

		navPos = lastTargetPos;
		if(CheckExistance(enemyTarget))
		{
			if(collision_line(x,y,enemyTarget.x, enemyTarget.y, base_PhysWall, false, true) == noone)
			{
				nextState = AISTATE.ENGAGE;
				canTransition = true;
			}else
			{
				reqPath = true;
				navPos = lastTargetPos;	
			}
			if(lastTargetPos.x != -infinity && lastTargetPos.y != -infinity)
			{
				if(point_distance(x,y,lastTargetPos.x, lastTargetPos.y) <= 32)
				{
					nextState = AISTATE.IDLE;
					canTransition = true;
				}
			}
		}else
		{
			nextState = AISTATE.ROAM;
			canTransition = true;	
		}
	break;
	case AISTATE.ENGAGE:
		if(CheckExistance(enemyTarget))
		{	
			if(collision_line(x,y,enemyTarget.x, enemyTarget.y, base_PhysWall, false, true) != noone)
			{
				nextState = AISTATE.SEEK;
				canTransition = true;
			}else
			{
				navPos = new Vector2(enemyTarget.x, enemyTarget.y);
				reqPath = true;
				if(slavedObj.weapon1.ai_engageRange > point_distance(x,y,enemyTarget.x,enemyTarget.y))
				{
					slavedObj.inAbility1 = 1;	
				}else
				{
					slavedObj.inAbility1 = 0;	
				}
			}
			
		}else
		{
			nextState = AISTATE.SEEK;
			canTransition = true;
		}
	break;
	case AISTATE.FOLLOW:

	break;
	default:
		nextState = AISTATE.IDLE;
		canTransition = true;
	break;
}

navCounter += 1;
if(reqPath)
{
	if(navCounter >= navResolution)
	{
		navCounter = 0;
		var canPath = false
		//mp_potential_settings(30,15,10,true);
		/*
		mp_potential_path_object(navPath,navPos.x, navPos.y,8,4,base_PhysObstacle);
	
		if(collision_line(x,y,path_get_x(navPath,1), 
			path_get_y(navPath,1), base_PhysObstacle, true, true) == noone)
		{
			canPath = true;
		}else
		{
			canPath = false;	
		}
	
		//If it cannot path to target position using potential_path, uses Grid Pathing instead.
		while(canPath == false)
		{
			canPath = mp_grid_path(global.walkNavGrid, navPath, x,y,navPos.x, navPos.y, true);
			if(canPath == false)
			{
				navPos.x = path_get_x(navPath,0.9);
				navPos.y = path_get_y(navPath,0.9);
			}
		}
	
	}
	if(navPath != undefined)
	{
		//Checks to ensure it is moving towards the nearest NavPoint that isn't its current position
	
	
		//var tDist = point_distance(x,y,path_get_point_x(navPath,i), path_get_point_y(navPath,i))
	
	
	
		var hasPos = false;
		while(hasPos == false)
		{
			var pX = path_get_point_x(navPath,0);
			var pY = path_get_point_y(navPath,0);
	
			if(point_distance(x,y,pX,pY) < 8)
			{
				path_delete_point(navPath,0);	
			}else
			{
				hasPos = true;	
			}
		}

	
		movPos = new Vector2(pX, pY);
	
	}
}

var tVec = new Vector2(0,0);
if(point_distance(x,y,movPos.x,movPos.y) > 1)
{
	var tDir = point_direction(x,y,movPos.x,movPos.y) - 90;
	tVec = GetCoord(tDir);
}
	slavedObj.mx = tVec.x;
	slavedObj.my = tVec.y;	

*/
