/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Fighter;
ai_combatPriority = 2;

baseSpeed = 2;
moveSpeed = baseSpeed;

char_hasLight = false;

//char_armLvl = 4;
char_baseShdLvl = 5;

char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_Shortsword;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_YakiHat, id);