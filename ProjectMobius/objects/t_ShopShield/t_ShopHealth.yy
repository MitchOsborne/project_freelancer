{
  "spriteId": {
    "name": "spr_vendGreen",
    "path": "sprites/spr_vendGreen/spr_vendGreen.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "base_StatVendor",
    "path": "objects/base_StatVendor/base_StatVendor.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Shop",
    "path": "folders/Objects/Agents/Shop.yy",
  },
  "resourceVersion": "1.0",
  "name": "t_ShopHealth",
  "tags": [],
  "resourceType": "GMObject",
}