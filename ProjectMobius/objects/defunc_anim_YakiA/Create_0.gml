/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_YakiA_idle;

anim_idle_front = spr_YakiA_idle;
anim_idle_back = spr_YakiA_idle;

anim_walk_front = spr_YakiA_walk;
anim_walk_back = spr_YakiA_walk;

anim_dash_front = spr_YakiA_dash;
anim_dash_back = spr_YakiA_dash;

anim_incap = spr_YakiA_incap;
anim_stun_front = spr_YakiA_dash;
anim_stun_back = spr_YakiA_dash;