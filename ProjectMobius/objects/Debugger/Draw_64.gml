/// @description Insert description here
// You can write your code in this editor
draw_set_font(fnt_default);
draw_set_color(c_white);
draw_text(10,10,"FPS: " + string(round(1 / FPSDisplay)));
		
if(global.debug)
{
	
	draw_text(100,100,"Enemy Scaling: " + string(global.enemyIntensity));
	draw_text(100,110,"Enemies Remaining: " + string(array_length(global.spawnPool) + ds_list_size(global.enmList)));
	draw_text(500,610,"Total Projectiles: " + string(instance_number(base_Projectile)));
	draw_text(500,620,"Agent Locomotors: " + string(instance_number(base_Locomotor)));
	draw_text(500,630,"Projectile Locomotors: " + string(instance_number(base_ProjLocomotor)));
	draw_text(500,650,"Enemy Count: " + string(ds_list_size(global.enmList)));
	
}