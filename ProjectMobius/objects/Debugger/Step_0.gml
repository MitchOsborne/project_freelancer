/// @description Insert description here
// You can write your code in this editor
	
FPSArr[FPSCtr] = GetFixedDeltaTime(1);
FPSCtr += 1;
if(FPSCtr >= FPSAvgSize)
{
	FPSCtr = 0;
	FPSDisplay = 0;
	for(var i = 0; i < FPSAvgSize; i += 1)
	{
		FPSDisplay += FPSArr[i];	
	}
	FPSDisplay /= FPSAvgSize;
}


if(global.debug == DEBUG_MODES.PHYSICS)
{
	layer_set_visible("Physics", true);	
	layer_set_visible("debug", true);
	layer_set_visible("Tiles_Physics", true);
	layer_set_visible("Doors", true);
	layer_set_visible("Patrol", true);
	
}else
{
	layer_set_visible("Physics", false);
	layer_set_visible("debug", false);
	layer_set_visible("Tiles_Physics", false);
	layer_set_visible("Doors", false);
	layer_set_visible("Patrol", false);
}