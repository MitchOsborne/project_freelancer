/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(CheckExistance(slavedObj))
{
	timeScale = slavedObj.timeScale;
	x = slavedObj.x;
	y = slavedObj.y;
	direction = slavedObj.direction;
}else
{
	instance_destroy(id);	
}