/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

char_StatArch = global.statArch_Psion;

char_AC.ac_bpLd.ld_slots.ld_wep1  = new PKG_SinglePack(wep_AK);
char_AC.ac_bpLd.ld_slots.ld_wep2  = PKG_Empty;
char_AC.ac_bpLd.ld_slots.ld_util1 = new PKG_Evade();
char_AC.ac_bpLd.ld_slots.ld_util2 = PKG_Empty;
char_AC.ac_bpLd.ld_slots.ld_abl1  = new PKG_PsiThrow();
char_AC.ac_bpLd.ld_slots.ld_abl2  = PKG_Empty;
char_AC.ac_bpLd.ld_slots.ld_core  = PKG_Empty;

shoppingArr = [PKG_PsiShot, PKG_PsiWave, PKG_MassControl];

char_tipArr = [
"The Psion's Light Saber can reflect projectiles back at enemies", 
"Psi-Wave will tag enemy projectiles for your Psi_Throw ability",
"Mass Control will stun all enemies in the Psi-Radius, and allow them to be affected by Psi-Throw"
]


char_baseDamage = 5;
char_damageLvl = 1;
char_damagePerLvl =  char_StatArch.dmg;

char_baseHealth = 20;
char_healthLvl = 1;
char_healthPerLvl = char_StatArch.HP;

maxVit = char_maxHP/2;
vitStat = 0;

char_shieldLvl = 0;
char_maxShd = char_maxHP * ((char_shieldLvl*char_shieldPerLvl)/100);
char_shdBufferMax = char_maxShd/2;
char_shdChargeRate = char_maxShd/20;

staminaRegenDelay = 0.5;

char_armStat = 0;

baseSpeed = 1.2;
moveSpeed = baseSpeed;

baseSightRadius = 160;

canSubscribe = true;
canRead = true;
canWrite = true;


ai_defaultOperator = ai_Default;
animator = InitAnimator(anim_Armstrong,id);
teamID = TEAMID.Ally;
