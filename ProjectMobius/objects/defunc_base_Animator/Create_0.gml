/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

anim_default = spr_error;

anim_idle_front = anim_default;
anim_idle_back = anim_default;

anim_walk_front = anim_default;
anim_walk_back = anim_default;

anim_dash_front = anim_default;
anim_dash_back = anim_default;

anim_incap = anim_default;
anim_stun_front = anim_default;
anim_stun_back = anim_default;

hasIncap = false;