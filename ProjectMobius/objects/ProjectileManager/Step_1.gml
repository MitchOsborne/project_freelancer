/// @description Insert description here
// You can write your code in this editor

ClearProjGrid();
array_clear(PlayerRendArr);
array_clear(P1RendArr);
array_clear(P2RendArr);
array_clear(AllyRendArr);
array_clear(EnmRendArr);
array_clear(NeutRendArr);

for(var i = 0; i < array_length(global.allyProjArr); i += 1)
{
	var tInst = global.allyProjArr[i];
	tInst.dmg_PreUpdate();
	tInst.dmg_Update();
	tInst.dmg_EndUpdate();
	
	if(tInst.lifeTime <= tInst.lifeTimer || tInst.hp <= 0)
	{
		delete tInst;
		array_delete(global.allyProjArr, i, 1);
		i -= 1;
	}else
	{
		var tVec = new Vector2(floor(tInst.x/projGridSize), floor(tInst.y/projGridSize));
		PushProjToGrid(tInst, tVec);
		if(tInst.owner.PlayerControlled)
		{
			var tI = -1;
			for(var n = 0; n < array_length(PlayerRendArr); n += 1)
			{
				var tRend = PlayerRendArr[n];
				if(tRend[0].owner == tInst.owner)
				{
					tI = n;
					break;
				}
			}
			//Inserts a fresh array for the specific Player
			if(tI == -1)
			{
				array_push(PlayerRendArr, [tInst]);
			}
			//Otherwise adds projectile to existing array index
			else array_push(PlayerRendArr[tI], tInst);
		}
		else array_push(AllyRendArr, tInst);
	}
}


for(var i = 0; i < array_length(global.enmProjArr); i += 1)
{
	var tInst = global.enmProjArr[i];
	tInst.dmg_PreUpdate();
	tInst.dmg_Update();
	tInst.dmg_EndUpdate();
	
	if(tInst.lifeTime <= tInst.lifeTimer || tInst.hp <= 0)
	{
		delete tInst;
		array_delete(global.enmProjArr, i, 1);
		i -= 1;
	}else
	{
		array_push(EnmRendArr, tInst);
		if(!tInst.dmg_isCore)
		{
			var tVec = new Vector2(floor(tInst.x/projGridSize), floor(tInst.y/projGridSize));
			PushProjToGrid(tInst, tVec);
		}
	}
	
}

//Clean up Core Projectile and Counter Projectile Arrays
for(var i = 0; i < array_length(global.coreProjArr); i += 1)
{
	var tInst = global.coreProjArr[i];
	
	if(tInst == undefined || tInst.lifeTime <= tInst.lifeTimer || tInst.hp <= 0)
	{
		delete tInst;
		array_delete(global.coreProjArr, i, 1);
		i -= 1;
	}
}

for(var i = 0; i < array_length(global.counterProjArr); i += 1)
{
	var tInst = global.counterProjArr[i];
	
	if(tInst == undefined || tInst.lifeTime <= tInst.lifeTimer || tInst.hp <= 0)
	{
		delete tInst;
		array_delete(global.counterProjArr, i, 1);
		i -= 1;
	}
}

for(var i = 0; i < instance_number(base_Character); i += 1)
{
	var tInst = instance_find(base_Character, i);
	var tVec = new Vector2(floor(tInst.x/projGridSize), floor(tInst.y/projGridSize));
	var tArr = PullProjFromGrid(tVec);
	
	for(var n = 0; n < array_length(tArr); n += 1)
	{
		var tProj = tArr[n];
		if(ProjTeamCheck(tProj, tInst) == true)
		{
			tProj.dmg_CollideCheck(tInst);
		}
	}
	
	for(var n = 0; n < array_length(global.coreProjArr); n += 1)
	{
		var tProj = global.coreProjArr[n];
		if(ProjTeamCheck(tProj, tInst) == true)
		{	
			tProj.dmg_CollideCheck(tInst);
		}
	}
}

for(var i = 0; i < instance_number(base_Prop); i += 1)
{
	var tInst = instance_find(base_Prop, i);
	var tVec = new Vector2(floor(tInst.x/projGridSize), floor(tInst.y/projGridSize));
	var tArr = PullProjFromGrid(tVec);
	
	for(var n = 0; n < array_length(tArr); n += 1)
	{
		var tProj = tArr[n];
		if(ProjTeamCheck(tProj, tInst) == true)
		{
			tProj.dmg_CollideCheck(tInst);
		}
	}
	
	for(var n = 0; n < array_length(global.coreProjArr); n += 1)
	{
		var tProj = global.coreProjArr[n];
		if(ProjTeamCheck(tProj, tInst) == true)
		{	
			tProj.dmg_CollideCheck(tInst);
		}
	}	
}

for(var i = 0; i < array_length(global.counterProjArr); i += 1)
{
	var tInst = global.counterProjArr[i]
	if(tInst.dmg_canDamage)
	{
		var tVec = new Vector2(floor(tInst.x/projGridSize), floor(tInst.y/projGridSize));
		var tArr = PullProjFromGrid(tVec);
	
		for(var n = 0; n < array_length(tArr); n += 1)
		{
			var tProj = tArr[n];
			if(ProjTeamCheck(tProj, tInst) == true)
			{
				ProjoCollideCheck(tProj, tInst);
			}
		}		
	}
}
