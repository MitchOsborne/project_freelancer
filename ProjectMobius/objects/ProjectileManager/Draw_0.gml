/// @description Insert description here
// You can write your code in this editor

for(var ix = 0; ix < array_length(PlayerRendArr); ix += 1)
{
	
	var tCol = PlayerRendArr[ix][0].owner.ownerColor;
	DrawProjectileSurface(PlayerRendArr[ix], tCol, projSurf);
}
//DrawProjectileSurface(P1RendArr, c_player1Blue, projSurf);
//DrawProjectileSurface(P2RendArr, c_player2Yellow, projSurf);
DrawProjectileSurface(AllyRendArr, c_allyGreen, projSurf);
DrawProjectileSurface(EnmRendArr, c_enemyRed, projSurf);

if(global.debug == DEBUG_MODES.PROJECTILES)
{
	for(var ix = 0; ix < array_length(global.projGrid); ix += 1)
	{
		for(var iy = 0; iy < array_length(global.projGrid[ix]); iy += 1)
		{
			var c_col = c_yellow;
			if(array_length(global.projGrid[ix][iy]) > 0) c_col = c_red;
			draw_rectangle_color(ix*projGridSize, iy*projGridSize, (ix+1)*projGridSize, (iy+1)*projGridSize,c_col, c_aqua, c_col, c_aqua, false);
			
		}
	}
}

