/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

itemStock = 1;
soldSlot = choose("Weapon1", "Weapon2");
soldItem = choose(wep_MP5, wep_AK, wep_SPAS12, wep_Storm,wep_Drake, 
wep_Stryker, wep_Katana, wep_LightSaberL, wep_BurstRifle, wep_Skorpion, wep_SCAR, wep_HuntingRifle, wep_P90);

var tItem = new soldItem();
itemCost = tItem.ablCost*(1+(global.enemyIntensity*2));
itemName = tItem.ablName;


delete(tItem);
face_icon = spr_gunShop_face;