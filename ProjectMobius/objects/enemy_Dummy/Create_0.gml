/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();

baseSpeed = 1;
moveSpeed = baseSpeed;


char_baseDamage = 1;
char_damageLvl = 0;
char_damagePerLvl = 1;

char_baseHealth = 1;
char_healthLvl = 1;
char_healthPerLvl = char_StatArch.HP;
char_maxHP = 1;

char_AC.ac_bpLd.ld_slots.ld_wep1  = new PKG_SinglePack(wep_DummyGun);

teamID = TEAMID.Enemy;
animator = anim_Dummy;