/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();
intr_weight = 5;
intr_holdTime = 0.5;
func_OnInteract = function()
{
	global.spawnEnm = true;
	global.alertMode = ALERT_STATE.Danger;
	array_push(global.spawnPool, choose(enemy_Yaki, enemy_Yaki_Gun));
	room_SpawnEnemies();
}