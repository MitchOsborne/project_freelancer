/// @description Insert description here
// You can write your code in this editor
event_inherited();

debugColour = c_aqua;

collisionClass = global.AgentCollisionClass;
loco_hitList = ds_list_create();
loco_tempList = ds_list_create();

lastX = x;
lastY = y;

velX = 0;
velY = 0;
velLerp = 0.2;
