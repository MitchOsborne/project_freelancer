/// @description Insert description here
// You can write your code in this editor


var surf_width = global.window_width/2;
var divrnd = global.window_height/global.view_height;
var surf_height = global.view_height*divrnd;


if(global.gameRunning)
{
	if(surface_exists(global.lightSurface))
	{
		if(global.lt_ambAlpha > 0)
		{
			var sC = 0.5;	//Surface Scale
			var tVec = new Vector2(camera_get_view_width(view_camera[0])*sC,camera_get_view_height(view_camera[0])*sC);
		
			surface_resize(global.lightSurface, tVec.x, tVec.y);
			surface_set_target(global.lightSurface);
			draw_clear_alpha(c_white, 0);
		
			draw_set_color(global.lt_ambColour);
			draw_set_alpha(global.lt_ambAlpha);
			draw_rectangle(0,0,tVec.x, tVec.y,false);


			if(global.lt_lightStr >= 0.1)
			{
				gpu_set_blendmode(bm_max);
				for(var i = 0; i < array_length(LightManager.lt_LightArr); i += 1)
				{	
					var tLight = LightManager.lt_LightArr[i];
					with(tLight) {
				
						var tO = new Vector2((CameraMarker.x - tVec.x), (CameraMarker.y - tVec.y));
						var tCol = merge_color(lt_colour, global.lt_ambColour, 0.25);
						
						draw_sprite_ext(lt_sprite,-1,(x - tO.x)*sC, (y - tO.y)*sC,(lt_scaleVec.x*sC)/1.25,(lt_scaleVec.y*sC)/1.25,lt_rotation,lt_colour,0.5);
						draw_sprite_ext(lt_sprite,-1,(x - tO.x)*sC, (y - tO.y)*sC,(lt_scaleVec.x*sC),(lt_scaleVec.y*sC),lt_rotation,tCol,1);
					}
				}
			}
		
			draw_set_alpha(1);
			gpu_set_blendmode(bm_normal);
		
		
			surface_reset_target();
		
		
			shader_set(shdr_lighting);
		
			shader_set_uniform_f(global.shdr_lightStr, global.lt_lightStr);
		
			var tex = surface_get_texture(global.lightSurface);
			var handle = shader_get_sampler_index(shdr_lighting,"lighting");
			texture_set_stage(handle,tex);
		
			tex = surface_get_texture(global.game_Surface);
			handle = shader_get_sampler_index(shdr_lighting,"gameview");
			texture_set_stage(handle,tex);
		}
		
		draw_surface_stretched(global.game_Surface, global.window_width/4,0,surf_width, surf_height);
		shader_reset();
		
		//draw_surface(global.lightSurface, 400,400);
			
	}
	//else draw_surface_stretched(global.game_Surface, global.window_width/4,0,surf_width, surf_height);


	if(global.radarEnabled)
	{
		surface_copy_part(radarRendSurf, 0,0, global.radar_Surface, x-(global.view_width*0.5), y-(global.view_height*0.5), global.view_width, global.view_height);
		draw_surface_stretched(radarRendSurf, global.radar_Pos.x, global.radar_Pos.y, global.radar_Size.x, global.radar_Size.y);
	}
}