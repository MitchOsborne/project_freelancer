/// @description Insert description here
// You can write your code in this editor
if(surface_exists(global.game_Surface) == false)
{
	global.game_Surface = surface_create(global.cam_width*global.resolution_Scale, global.cam_height*global.resolution_Scale);
}

if(surface_exists(global.radar_Surface) == false)
{
	global.radar_Surface = surface_create(room_width, room_height);
}

if(surface_exists(radarRendSurf) == false)
{
	radarRendSurf = surface_create(global.view_width, global.view_height);
}
	application_surface_draw_enable(false);
	
if(surface_exists(global.game_Surface))
{
	view_set_surface_id(view_camera[0],global.game_Surface);
}

if(global.radarEnabled)
{
	radarFlicker -= GetFixedDeltaTime();
	surface_set_target(radarRendSurf);
	draw_rectangle_color(0,0,room_width, room_height, c_black, c_black, c_black, c_black, false);
	surface_reset_target();
	surface_set_target(global.radar_Surface);	
	draw_rectangle_color(0,0,room_width, room_height, c_black, c_black, c_black, c_black, false);
	
	var tGrid = layer_tilemap_get_id("Tiles_Physics");
	for(var ix = 0; ix < tilemap_get_width(tGrid); ix += 1)
	{
		for(var iy = 0; iy < tilemap_get_height(tGrid); iy += 1)
		{
			var tTile = tilemap_get(tGrid, ix, iy);
			if(tTile != 0)
			{
				var tVec = new Vector2(global.Player1Char.x/global.gridSize, global.Player1Char.y/global.gridSize);
				var tVal = sin(radarFlicker + point_distance(tVec.x, tVec.y, ix, iy)*0.1);
				tVal = Round_Ext(tVal, 0.3);
				draw_set_alpha(tVal);
				var tVec = new Vector2(ix*global.gridSize, iy*global.gridSize);
				//draw_sprite(spr_Region, 0, tVec.x, tVec.y)
				draw_rectangle_color(tVec.x, tVec.y, tVec.x+(global.gridSize), tVec.y+(global.gridSize), c_lime, c_lime, c_lime, c_lime, false);
				
			}
		}
	}
	
	draw_set_alpha(sin(radarFlicker));
	for(var i = 0; i < instance_number(base_PhysObstacle); i += 1)
	{
		var tI = instance_find(base_PhysObstacle, i);
		
		draw_rectangle_color(tI.bbox_left, tI.bbox_top, tI.bbox_right, tI.bbox_bottom, c_lime, c_lime, c_lime, c_lime, true);
	}
	
	for(var i = 0; i < ds_list_size(global.enmList); i += 1)
	{
		
		var tEnm = global.enmList[|i];
		
		var tPos = new Vector2(tEnm.x, tEnm.y);
		DrawCone(tPos, tEnm.direction+90, tEnm.sightRadius, tEnm.sightAngle, 8, c_red, 0.5, false);
		draw_rectangle_color(tEnm.locoBBoxTL.x, tEnm.locoBBoxTL.y, tEnm.locoBBoxBR.x, tEnm.locoBBoxBR.y, tEnm.ownerColor, tEnm.ownerColor, tEnm.ownerColor, tEnm.ownerColor, false);
	}
	
	for(var i = 0; i < ds_list_size(global.allyList); i += 1)
	{
		var tAlly = global.allyList[|i];
		draw_rectangle_color(tAlly.locoBBoxTL.x, tAlly.locoBBoxTL.y, tAlly.locoBBoxBR.x, tAlly.locoBBoxBR.y, tAlly.ownerColor, tAlly.ownerColor, tAlly.ownerColor, tAlly.ownerColor, false);
	}
	surface_reset_target();
	draw_set_alpha(1);
}