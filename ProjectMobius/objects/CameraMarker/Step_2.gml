/*var P1 = undefined;
var P2 = undefined;
if(global.Player1ID != undefined) P1 = global.Player1ID.slavedObj;
if(global.Player2ID != undefined) P2 = global.Player2ID.slavedObj;
*/
var camPos = new Vector2(-1,-1);
/*
if(CheckExistance(P1) && CheckExistance(P2))
{
	camPos = new Vector2(P1.x, P1.y);
	if(global.P2Enabled)
	{
		camPos.x -= (camPos.x - P2.x)/2;
		camPos.y -= (camPos.y - P2.y)/2;
	}else
	{
		camPos.x -= (camPos.x - P1.active_operator.aimReticle.x)/2;	
		camPos.y -= (camPos.y - P1.active_operator.aimReticle.y)/2;	
	}
}else
*/
{
	for(var i = 0; i < ds_list_size(targetList); i += 1)
	{
		var tChar = targetList[|i];
		
		if(camPos.x == -1 && camPos.y == -1) 
		{
			camPos = new Vector2(tChar.x, tChar.y);
		}else
		{
			camPos.x -= (camPos.x - tChar.x)*0.5;
			camPos.y -= (camPos.y - tChar.y)*0.5;
		}
	}
}
	
	var tX = camPos.x;
	var tY = camPos.y;

	var rHeight = room_height;
	var rWidth = room_width;
	var vHeight = camera_get_view_height(view_camera[0]);
	var vWidth = camera_get_view_width(view_camera[0]);

	var camLock = true;
	/*
	if(global.P2Enabled && CheckExistance(P1) && CheckExistance(P2))
	{
		var tDist = point_distance(P1.x, P1.y, P2.x, P2.y);
		var camDist = round((global.view_width + global.view_height)/3);
		var camZoom = round(tDist - camDist);
		
		if(tDist > camDist)	//Lerp the zoom to the appropriate level to allow both targets to be visible
		{
			camLock = false;
			global.currZoom = lerp(global.currZoom, camZoom, lerpSpeed);
			
			if((global.currZoom - camZoom) < 1) global.currZoom = camZoom;	//Helps the camera snap to its target position quickly to avoid sub-pixel rendering
			
		}else	//Lerp the camera back to standard size if under the minimum camDist to zoom
		{
			global.currZoom = lerp(global.currZoom, 1, lerpSpeed);
			if((global.currZoom -1) < 1) global.currZoom = 1;		//Helps the camera snap to its target position quickly to avoid sub-pixel rendering
		}
		global.currZoom = clamp(global.currZoom, 1, camDist);	//Stop the camera from zooming beyond the maximum camdist
		var viewScale = global.view_height/global.view_width;
		camera_set_view_size(view_camera[0], global.view_width + global.currZoom, global.view_height + (global.currZoom * viewScale));
	}
	*/
	
	if(camLock)
	{
		tX = clamp(tX,vWidth/2,rWidth-(vWidth/2));
		tY = clamp(tY,vHeight/2,rHeight - (vHeight/2));
	}
	
	
	if(global.camShakeStr > 0)
	{
		global.camShakeStr -= GetFixedDeltaTime() * camMaxShake;
		global.camShakeStr = clamp(floor(global.camShakeStr), 0, camMaxShake);
		tX += choose(-global.camShakeStr, global.camShakeStr);
		tY += choose(-global.camShakeStr, global.camShakeStr);
	}
	x = lerp(x,round(tX),lerpSpeed);
	y = lerp(y,round(tY),lerpSpeed);
	
	camera_set_view_pos(view_camera[0],round(x-(vWidth/2)),round(y - (vHeight/2)));
	camSize = new Vector2(camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]));
	if(surface_exists(global.game_Surface))
	{
		var tBlur = clamp(2 * global.camBlurScale, 0.1, 2);
		
		surface_resize(global.game_Surface,camSize.x*tBlur, camSize.y*tBlur);
	}
	