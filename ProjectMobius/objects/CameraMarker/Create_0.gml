/// @description Insert description here
// You can write your code in this editor
targetList = ds_list_create();
targetRes = 0.5;
targetTmr = targetRes;

override = false;
overrideTarget = undefined;

lerpSpeed = 0.05;
global.currZoom = 1;
global.camBlurScale = 1;

lt_flickerTmr = -1;
lt_flickerStr = 2;



global.camShakeStr = 1;
camMaxShake = 50;

camSize = new Vector2(0,0);
debugSpr = spr_tCamera;

window_set_size(global.window_width,global.window_height);
radarRendSurf = -1;
radarFlicker = 0;