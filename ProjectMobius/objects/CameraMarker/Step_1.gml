/// @description Insert description here
// You can write your code in this editor

if(global.alertMode >= 1) global.radarEnabled = false;
else global.radarEnabled = false;


targetTmr += GetDeltaTime(global.timeScale);

ds_list_clear(targetList);
if(override) ds_list_add(targetList, overrideTarget);
else
{
	for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
	{
		var tChar = global.PlayerIDList[|i].slavedObj;
		if(tChar.isIncap == false)
		{
			ds_list_add(targetList, tChar);
			if(tChar.PlayerControlled && (tChar.active_operator.aimReticle != undefined))
				ds_list_add(targetList, tChar.active_operator.aimReticle);
		}
	}
}	


if(global.debug) sprite_index = spr_tCamera;
else sprite_index = spr_None;