/// @description Initialisation of Stuff
// You can write your code in this editor
event_inherited();


char_StatArch = global.statArch_Runner;
ai_combatPriority = 2;

ai_defaultOperator = ai_AdvRogueAgent;

ai_canEvade = true;

baseSpeed = 3.2;
moveSpeed = baseSpeed;

char_hasLight = false;

char_AC.ac_bpLd.ld_slots.ld_wep1 = global.bp_Abilities.bp_NezumiBlade;
char_AC.ac_bpLd.ld_slots.ld_wep2 = global.bp_Abilities.bp_M9T;

teamID = TEAMID.Enemy;
animator = InitAnimator(anim_NezumiRogue, id);