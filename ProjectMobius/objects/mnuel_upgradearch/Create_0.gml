/// @description Insert description here
// You can write your code in this editor


// Inherit the parent event
event_inherited();

uiEl_AblArch = undefined;
uiEl_UpgradeCost = -1;
uiEl_ArchText = "Upgrade Licence: ";

uiEl_tooltipTxt = "";

uiEl_Function = function()
{
	if(uiEl_AblArch != undefined)
	{
		uiEl_UpgradeCost = uiEl_AblArch.arch_Level * 100;
		if(global.credits >= uiEl_UpgradeCost)
		{
			uiEl_AblArch.arch_Level += 1;
			global.credits -= uiEl_UpgradeCost;
			uiEl_UpgradeCost = uiEl_AblArch.arch_Level*100;
			audio_play_sound(sfx_lvlup, 5, false);
		}
	}
}

