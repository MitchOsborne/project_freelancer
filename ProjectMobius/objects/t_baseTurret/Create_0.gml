/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

owner = undefined;

baseSpeed = 0;
moveSpeed = baseSpeed;


char_baseDamage = 10;
char_damageLvl = 1;
char_damagePerLvl = 2;
char_damageStat = 12;

char_baseHealth = 0;
char_healthLvl = 0;
char_healthPerLvl = 0;

baseAtkSpd = 0;
atkSpdLvl = 0;
atkSpdPerLvl = 0;
atkSpdStat = 0;

baseSightRadius = 160;