/// @description Insert description here
// You can write your code in this editor


if(uiEl_parentMenu == global.activeUI && uiEl_parentMenu.ui_hasInit && !uiEl_parentMenu.ui_animOpen)
{
	var tC = c_white;
	if(uiEl_isSelected) tC = c_teal;
	if(uiEl_sprite != spr_None) draw_sprite_ext(uiEl_sprite, -1, x, y, 1, 1, 0, tC, 1);
	draw_set_color(tC);
	draw_text_ext(x, y, uiEl_text, font_get_size(0)+2, uiEl_parentMenu.ui_opXSpacing);
	if(uiEl_isSelected) draw_text(global.activeUI.x, global.activeUI.y - 20, uiEl_tooltipTxt);
	draw_set_color(c_white);
}