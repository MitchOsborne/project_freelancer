/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

trap_triggerTime = 2;

trap_LockRes = 0.1;
trap_LockTmr = 0;

trap_Target = undefined;

trap_triggerFunc = function()
{	
	if(trap_Target != undefined)
	{
		CreateLiteProjectile(dmg_LaserBolt, Dummy.DummyWep, Dummy.char_damageStat, new Vector2(x,y), point_direction(x,y,trap_Target.x, trap_Target.y)-90, undefined);	
	}
	trap_Target = undefined;
}