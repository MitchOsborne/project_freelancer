/// @description Insert description here
// You can write your code in this editor

trap_LockTmr += GetDeltaTime(1);

if(trap_LockTmr >= trap_LockRes)
{
	trap_LockTmr = 0;
	var tTarg = undefined;
	for(var i = 0; i < ds_list_size(global.allyList); i += 1)
	{
		
		var tAlly = global.allyList[|i];
		if((Collision_Line_Tilemap(id, tAlly, global.ProjectileTileCollisionClass) == false &&
		Collision_Line_Obj(id, tAlly, global.ProjectileCollisionClass) == false))
		{
			if(tTarg == undefined) tTarg = new Vector2(tAlly.x, tAlly.y);
			else if (point_distance(x,y,tTarg.x, tTarg.y) > point_distance(x,y,tAlly.x,tAlly.y)) tTarg = new Vector2(tAlly.x, tAlly.y);
		}
	}
	trap_Target = tTarg;
}


