// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

/*
function PKG_EquipLoadout() : base_AbilityPackage() constructor
{
	AbilityType = ABILITY_TYPE.PACKAGE;
	pkg_Ability = abl_ResetLoadout;
	PackageType = PACKAGE_TYPE.DOUBLE_EXCLUSIVE;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.CHARGE;
	pkg_Icon = spr_Nanolathe;
}



function PKG_EquipMinigun() : PKG_EquipLoadout() constructor
{
	pkg_AltAbility = abl_EquipMinigun;
	pkg_Name = "Equip Minigun";
	pkg_DefaultSlot = "Ability1";
}

function PKG_EquipRPG() : PKG_EquipLoadout() constructor
{
	pkg_AltAbility = abl_EquipRPG;
	pkg_Name = "Equip RPG";
	pkg_DefaultSlot = "Ability2";
}


function PKG_EquipRatPack() : PKG_EquipLoadout() constructor
{
	pkg_AltAbility = abl_EquipRatPack;
	pkg_Name = "Equip RatPack";
	pkg_DefaultSlot = "Ability2";
}


function PKG_Scattershot() : base_AbilityPackage() constructor
{
	pkg_Ability = wep_ScatterBolt;
	pkg_AltAbility = wep_ScatterBoltAlt;
	PackageType = PACKAGE_TYPE.DOUBLE_INV;
	pkg_Icon = spr_DBShotgun;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.AMMO;
	pkg_DefaultSlot = "Weapon1";
}

function PKG_SinglePack(a_wep) : base_AbilityPackage() constructor
{
	pkg_Ability = new a_wep();
	pkg_AltAbility = undefined;
	pkg_Icon = abl_Icon;
	pkg_Name = ablName;
	if(AbilityType == ABILITY_TYPE.MELEE)
	{
		pkg_outputType = PACKAGE_OUTPUT_TYPE.STAMINA;
	}else
	{
		pkg_outputType = PACKAGE_OUTPUT_TYPE.AMMO;	
	}
	pkg_DefaultSlot = "Weapon1";
}

function PKG_DBShotgun() : base_AbilityPackage() constructor
{
	pkg_Ability = new wep_SBShotgun();
	pkg_AltAbility = new wep_SBShotgun();
	PackageType = PACKAGE_TYPE.DOUBLE_INV;
	pkg_Icon = abl_Icon;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.AMMO;
	pkg_DefaultSlot = "Weapon1";
}

function PKG_TwinSaber() : base_AbilityPackage() constructor
{
	pkg_Ability = new wep_LightSaberL();
	pkg_AltAbility = new wep_LightSaberR();
	PackageType = PACKAGE_TYPE.DOUBLE_INV;
	pkg_Icon = abl_Icon;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.STAMINA;
	pkg_DefaultSlot = "Weapon1";
}

function PKG_PsiWave() : base_AbilityPackage() constructor
{
	pkg_Ability = new wep_PsiWave();
	pkg_AltAbility = undefined;
	pkg_Icon = abl_Icon;
	pkg_Name = "Psi-Wave";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Utility2";
}

function PKG_BladeWave() : base_AbilityPackage() constructor
{
	pkg_Ability = new wep_BladeWave();
	pkg_AltAbility = undefined;
	pkg_Icon = spr_IconBladeWave;
	pkg_Name = "Blade Wave";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Core";
}

function PKG_SummonMech() : base_AbilityPackage() constructor
{
	pkg_Ability = smn_SummonYVRN;
	PackageType = PACKAGE_TYPE.SINGLE;
	pkg_Icon = spr_SummonAlly;
	pkg_Name = "Summon Y.V.R.N";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Core";
}

function PKG_ArmourRepair() : base_AbilityPackage() constructor
{
	pkg_Ability = abl_ArmourRepair;
	PackageType = PACKAGE_TYPE.SINGLE;
	pkg_Icon = spr_Repair;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Utility2";
}

function PKG_Empty() : base_AbilityPackage() constructor
{
	pkg_Icon = spr_Locked;
}

function PKG_Evade() : base_AbilityPackage() constructor
{
	pkg_Ability = evade_Dash;
	pkg_AltAbility = abl_Sprint;
	PackageType = PACKAGE_TYPE.DOUBLE;
	pkg_Icon = spr_Evade;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.STAMINA;
	pkg_DefaultSlot = "Utility1";
}

function PKG_MechSprint() : base_AbilityPackage() constructor
{
	pkg_Ability = abl_Sprint;
	pkg_Icon = spr_Evade;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.STAMINA;
	pkg_DefaultSlot = "Utility1";
}

function PKG_KatanaFinisher() : base_AbilityPackage() constructor
{
	pkg_Ability = wep_KatanaFinisher;
	pkg_Icon = spr_IconFinisher;
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_Name = "Finishing Blow";
	pkg_DefaultSlot = "Ability2";
}
function PKG_TripleSlash() : base_AbilityPackage() constructor
{
	pkg_Ability = wep_TripleSlash;
	pkg_Icon = spr_IconSpinSlash;
	pkg_Name = "Blade Dance";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.AMMO;
	pkg_DefaultSlot = "Utility2";
}


function PKG_PsiThrow() : base_AbilityPackage() constructor
{
	pkg_Ability = abl_PsiGlobalThrow;
	PackageType = PACKAGE_TYPE.SINGLE;
	pkg_Icon = spr_Psionics;
	pkg_Name = "Psi-Throw";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Ability1";
}

function PKG_MassControl() : base_AbilityPackage() constructor
{
	pkg_Ability = abl_PsiTagRadius;
	PackageType = PACKAGE_TYPE.SINGLE;
	pkg_Icon = spr_PsiUlt;
	pkg_Name = "Mass Control";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Core";
}

function PKG_PsiShot() : base_AbilityPackage() constructor
{
	pkg_Ability = abl_PsiSpawnProj;
	PackageType = PACKAGE_TYPE.PASSIVE;
	pkg_Icon = spr_Psishot;
	pkg_Name = "Psi-Bolts";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_DefaultSlot = "Ability2";
}
*/