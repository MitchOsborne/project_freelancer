// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_SequenceOperator() : base_Operator() constructor{
	seqOp_Object = undefined;
		
	func_Init = SeqOp_Init;
	func_PreUpdateStep = SeqOp_PreUpdateStep;
	func_UpdateStep = SeqOp_UpdateStep;
	func_EndUpdateStep = SeqOp_EndUpdateStep;
	func_DrawStep = SeqOp_DrawStep;
}

function SeqOp_Init(a_SeqOp)
{
	with a_SeqOp
	{
		Oper_Init(self);
	}
}


function SeqOp_PreUpdateStep(a_SeqOp)
{
	with a_SeqOp
	{
		Oper_PreUpdateStep(self);
	}
}

function SeqOp_UpdateStep(a_SeqOp)
{
	with a_SeqOp
	{
		Oper_UpdateStep(self);
		
		if(seqOp_Object != undefined)
		{
			var tDist = point_distance(slavedObj.x, slavedObj.y, seqOp_Object.x, seqOp_Object.y);
			if(tDist >= 8 || tDist < 1)	//Snap the Character to the Event Object if close enough or too far away
			{
				slavedObj.x = seqOp_Object.x;
				slavedObj.y = seqOp_Object.y;
			}else if(tDist >= 1)
			{
				var tMovDir = GetCoord(point_direction(slavedObj.x, slavedObj.y, seqOp_Object.x, seqOp_Object.y));
				var tSpd = 1;
				if(tDist < 4)tSpd = InvLerp(1, 4, tDist);
				slavedObj.mx = tMovDir.x * tSpd;
				slavedObj.my = tMovDir.y * tSpd;
			}
		}
	}
}

function SeqOp_EndUpdateStep(a_SeqOp)
{
	with a_SeqOp
	{
		Oper_EndUpdateStep(self);
	}
}

function SeqOp_DrawStep(a_SeqOp)
{
	with a_SeqOp
	{
		Oper_DrawStep(self);
	}
}