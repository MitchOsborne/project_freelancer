// Auto-generated stubs for each available event.

function SpawnShockwave()
{
	var obj = instance_find(enemy_Flamer,0);
	if(!obj.isStunned)
	{
		CreateLiteProjectile(base_LiteWave, Dummy.DummyWep, Dummy.char_damageStat, obj, 0, Dummy.DummyWep.abl_bpStats);
	}
}

function SpawnConeAoE()
{
	var obj = instance_find(enemy_Flamer,0);
	CreateLiteProjectile(dmg_FlamerCone, Dummy.DummyWep, Dummy.char_damageStat, obj, obj.direction, Dummy.DummyWep.abl_bpStats);
	var tCoord = GetCoord(obj.direction + 60);
	obj.in_lx = tCoord.x;
	obj.in_ly = tCoord.y;
}

function RotateFlamer()
{
	
}