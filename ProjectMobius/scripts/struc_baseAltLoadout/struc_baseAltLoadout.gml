// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function defunc_base_AltLoadout() constructor
{
	
	 
	
	modsToApply = [];
	
	alt_wep1  = undefined;
	alt_wep2  = undefined;
	alt_util1 = undefined;
	alt_util2 = undefined;
	alt_abl1  = undefined;
	alt_abl2  = undefined;
	alt_core  = undefined;
	
	func_UpdateStep = AltLoadoutUpdateStep;
	func_InitAbility = AltLoadoutInit;
	ldt_ActivateAbility = function(a_abl){};
	ldt_Duration = -1;
	ldt_Timer = 0;
}

function AltLoadoutInit(a_abl)
{
	with a_abl
	{
		//InitAbilityPackage(alt_wep1, owner);
		//InitAbilityPackage(alt_wep2, owner);
		//InitAbilityPackage(alt_util1, owner);
		//InitAbilityPackage(alt_util2, owner);
		//InitAbilityPackage(alt_abl1, owner);
		//InitAbilityPackage(alt_abl2, owner);
		//InitAbilityPackage(alt_core, owner);
	}
}

function DiscreetLoadoutUpdate(a_abl)
{
	with a_abl
	{
		if(ldt_Duration > 0)
		{
			ldt_Timer += GetDeltaTime(timeScale);
			if(ldt_Timer >= ldt_Duration)
			{
				DeactivateAltLoadout(self);
			}
		}
		
		for(var i = 0; i < array_length(modsToApply); i += 1)
		{
			AttachMod(modsToApply[i], owner, owner, 0.1);	
		}
	}
}

function AltLoadoutUpdateStep(a_abl)
{
	with a_abl
	{
		AbilityUpdateStep(self);
		if(activate)
		{
			didActivate = true;
			script_execute(ldt_ActivateAbility, self);
		}
		
		if(!owner.char_RunAltLoadout)
		{
			if(alt_wep1 != undefined) Update_AbilityPackage(alt_wep1);
			if(alt_wep2 != undefined) Update_AbilityPackage(alt_wep2);
			if(alt_util1 != undefined) Update_AbilityPackage(alt_util1);
			if(alt_util2 != undefined) Update_AbilityPackage(alt_util2);
			if(alt_abl1 != undefined) Update_AbilityPackage(alt_abl1);
			if(alt_abl2!= undefined) Update_AbilityPackage(alt_abl2);
			if(alt_core != undefined) Update_AbilityPackage(alt_core);
			
		}
	}
}

function ActivateAltLoadout(a_abl)
{
	with a_abl
	{
		ClearAltLoadout(owner);
		owner.char_altld.ld_slots.ld_wep1	= alt_wep1;
		owner.char_altld.ld_slots.ld_wep2	= alt_wep2;
		owner.char_altld.ld_slots.ld_util1	= alt_util1;
		owner.char_altld.ld_slots.ld_util2	= alt_util2;
		owner.char_altld.ld_slots.ld_abl1	= alt_abl1;
		owner.char_altld.ld_slots.ld_abl2	= alt_abl2;
		owner.char_altld.ld_slots.ld_core	= alt_core;
		owner.char_RunAltLoadout = true;
		owner.char_AC.ac_activeLoadout = self;
		RefreshLoadout(owner);
		SanityCheckLoadout(owner);
		
	}
}

function DeactivateAltLoadout(a_abl)
{
	with a_abl
	{
		ClearAltLoadout(owner);
		owner.char_RunAltLoadout = false;
		owner.char_AC.ac_activeLoadout = undefined;
		RefreshLoadout(owner);
		SanityCheckLoadout(owner);
	}
}


