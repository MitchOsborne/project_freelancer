// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.sv_LoadGame = false;

global.sv_Player1Char = char_Kamui;

global.sv_PlayerLvl = 5;

global.sv_StatInv = [new Stat_EmptySlot()];

global.sv_Player1Data = 
{
	sv_charID : -1,
	sv_wep1 : undefined,
	sv_wep2 : undefined,
	sv_util1 : undefined,
	sv_util2 : undefined,
	sv_abl1 : undefined,
	sv_abl2 : undefined,
	sv_core : undefined,
}

global.sv_Player2Data = 
{
	sv_charID : -1,
	sv_wep1 : undefined,
	sv_wep2 : undefined,
	sv_util1 : undefined,
	sv_util2 : undefined,
	sv_abl1 : undefined,
	sv_abl2 : undefined,
	sv_core : undefined,
}