function CreateNavGrids(a_gridSize) {

	if(global.walkNavGrid != undefined)mp_grid_destroy(global.walkNavGrid);
	if(global.crawlNavGrid != undefined)mp_grid_destroy(global.crawlNavGrid);
	//if(global.flyNavGrid != undefined)mp_grid_destroy(global.flyNavGrid);

	global.walkNavGrid = mp_grid_create(0, 0, room_width / a_gridSize, room_height /a_gridSize, a_gridSize, a_gridSize);
	global.crawlNavGrid = mp_grid_create(0, 0, room_width / a_gridSize, room_height /a_gridSize, a_gridSize, a_gridSize);
	
	for(var i = 0; i < array_length(global.AgentCollisionClass); i += 1)
	{
		mp_grid_add_instances(global.walkNavGrid, global.AgentCollisionClass[i], true);	
		mp_grid_add_instances(global.crawlNavGrid, global.AgentCollisionClass[i], true);	
	}
	
	for(var iX = 0; iX < floor(room_width/global.gridSize); iX += 1)
	{
		for(var iY = 0; iY < floor(room_height/global.gridSize); iY += 1)
		{
			var tm = layer_tilemap_get_id("Tiles_Physics");
			var tTileCell = tilemap_get(tm, iX, iY);
			if(tTileCell != 0)
			{
				mp_grid_add_cell(global.walkNavGrid, iX,iY);
				if(tTileCell != 3) mp_grid_add_cell(global.crawlNavGrid, iX,iY);
			}
		}
	}

}
