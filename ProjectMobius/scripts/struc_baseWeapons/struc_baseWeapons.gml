// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function base_Weapon(a_bp) : base_Ability(a_bp) constructor
{
	var st = abl_bpStats;
	ai_PrepTime = 0.5;
	ai_Score = 100;
	array_push(abl_Components, new abl_comp_OnPress(self));	
	array_push(abl_Components, new abl_comp_AIOutOfCombat(self));	//Component to prevent AI usage when out of combat
	array_push(abl_Components, new abl_comp_AITargetInRange(self));	//Component to prevent usage when target is out of range
	array_push(abl_Components, new abl_comp_AbilityLock(self));		//Prevents usage of ability whilst ability locked
	
	array_push(abl_Components, new abl_comp_Firerate(self));
	
	for(var i = 0; i < array_length(a_bp.bp_Components); i += 1)
	{
		var tComp = new a_bp.bp_Components[i](self);
		array_push(abl_Components, tComp);
	}
	
}


function base_Gun(a_bp) : base_Weapon(a_bp) constructor
{
	abl_isSilent = false;
	abl_posOffset = new Vector2(0,-2);
	
	array_push(abl_Components, new abl_comp_AttackData(self, a_bp.bp_AttackArr, a_bp.bp_AttackFin));
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,a_bp.bp_WepStats.bp_SpeedCurve);
}

function base_wep_Rifle(a_bp) : base_Gun(a_bp) constructor{

	wep_SlotType = SLOT_TYPE.GUN_MED;
	array_push(abl_Components, new abl_comp_DrawCasing(self, spr_BulletCasing));
	
	abl_forceStrength = 1;
	abl_stunDur = 0.1;
}

function base_wep_Sniper(a_bp) : base_Gun(a_bp) constructor{

	wep_SlotType = SLOT_TYPE.GUN_MED;
	
	array_push(abl_Components, new abl_comp_DrawCasing(self, spr_BulletCasing));
	
	abl_forceStrength = 5;
	abl_stunDur = 0.3;
}

function base_Grenade(a_bp) : base_Weapon(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.ABILITY1;
	
	array_push(abl_Components, new abl_comp_Attack(self, new atk_Grenade(a_bp.bp_WepStats.bp_ProjectileType, a_bp.bp_WepStats.bp_ProjectileCount)));
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed, a_bp.bp_WepStats.bp_SpeedCurve);		
}

function base_wep_Tranq(a_bp) : base_Gun(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Tranq(self));
	abl_forceStrength = 0;
	abl_stunDur = 0;
}

function base_wep_SpikeTrap(a_bp) : base_Grenade(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Dragnet(self));
}

function base_wep_HarpoonRifle(a_bp) : base_wep_Rifle(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Harpoon(self));	
}

function base_wep_DragnetRifle(a_bp) : base_wep_Shotgun(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Dragnet(self));
}

function base_power_GravLance(a_bp) : base_power_EnergyCharger(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_ChargeAttack(self, [atk_GravLance1, atk_GravLance2, atk_GravLance3, atk_GravLance4], 1));
}

function base_wep_Soulfire(a_bp) : base_Gun(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.ABILITY1;
	ai_Score = 110;
	array_push(abl_Components, new abl_comp_ReqRelease(self));
	
	abl_forceStrength = 1;
	abl_stunDur = 0.1;	
}

function base_wep_SMG(a_bp) : base_wep_Rifle(a_bp) constructor{

	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
	
	abl_forceStrength = 0.7;
	abl_stunDur = 0.1;
}

function base_wep_KnifeThrow(a_bp) : base_Gun(a_bp) constructor
{
	
	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
	
	abl_forceStrength = 1;
	abl_stunDur = 0.3;
}

function base_wep_Sidearm(a_bp) : base_Gun(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
		
	abl_forceStrength = 0.5;
	abl_stunDur = 0.1;	
}

function base_wep_Shotgun(a_bp) : base_Gun(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
	ai_Score = 110;
	array_push(abl_Components, new abl_comp_ReqRelease(self));
	array_push(abl_Components, new abl_comp_DrawCasing(self, spr_ShellCasing));
	abl_forceStrength = 1;
	abl_stunDur = 0.1;	
}

function base_wep_Flamer(a_bp) : base_Gun(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
	ai_Score = 110;
	array_push(abl_Components, new abl_comp_Attack(self, new atk_Flamer()));
	
	abl_forceStrength = 1;
	abl_stunDur = 0.1;		
}

function base_wep_Wave(a_bp) : base_Gun(a_bp) constructor
{
	wep_SlotType = SLOT_TYPE.GUN_LIGHT;
	
	array_push(abl_Components, new abl_comp_Attack(self, new atk_Wave()));
	array_push(abl_Components, new abl_comp_ReqRelease(self));
	
	abl_forceStrength = 1;
	abl_stunDur = 0.1;		
}


function base_power_EnergyCharger(a_bp) : base_Ability(a_bp) constructor
{
	var st = BP_CalcStats(a_bp);
	ai_Score = 120;
	ai_PrepTime = 0.5;
	wep_SlotType = SLOT_TYPE.ABILITY1;
	wep_parryType = PARRY_TYPE.NONE;
	
}

function base_wep_Melee(a_bp) : base_Ability(a_bp) constructor
{
	var st = abl_bpStats;
	ai_Score = 120;
	ai_PrepTime = 0.5;
	wep_SlotType = SLOT_TYPE.MELEE_MED;
	wep_parryType = PARRY_TYPE.REFLECT;
	array_push(abl_Components, new abl_comp_OnPress(self));	
	
	array_push(abl_Components, new abl_comp_Firerate(self));
	array_push(abl_Components, new abl_comp_ReqRelease(self));
	array_push(abl_Components, new abl_comp_AttackData(self, a_bp.bp_AttackArr, a_bp.bp_AttackFin));
	array_push(abl_Components, new abl_comp_AITargetInRange(self));	//Component to prevent usage when target is out of range
	array_push(abl_Components, new abl_comp_AbilityLock(self));		//Prevents usage of ability whilst ability locked
	
}

function base_wep_LungeBite(a_bp) : base_wep_Melee(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, debuff_slowed, 2, 8, true));
	array_push(abl_Components, new abl_comp_ExposedAttack(self));
}

function base_NezumiKnife(a_bp) : base_wep_Melee(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Tranq(self));	
}

function BP_CalcStats(a_bp)
{
	with a_bp
	{
		var tStat = new bp_WeaponStats();
		Copy_Struct(tStat,bp_WepStats);
		tStat.bp_Range *= bp_StatMods.bp_Range;
		tStat.bp_Spread *= bp_StatMods.bp_Spread;
		tStat.bp_DMGScale *= bp_StatMods.bp_DMGScale;
		tStat.bp_RPM *= bp_StatMods.bp_RPM;
		tStat.bp_Ammo *= bp_StatMods.bp_Ammo;
		tStat.bp_ReloadSpd *= bp_StatMods.bp_ReloadSpd;
		tStat.bp_SpeedCurve = animcurve_get_channel(crv_ProjectileSpeed,tStat.bp_SpeedCurve);
	}
	return tStat;
}