// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_Pickup() constructor{
	
	timeScale = 1;
	lifeTime = 8;
	lifeTimer = 0;
	blinkTime = 4;	//Divisible by 2 for clean animations
	
	pkup_animSpeed = 1;
	pkup_animIndex = 0;
	
	blinkVal = 0;
	
	trackRand =  random_range(-25,25);
	
	pkup_sprite = spr_error;
	pkup_rot = 0;
	
	pickupRange = 80;
	collectionRange = 16;
	
	isCollected = false;
	moveSpeed = 2;
	
	moveScale = 1;
	spawnLerpDecay = 0.05;
	
	pkup_sfx = undefined;
	
	moveVec = new Vector2(random_range(-1,1),random_range(-1,1));
	x = 0;
	y = 0;
	
	func_UpdateStep = Pickup_Update;
	func_EndUpdateStep = Pickup_EndUpdate;
	func_DrawStep = Pickup_Draw;
	func_OnPickup = Pickup_OnPickup;
}

function Pickup_OnPickup(a_pkup)
{
	with a_pkup
	{
		for(var i = 0; i < instance_number(base_Character); i += 1)
		{
			var tInst = instance_find(base_Character, i);
			if(tInst.teamID == TEAMID.Ally)
			{
				if(point_distance(x,y,tInst.x, tInst.y) <= collectionRange && (lifeTimer > 0.2 || lifeTimer > lifeTime/2))
				{
					isCollected = true;	
					if(pkup_sfx != undefined) audio_play_sound(pkup_sfx, 5, false);
				}
			}
		}
	}
}

function Pickup_Draw(a_pkup)
{
	with a_pkup
	{
		pkup_animSpeed = 1/sprite_get_speed(pkup_sprite);
		pkup_animIndex += pkup_animSpeed;
		if(pkup_animIndex > sprite_get_number(pkup_sprite)) pkup_animIndex = 0;
		draw_sprite_ext(pkup_sprite, floor(pkup_animIndex), x,y,1,1,pkup_rot,c_white, 1);
	}
}

function Pickup_Update(a_pkup)
{
	with a_pkup	
	{
		timeScale = global.timeScale;		
		var closest = undefined;
		var tDist = 0;
		for(var i = 0; i < instance_number(base_Character); i += 1)
		{
			var tP = instance_find(base_Character,i);
			if(tP.teamID == TEAMID.Ally)
			{
				tDist = point_distance(x,y,tP.x,tP.y);
				if(closest == undefined) closest = tP;
				else if (point_distance(x,y,closest.x,closest.y) > tDist)
				{
					closest = tP;
				}
			}
		
		}
		if(closest != undefined)
		{
			tDist =	point_distance(x,y,closest.x,closest.y);
			if(tDist <= pickupRange)
			{
				var coord = GetCoord(point_direction(x,y,closest.x,closest.y) + (90 + trackRand));
				moveVec.x = lerp(moveVec.x, coord.x, 0.2);
				moveVec.y = lerp(moveVec.y, coord.y, 0.2);
				moveScale = 1-(tDist/pickupRange);
			}else
			{
				moveScale = lerp(moveScale,0,spawnLerpDecay);	
			}
		}
		
		x -= moveVec.x * ((moveSpeed)*moveScale)*timeScale;
		y -= moveVec.y * ((moveSpeed)*moveScale)*timeScale;
		
		
		
		lifeTimer += GetDeltaTime(timeScale);
		if(lifeTimer >= (lifeTime-blinkTime))
		{
			blinkVal += GetDeltaTime(timeScale*10);
			if(blinkVal >= (pi * 2)) blinkVal = 0;
			
			image_alpha = 1 - sin(blinkVal);
		}	
	}
}

function Pickup_EndUpdate(a_pkup)
{
	with a_pkup
	{
		if(lifeTimer > lifeTime)
		{
			//ds_list_delete(global.pkup_List, ds_list_find_index(global.pkup_List, self));
			//delete(self);	
		}	
	}
}