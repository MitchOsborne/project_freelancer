// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SpawnHPPickups(){
	var hpMissing = 0;
	var hpTotal = 0;
	var baseHPSpawnRate = 50;
	var tP = undefined;
	
	for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
	{
	
		tP = global.PlayerIDList[|i].slavedObj;
		hpMissing += tP.char_maxHP - tP.hp;
		hpTotal += tP.char_maxHP;
		
		if(CheckExistance(tP.smn_Minion))
		{
			var tMin = tP.smn_Minion;
			hpMissing += tMin.char_maxHP - tMin.hp;
			hpTotal += tMin.char_maxHP;
		}
	
	}
	
	var hpRate = (hpMissing/hpTotal);
	
	var spRoll = random_range(0,100);
	if(spRoll <= (baseHPSpawnRate*hpRate))
	{
		var tInst = new Pkup_HPBox();
		tInst.x = x;
		tInst.y = y;
		ds_list_add(global.pkup_List, tInst);	
	}
	
}

function ForceSpawnStatPickups(a_pos)
{
	var tInst = choose( new Pkup_ATKCore(), new Pkup_DEFCore(), 
							new Pkup_ABLCore(), new Pkup_LUCKCore(),
							new Pkup_RivenCore());
	if(a_pos != undefined)
	{
		tInst.x = a_pos.x;
		tInst.y = a_pos.y;
	}else
	{
		tInst.x = global.Player1Char.x;
		tInst.y = global.Player1Char.y;
	}
	ds_list_add(global.pkup_List, tInst);
}

function SpawnStatPickups(a_pos){
	var tRand = irandom_range(0,100);
	if(tRand <= 15)
	{
		ForceSpawnStatPickups(a_pos);
	}
}
