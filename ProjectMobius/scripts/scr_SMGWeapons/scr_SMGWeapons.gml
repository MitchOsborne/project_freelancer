// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function wep_MP5() : defunc_base_wep_SMG() constructor {
	ablCost = 300;
	ablName = "MP5"
	ammoMax = 25;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	
	range *= 1;
	damageMultiplier = 0.8;
	weaponSprite = spr_MP5;
	
	wep_projectileSpread *= 1;
	RPM = 850;
}

function wep_P90() : defunc_base_wep_SMG() constructor {
	ablCost = 750;
	ablName = "P90"
	ammoMax = 52;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	range *= 1;
	damageMultiplier = 0.8;
	weaponSprite = spr_P90;
	
	wep_projectileSpread *= 1.5;
	RPM = 1100;
}



