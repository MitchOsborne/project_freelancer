// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Draw_Self_Ext(a_obj)
{
	with a_obj
	{
		hitColTmr += GetDeltaTime(timeScale);
		if(hitColTmr >= hitColDur) hitCol = -1;
		if(hitCol != -1) DrawOwnerColor(hitCol);
		else if(ownerColor != c_None) DrawOwnerColor(ownerColor);
		draw_sprite_ext(sprite_index,-1,round(x), round(y),image_xscale,image_yscale,image_angle,hitCol,image_alpha);
		shader_reset();
	}
}


function DrawOutline(a_obj, a_color, a_width){
	with a_obj
	{
		var cRed = color_get_red(a_color)/255;
		var cGreen = color_get_green(a_color)/255;
		var cBlue = color_get_blue(a_color)/255;
		
		global.shaderData.shdr_texelH	= texture_get_texel_height(sprite_get_texture(sprite_index, 0));
		global.shaderData.shdr_texelW = texture_get_texel_width(sprite_get_texture(sprite_index, 0));
		
		shader_set(shdr_outline);
		shader_set_uniform_f(global.shaderData.shdr_pixelW, global.shaderData.shdr_texelW * a_width);
		shader_set_uniform_f(global.shaderData.shdr_pixelH, global.shaderData.shdr_texelH * a_width);
		shader_set_uniform_f(global.shaderData.shdr_color, cRed, cGreen, cBlue);
		Draw_Self_Ext(self);
		shader_reset();
	}
}

function DrawOutlineSurface(a_surf, a_pos, a_color, a_width){
	var cRed = color_get_red(a_color)/255;
	var cGreen = color_get_green(a_color)/255;
	var cBlue = color_get_blue(a_color)/255;
	
	
	global.shaderData.shdr_texelH = texture_get_texel_height(surface_get_texture(a_surf));
	global.shaderData.shdr_texelW = texture_get_texel_width(surface_get_texture(a_surf));
	
	shader_set(shdr_outline);
	shader_set_uniform_f(global.shaderData.shdr_pixelW, global.shaderData.shdr_texelW * a_width);
	shader_set_uniform_f(global.shaderData.shdr_pixelH, global.shaderData.shdr_texelH * a_width);
	shader_set_uniform_f(global.shaderData.shdr_color, cRed, cGreen, cBlue);	
	draw_surface(a_surf, a_pos.x, a_pos.y);
	shader_reset();
}

function DrawOwnerColor(a_color){
	var cRed = color_get_red(a_color)/255;
	var cGreen = color_get_green(a_color)/255;
	var cBlue = color_get_blue(a_color)/255;
			
	shader_set(shdr_ownerColor);
	shader_set_uniform_f(shdr_color, cRed, cGreen, cBlue);
}


function DrawBar(a_value, a_width, a_height, a_slot, a_offset, a_color)
{
			var slotHeight = 6;
			draw_healthbar(a_offset.x - a_width/2, a_offset.y - a_height/2 + (slotHeight * a_slot),
			a_offset.x + a_width/2, a_offset.y + a_height/2 + (slotHeight * a_slot),
			a_value, c_black, a_color, a_color, 0,true, true);
}

function DrawTextOutline(a_x,a_y, a_scale, a_text,a_colA, a_colB, a_charLimit, a_alignMid)
{
	var tText = [];
	if(a_charLimit == undefined)
	{
		array_push(tText, a_text);
	}else
	{
		if(string_length(a_text) > a_charLimit)
		{
			var tStrOrig = a_text;
			while(string_length(tStrOrig) != 0)
			{
				var tIndex = 0;
				var tStr = string_copy(tStrOrig, 1, a_charLimit+1);
				if(string_length(tStr) < a_charLimit) tIndex = string_length(tStr)+1;
				else tIndex = string_last_pos(" ", tStr);
				if(tIndex = 0) tIndex = string_length(tStr)+1;
				var tStrCut = string_copy(tStr, 1, tIndex-1);
				tStrOrig = string_delete(tStrOrig, 1, tIndex);
				array_push(tText, tStrCut);
			}
		}else array_push(tText, a_text);
	}
	
	for(var i = 0; i < array_length(tText); i += 1)
	{
		
		var txtSize = font_get_size(draw_get_font());
		
		if(a_alignMid) draw_set_halign(fa_center)
		draw_set_color(a_colB);
		draw_text_transformed((a_x),(a_y) + ((txtSize*i)*a_scale) + (i*5), tText[i], a_scale, a_scale,0);
		draw_set_color(a_colA);
		draw_text_transformed((a_x-1),(a_y-1) + ((txtSize*i)*a_scale) + (i*5), tText[i], a_scale, a_scale,0);	
		draw_set_halign(fa_left);
	}
}

function DrawHPBar(a_obj, a_pos, a_size, a_text)
{
	with a_obj
	{
		
		var barWidth = a_size.x;
		var barHeight = a_size.y;
		a_pos.x -= barWidth/2;
		a_pos.y -= barHeight/2;
		
		var hpVal = 100 * hp/char_maxHP;
		draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight, hpVal, c_red, c_lime, c_lime, 0,true, true);
		
		var KOVal = (char_KO/hp)*100;
		draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y+barHeight/2, KOVal, c_black, c_ltgrey, c_ltgrey, 0,false, true);
		
		var armVal = 100 * char_armStat/char_maxArm;
		draw_healthbar(a_pos.x, (a_pos.y+barHeight), a_pos.x + barWidth, (a_pos.y+barHeight)+barHeight/2, armVal, c_black, c_yellow, c_yellow, 0,false, true);
		
		var c_vit = $00ff66;
		var vitVal = 100 * vitStat/maxVit;
		draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/2, vitVal, c_black, c_vit, c_vit, 0,false, true);
		
		var woundVal = char_woundLvl/100;
		if(woundVal > 0) draw_healthbar(a_pos.x + (barWidth - (barWidth* woundVal)), a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight, 100, c_dkgray, c_dkgray, c_dkgray, 1,true, true);
		
		var tShdStat = 0;	//So Negative SHD Stats dont get calculated twice when adjusting displayed HP amounts
		if(char_shieldLvl > 0)
		{
			tShdStat = char_shdStat;
			var shdVal = 100 * char_shdStat/char_maxHP;
			var overShd1Val = shdVal - 100;
			var overShd2Val = overShd1Val - 100;
			var overShd3Val = overShd2Val - 100;
			draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/3, shdVal, c_black, c_aqua, c_aqua, 0,false, true);
			if(overShd1Val > 0) draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/3, overShd1Val, c_black, c_osCyan, c_osCyan, 0,false, true);
			if(overShd2Val > 0) draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/3, overShd2Val, c_black, c_osPurple1, c_osPurple1, 0,false, true);
			if(overShd3Val > 0) draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/3, overShd3Val, c_black, c_osPurple2, c_osPurple2, 0,false, true);
			
			var shdBuffVal = 0;
			if(char_shdBuffer != 0)
			{
				shdBuffVal = clamp((100*char_shdBuffer/char_shdBufferMax),0,100);	
			}else if(char_shdDelayTimer < char_shdDelayRate)
			{
				shdBuffVal = 100 - 100 * char_shdDelayTimer/char_shdDelayRate;	
			}
			draw_healthbar(a_pos.x, a_pos.y, a_pos.x + barWidth, a_pos.y + barHeight/3, shdBuffVal, c_black, c_blue, c_blue, 0,false, true);
		}
		if(a_text)
		{
			var hpMaxTotal = char_maxHP;
			var hpCurrTotal = hp + tShdStat + vitStat + char_armStat;
			DrawTextOutline(a_pos.x + barWidth/6 ,a_pos.y + barHeight/2,1,string(hpCurrTotal) + " / " + string(hpMaxTotal), c_black, c_ltgrey);
		}
	}
}

function DrawCone(a_pos, a_dir, a_len, a_width, a_res, a_col, a_alpha, a_outline)
{
	draw_set_colour(a_col);
			draw_set_alpha(a_alpha);
			for(var i = 0; i < a_res-1; i += 1)
			{
				var tSeg = (a_width/a_res);
				var tIt = -(a_width/2) + (tSeg*i);
			
				var tPos1 = LengthDir_Vec(a_len, a_dir+tIt);
				var tPos2 = LengthDir_Vec(a_len, a_dir+tIt+tSeg);
			
				draw_triangle(a_pos.x, a_pos.y, a_pos.x+tPos1.x, a_pos.y+tPos1.y, a_pos.x+tPos2.x, a_pos.y+tPos2.y, a_outline);	
			}	
}

function DrawProjectileSurface(a_arr, a_col, a_surf)
{
	surface_set_target(a_surf);
	draw_clear_alpha(c_white,0);
	for(var i = 0; i < array_length(a_arr); i += 1)
	{
		var tProj = a_arr[i];
		tProj.dmg_Draw();
	}

	surface_reset_target();
	DrawOutlineSurface(a_surf, new Vector2(0,0), a_col, 1);
	draw_surface(a_surf, 0,0);

}