// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function wep_DragonFire() : defunc_base_wep_SMG() constructor {
	ablCost = 500;
	ablName = "DragonFire"
	ammoMax = 125;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 10;
	
	
	range *= 10;
	damageMultiplier = 1;
	weaponSprite = spr_Flamethrower;
	
	wep_randomSpread = true;
	wep_projectileSpread *= 10;
	wep_projectileCount = 3;
	projSizeScale = 2;
	RPM = 1000;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapFast");
}

function wep_Minigun() : defunc_base_wep_SMG() constructor {
	ablCost = 1200;
	ablName = "Minigun"
	ammoMax = 125;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 10;
	
	range *= 2;
	damageMultiplier = 0.2;
	weaponSprite = spr_wep_Minigun;
	
	wep_randomSpread = true;
	wep_projectileSpread *= 3;
	wep_projectileCount = 2;
	projSizeScale = 1;
	RPM = 1500;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Rifle");
	abl_posOffset = new Vector2(0,3);
	
	abl_sfx = sfx_shoot2;
}

function wep_MechMinigun() :  wep_Minigun() constructor {
	ablCost = 1200;
	ablName = "Mech Minigun"
	ammoMax = 250;
	ammoCount = ammoMax;
	reloadAmount = ammoMax/10;
	reloadTime = 0.75;
	
}

function wep_RPG() : base_wep_Rifle() constructor {
	
	ds_list_clear(projectileList);
	ds_list_add(projectileList,proj_Rocket);
	
	ablCost = 750;
	ablName = "RPG"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 3;
	
	range *= 5;
	
	damageMultiplier *= 5;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Missile");
	wep_randomSpread = false;
	wep_projectileSpread *= 0.1;
	wep_projectileCount = 1;
	projSizeScale = 1;
	RPM = 60;
	
	weaponSprite = spr_RPG;
	abl_posOffset = new Vector2(0,-2);
	abl_Icon = spr_Rocket;
}

function wep_MechRPG() : wep_RPG() constructor {
	
	
	ablCost = 1800;
	ablName = "Mech RPG"
	ammoMax = 4;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 5;
	
	damageMultiplier *= 2.5;
	
	abl_Icon = spr_Rocket;
}


function wep_RatPack() : defunc_base_wep_Shotgun() constructor {
	
	ds_list_clear(projectileList);
	ds_list_add(projectileList,proj_DrunkMissile);
	
	ablCost = 1000;
	ablName = "Rat Pack Missile Rack"
	ammoMax = 6;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 1.5;
	
	range *= 5;
	
	damageMultiplier *= 0.7;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Missile");
	wep_randomSpread = false;
	wep_projectileSpread = 90;
	wep_projectileCount = 2;
	projSizeScale = 1;
	RPM = 720;
	
	weaponSprite = spr_RPG;
	abl_posOffset = new Vector2(0,-2);
	abl_Icon = spr_MissileRack;
}

function wep_MechRatPack() : wep_RatPack() constructor {
	
	ds_list_clear(projectileList);
	ds_list_add(projectileList,proj_DrunkMissile);
	
	ablCost = 1800;
	ablName = "Mech Rat Pack "
	ammoMax = 3;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 3;

	damageMultiplier *= 0.4;

	RPM = 360;
	wep_projectileCount = 12;
	abl_Icon = spr_MissileRack;
}

function wep_DragonWaveBoss() : wep_DragonWave() constructor {

	RPM = 50;
	wep_projectileSpread = 270;
	wep_projectileCount = 18;
	projSizeScale = 2;
	ammoMax = 12;
}

function wep_DragonWave() : defunc_base_wep_Shotgun() constructor {
	ablCost = 50;
	ablName = "DragonWave"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 1;
	
	range *= 5;
	
	damageMultiplier *= 1;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapSlow");
	wep_randomSpread = false;
	wep_projectileSpread = 120;
	wep_projectileCount = 8;
	projSizeScale = 1;
	RPM = 120;
}

function wep_DragonField() : defunc_base_wep_Shotgun() constructor {
	ablCost = 50;
	ablName = "DragonField"
	ammoMax = 15;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 30;
	
	range *= 5;
	
	damageMultiplier *= 1;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapSlow");
	wep_randomSpread = false;
	wep_projectileSpread = 270;
	wep_projectileCount = 18;
	projSizeScale = 3;
	RPM = 120;
}


function wep_DummyGun() : defunc_base_wep_Sidearm() constructor {
	
	ablCost = 0;
	ablName = "Dummy Gun"
	ammoMax = 0;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	
	range *= 0;
	damageMultiplier = 0;
	weaponSprite = spr_None;
	
	wep_projectileSpread *= 1;
	RPM = 300;
}

function wep_ScatterBolt() : base_wep_Rifle() constructor {
	
	ablCost = 750;
	ablName = "Scatterbolt"
	ammoMax = 16;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	range *= 1.1;
	damageMultiplier *= 1.5;
	weaponSprite = spr_DBShotgun;
	
	wep_projectileSpread *= 1;
	RPM = 240;
}


function wep_ScatterBoltAlt() : defunc_base_wep_Shotgun() constructor {
	ablCost = 0;
	ablName = "Scatterbolt"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 1.2;
	
	range *= 1.3;
	damageMultiplier *= 0.8;
	weaponSprite = spr_DBShotgun;
	
	wep_randomSpread = false;
	wep_projectileSpread *= 0.6;
	RPM = 120;
}