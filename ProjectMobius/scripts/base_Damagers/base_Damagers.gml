// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information



function base_Damager() constructor
{
	
	weapon = undefined;
	owner = undefined;
	teamID = TEAMID.Neutral;
	
	dmg_isCore = false;
	dmg_hitOthers = false;
	
	friendlyFire = false;
	friendlyOnly = false;
	
	objectsDamaged = [];
	dmg_activeFrames = [true];
	dmg_canDamage = false;
	
	dmg_Components = [];	//For Internal components for modifying behaviour
	dmg_modArr = [];		//For sources of external manipulation of behaviour, e.g Gravity Well, Slow, etc
	
	hp = 1;
	
	x = 0;
	y = 0;
	mx = 0;
	my = 0;
	
	direction = 0;
	image_xscale = 1;
	image_yscale = 1;
	image_angle = 0;
	
	lifeTime = 10;
	lifeTimer = 0;
	
	range = 0;
	distTravelled = 0;
	distLerp = 0;
	
	timeScale = 1;
	
	dmg_maxBounces = 0;
	dmg_hitSlow = 0;
	forceDir = 0;
	forceDur = 0;
	stunDur = 0;
	
	armorBlock = false;
	
	baseSpeed = 1;
	moveSpeed = 1;
	dmg_speedCurve = undefined;
	randSpeedMod = random_range(0.9,1.1);
	
	ignoreObstacles = false;
	
	dmg_sprite = spr_tEnmBullet;
	dmg_spriteIndex = 0;
	
	locomotorType = ploco_1;
	locomotor = undefined;
	array_push(dmg_Components, new dmg_comp_Locomote(self, ploco_1));
	
	dmg_PreUpdate = function()
	{
		
		damageType = weapon.abl_DmgType;
		moveSpeed = baseSpeed;
		
		for(var i = 0; i < array_length(dmg_Components); i += 1)
		{
			dmg_Components[i].dmg_comp_Update();	
		}
				
		//Because image index is stored as a decimal, and I need to convert it to whole image indexes, then convert it to an array index
		var tFrame = floor(dmg_spriteIndex);
		var tI = clamp(tFrame, 0, array_length(dmg_activeFrames)-1);	//So the frames dont go beyond the boundaries of the activeFrames array
		
		dmg_canDamage = dmg_activeFrames[tI];

	}
	
	dmg_Update = function()
	{
		
		var t_speedMod = animcurve_channel_evaluate(dmg_speedCurve, distLerp) * randSpeedMod;
		moveSpeed = baseSpeed * t_speedMod
		if(distLerp > 1 && !dmg_persists)
		{
			lifeTimer = lifeTime;	
		}
		
	}
	
	dmg_EndUpdate = function()
	{
		lifeTimer += GetDeltaTime(timeScale);
	}
	
	dmg_CollideCheck = function(a_other)
	{
		AssignProjomotor(self);
		var tHit = false;
		with(global.projomotor)
		{
			if(place_meeting(x,y,a_other) == true)
			{
				tHit = true;
			}
		}
		if(tHit) dmg_OnCollide(a_other);
	}
	
	dmg_OnCollide = function(a_other)
	{
		if(object_is_ancestor(a_other.object_index, base_Character))
		{
			OnProjHit(self, a_other);	
		}else if(object_is_ancestor(a_other.object_index, base_Prop))
		{
			a_other.hp -= weapon.abl_powerMultiplier;
		}
	}
	
	dmg_Draw = function()
	{
		var tSpd = sprite_get_speed(dmg_sprite);
		dmg_spriteIndex += GetDeltaTime(timeScale)*tSpd;
		if(dmg_spriteIndex >= sprite_get_number(dmg_sprite)) dmg_spriteIndex = 0;
		
		draw_sprite_ext(dmg_sprite, floor(dmg_spriteIndex), x, y, image_xscale, image_yscale, image_angle, c_white, 1);
	}
}

function base_LiteMelee() : base_Damager() constructor
{
	dmg_isCore = true;
	dmg_hitOthers = true;
	dmg_activeFrames = [false, true, false, false];
	dmg_hitSlow = 0.2;
	
	ignoreObstacles = true;
	
	dmg_Update = function()
	{
		x = owner.x;
		y = owner.y;
	}
	
	dmg_Draw = function()
	{
		var frameSpeed = sprite_get_speed(dmg_sprite);
		dmg_spriteIndex += GetDeltaTime(timeScale)*frameSpeed;
		if(dmg_spriteIndex >= sprite_get_number(dmg_sprite))
		{
			lifeTime = 0;
			hp = 0;
		}
		
		draw_sprite_ext(dmg_sprite, floor(dmg_spriteIndex), x, y, image_xscale, image_yscale, image_angle, c_white, 1);
	}
}

function base_DummyProj() : base_Damager() constructor
{
	lifeTime = infinity;
	hp = infinity;
	baseSpeed = 0;
}
	

function base_LiteWave() : base_Damager() constructor
{
	dmg_isCore = true;
	hp = infinity;

	ignoreObstacles = true;
	ltWave_radius = 1;
	ltWave_thickness = 4;
	ltWave_resolution = 32;
	ltWave_sprite = spr_liteWave;
	ltWave_waveSpeed = 1;
	baseSpeed = 0;
	
	dmg_Update = function()
	{
		ltWave_radius += ltWave_waveSpeed;
	}
	
	dmg_Draw = function()
	{
		for(var i = 0; i < ltWave_resolution; i += 1)
		{
			var tSeg = (360/ltWave_resolution);
			var tA = new Vector2(lengthdir_x(ltWave_radius, tSeg*i), lengthdir_y(ltWave_radius, tSeg*i));
			var tB = new Vector2(lengthdir_x(ltWave_radius, tSeg*(i+1)), lengthdir_y(ltWave_radius, tSeg*(i+1)));
			var tDist = point_distance(tA.x, tA.y, tB.x, tB.y);
			var tDir = point_direction(tA.x, tA.y, tB.x, tB.y)+90;
			draw_sprite_ext(ltWave_sprite, -1, x+tA.x, y+tA.y, ltWave_thickness, tDist, tDir, c_white, 1);
		}	
	}
	
	dmg_CollideCheck = function(a_other)
	{
		var tDist = point_distance(x,y,a_other.x, a_other.y);
		
		if(tDist <= ltWave_radius && tDist >= (ltWave_radius-ltWave_thickness))
		{
			dmg_OnCollide(a_other);
		}
	}
	
}

function base_AoE() : base_Damager() constructor
{
	dmg_isCore = true;
	baseSpeed = 0;
	
	aoe_Delay = 1;
	aoe_Timer = 0;
	lifeTime = 1.5;
	aoe_Lerp = 0;
	
	dmg_Update = function()
	{
		aoe_Timer += GetDeltaTime(timeScale);
		aoe_Lerp = clamp(aoe_Timer/aoe_Delay,0,1);
		if(aoe_Timer <= aoe_Delay) dmg_canDamage = false;
	}
}

function base_AoECone() : base_AoE() constructor
{
	aoe_ConeWidth = 45;
	aoe_ConeLength = 128;
	aoe_ConeResolution = 8;
	
	dmg_Draw = function()
	{
		if(aoe_Lerp < 1)
		{
			DrawCone(new Vector2(x,y), direction, aoe_ConeLength, aoe_ConeWidth, aoe_ConeResolution, c_orange, 0.25, false);
			DrawCone(new Vector2(x,y), direction, aoe_ConeLength*aoe_Lerp, aoe_ConeWidth, aoe_ConeResolution, c_red, 0.25, false);
		}else
		{
			DrawCone(new Vector2(x,y), direction, aoe_ConeLength, aoe_ConeWidth, aoe_ConeResolution, c_black, 1, false);
		}
		
		draw_set_alpha(1);
	}
	
	dmg_CollideCheck = function(a_other)
	{
		var tDist = point_distance(x,y,a_other.x, a_other.y);
		
		if(tDist <= aoe_ConeLength)
		{
			var tDir = point_direction(x,y,a_other.x, a_other.y);
			if(abs(angle_difference(tDir, direction)) < aoe_ConeWidth/2) dmg_OnCollide(a_other);
		}
	}
}

function GetDistAngle(a_dist, a_ang)
{
	var retVal = a_dist/sin(a_ang);
	return retVal;
}