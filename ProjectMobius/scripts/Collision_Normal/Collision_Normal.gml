function Collision_Normal() {
	// Contributors: xot, strawbryjam 

	/// collision_normal(x1,y1,obj [,rad [,res]])
	//
	//  Returns a 2D "surface normal" (in degrees) at a given point 
	//  on or near an instance detected within a circular test area.
	//  Makes approximately pi*(rad*rad)/(res*res) collision calls.
	//  If no collision is found, (-1) is returned.
	//
	//      x1,y1       point on the surface, real
	//      obj         object or instance (or all), real
	//      rad         radius of test area (default 4), real
	//      res         resolution of test (default 1), real
	//
	/// GMLscripts.com/license
	{
	    var xx  = argument[0];
	    var yy  = argument[1];
	    var obj = argument[2];
	    var rad = 4;
	    var res = 1;
	    if (argument_count > 3) rad = argument[3];
	    if (argument_count > 4) res = argument[4];
	    if (rad <= 0) rad = 4;
	    if (res <= 0) res = 1;
	    var nx = 0;
	    var ny = 0;
	    if (collision_circle(xx, yy, rad, obj, true, true)) {
	        for (var j=res; j<=rad; j+=res) {
	            for (var i=0; i<rad; i+=res) {
	                if (point_distance(0, 0, i, j) <= rad) {
	                    if (!collision_point(xx+i, yy+j, obj, true, true)) { nx += i; ny += j; }
	                    if (!collision_point(xx+j, yy-i, obj, true, true)) { nx += j; ny -= i; }
	                    if (!collision_point(xx-i, yy-j, obj, true, true)) { nx -= i; ny -= j; }
	                    if (!collision_point(xx-j, yy+i, obj, true, true)) { nx -= j; ny += i; }
	                }
	            }
	        }
	        if (nx == 0 && ny == 0) return (-1);
	        return point_direction(0, 0, nx, ny);
	    }else{
	        return (-1);
	    }
	}


}

function TileMap_Collision_Normal()
{
	//Modified by Mitchell Osborne to support Tilemaps
	// Contributors: xot, strawbryjam 

	/// collision_normal(x1,y1,obj [,rad [,res]])
	//
	//  Returns a 2D "surface normal" (in degrees) at a given point 
	//  on or near an instance detected within a circular test area.
	//  Makes approximately pi*(rad*rad)/(res*res) collision calls.
	//  If no collision is found, (-1) is returned.
	//
	//      x1,y1       point on the surface, real
	//      obj         object or instance (or all), real
	//      rad         radius of test area (default 4), real
	//      res         resolution of test (default 1), real
	//
	/// GMLscripts.com/license
	{
		 var xx  = argument[0];
	    var yy  = argument[1];
	    var obj = argument[2];
	    var rad = 4;
	    var res = 1;
	    if (argument_count > 3) rad = argument[3];
	    if (argument_count > 4) res = argument[4];
	    if (rad <= 0) rad = 4;
	    if (res <= 0) res = 1;
	    var nx = 0;
	    var ny = 0;
	    if (TileMeetingBox(xx+rad,xx-rad,yy+rad,yy-rad)) {
	        for (var j=res; j<=rad; j+=res) {
	            for (var i=0; i<rad; i+=res) {
	                if (point_distance(0, 0, i, j) <= rad) {
	                    if (!tilemap_get_at_pixel(obj, xx+i, yy+j)) { nx += i; ny += j; }
	                    if (!tilemap_get_at_pixel(obj, xx+j, yy-i)) { nx += j; ny -= i; }
	                    if (!tilemap_get_at_pixel(obj, xx-i, yy-j)) { nx -= i; ny -= j; }
	                    if (!tilemap_get_at_pixel(obj, xx-j, yy+i)) { nx -= j; ny += i; }
	                }
	            }
	        }
	        if (nx == 0 && ny == 0) return (-1);
	        return point_direction(0, 0, nx, ny);
	    }else{
	        return (-1);
	    }
	}


}

function Collision_Normal_Combined(xx,yy,physHitArr,tileHitArr,rad,res)
{
	//Modified by Mitchell Osborne to support Tilemaps
	// Contributors: xot, strawbryjam 

	/// collision_normal(x1,y1,obj [,rad [,res]])
	//
	//  Returns a 2D "surface normal" (in degrees) at a given point 
	//  on or near an instance detected within a circular test area.
	//  Makes approximately pi*(rad*rad)/(res*res) collision calls.
	//  If no collision is found, (-1) is returned.
	//
	//      x1,y1       point on the surface, real
	//      obj         object or instance (or all), real
	//      rad         radius of test area (default 4), real
	//      res         resolution of test (default 1), real
	//
	/// GMLscripts.com/license
	{
		if (rad <= 0) rad = 4;
	    if (res <= 0) res = 1;
	    var nx = 0;
	    var ny = 0;
	    
		var tm = layer_tilemap_get_id("Tiles_Physics");
	
		var tileHit = false;
		var physHit = false;
		for(var n = 0; n < array_length(physHitArr); n += 1) 
		{
			if(collision_circle(xx, yy, rad, physHitArr[n], true, true)) physHit = true;
		}
		if(TileMeetingBox(xx+rad,xx-rad,yy+rad,yy-rad)) tileHit = true;
	    if (physHit || tileHit) {
	        for (var j=res; j<=rad; j+=res) {
	            for (var i=0; i<rad; i+=res) {
	                if (point_distance(0, 0, i, j) <= rad) {
						if(tileHit)
						{
							for (var n = 0; n < array_length(tileHitArr); n += 1)
							{
			                    if (tilemap_get_at_pixel(tm, xx+i, yy+j) != tileHitArr[n]) { nx += i; ny += j; }
			                    if (tilemap_get_at_pixel(tm, xx+j, yy-i) != tileHitArr[n]) { nx += j; ny -= i; }
			                    if (tilemap_get_at_pixel(tm, xx-i, yy-j) != tileHitArr[n]) { nx -= i; ny -= j; }
			                    if (tilemap_get_at_pixel(tm, xx-j, yy+i) != tileHitArr[n]) { nx -= j; ny += i; }
							}
						}
						if(physHit)
						{
							for (var n = 0; n < array_length(physHitArr); n += 1)
							{
								if (!collision_point(xx+i, yy+j, physHitArr[n], true, true)) { nx += i; ny += j; }
								if (!collision_point(xx+j, yy-i, physHitArr[n], true, true)) { nx += j; ny -= i; }
								if (!collision_point(xx-i, yy-j, physHitArr[n], true, true)) { nx -= i; ny -= j; }
								if (!collision_point(xx-j, yy+i, physHitArr[n], true, true)) { nx -= j; ny += i; }		
							}
						}
	                }
	            }
	        }
	        if (nx == 0 && ny == 0) return (-1);
	        return point_direction(0, 0, nx, ny);
	    }else{
	        return (-1);
	    }
	}


}
	
