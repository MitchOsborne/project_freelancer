// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function evade_Dash (a_bp) : base_Evade(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Evade(self, .4, 110, .3));
	//array_push(abl_Components, new abl_comp_SelfBuff(self, buff_sprint));
	array_push(abl_Components, new abl_comp_Cooldown(self, .45));
	ai_Score = 50;
	abl_sfx = sfx_dash;
}

function evade_Blitz (a_bp) : base_Evade(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_Evade(self, .3, 110, .25));
	array_push(abl_Components, new abl_comp_Cooldown(self, .45));
	
	ai_Score = 60;
	abl_sfx = sfx_dash;
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_AtkSpdUp, 1));
}
