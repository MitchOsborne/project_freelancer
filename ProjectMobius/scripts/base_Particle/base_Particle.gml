// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function base_Particle(a_x, a_y, a_mx, a_my) constructor{
	baseSpeed = 1;
	
	killOnAnimEnd = true;

	lifeTime = 5;
	lifeTimer = 0;

	//mx = random_range(-baseSpeed, baseSpeed);
	//my = random_range(-baseSpeed, baseSpeed);
	
	x = a_x;
	y = a_y;
	
	mx = a_mx;
	my = a_my;
	
	rot = irandom_range(0,359);
	ptl_Gravity = 0;
	ptl_Scale = 1;
	ptl_AnimCurve = "Linear";
	ptl_Sprite = spr_error;
	ptl_Text = undefined;
	ptl_SpriteSpeed = 12;
	animLerp = 0;
	animCtr = 0;
	animLifeTime = false;
	
	Particle_Update = function()
	{
		var retVal = 0;
		animLerp = lifeTimer/lifeTime;
		var spdMod = animcurve_channel_evaluate(animcurve_get_channel(crv_Misc, ptl_AnimCurve), animLerp);
		ptl_Scale = clamp(animLerp,1,2);
		ptl_Alpha = 2 - ptl_Scale;
		my += ptl_Gravity*(GetDeltaTime(1));
	
		x += mx*(baseSpeed*spdMod);
		y += my*(baseSpeed*spdMod);

		lifeTimer += GetDeltaTime(1);
		
		if(animLifeTime) 
		{
			animCtr = floor((sprite_get_number(ptl_Sprite)-1)*animLerp);
		}
		else animCtr = clamp(animCtr + GetDeltaTime(ptl_SpriteSpeed),0,sprite_get_number(ptl_Sprite)-1);
		
		if(lifeTimer >= lifeTime)
		{
			if(killOnAnimEnd) retVal = 1;	
			else if (lifeTimer >= lifeTime * 2) retVal = 1;
		}
		return retVal;
	}
}

function Ptl_SwordHit(a_x, a_y, a_mx, a_my) : base_Particle(a_x, a_y, a_mx, a_my) constructor
{
	animLifeTime = true;
	lifeTime = 0.2;
	ptl_Sprite = spr_HitParticle;
}

function Ptl_Blood(a_x, a_y, a_mx, a_my) : base_Particle(a_x, a_y, a_mx, a_my) constructor
{
	lifeTime = 1;
	killOnAnimEnd = false;
	ptl_AnimCurve = "BloodParticle";
	ptl_Sprite = spr_BloodParticle;
}

function Ptl_Text(a_x, a_y, a_mx, a_my, a_txt) : base_Particle(a_x, a_y, a_mx, a_my) constructor
{
	lifeTime = 0.5;
	killOnAnimEnd = false;
	ptl_Text = a_txt;
}

function Ptl_BulletCasing(a_x, a_y, a_mx, a_my, a_grav, a_spr) : base_Particle(a_x, a_y, a_mx, a_my) constructor
{
	lifeTime = 0.5;
	killOnAnimEnd = false;
	ptl_Gravity = a_grav;
	ptl_Sprite = a_spr;
}

function Ptl_ShotgunCasing(a_x, a_y, a_mx, a_my, a_grav) : Ptl_BulletCasing(a_x, a_y, a_mx, a_my, a_grav) constructor
{
	ptl_Sprite = spr_ShotgunCasing;
}