// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Tasks(){

}

function ai_task_MoveIntoRange(a_Caller, a_Target, a_Range)
{
	var nextTask = false;	//Whether to continue to the next task in the list
	var a = a_Caller.slavedObj;
	var b = a_Target;
	if(CheckExistance(a) && CheckExistance(b))
	{
		var tDist = point_distance(a.x, a.y, b.x, b.y);
		
		if(tDist <= a_Range && a_Caller.ai_directPath && a_Caller.ai_targetVisible)
		{
			nextTask = true;
		}else
		{
			ai_act_PathToPosition(a, b.x, b.y);
		}
	}
	
	return nextTask;
}

function ai_ptrlTask_Scout(a_Caller, a_PtrlPoint)
{
	with a_Caller
	{
		a_PtrlPoint.ptrl_taskTmr += GetDeltaTime(slavedObj.timeScale);
		var tDir = GetDirection(slavedObj.x, slavedObj.y, a_PtrlPoint.x, a_PtrlPoint.y, false, true);
		tDir += (90*(a_PtrlPoint.ptrl_taskTmr/a_PtrlPoint.ptrl_taskDur));
		var tVec = GetCoord(tDir);
		ai_lookPoint = new Vector2(slavedObj.x + tVec.x, slavedObj.y + tVec.y);
	
		if(a_PtrlPoint.ptrl_taskTmr >= a_PtrlPoint.ptrl_taskDur)
		{
			a_PtrlPoint.ptrl_TaskComplete = true;
		}
	}
}