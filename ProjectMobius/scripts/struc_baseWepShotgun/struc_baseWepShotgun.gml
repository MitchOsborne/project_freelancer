// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_base_wep_Shotgun() : defunc_base_Weapon() constructor{

	ds_list_add(projectileList,proj_Bullet);

	abl_canReload = true;
	autoReload = false;
	reloadTime = 1.5;
	reloadTimer = 0;
	reloadAmount = ammoMax;	//Amount of ammo added when the gun reloads

	fireRate = 0.1;
	fireRateTimer = 0;

	wep_projectileCount = 8;
	wep_projectileSpread = 30;
	wep_randomSpread = true;
	
	range = 80;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Shotgun");
	RPM = 600;
	
	abl_forceStrength = 1.2;
	abl_stunDur = 0.2;
	
	abl_sfx = sfx_shoot2;
}