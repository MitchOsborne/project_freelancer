// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_ComplexRoom() : base_RoomData() constructor
{
	rd_region = REGION.YAKIBASE;
	rd_BGM = bgm_YakiBase;
	rd_Outdoors = false;
	rd_ambAlpha = 0;
	rd_EnemyLevel = 15;
}

function roomData_Complex1() : base_ComplexRoom() constructor
{
	rd_roomID = "Complex1";
	rd_roomObj = t_room_complex_1;
	array_push(rd_lExit,roomData_WoodsComplex);
	array_push(rd_rExit,roomData_Complex2);
}

function roomData_Complex2() : base_ComplexRoom() constructor
{
	rd_roomID = "Complex2";
	rd_roomObj = t_room_complex_2;
	array_push(rd_lExit,roomData_Complex1);
	array_push(rd_rExit,roomData_Complex3);
}

function roomData_Complex3() : base_ComplexRoom() constructor
{
	rd_roomID = "Complex3";
	rd_roomObj = t_room_complex_1;
	array_push(rd_lExit, roomData_Complex2);
	array_push(rd_rExit,roomData_Strut_ShaftRoof);
}

function roomData_ComplexMidBoss() : base_ComplexRoom() constructor
{
	rd_roomID = "ComplexMidBoss";
	rd_region = REGION.YAKIBASE;
	rd_BGM = bgm_YakiBossIntro;
	rd_roomObj = t_room_complex_mBoss;
	array_push(rd_lExit, roomData_Complex2);
}
