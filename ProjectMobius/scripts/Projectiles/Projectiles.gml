// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function dmg_Bullet() : base_Damager() constructor
{
	baseSpeed = 8;
	dmg_sprite = spr_tBullet1;
}

function dmg_KnifeThrow() : base_Damager() constructor
{
	baseSpeed = 4;
	dmg_sprite = spr_proj_Knife;
}

function dmg_LaserBolt() : base_Damager() constructor
{
	base_Speed = 2;
	dmg_sprite = spr_tLaserRed;
}

function dmg_Tranq() : dmg_Bullet() constructor
{
	armorBlock = true;
}

function dmg_Pellet() : base_Damager() constructor
{
	baseSpeed = 8;
	dmg_sprite = spr_tBuckshot;
}

function dmg_FlameBall() : base_Damager() constructor
{
	baseSpeed = 3;
	dmg_sprite = spr_tBlob;
	
}

function dmg_SoulFire() : base_Damager() constructor
{
	array_push(dmg_Components, new dmg_comp_Homing(self, 5, 5, 45, 0.3));
}

function dmg_SlashLeft() : base_LiteMelee() constructor
{
	dmg_sprite = spr_slash1;	
}

function dmg_SlashRight() : base_LiteMelee() constructor
{
	dmg_sprite = spr_slash2;	
}

function dmg_SlashSpin() : base_LiteMelee() constructor
{
	dmg_activeFrames = [false, true, true, true,true, false];
	dmg_sprite = spr_slash3;	
}

function dmg_BashCharge() : base_LiteMelee() constructor
{
	dmg_activeFrames = [false, true, true, true, false];
	dmg_sprite = spr_bash3;	
}

function dmg_Bite() : base_LiteMelee() constructor
{
	dmg_activeFrames = [false, false, true, true, false, false];
	dmg_sprite = spr_bite;	
}

function dmg_FlamerCone() : base_AoECone() constructor
{
	aoe_ConeLength = 256;
	aoe_ConeWidth = 90;
}