enum STAT_CAT{
	NONE,
	ATTACK,
	DEFENSE,	
	ABILITY,
	LUCK,
}

enum STAT_TYPE
{
	NONE,
	DAMAGE,
	ATKSPEED,
	HEALTH,
	SHIELDS,
	COOLDOWN,
	DURATION,
	CRITRATE,
	PROCRATE,
	EOL,	//End of Enum
}

function StatCard() constructor
{
	
	crd_MaxStatSlots = 20;
	crd_StatSlotArr = [];
	
	for(var i = 0; i < crd_MaxStatSlots; i += 1)
	{
		array_push(crd_StatSlotArr, new StatCardSlot());	
	}
	crd_CardLvl = 0;
	
	statDiv = 0.5;
	
	crd_HPLvl = 0;
	crd_SHDLvl = 0;
	
	crd_DMGLvl = 0;
	crd_SPDLvl = 0;
	
	crd_CDLvl = 0;
	crd_DURLvl = 0;
	
	crd_CRITLvl = 0;
	crd_PROCLvl = 0;
	
	crd_HPMod = 1;
	crd_SHDMod = 1;
	crd_DMGMod = 1;
	crd_SPDMod = 1;
	crd_CDMod = 1;
	crd_DURMod = 1;
	crd_CRITMod = 1;
	crd_PROCMod = 1;
	
}

function base_StatCardSlot() constructor
{
	stat_HPLvl		= 0;
	stat_SHDLvl		= 0;
	stat_DMGLvl		= 0;
	stat_SPDLvl		= 0;
	stat_CDLvl		= 0;
	stat_DURLvl		= 0;
	stat_CRITLvl	= 0;
	stat_PROCLvl	= 0;
}

function base_StatUnlockSlot() constructor
{
	stat_ATKLvl = 0;
	stat_DEFLvl = 0;
	stat_ABLLvl = 0;
	stat_LUCKLvl = 0;
	stat_CanRiven = false;
}

function StatCardSlot() constructor
{
	stat_MainStat = STAT_TYPE.NONE;
	stat_AltStat = STAT_TYPE.NONE;
		
	stat_MainType = STAT_CAT.NONE;
	stat_AltType = STAT_CAT.NONE;
	
	stat_mainTxt = "";
	stat_altTxt = "";
	
	stat_icon = spr_EmptyIcon;
}

global.StatCardUnlocks = 
{
	stat_UnlockArr : array_create(20, undefined),
}

for(var i = 0; i < array_length(global.StatCardUnlocks.stat_UnlockArr); i += 1)
{
	global.StatCardUnlocks.stat_UnlockArr[i] = new base_StatUnlockSlot();
}

function GetStatInValidSlot(a_StatType, a_SlotIndex) //Checks if the given StatSlot is within a valid Card Slot range
{
	var retVal = true;
	if(a_SlotIndex < global.SlotRange.ATK)
	{
		if(a_StatType != STAT_TYPE.DAMAGE && a_StatType != STAT_TYPE.ATKSPEED) retVal = false;
	}
	else if(a_SlotIndex < global.SlotRange.DEF)
	{
		if(a_StatType != STAT_TYPE.HEALTH && a_StatType != STAT_TYPE.SHIELDS) retVal = false;
	}
	else if(a_SlotIndex < global.SlotRange.ABL) 
	{
		if(a_StatType != STAT_TYPE.DURATION && a_StatType != STAT_TYPE.COOLDOWN) retVal = false;
	}
	else if(a_SlotIndex < global.SlotRange.LUCK) 
	{
		if(a_StatType != STAT_TYPE.CRITRATE && a_StatType != STAT_TYPE.PROCRATE) retVal = false;
	}	
	
	return retVal;
}

function GetStatValByType(a_SlotType, a_SlotIndex)
{
	var tCore = global.StatCardUnlocks.stat_UnlockArr[a_SlotIndex];
	var retVal = 0;
	switch a_SlotType
	{
		case STAT_TYPE.DAMAGE:
		retVal = tCore.stat_ATKLvl;
		break;
		case STAT_TYPE.ATKSPEED:
		retVal = tCore.stat_ATKLvl;
		break;
		case STAT_TYPE.HEALTH:
		retVal = tCore.stat_DEFLvl;
		break;
		case STAT_TYPE.SHIELDS:
		retVal = tCore.stat_DEFLvl;
		break;
		case STAT_TYPE.DURATION:
		retVal = tCore.stat_ABLLvl;
		break;
		case STAT_TYPE.COOLDOWN:
		retVal = tCore.stat_ABLLvl;
		break;
		case STAT_TYPE.CRITRATE:
		retVal = tCore.stat_LUCKLvl;
		break;
		case STAT_TYPE.PROCRATE:
		retVal = tCore.stat_LUCKLvl;
		break;
		case STAT_TYPE.EOL:
		retVal = -1;
		break;
	}
	
	return retVal;
	
}


function GetStatNameByType(a_SlotType)
{
	var retVal = "NONE";
	switch a_SlotType
	{
		case STAT_TYPE.DAMAGE:
		retVal = "DAMAGE";
		break;
		case STAT_TYPE.ATKSPEED:
		retVal = "ATKSPEED";
		break;
		case STAT_TYPE.HEALTH:
		retVal = "HEALTH";
		break;
		case STAT_TYPE.SHIELDS:
		retVal = "SHIELDS";
		break;
		case STAT_TYPE.DURATION:
		retVal = "DURATION";
		break;
		case STAT_TYPE.COOLDOWN:
		retVal = "COOLDOWN";
		break;
		case STAT_TYPE.CRITRATE:
		retVal = "CRITRATE";
		break;
		case STAT_TYPE.PROCRATE:
		retVal = "PROCRATE";
		break;
		case STAT_TYPE.EOL:
		retVal = "NYUK NYUK";
		break;
	}
	
	return retVal;
	
}

function defunc_base_StatSlot() constructor
{
	stat_MainType = STAT_CAT.NONE;	
	altStatType = STAT_CAT.NONE;
	stat_HPMod = 0;
	stat_SHDMod = 0;
	stat_DMGMod = 0;
	stat_SPDMod = 0;
	stat_CDMod = 0;
	stat_DURMod = 0;
	stat_CRITMod = 0;
	stat_PROCMod = 0;
	stat_currLvl = 1;
	stat_maxLvl = 5;
	stat_quality = 1;	//Increases the skewing of Riven mods
	
	mainStat = STAT_TYPE.NONE;
	altStat = STAT_TYPE.NONE;
	
	statScale = 0;
	
	stat_mainTxt = "";
	stat_altTxt = "";
	
	stat_icon = spr_EmptyIcon;
	stat_icon1 = spr_EmptyIcon;
	stat_icon2 = spr_EmptyIcon;
	
}

function Init_StatSlot(a_slot, a_slotIndex)
{
	with (a_slot)
	{
		var statVal = GetStatValByType(stat_MainStat, a_slotIndex);	
		stat_mainTxt = "";
		stat_altTxt = "";
		switch stat_MainStat
		{
			case STAT_TYPE.DAMAGE:
			stat_MainType = STAT_CAT.ATTACK;
			stat_icon = spr_ModDMG1;
			stat_mainTxt = "DMG Mod:" + string(statVal);
			break;
			case STAT_TYPE.ATKSPEED:
			stat_MainType = STAT_CAT.ATTACK;
			stat_icon = spr_ModSPD;
			stat_mainTxt = "SPD Mod:" + string(statVal);
			break;
			case STAT_TYPE.HEALTH:
			stat_MainType = STAT_CAT.DEFENSE;
			stat_icon = spr_ModHP1;
			stat_mainTxt = "HP Mod:" + string(statVal);
			break;
			case STAT_TYPE.SHIELDS:
			stat_MainType = STAT_CAT.DEFENSE;
			stat_icon = spr_ModSHD1;
			stat_mainTxt = "SHD Mod:" + string(statVal);
			break;
			case STAT_TYPE.DURATION:
			stat_MainType = STAT_CAT.ABILITY;
			stat_icon = spr_ModDUR;
			stat_mainTxt = "DUR Mod:" + string(statVal);
			break;
			case STAT_TYPE.COOLDOWN:
			stat_MainType = STAT_CAT.ABILITY;
			stat_icon = spr_ModCD;
			stat_mainTxt = "CD Mod:" + string(statVal);
			break;
			case STAT_TYPE.CRITRATE:
			stat_MainType = STAT_CAT.LUCK;
			stat_icon = spr_ModCrit;
			stat_mainTxt = "CRIT Mod:" + string(statVal);
			break;
			case STAT_TYPE.PROCRATE:
			stat_MainType = STAT_CAT.LUCK;
			stat_icon = spr_ModProc;
			stat_mainTxt = "PROC Mod:" + string(statVal);
			break;
		}
		if(stat_MainStat != stat_AltStat)
		{
			statVal = -(GetStatValByType(stat_MainStat, a_slotIndex));
			stat_icon = spr_ModRiven;
			switch stat_AltStat
			{
				case STAT_TYPE.DAMAGE:
				stat_AltType = STAT_CAT.ATTACK;
				stat_altTxt = "DMG Mod:" + string(statVal);
				break;
				case STAT_TYPE.ATKSPEED:
				stat_AltType = STAT_CAT.ATTACK;
				stat_altTxt = "SPD Mod:" + string(statVal);
				break;
				case STAT_TYPE.HEALTH:
				stat_AltType = STAT_CAT.DEFENSE;
				stat_altTxt = "HP Mod:" + string(statVal);
				break;
				case STAT_TYPE.SHIELDS:
				stat_AltType = STAT_CAT.DEFENSE;
				stat_altTxt = "SHD Mod:" + string(statVal);
				break;
				case STAT_TYPE.DURATION:
				stat_AltType = STAT_CAT.ABILITY;
				stat_altTxt = "DUR Mod:" + string(statVal);
				break;
				case STAT_TYPE.COOLDOWN:
				stat_AltType = STAT_CAT.ABILITY;
				stat_altTxt = "CD Mod:" + string(statVal);
				break;
				case STAT_TYPE.CRITRATE:
				stat_AltType = STAT_CAT.LUCK;
				stat_altTxt  = "CRIT Mod:" + string(statVal);
				break;
				case STAT_TYPE.PROCRATE:
				stat_AltType = STAT_CAT.LUCK;
				stat_altTxt  = "PROC Mod:" + string(statVal);
				break;
			}
		}
	}
}

function Stat_SetSlotStats(a_mod, a_dmg, a_spd, a_hp, a_shd, a_dur, a_cd, a_crit, a_proc)
{
	with a_mod
	{
		stat_DMGMod = a_dmg;
		stat_SPDMod = a_spd;
		stat_HPMod = a_hp;
		stat_SHDMod = a_shd;
		stat_DURMod = a_dur;
		stat_CDMod = a_cd;
		stat_CRITMod = a_crit;
		stat_PROCMod = a_proc;
	}
}


function Update_StatCard(a_char)
{
	a_char.char_UpdateCard = false;
	with a_char.char_StatCard
	{
		crd_HPLvl = 0;
		crd_SHDLvl = 0;
		crd_DMGLvl = 0;
		crd_SPDLvl = 0;
		crd_CDLvl = 0;
		crd_DURLvl = 0;
		crd_CRITLvl = 0;
		crd_PROCLvl = 0;
		crd_CardLvl = 0;

		
		for(var i = 0; i < array_length(crd_StatSlotArr); i += 1)
		{
			var tSlot = crd_StatSlotArr[i];
			var tCore = global.StatCardUnlocks.stat_UnlockArr[i];
			var rivenSkew = 1.2;
			var tLvl = 1;
			
			//When using a Riven Stat, uses the Lvl of the lower stat
			if(!tCore.stat_CanRiven) tSlot.stat_AltStat = tSlot.stat_MainStat;
			if(tSlot.stat_AltStat == STAT_TYPE.NONE) tSlot.stat_AltStat = tSlot.stat_MainStat;
			var tMain = GetStatValByType(tSlot.stat_MainStat, i);
			var tAlt = GetStatValByType(tSlot.stat_AltStat, i);
			
			if(tAlt < tMain) tLvl = tAlt;
			else tLvl = tMain;
			
			switch tSlot.stat_MainStat
			{
				case STAT_TYPE.DAMAGE:
				crd_DMGLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.ATKSPEED:
				crd_SPDLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.HEALTH:
				crd_HPLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.SHIELDS:
				crd_SHDLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.DURATION:
				crd_DURLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.COOLDOWN:
				crd_CDLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.CRITRATE:
				crd_CRITLvl += tLvl*rivenSkew;
				break;
				case STAT_TYPE.PROCRATE:
				crd_PROCLvl += tLvl*rivenSkew;
				break;
			}
			rivenSkew -= 1;	//So Main and Alt stats sum out to 1
			switch tSlot.stat_AltStat
			{
				case STAT_TYPE.DAMAGE:
				crd_DMGLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.ATKSPEED:
				crd_SPDLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.HEALTH:
				crd_HPLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.SHIELDS:
				crd_SHDLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.DURATION:
				crd_DURLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.COOLDOWN:
				crd_CDLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.CRITRATE:
				crd_CRITLvl -= tLvl*rivenSkew;
				break;
				case STAT_TYPE.PROCRATE:
				crd_PROCLvl -= tLvl*rivenSkew;
				break;
			}
			/*
			crd_HPLvl += tCore.stat_HPLvl;
			crd_SHDLvl += tCore.stat_SHDLvl;
			
			crd_DMGLvl += tCore.stat_DMGLvl;
			crd_SPDLvl += tCore.stat_SPDLvl;
			
			crd_CDLvl += tCore.stat_CDLvl;
			crd_DURLvl += tCore.stat_DURLvl;
			
			crd_CRITLvl += tCore.stat_CRITLvl;
			crd_PROCLvl += tCore.stat_PROCLvl;
			*/
			
		}
		
		//Ensures you lose less points in a classes positive stat when that stat goes into negative levels
		crd_HPLvl += abs(crd_HPLvl -(crd_HPLvl*crd_HPMod));
		crd_SHDLvl += abs(crd_SHDLvl -(crd_SHDLvl*crd_SHDMod));
		crd_DMGLvl += abs(crd_DMGLvl -(crd_DMGLvl*crd_DMGMod));
		crd_SPDLvl += abs(crd_SPDLvl -(crd_SPDLvl*crd_SPDMod));
		crd_DURLvl += abs(crd_DURLvl -(crd_DURLvl*crd_DURMod));
		crd_CDLvl += abs(crd_CDLvl -(crd_CDLvl*crd_CDMod));
		crd_CRITLvl += abs(crd_CRITLvl -(crd_CRITLvl*crd_CRITMod));
		crd_PROCLvl += abs(crd_PROCLvl -(crd_PROCLvl*crd_PROCMod));
		
		
		
		crd_CardLvl = (crd_HPLvl + crd_SHDLvl + crd_DMGLvl + crd_SPDLvl
					+ crd_CDLvl + crd_DURLvl + crd_CRITLvl + crd_PROCLvl);
		crd_CardLvl *= statDiv;	//Because when your CharLvl levels up you gain 2 stat levels (1 in HP and 1 in DMG), so the statDiv is 0.5 so every 2 levels in a stat is equal to 1 level on the card, to match CharLvl calculations
	}
}


function Stat_EmptySlot() : defunc_base_StatSlot() constructor
{
	stat_currLvl = 0;
	stat_MainType = STAT_CAT.NONE;
}

function Stat_HPSlot() : defunc_base_StatSlot() constructor
{
	stat_HPMod = 1;
	stat_MainType = STAT_CAT.DEFENSE;
	mainStat = STAT_TYPE.HEALTH;
	altStat = STAT_TYPE.HEALTH;
	Init_StatSlot(self);
}
function Stat_DMGSlot() : defunc_base_StatSlot() constructor
{
	stat_DMGMod = 1;
	stat_MainType = STAT_CAT.ATTACK;
	mainStat = STAT_TYPE.DAMAGE;
	altStat = STAT_TYPE.DAMAGE;
	Init_StatSlot(self);
}
function Stat_SPDSlot() : defunc_base_StatSlot() constructor
{
	stat_SPDMod = 1;
	stat_MainType = STAT_CAT.ATTACK;
	mainStat = STAT_TYPE.ATKSPEED;
	altStat = STAT_TYPE.ATKSPEED;
	Init_StatSlot(self);
}
function Stat_SHDSlot() : defunc_base_StatSlot() constructor
{
	stat_SHDMod = 1;
	stat_MainType = STAT_CAT.DEFENSE;
	mainStat = STAT_TYPE.SHIELDS;
	altStat = STAT_TYPE.SHIELDS;
	Init_StatSlot(self);
}

function Stat_CDSlot() : defunc_base_StatSlot() constructor
{
	stat_CDMod = 1;
	stat_MainType = STAT_CAT.ABILITY;
	mainStat = STAT_TYPE.COOLDOWN;
	altStat = STAT_TYPE.COOLDOWN;
	Init_StatSlot(self);
}

function Stat_DURSlot() : defunc_base_StatSlot() constructor
{
	stat_DURMod = 1;
	stat_MainType = STAT_CAT.ABILITY;
	mainStat = STAT_TYPE.DURATION;
	altStat = STAT_TYPE.DURATION;
	Init_StatSlot(self);
}


function Stat_CRITSlot() : defunc_base_StatSlot() constructor
{
	stat_CRITMod = 1;
	stat_MainType = STAT_CAT.LUCK;
	mainStat = STAT_TYPE.CRITRATE;
	altStat = STAT_TYPE.CRITRATE;
	Init_StatSlot(self);
}


function Stat_PROCSlot() : defunc_base_StatSlot() constructor
{
	stat_PROCMod = 1;
	stat_MainType = STAT_CAT.LUCK;
	mainStat = STAT_TYPE.PROCRATE;
	altStat = STAT_TYPE.PROCRATE;
	Init_StatSlot(self);
}

function Stat_RivenSlot() : defunc_base_StatSlot() constructor
{
	stat_MainType = choose(STAT_CAT.ATTACK, STAT_CAT.DEFENSE, STAT_CAT.ABILITY, STAT_CAT.LUCK);
	switch stat_MainType
	{
		case STAT_CAT.ATTACK:
		mainStat = choose(STAT_TYPE.DAMAGE, STAT_TYPE.ATKSPEED);
		break;
		case STAT_CAT.DEFENSE:
		mainStat = choose(STAT_TYPE.HEALTH, STAT_TYPE.SHIELDS);
		break;
		case STAT_CAT.ABILITY:
		mainStat = choose(STAT_TYPE.COOLDOWN, STAT_TYPE.DURATION);
		break;
		case STAT_CAT.LUCK:
		mainStat = choose(STAT_TYPE.CRITRATE, STAT_TYPE.PROCRATE);
		break;
		default:
		break;
	}
	altStat = mainStat
	while(altStat == mainStat)
	{
		altStatType = choose(STAT_CAT.ATTACK, STAT_CAT.DEFENSE, STAT_CAT.ABILITY, STAT_CAT.LUCK);
		switch altStatType
		{
			case STAT_CAT.ATTACK:
			altStat = choose(STAT_TYPE.DAMAGE, STAT_TYPE.ATKSPEED);
			break;
			case STAT_CAT.DEFENSE:
			altStat = choose(STAT_TYPE.HEALTH, STAT_TYPE.SHIELDS);
			break;
			case STAT_CAT.ABILITY:
			mainStat = choose(STAT_TYPE.COOLDOWN, STAT_TYPE.DURATION);
			break;
			case STAT_CAT.LUCK:
			mainStat = choose(STAT_TYPE.CRITRATE, STAT_TYPE.PROCRATE);
			break;
			default:
			break;
		}
	}
	Init_StatSlot(self);
}