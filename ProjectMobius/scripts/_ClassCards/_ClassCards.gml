// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


//------------------


with global.ClassCards.crd_Gladiator
{
	array_push(crd_wep1Slots, SLOT_TYPE.MELEE_MED);
	array_push(crd_wep2Slots, SLOT_TYPE.MELEE_MED);	
}

with global.ClassCards.crd_Rogue
{
	array_push(crd_wep1Slots, SLOT_TYPE.GUN_MED);	
}