// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function buff_notice(a_txt) : base_Modifier() constructor
{
	name = a_txt;
	displayName = true;
}

function buff_evade() : base_Modifier() constructor{

	name = "Evade";
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.isPhased = true;
			subject.isDashing = true;
		}	
	}
	
	mod_Destroy = function()
	{
		
		if(CheckExistance(subject))
		{
			//subject.isPhased = false;
			subject.isDashing = false;
		}
		Destroy_Mod(self);
	}
}

function buff_invis() : base_Modifier() constructor{

	name = "Invisibility";
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.isInvis = true;
		}	
	}
	
	mod_Destroy = function()
	{
		
		if(CheckExistance(subject))
		{
			
		}
		Destroy_Mod(self);
	}
}

function buff_Mercy() : base_Modifier() constructor
{
	duration = infinity;
	name = "Mercy";
	stackLimit = 1;
}

function buff_bladeDash() : base_Modifier() constructor{

	duration = 0.3;
	name = "Blade Dash";
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.isPhased = true;
			subject.isDashing = true;
		}	
	}
	
	mod_Destroy = function()
	{
		
		if(CheckExistance(subject))
		{
			//subject.isPhased = false;
			subject.isDashing = false;
		}
		Destroy_Mod(self);
	}
}

function buff_hitInvul() : base_Modifier() constructor{

	name = "Invulnerable";
	displayName = true
	blinkVal = 0;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			
			subject.isPhased = true;
			
			blinkVal += GetDeltaTime(timeScale*10);
			if(blinkVal >= (pi * 2)) blinkVal = 0;
			subject.image_alpha = 1 - sin(blinkVal);
		}	
	}
	
	mod_Destroy = function()
	{
		
		if(CheckExistance(subject))
		{
			//subject.isPhased = false;
			subject.image_alpha = 1;
		}
		Destroy_Mod(self);
	}
}


function buff_sprint() : base_Modifier() constructor{

	name = "Sprint";
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			if((abs(subject.mx) + abs(subject.my)) < 0.9) duration = 0;
			else 
			{
				duration = infinity;
				subject.isSprinting = true;
			}
		}	
	}
	
	mod_Destroy = function()
	{
		
		if(CheckExistance(subject))
		{
			subject.isSprinting = false;
		}
		Destroy_Mod(self);
	}
}

function buff_AtkSpdUp() : base_Modifier() constructor{

	name = "AtkSpd-Up";
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		mod_SPD = stacks*1.5;
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_AltLoadout() : base_Modifier() constructor{

	name = "Alt Loadout";
	displayName = false;
	stackLimit = 1;
	mod_ld = new Loadout();
	
	mod_Update = function()
	{
		if(duration != 0)
		{
			subject.char_hasAltLoadout = true;
			subject.char_AC.ac_altbpLd = mod_ld;
		}
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_staminaSurge() : base_Modifier() constructor{

	name = "Stamina Surge"
	displayName = true;
	stackLimit = 3;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.staminaStat += ((subject.staminaRegen/2)*(GetDeltaTime(subject.timeScale)))*stacks;
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_hpRegen() : base_Modifier() constructor{

	name = "HP Regen"
	displayName = true;
	stackLimit = 200;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.hp += ((subject.char_maxHP/100)*stacks) * GetDeltaTime(timeScale);
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_lifeSteal() : base_Modifier() constructor
{
	name = "Life-Steal"
	displayName = true;
	stackLimit = 1000;
	duration = infinity;
	stackType = STACK_TYPE.ALL;
	
	mod_OnHit = function(a_Report)
	{
		if(CheckExistance(subject))
		{
			var tScale = stacks/100;
			subject.vitStat += a_Report.hit_Dmg * tScale;	
		}
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_critBuff() : base_Modifier() constructor{

	name = "Crit-Up"
	displayName = true;
	stackLimit = 4;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			mod_CRIT = stacks;
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}


function buff_ArmourRegen() : base_Modifier() constructor{

	name = "Armour Regen"
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			subject.char_armStat += ((subject.char_maxArm/25)*(GetDeltaTime(subject.timeScale)))*stacks;
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_StormDrake() : base_Modifier() constructor
{
	name = "Storm Drake";
	displayName = true;
	stackLimit = 99;
	duration = 15;
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_BoomerBomb() : base_Modifier() constructor
{
	name = "Boomber Bomb";
	displayName = false;
	stackLimit = 1;
	duration = infinity;
	mod_OnDeath = function()
	{
		CreateProjectile(tExplosion, subject.char_AC.ac_activeLoadout.ld_slots.ld_wep1, subject.char_damageStat, subject, 0, 1);
	}
}

function buff_KobaldTunnel() : base_Modifier() constructor
{
	name = "Kobold Tunnel";
	displayName = false;
	duration = 0.01;
	stackLimit = 1;
	mod_Init = function()
	{
		CreateProjectile(dmg_SlashSpin, subject.char_AC.ac_activeLoadout.ld_slots.ld_wep1, owner.char_damageStat*4, owner, owner.direction, 1);
	}
	mod_Update = function()
	{
		if(subject != owner)
		{
			subject.x = owner.x;
			subject.y = owner.y;
		}
	}
	
	mod_Destroy = function()
	{
		CreateProjectile(dmg_SlashSpin, subject.char_AC.ac_activeLoadout.ld_slots.ld_wep1, owner.char_damageStat*4, owner, owner.direction, 1);
		Destroy_Mod(self);
	}
}

function buff_KobaldTrio() : base_Modifier() constructor
{
	name = "Kobald Trio";
	displayName = false;
	duration = infinity;
	stackLimit = 2;
	
	kbld_Chars = [];
	
	mod_Init = function()
	{
		for(var n = 0; n < array_length(kbld_Chars); n += 1)
		{
			if(CheckExistance(kbld_Chars[n])) instance_destroy(kbld_Chars[n]);
			n -= 1;
		}
		array_clear(kbld_Chars);
		for(var i = 0; i < stacks; i += 1)
		{
			var tChar = SpawnCharacter(char_Kobald, subject, undefined, subject.teamID);
			ds_list_add(global.coreAllyList, tChar);
			
			CopyLoadout(tChar, subject);
			
			RefreshLoadout(tChar);
			
			array_push(kbld_Chars, tChar);
		}
		
	}
	
	mod_Update = function()
	{
		for(var i = 0; i < array_length(kbld_Chars); i += 1)
		{
			var tChar = kbld_Chars[i];	
			tChar.char_healthLvl = subject.char_healthLvl;
			tChar.char_damageLvl = subject.char_damageLvl;
			tChar.char_shieldLvl = subject.char_shieldLvl;
			tChar.char_spdLvl = subject.char_spdLvl;
			tChar.char_CDLvl = subject.char_CDLvl;
			tChar.char_durLvl = subject.char_durLvl;
			tChar.char_critLvl = subject.char_critLvl;
			tChar.char_procLvl = subject.char_procLvl;
			
			if(CompareLoadout(tChar, subject) == false)
			{
				CopyLoadout(tChar, subject);
				RefreshLoadout(tChar);
			}
			
		}
	}
}