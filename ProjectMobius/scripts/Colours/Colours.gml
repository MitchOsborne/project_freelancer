// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

global.shaderData = 
{
	shdr_pixelH : shader_get_uniform(shdr_outline, "pixelH"),
	shdr_pixelW : shader_get_uniform(shdr_outline, "pixelW"),
	shdr_color : shader_get_uniform(shdr_outline, "outlineColor"),	
	shdr_texelH : undefined,
	shdr_texelW : undefined,
}


// Color Format BB-GG-RR for whatever reason
// For Ref______RR-GG-BB
globalvar c_psiDPurple;
globalvar c_psiLPurple;
c_psiDPurple = $ff0033;
c_psiLPurple = $ff0088;

globalvar c_osCyan;
globalvar c_osPurple1;
globalvar c_osPurple2;

c_osCyan    = $ff5555;
c_osPurple1 = $7700ff;
c_osPurple2 = $ff00ff;


globalvar c_player1Blue;
globalvar c_player2Yellow;
globalvar c_allyGreen;
globalvar c_enemyRed;

c_player1Blue = $ff8800;
c_player2Yellow = $00c8ff;
c_allyGreen = $33ff33;
c_enemyRed = $0000ff;

globalvar c_Gold;
c_Gold = $1ac9c9;

globalvar c_None;
c_None = $000000;

globalvar c_ltRed;
c_ltRed = $8888FF;

globalvar c_hands;
c_hands = $9ac3ee

globalvar c_gloves;
c_gloves = $342022