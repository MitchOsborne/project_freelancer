// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.PlayerColArr = 
[
c_player1Blue,
c_player2Yellow,
c_allyGreen
]

function AddPlayer(a_char, a_input)
{
	var tI = ds_list_size(global.PlayerIDList);
	var tPlayer = new Player(a_char, tI);
	tPlayer.inputSource = a_input;
	ds_list_add(global.PlayerIDList, tPlayer);
	if(tI == 0)
	{
		global.Player1ID = tPlayer;
		global.Player1Char = tPlayer.slavedObj;
	}
}

function Player(a_char, a_index) : base_Player() constructor
{
	
	PlayerIndex = a_index;
	
	slavedObj = a_char;
	slavedObj = SpawnCharacter(slavedObj,instance_find(entry_default, 0),self, teamID);	
	ds_list_add(global.coreAllyList, slavedObj);
	SanityCheckCharStats(slavedObj);
	slavedObj.hp = slavedObj.char_maxHP;
	slavedObj.PlayerControlled = true;
	
	color = global.PlayerColArr[clamp(a_index, 0, 2)];
	aimReticle = instance_create_layer(0,0,"Instances",base_AimReticle);
	aimReticle.retCol = color;
	
	if(a_index == 0) uiOffset = new Vector2(60, 200);
	else uiOffset = new Vector2(650, global.view_height/4);
}

function Player1(a_char, a_index) : base_Player() constructor{
	
	global.Player1ID = self;
	
	PlayerIndex = a_index;
	
	slavedObj = a_char;
	slavedObj = SpawnCharacter(slavedObj,instance_find(entry_default, 0),self, teamID);	
	ds_list_add(global.coreAllyList, slavedObj);
	ds_list_add(global.PlayerIDList, slavedObj);
	SanityCheckCharStats(slavedObj);
	slavedObj.hp = slavedObj.char_maxHP;
	slavedObj.PlayerControlled = true;
	
	global.Player1Char = slavedObj;
	color = c_player1Blue;
	aimReticle = instance_create_layer(0,0,"Instances",tAimReticleP1);
	
	uiOffset = new Vector2(60, 200);
}

function Player2(a_char) : base_Player() constructor{

	global.Player2ID = self;

	
	slavedObj = a_char;
	slavedObj = SpawnCharacter(slavedObj,instance_find(entry_default, 0),self, teamID);	
	ds_list_add(global.coreAllyList, slavedObj);
	SanityCheckCharStats(slavedObj);
	
	slavedObj.hp = slavedObj.char_maxHP;
	if(global.P2Enabled)
	{
		slavedObj.active_operator = id;
		slavedObj.PlayerControlled = true;
	}else
	{
		slavedObj.PlayerControlled = false;
		slavedObj.active_operator = slavedObj.ai_operator;	
	}
	
	global.Player2Char = slavedObj;
	aimReticle = instance_create_layer(0,0,"Instances",tAimReticleP2);
	color = c_player2Yellow;
	
	
	uiOffset = new Vector2(650, global.view_height/4);
}