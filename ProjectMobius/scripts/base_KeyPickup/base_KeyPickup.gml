// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_KeyPickup() : base_Pickup() constructor{
	
	lifeTime = 300;
	blinkTime = 10;
	
	
	moveSpeed = 10;
	pickupRange = 64;
	
	keyType = 0;
	func_EndUpdateStep = Key_EndUpdateStep;
}

function Key_EndUpdateStep(a_pkup)
{
	with a_pkup
	{	
		if(isCollected)
		{
			if(keyType == "small") global.sKeyCount += 1;
			lifeTimer = lifeTime;
		}
		Pickup_EndUpdate(self);
	}
}