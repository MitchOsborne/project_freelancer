// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum TEAMID
{
	Ally,
	Neutral,
	Enemy,
	Other,	//Used for targeting opposing teams
	Same,	//USed for targeting members of the same team
}


//Stat Growths per Lvl

//Stat Archetypes
//global.statArch_Shooter
global.statArch_Shooter = 
{	
	HP : 2,
	dmg : 2,
}

global.statArch_Fighter = 
{
	HP : 4,
	dmg : 1,
}

//global.statArch_Runner
global.statArch_Runner = 
{
	HP : 2,
	dmg : 1,
}

//global.statArch_Swarm
global.statArch_Swarm = 
{
	HP : 1,
	dmg : 1,
}

//global.statArch_Heavy
global.statArch_Heavy = 
{
	HP : 8,
	dmg : 2,
}

global.statArch_FlamerBoss = 
{
	HP: 10,
	dmg : 1,
}

//Player Characters

//Kamui
global.statArch_Kamui =
{
	HP : 4,
	dmg : 1.125,
}

//Shepherd
global.statArch_Shepherd =
{
	HP : 4.5,
	dmg : 1,
}




//-----------------------------//
//Unused

//Kobald
global.statArch_KobaldTrio =
{
	HP : 3,
	dmg : 1,
}

//Paladin
global.statArch_Paladin =
{
	HP : 6,
	dmg : 1,
}


//Psion
global.statArch_Psion =
{
	HP : 9,
	dmg : 3,
}

/*
//Mech
globalvar Mech;
Mech = 
{
	HP : 8,
	dmg : 1.5,
}
*/


