// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function RadiusCheck(inst, radius, targetObj, sightRequired)
{

	
	var hitList = ds_list_create();
	
	
	//collision_circle_list(inst.x,inst.y,radius,targetObj,false,true,hitList,false);
	collision_rectangle_list(x + radius/4, y + radius/4, x - radius/4, y - radius/4, targetObj,false,true, hitList, false);
	
	
	if(sightRequired)
	{
		for(var i = 0; i < ds_list_size(hitList); i++)
		{
			var tInst = hitList[|i];
			if collision_line(inst.x,inst.y,tInst.x,tInst.y,base_PhysObstacle,false,true)
			{
				ds_list_delete(hitList,i);
				i -= 1;
			}
		}
	}
	
	return hitList;
	
}



function SightCheck(inst, targetObj, sightRequired)
{
		
	
	var isVis = false;
	var tDir = GetDirection(inst.x,inst.y,targetObj.x,targetObj.y,false,true);
	var tDiff = angle_difference(tDir, (targetObj.direction))
	if(abs(tDiff) > (targetObj.sightRadius/2))
	{
		if (sightRequired && collision_line(inst.x,inst.y,targetObj.x,targetObj.y,base_PhysObstacle,false,true))
		{
			isVis = true;
		}
	}
		
	return isVis;
}

function SightCheckList(inst, aList, sightDegrees, sightDir, sightRequired)
{
	var hitList = ds_list_create();
	ds_list_copy(hitList, aList);
	
		for(var i = 0; i < ds_list_size(hitList); i++)
	{
		var del = false;
		var tInst = hitList[|i];
		var tDir = GetDirection(inst.x,inst.y,tInst.x,tInst.y,false,true);
		var tDiff = angle_difference(tDir, (sightDir))
		if(abs(tDiff) > (sightDegrees/2))
		{
			del = true;
		}
		if (sightRequired && collision_line(inst.x,inst.y,tInst.x,tInst.y,base_PhysObstacle,false,true))
		{
			del = true;
		}
		
		if(del)
		{
			ds_list_delete(hitList,i);
			i -= 1;	
		}
	}
	
	return hitList;
}