// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_WoodsRoom() : base_RoomData() constructor
{
	rd_region = REGION.DESERT;
	rd_BGM = bgm_Woods;
	rd_ambAlpha = 0;
	rd_EnemyLevel = 10;
}

function roomData_Woods1() : base_WoodsRoom() constructor
{
	rd_roomID = "Woods1";
	rd_roomObj = t_room_cave_camp;
	array_push(rd_tExit,roomData_Woods2);
	array_push(rd_rExit,roomData_ComplexMidBoss);
}

function roomData_Woods2() : base_WoodsRoom() constructor
{
	rd_roomID = "Woods2";
	rd_roomObj = t_room_desert_2;
	array_push(rd_bExit,roomData_CaveEntry2);
	array_push(rd_tExit,roomData_Woods3);
	array_push(rd_tExit,roomData_SolarFarm1);
	rd_events = [new rmEvent_Sand()];
}

function roomData_Woods3() : base_WoodsRoom() constructor
{
	
	rd_roomID = "Woods3";
	rd_roomObj = t_room_desert_3;
	array_push(rd_bExit, roomData_Woods2);
	array_push(rd_tExit, roomData_Desert1);
	array_push(rd_rExit, roomData_WoodsComplex);
}

function roomData_SolarFarm1() : base_DesertRoom() constructor
{
	rd_roomID = "SolarFarm1";
	rd_roomObj = t_room_solarfarm_1;
	array_push(rd_bExit, roomData_Woods2);
	array_push(rd_tExit, roomData_Desert1);
}

function roomData_WoodsComplex() : base_WoodsRoom() constructor
{
	rd_roomID = "WoodsComplex";
	rd_region = REGION.YAKIBASE;
	rd_roomObj = t_room_desert_complex;
	array_push(rd_lExit, roomData_Woods3);
	array_push(rd_rExit, roomData_Complex1);
}
