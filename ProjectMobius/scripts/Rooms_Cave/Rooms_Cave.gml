// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_CaveRoom() : base_RoomData() constructor
{
	rd_region = REGION.CAVE;
	rd_BGM = bgm_Cave;
	rd_Outdoors = false;
	rd_EnemyLevel = 1;
	rd_ambAlpha = 1;
	rd_ambColour = c_black;
}

function roomData_CaveCamp() : base_CaveRoom() constructor
{
	rd_Outdoors = true;
	rd_roomID = "CaveCamp";
	rd_roomObj = t_room_cave_camp;
	
	rd_events = [new rmEvent_Intro(), new rmEvent_Test()];
}

	
function roomData_CaveEntry1() : base_CaveRoom() constructor
{
	rd_roomID = "CaveEntry1";
	rd_roomObj = t_room_cave_entry_1;
}

function roomData_CaveEntry2() : base_CaveRoom() constructor
{
	rd_roomID = "CaveEntry2";
	rd_roomObj = t_room_cave_entry_2;
}

function roomData_CaveEntry3() : base_CaveRoom() constructor
{
	rd_roomID = "CaveEntry3";
	rd_roomObj = t_room_cave_entry_3;
}

function roomData_CaveEntry4() : base_CaveRoom() constructor
{
	rd_roomID = "CaveEntry4";
	rd_roomObj = t_room_cave_entry_4;
}

