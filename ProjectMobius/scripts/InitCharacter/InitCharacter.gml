
function SpawnCharacter(a_char, a_pos, a_operator, a_teamID)
{
	var tInst = instance_create_layer(a_pos.x,a_pos.y,"Instances", a_char);
	if(a_operator != undefined)	tInst.active_operator = a_operator;
	tInst.teamID = a_teamID;
	
	if(a_teamID != TEAMID.Ally)
	{
		tInst.charLvl = global.enemyLevel + global.worldLvl;
		tInst.diffScaling = global.enemyIntensity;
		tInst.teamID = a_teamID;	
	}
		
	if(tInst.teamID == TEAMID.Ally) ds_list_add(global.allyList,tInst);
	else if(tInst.teamID == TEAMID.Enemy && a_char != Dummy) ds_list_add(global.enmList,tInst);
	

	/*
	if(smn_Owner != undefined)
	{
		smn_Owner.smn_Minion = tInst;
		tInst.smn_Master = smn_Owner.owner;
		smn_Owner.smn_SummoningMinion = false;
		UpdateSummonStats(tInst);
	}
	*/
	
	SanityCheckOperator(tInst);
	SanityCheckLoadout(tInst);
	SanityCheckCharStats(tInst);
	tInst.hp = tInst.char_maxHP;
	tInst.char_shdStat = tInst.char_maxShd;
	tInst.char_armStat = tInst.char_maxArm;
	tInst.charData = new CharData(tInst.char_Name, a_char, tInst.animator, tInst.char_AC.ac_bpLd);
	return tInst;
}
