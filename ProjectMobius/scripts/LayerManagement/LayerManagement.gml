// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SanityCheckLayers()
{
	if(!layer_exists("Interactables"))layer_create(500, "Interactables");	
	if(!layer_exists("Debug"))layer_create(-500, "Debug");	
	
}