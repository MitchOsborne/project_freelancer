// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum ABILITY_TYPE
{
	NONE,
	RANGED,
	MELEE,
	ABILITY,
	UTILITY,
}


enum ABL_ANIM_STATE
{
	NONE,
	RELOAD,
	EQUIP,
	FLOURISH,
	ATK1,
	ATK2,
	ATK3,
	ATK4,
}

enum CLASS_TYPE
{
	NONE,
	FIGHTER,
	HUNTER,
	ENGINEER,
	DYNAMIST,
}

enum SLOT_TYPE
{
	NONE,
	EMPTY,
	GUN,
	GUN_LIGHT,
	GUN_MED,
	GUN_HEAVY,
	MELEE,
	MELEE_LIGHT,
	MELEE_MED,
	MELEE_HEAVY,
	WEAPON,
	UTILITY,
	EVADE,
	ABILITY1,
	ABILITY2,
	CORE,
	CLASS,
}

enum LOADOUT_SLOT
{
	NONE,
	ALL,
	WEAPON1,
	WEAPON2,
	UTILITY1,
	UTILITY2,
	ABILITY1,
	ABILITY2,
	CORE,
	CLASS,
}

function base_Ability(a_bp) constructor
{
	
	abl_AbilityType = a_bp.bp_AbilityType;
	abl_animState = ABL_ANIM_STATE.NONE;
	abl_animLerp = 0;
	abl_currAtk = undefined;
	abl_DmgType = a_bp.bp_DMGType;
	
	owner = undefined;
	abl_controller = undefined;
	abl_blueprint = a_bp;
	abl_intrinsic = undefined;
	abl_Archetype = undefined;
	abl_DisplayActive = a_bp.bp_DisplayActive;	//Whether the ability overrides the active weapon when used
	abl_StayActive =  a_bp.bp_StayActive;	//Whether the ability becomes the active weapon when used
	abl_isSilent = true;
	
	abl_Components = [];
	
	abl_bpStats = BP_CalcStats(a_bp);
	abl_ammo = new abl_comp_Ammo(self, abl_bpStats.bp_Ammo, 1, abl_bpStats.bp_ReloadSpd, abl_bpStats.bp_ReloadAmount, false);
	array_push(abl_Components, abl_ammo);
	abl_anim = undefined;
	abl_fireRate = new abl_comp_Firerate(self);
	abl_equip = new abl_comp_EquipWeapon(self, abl_bpStats.bp_EquipTime);
	array_push(abl_Components, abl_equip);
	ablName = a_bp.bp_Name;
	ablCost = 1000;
	
	abl_Icon = spr_error;
	
	inPressed = false;
	forcePressed = false;

	staminaCost = 0;
	baseStaminaCost = 0;
	
	abl_sfx = undefined;
	sfxArr = [];
	sfxIndex = 0;

	cooldown = 0;
	RPM = 0;
	cooldownTimer = 0;
	activate = false;
	didActivate = false;
	
	abl_powerMultiplier = 1;
	abl_CalcPowerScale = 1;
	abl_CalcSpeedScale = 1;
	abl_CalcDurScale = 1;
	abl_CalcCDScale = 1;
	
	resourceDisplay = 0;
	cdDisplay = 0;


	abl_posOffset = new Vector2(0,0);
	abl_dirOffset = 0;

	ai_engageRange = 10000;
	ai_engagePriority = -1;
	ai_reqRelease = false;
	ai_CanAttack = true;
	
	ai_forceUsage = false;

	ai_abl_Offensive = 1;
	ai_abl_Defensive = 1;
	ai_abl_Support = 1;
	
	ai_Score = -1;
	ai_PrepTime = 0;

	ai_abl_taskList = ds_list_create();

	ai_method = new AI_Method(ai_abl_Offensive, ai_abl_Defensive, ai_abl_Support, ai_abl_taskList);
	
	
	func_PreUpdateStep = AbilityPreUpdateStep;
	func_UpdateStep = AbilityUpdateStep;
	func_EndUpdateStep = AbilityEndUpdateStep;
	func_InitAbility = DefaultInitialise;
	func_DrawStep = AbilityDrawStep;
	
	abl_forceStrength = 0;
	abl_stunDur = 0;
	abl_LockDur = 0;
	abl_IgnoreLock = false;
	abl_IgnoreEquip = false;
	
	abl_canReload = false;
	abl_isOverride = false;
	
	abl_Instances = ds_list_create();
	abl_hitReports = [];
	abl_killReports = [];
	
	abl_OnHitProcs = [];
	abl_OnKillProcs = [];
	
	abl_AnimAlerts = [];
		
	abl_endProc = false;

	if(a_bp.bp_SpriteSet != undefined)	
	{
		abl_anim = new abl_comp_Animation(self, a_bp.bp_SpriteSet)
		//array_push(abl_Components, abl_anim);
	}
	array_push(abl_Components, new abl_comp_Audio(self));
	//array_push(abl_Components, abl_ammo);
	
	
}


function base_PrimaryFire() : base_Ability() constructor
{
	
}

function DefaultInitialise(a_ability)
{
	with a_ability
	{
		
		ai_engageRange = abl_bpStats.bp_Range;
		
		if(abl_blueprint.bp_DMGType == DMG_TYPE.SOFT) array_push(abl_Components, new abl_comp_KO(self));
				
		/*
		if(array_length(abl_blueprint.bp_AttackArr) > 0)
		{
			var tComp = undefined;
			tComp = new abl_comp_AttackData(self, abl_blueprint.bp_AttackArr, abl_blueprint.bp_AttackFin);
			array_push(abl_Components, tComp);	
		}
		*/
		
		if(abl_blueprint.bp_Archetype == undefined) abl_blueprint.bp_Archetype = global.archDefault;
		abl_controller = owner.char_AC;
		
		for(var i = 0; i < array_length(abl_Components); i += 1)
		{
			var tComp = abl_Components[i];
			tComp.abl_comp_Init(self, tComp);
		}
		
		if(abl_anim != undefined) abl_anim.abl_comp_Init(self, abl_anim);
	}
}

function AbilityDrawStep(a_ability)
{
	with a_ability
	{
		for(var i = 0; i < array_length(abl_Components); i += 1)
		{
			var tComp = abl_Components[i];
			tComp.abl_comp_Draw();
		}
		/*
		
		if(abl_ammo != undefined)
		{
			abl_ammo.abl_comp_Draw();
		}
		*/
		if(abl_anim != undefined)
		{
			abl_anim.abl_comp_Draw();
		}
		
	}
}

function AbilityUpdateStep(a_ability)
{
	with a_ability
	{
		if(activate)
		{
			for(var i = 0; i < array_length(abl_Components); i += 1)
			{
				var tComp = abl_Components[i];
				tComp.abl_comp_OnActivate();
				owner.char_AC.ac_activeAbility = self;
			}
			/*
			if(abl_ammo != undefined)
			{
				abl_ammo.abl_comp_OnActivate();
			}
			*/
			if(abl_anim != undefined)
			{
				abl_anim.abl_comp_OnActivate();
			}
			
		}
		
		if(abl_endProc)
		{
			abl_endProc = false;
			for(var i = 0; i < array_length(abl_Components); i += 1)
			{
				var tComp = abl_Components[i];
				tComp.abl_comp_OnAblEnd();
			}
			/*
			if(abl_ammo != undefined)
			{
				abl_ammo.abl_comp_OnAblEnd();
			}
			*/
			if(abl_anim != undefined)
			{
				abl_anim.abl_comp_OnAblEnd();
			}
			
		}
		//abl_CycleAudio(self);
	}
}


function AbilityPreUpdateStep(a_ability)
{
	with a_ability
	{
		abl_dirOffset = 0;	//reset so multiple components can write to this at once
		activate = true;
		timeScale = owner.timeScale;
		abl_powerMultiplier = abl_bpStats.bp_DMGScale * (1 + (abl_blueprint.bp_Archetype.arch_Level * 4)/100);	//Used so stuff like props can get raw damage scaling before character stats
		abl_CalcPowerScale = (owner.char_damageStat * abl_powerMultiplier)			//Damage Stat of character is flat, and multiplied by Ability DMGScale
		abl_CalcSpeedScale = 1 + (owner.char_spdStat* abl_bpStats.bp_SPDScale);
		abl_CalcDurScale = 1 + (owner.char_durStat * abl_bpStats.bp_DURScale);
		abl_CalcCDScale = 1 + (owner.char_CDStat * abl_bpStats.bp_CDScale);
		ai_CanAttack = true;
		abl_animState = ABL_ANIM_STATE.NONE;
		abl_animLerp = -1;
		if(!owner.isIncap)
		{
			for(var i = 0; i < array_length(abl_Components); i += 1)
			{
				var tComp = abl_Components[i];
				if(tComp.abl_comp_Update() == false) activate = false;
			}
			/*
			if(abl_ammo != undefined)
			{
				if(abl_ammo.abl_comp_Update() == false) activate = false;
			}
			*/
			if(abl_anim != undefined)
			{
				if(abl_anim.abl_comp_Update() == false) activate = false;
			}
			
		}
		//if(abl_currAtk != undefined) abl_sfx = abl_currAtk.atk_sfx;
	}
}




function AbilityEndUpdateStep(a_ability)
{
	with a_ability
	{
		//inPressed = false;
		if(didActivate)
		{
			
			for(var i = 0; i < array_length(abl_ModsToSelf); i += 1)
			{
				AttachMod(abl_ModsToSelf[i], owner, owner, undefined);
			}
						
			if(abl_LockDur > 0)
			{
				AttachMod(debuff_abilityLocked, owner, owner, abl_LockDur);
				owner.abl_ActiveAbility = self;
			}
			
			if(abl_sfx != undefined)
			{
				audio_play_sound(abl_sfx,5,false);
			}
		}
		
		
		//Runs On-Hit Procs
		for(var a = 0; a < array_length(abl_Components); a += 1)
		{
			for(var b = 0; b < array_length(abl_hitReports); b += 1)
			{
				abl_Components[a].abl_comp_OnHit(abl_hitReports[b]);	
			}
		}
		
		
		
		//Runs On-Kill Procs
		for(var a = 0; a < array_length(abl_Components); a += 1)
		{
			for(var b = 0; b < array_length(abl_killReports); b += 1)
			{
				abl_Components[a].abl_comp_OnKill(abl_killReports[b]);
			}
		}
		
		//Runs Animation Alert Procs
		
		if(owner.char_AC.ac_activeWeapon == self
		||owner.char_AC.ac_activeAbility == self)
		{
			for(var n = 0; n < array_length(abl_AnimAlerts); n += 1)
			{
				for(var a = 0; a < array_length(abl_Components); a += 1)
				{
					var tM = abl_AnimAlerts[n];
					switch (tM)
				    {
						case "FireStart":
							abl_Components[a].abl_comp_OnFireStart();
				        break;
				        case "FireTrigger":
				            abl_Components[a].abl_comp_OnFireTrigger();
				        break;
						case "FireCycle":
				            abl_Components[a].abl_comp_OnFireCycle();
				        break;
						case "FireEnd":
				            abl_Components[a].abl_comp_OnFireEnd();
				        break;
						case "AudioCycleLow":
							audio_play_sound(sfx_Cycle_Low,5, false, 0.2);
						break;
						case "AudioCycleMid":
							audio_play_sound(sfx_Cycle_Mid,5, false, 0.05);
						break;
						case "AudioCycleHigh":
							audio_play_sound(sfx_Cycle_High,5, false, 0.05);
						break;
				    }
				}
			}
		}
		array_delete(abl_AnimAlerts,0,array_length(abl_AnimAlerts));
		array_delete(abl_hitReports,0,array_length(abl_hitReports));
		array_delete(abl_killReports,0,array_length(abl_killReports));
	}
}

function abl_CycleAudio(a_wep)
{
	
	with a_wep
	{
		if(didActivate)
		{
			var tSize = array_length(sfxArr) 
			sfxIndex = clamp(sfxIndex,0,tSize-1)
			if(tSize > 0)
			{
				abl_sfx = sfxArr[sfxIndex];
				sfxIndex += 1;
				
				if(sfxIndex >= tSize) sfxIndex = 0;
				
			}
		}
	}
}

function abl_Empty(a_bl) : base_Ability(a_bl) constructor
{
	array_push(abl_Components, new abl_comp_Empty(self));
}

