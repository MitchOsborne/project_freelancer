// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_StrutARoom() : base_RoomData() constructor
{
	rd_region = REGION.STRUT_F_INNER;
	rd_BGM = bgm_YakiBase;
	rd_Outdoors = false;
	rd_ambAlpha = 0;
	rd_ambColour = c_black;
	rd_EnemyLevel = 10;
}

function roomData_Strut_Entrance() : base_StrutARoom() constructor
{
	rd_roomID = "StrutStart";
	rd_roomObj = room_strut_entrance;
}


function roomData_Strut_ShaftTop() : base_StrutARoom() constructor
{
	rd_roomID = "StrutShaftTop";
	rd_roomObj = room_strut_entrance2;
	//array_push(rd_tExit,roomData_Strut_Start);
	//array_push(rd_bExit,roomData_Strut_Rooftop);
	
}

function roomData_Strut_Rooftop() : base_StrutARoom() constructor
{
	rd_roomID = "StrutRooftop";
	rd_Outdoors = true;
	rd_roomObj = room_strut_rooftop;
}

function roomData_Strut_Bld_A() : base_StrutARoom() constructor
{
	rd_roomID = "StrutBldA";
	rd_roomObj = room_strut_rooftop_bld_a;
	
}

function roomData_Strut_Bld_B_Hall() : base_StrutARoom() constructor
{
	rd_roomID = "StrutBldBHall";
	rd_roomObj = room_strut_rooftop_bld_b_hall;
	
}
function roomData_Strut_Bld_B_Rooms() : base_StrutARoom() constructor
{
	rd_roomID = "StrutBldBRooms";
	rd_roomObj = room_strut_rooftop_bld_b_rooms;
	
}

function roomData_Strut_Bld_C_Midboss() : base_StrutARoom() constructor
{
	rd_roomID = "StrutBldCMidboss";
	rd_roomObj = room_strut_rooftop_bld_c_midboss;
	
	
	rd_events = [new rmEvent_FlamerBoss()];
}

