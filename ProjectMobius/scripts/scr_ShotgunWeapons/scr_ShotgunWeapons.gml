// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function wep_SBShotgun() : defunc_base_wep_Shotgun() constructor {
	ablCost = 50;
	ablName = "SB Shotgun"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	reqRelease = true;
	
	range *= 1.1;
	damageMultiplier *= 0.7;
	weaponSprite = spr_wep_DBShotgun;
	
	wep_projectileSpread *= 0.3;
	RPM = 600;
}


function wep_SPAS12() : defunc_base_wep_Shotgun() constructor {
	ablCost = 500;
	ablName = "SPAS-12"
	ammoMax = 6;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 0.5;
	
	range *= 1.3;
	damageMultiplier *= 0.5;
	weaponSprite = spr_wep_SPAS;
	
	wep_projectileSpread *= 0.7;
	RPM = 170;
}

function wep_Stryker() : defunc_base_wep_Shotgun() constructor {
	ablCost = 500;
	ablName = "Stryker"
	ammoMax = 15;
	ammoCount = ammoMax;
	reloadAmount = 1;
	reloadTime = 0.5;
	
	range *= 1;
	damageMultiplier *= 0.3;
	weaponSprite = spr_Stryker;
	
	wep_projectileSpread *= 0.7;
	RPM = 380;
}
