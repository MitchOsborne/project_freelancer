// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.StoryProgress =
{
	story_Intro : false,
	story_Sand : false,
	story_Test : false,
	boss_Flamer : false,
}

function CreateSequence(a_seq, a_pos, a_layer)
{
	var tSeq = layer_sequence_create(a_layer, a_pos.x, a_pos.y ,a_seq);
	array_push(global.SequenceArr, tSeq);
	return tSeq;
}

function DestroySequence(a_seq)
{
	layer_sequence_destroy(a_seq);
	a_seq = undefined;
}

function GetEventMarker(a_id)
{
	var retVal = undefined;
	for(var i = 0; i < instance_number(event_Marker); i += 1)
	{
		//Ensures an event marker is returned in the event a bad id is passed in
		retVal = instance_find(event_Marker, i);
		if (retVal.em_ID == a_id)break;
	}
	return retVal;
}

function RoomEvent() constructor
{
	rmEvent_isComplete = false;
	rmEvent_IsActive = false;
	rmEvent_Trigger = function() {return false;};
	rmEvent_OnTrigger = function() {};
	rmEvent_EventActive = function() {};
	rmEvent_EventInactive = function() {};
	rmEvent_OnEventEnd = function() 
	{
		rmEvent_IsActive = false;
		rmEvent_isComplete = true;
	};
}

function rmEvent_Intro() : RoomEvent() constructor
{
	camStartPos = new Vector2(0,0);
	camPanDur = 5;
	camPanTmr = 0;
	rmEvent_DialogueChain = undefined;
	rmEvent_Trigger = function() {return !global.StoryProgress.story_Intro;};
	rmEvent_OnTrigger = function()
	{
		rmEvent_IsActive = true;
		var tEM = GetEventMarker(0);
		camStartPos = new Vector2(tEM.x, tEM.y);
		CameraMarker.x = camStartPos.x;
		CameraMarker.y = camStartPos.y;
		CameraMarker.override = true;
		CameraMarker.overrideTarget = tEM;
	}
	rmEvent_EventActive = function()
	{
		camPanTmr += GetDeltaTime(1);
		camPanTmr = clamp(camPanTmr, 0, camPanDur);
		CameraMarker.override = true;
		var tLerp = camPanTmr/camPanDur;
		var tEM = GetEventMarker(1);
		var tY = lerp(camStartPos.y, tEM.y, tLerp);
		CameraMarker.overrideTarget = new Vector2(camStartPos.x, tY);
		
		if(camPanTmr >= camPanDur)
		{
			if(rmEvent_DialogueChain == undefined) rmEvent_DialogueChain = new DialogueChain(global.DialogueChains.dlg_Opening, self);
			global.StoryProgress.story_Intro = true;
		}
	}
	rmEvent_OnEventEnd = function() 
	{
		rmEvent_IsActive = false;
		rmEvent_isComplete = true;
		CameraMarker.override = false;	
	};
}	

function rmEvent_Sand() : RoomEvent() constructor
{
	rmEvent_DialogueChain = undefined;
	rmEvent_Trigger = function() {return !global.StoryProgress.story_Sand;};
	rmEvent_OnTrigger = function()
	{
		rmEvent_IsActive = true;
		
	}
	rmEvent_EventActive = function()
	{
		if(rmEvent_DialogueChain == undefined) rmEvent_DialogueChain = new DialogueChain(global.DialogueChains.dlg_Sand, self);
		global.StoryProgress.story_Sand = true;
		
	}
	rmEvent_OnEventEnd = function() 
	{
		rmEvent_IsActive = false;
		rmEvent_isComplete = true;
	};
}

function rmEvent_Test() : RoomEvent() constructor
{
	rmEvent_DialogueChain = undefined;
	rmEvent_Trigger = function() 
	{
		return (!global.StoryProgress.story_Test && global.StoryProgress.story_Sand);
	};
	rmEvent_OnTrigger = function()
	{
		rmEvent_IsActive = true;
	}
	rmEvent_EventActive = function()
	{
		if(rmEvent_DialogueChain == undefined) rmEvent_DialogueChain = new DialogueChain(global.DialogueChains.dlg_Test, self);
		global.StoryProgress.story_Test = true;
		
	}
	rmEvent_OnEventEnd = function() 
	{
		rmEvent_IsActive = false;
		rmEvent_isComplete = true;
	};
}

function rmEvent_FlamerBoss() : RoomEvent() constructor
{
	rmEvent_DialogueChain = undefined;
	bossObjs = undefined;
	bossSeq = undefined;
	phaseIndex = 0;
	phaseClears = 0;
	phaseComplete = false;
	tPoint = undefined;
	
	func_Init = function()
	{
		rmEvent_DialogueChain = undefined;
		bossObjs = undefined;
		phaseIndex = 0;
		phaseClears = 0;
		phaseComplete = false;
		tPoint = undefined;
	}
	
	rmEvent_Trigger = function() 
	{
		return (!global.StoryProgress.boss_Flamer);
	};
	
		
	func_InitFlamer = function()
	{
		tPoint = instance_find(entry_default,0);
		if(instance_number(event_Marker) != 0) tPoint = GetEventMarker(0);
		bossObj = SpawnCharacter(enemy_Flamer, tPoint, undefined, TEAMID.Enemy);
		bossObj.direction = 90;
		bossSeq = CreateSequence(Sequence1, tPoint, "Characters");
		var tInst = layer_sequence_get_instance(bossSeq);
		sequence_instance_override_object(tInst,enemy_Flamer, bossObj);	
	}
	
	rmEvent_OnTrigger = function()
	{
		func_Init();
		array_clear(global.spawnPool);
		rmEvent_IsActive = true;
		func_InitFlamer();
		global.alertMode = ALERT_STATE.Danger;
		global.BGM = bgm_Incursion;
			
	}

	
	rmEvent_EventInactive = function()
	{
		instance_deactivate_layer(layer_get_id("Event_0"));	
	}
	rmEvent_EventActive = function()
	{
		
		
		instance_activate_layer(layer_get_id("Event_0"));
		if(phaseComplete && phaseClears > 2)
		{
			global.BGM = global.currentRoom.rd_BGM;
			global.StoryProgress.boss_Flamer = true;
			bossObj.hp = 0;
			rmEvent_OnEventEnd();
		}else
		{
			if((CheckExistance(bossObj) && bossObj.hp <= 0) && phaseIndex != 1)
			{
				DestroySequence(bossSeq);
				phaseIndex = 1;
				if(phaseClears <= 2) array_fill(global.spawnPool, enemy_Vespid, 10);
			}
		
			if(phaseIndex >= 1)
			{
				room_SpawnEnemies();
				if(ds_list_size(global.enmList) <= 0 && array_length(global.spawnPool) <= 0)
				{
					phaseComplete = true;
					if(phaseComplete && phaseClears <= 2)
					{
						func_InitFlamer();
						phaseClears += 1;
						phaseIndex = 0;
						phaseComplete = false;
					}
				}
			}
		}
		
		
	}
	rmEvent_OnEventEnd = function() 
	{
		rmEvent_IsActive = false;
		rmEvent_isComplete = true;
	};
}