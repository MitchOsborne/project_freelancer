function OnProjHit(a_proj, a_other) {

	var targetHit = false;
	if(CheckExistance(a_other))
	{
		with (a_proj)
		{
			var fFire = (friendlyFire||friendlyOnly);
			targetHit = CheckDamageProc(a_proj,a_other,fFire)

			if (targetHit == true)
			{
				if(a_other.char_shdStat > 0 && a_proj.armorBlock)
				{
					audio_play_sound(sfx_coin_l, 1, false);
				}else
				{
					array_push(objectsDamaged,a_other);
					var tNum = 3;
					
					var tDmg = 0;
					var tDir = point_direction(x,y, a_other.x, a_other.y) - 90;
					if(damage != 0)
					{
						tDmg = damage;
						if(weapon.owner.teamID == TEAMID.Enemy) tDmg = weapon.owner.char_damageStat;
						tDmg *= CalcDmgScaling(weapon.owner, a_other);
					
			
						//var targetKill = DamageAgent(a_other,tDmg);
						var hitRoll = irandom_range(1,100);
						var critHit = floor((hitRoll + (weapon.owner.char_critLvl*weapon.owner.char_critPerLvl))/100);
						if(critHit != 0)
						{
							tNum *= critHit;
							tDmg += tDmg*critHit;
							audio_play_sound(sfx_boom, 5, false);
						}
					}
					for(var i = 0; i < tNum; i += 1)
					{
						var tVec = GetCoord(tDir + irandom_range(-45,45));
						array_push(Renderer.rndr_ParticleArr, new Ptl_Blood(a_other.x, a_other.y, tVec.x, tVec.y));
					}
					ReportHit(self, weapon, a_other, tDmg, tDir, weapon.abl_bpStats.bp_DMGType);
					/*var tReport = new HitReport(self, weapon, a_other, tDmg, tDir, damageType)
					array_push(a_other.inHitArr, tReport);
					*/
					//array_push(owner.char_OnHitProcs, tReport);
								
	
				
					if(forceStrength != 0)
					{
						AttachLinearForce(a_other, weapon.owner, 0.1, direction, forceStrength);					
						AttachLinearForce(a_other, weapon.owner, 0.1, direction, forceStrength/4);	
					}
				
					if(dmg_hitSlow != 0)
					{
						AttachMod(debuff_hitPause, weapon.owner, weapon.owner, dmg_hitSlow);	
					}
				
					if(stunDur != 0)
					{
						if(a_other.char_shdStat <= 0) AttachMod(debuff_stunned, a_other, weapon.owner, stunDur);	
					}
				
					//Handled by Character Damage processing
		/*
					if(weapon != undefined)
					{
						array_push(weapon.abl_hitReports, tReport);
					}
					if(owner != undefined)
					{
						array_push(owner.char_hitReports, tReport);
					}
		*/
	
				}
				hp -= 1;
			}
		}
	}
	return targetHit;


}
