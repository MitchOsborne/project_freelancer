// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ai_act_PathToPosition(a_Caller, a_PosX, a_PosY)
{
	var navPos = new Vector2(a_PosX, a_PosY);
	with a_Caller
	{
		var movPos = new Vector2(0,0);
		var locoPos = new Vector2(locomotor.x, locomotor.y);
		if(path_get_number(active_operator.ai_Path) == 0)
		{
			var canPath = false
			while(canPath == false)
			{
				MoveLoco(locomotor, id);
				canPath = Loco_NavPath(locomotor, active_operator.ai_Path, locoPos, navPos);
				if(canPath == false)
				{
					navPos.x = path_get_x(active_operator.ai_Path,0.9);
					navPos.y = path_get_y(active_operator.ai_Path,0.9);
					if(navPos.x == 0 && navPos.y == 0)
					{
						navPos.x = a_PosX;
						navPos.y = a_PosY;
					}
					canPath = true;
				}
			}
			
		}
		if(active_operator.ai_Path != undefined)
		{
			//Checks to ensure it is moving towards the nearest NavPoint that isn't its current position
			
			
			//var tDist = point_distance(x,y,path_get_point_x(navPath,i), path_get_point_y(navPath,i))
			
			var hasPos = false;
			var pX = 0;
			var pY = 0;
			while(hasPos == false)
			{
				pX = path_get_point_x(active_operator.ai_Path,0);
				pY = path_get_point_y(active_operator.ai_Path,0);
			
				if(pX == 0 && pY == 0)
				{
					break;	
				}
				if(point_distance(locoPos.x,locoPos.y,pX,pY) < 8)
				{
					path_delete_point(active_operator.ai_Path,0);	
				}else
				{
					hasPos = true;	
				}
			}
		
			if(pX == 0 && pY == 0) movPos = new Vector2(a_PosX, a_PosY);
			else movPos = new Vector2(pX, pY);
		
		}
		var tVec = new Vector2(0,0);
		if(point_distance(locoPos.x,locoPos.y,movPos.x,movPos.y) > 1)
		{
			var tDir = point_direction(locoPos.x,locoPos.y,movPos.x,movPos.y) - 90;
			tVec = GetCoord(tDir);
		}
		in_mx = tVec.x;
		in_my = tVec.y;	
	}
}