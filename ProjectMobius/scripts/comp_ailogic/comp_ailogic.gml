// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function abl_comp_AIHealSelf(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Update = function()
	{
		if(owner.hp >= owner.char_healCap) parentAbility.ai_CanAttack = false;
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 1;
		if(owner.hp < owner.char_healCap && parentAbility.ai_CanAttack) retVal = 5;	
		return retVal;
	}
}

function abl_comp_AICharges(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Charges = 1;
	abl_comp_ChargeTime = 1;
	chargeTmr = 0;
	chargeCtr = 0;
	
	abl_comp_Update = function()
	{
		if(owner.teamID == TEAMID.Ally) abl_comp_ChargeTime = 0.6;
		if(chargeCtr < abl_comp_Charges)
		{
			chargeTmr = clamp(chargeTmr + GetDeltaTime(owner.timeScale), 0, abl_comp_ChargeTime);
			if(chargeTmr >= abl_comp_ChargeTime)
			{
				chargeTmr = 0;
				chargeCtr = 0;
			}
		}
	}
	
	abl_comp_OnActivate = function()
	{
		chargeCtr += 1;	
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 1;
		if(chargeCtr >= abl_comp_Charges)
		{
			parentAbility.ai_CanAttack = false;
			retVal = 0;
		}
		return retVal;
	}
}

function abl_comp_AICantUse(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_AIUpdate = function()
	{
		parentAbility.ai_CanAttack = false;	
		return 1;
	}
}

function abl_comp_AITargetInRange(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_AIUpdate = function()
	{
		var tTarg = owner.active_operator.enemyTarget;
		if(CheckExistance(tTarg))
		{
			if(point_distance(tTarg.x, tTarg.y, owner.x, owner.y) > parentAbility.abl_bpStats.bp_Range) parentAbility.ai_CanAttack = false;	
		}else parentAbility.ai_CanAttack = false;
		return 1;
	}	
}

function abl_comp_AIKeepClose(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_followDist = 64;
		
	abl_comp_AIUpdate = function()
	{
		
		var retVal = 1;
		if(owner.teamID == TEAMID.Ally)
		{
			if(global.alertMode == ALERT_STATE.Clear || !CheckExistance(owner.active_operator.enemyTarget))
			{
				var tTarg = owner.active_operator.allyTarget;
				if(CheckExistance(tTarg))
				{
					var tDist = point_distance(tTarg.x, tTarg.y, owner.x, owner.y)
					if(tDist > abl_comp_followDist)
					{
						if(	Collision_Line_Tilemap(owner, tTarg, global.AgentTileCollisionClass) == false &&
							Collision_Line_Obj(owner, tTarg, global.AgentCollisionClassDashing) == false)
						{
							retVal = 5;
						}
					}else parentAbility.ai_CanAttack = false;
				}
			}
		}
		return retVal;
	}
}

function abl_comp_AIOutOfCombat(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Update = function()
	{
		if(global.alertMode == ALERT_STATE.Clear) parentAbility.ai_CanAttack = false;	
	}
}