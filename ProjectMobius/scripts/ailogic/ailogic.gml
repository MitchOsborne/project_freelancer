// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function AI_CheckVision(a_ai)
{
	with a_ai
	{	
		if(CheckExistance(enemyTarget))
		{
			if(global.alertMode != ALERT_STATE.Danger)
			{
				if(ai_targetVisible == true)
				{
					var tDist = point_distance(enemyTarget.x, enemyTarget.y, slavedObj.x, slavedObj.y);

					if(tDist <= slavedObj.sightRadius)
					{
						var tDir = point_direction(slavedObj.x, slavedObj.y,enemyTarget.x, enemyTarget.y);
						var tDiff = angle_difference(tDir-90, slavedObj.direction);
							if(abs(tDiff) < slavedObj.sightAngle/2)
						{
						//If close enough to see clearly, begin Engage
							if(tDist < slavedObj.sightRadius / 2)
							{
								ai_searchTmr += GetDeltaTime(timeScale);//*(slavedObj.sightRadius/tDist);
								if(ai_searchTmr >= ai_searchDelay)
								{
									a_ai.ai_CurrentState = AI_STATE.FIGHT;
								}
							}else	//Otherwise investigate
							{
								ai_searchTmr += GetDeltaTime(timeScale);
								if(ai_searchTmr <= ai_searchDelay) a_ai.ai_CurrentState = AI_STATE.SEARCH;	
								else a_ai.ai_CurrentState = AI_STATE.INVESTIGATE;
							}// End of If tDist EndIf
						}//End If(CollisionLine)
					}//End abs(tDiff)
				}
				
				if(!ai_targetVisible)
				{
					ai_searchTmr = clamp(ai_searchTmr-GetDeltaTime(timeScale),0,ai_searchDelay);
					if(ai_searchTmr == 0) ai_CurrentState = AI_STATE.IDLE;
				}
					
					
				//End if(tDist < sightRadius)
			}// End If(global.alertMode)
		}//End If (CheckExistance)
	}//End With AI
}

function AI_RaiseAlertMode(a_ai)
{
	with a_ai
	{
		if(global.alertMode != ALERT_STATE.Danger && !slavedObj.isStunned && !slavedObj.isIncap)
		{
			ai_alertTmr += GetDeltaTime(timeScale);
			if(ai_alertTmr >= ai_alertDelay)
			{
				global.alertMode += 1;
				ai_alertTmr = 0;
			}
		}	
	}
}

function AI_BaseInit(a_ai)
{
	with a_ai
	{
		Oper_Init(a_ai);
		ai_hasInit = true;	
	}
}

function AI_Idle(a_ai)
{
	//Play some idle animations after x time
	return true;
}

function AI_CheckTargetPos(a_ai)
{
	with a_ai
	{
		if(CheckExistance(enemyTarget))
		{
			//Checks if the target is visible to shooting
			if(Collision_Line_Tilemap(slavedObj, enemyTarget, global.ProjectileTileCollisionClass) == false &&
			Collision_Line_Obj(slavedObj, enemyTarget, global.ProjectileCollisionClass) == false) ai_targetVisible = true;
			else ai_targetVisible = false;
	
			//Checks if the target can be reached by walking directly towards
			if(Collision_Line_Tilemap(slavedObj, enemyTarget, global.AgentTileCollisionClass) == false &&
			Collision_Line_Obj(slavedObj, enemyTarget, global.AgentCollisionClass) == false) ai_directPath = true;
			else ai_directPath = false;
		}
	}
	return true;
}

function AI_EncircleTarget(a_ai)
{
	with a_ai
	{
		if(enemyTarget != undefined)
		{
			var engageRange = 140;
			if(chosenAbl != undefined) engageRange = chosenAbl.ai_engageRange;
			else
			{
				var itBroke = true;	
			}
			var targetDist = clamp(engageRange*4 , 0, 340);
			var tDist = point_distance(enemyTarget.x, enemyTarget.y, slavedObj.x, slavedObj.y);
			var tLerp = tDist/targetDist;
			if(tDist < clamp(engageRange*4.1 , 0, 340))
			{
				var tDir = point_direction(enemyTarget.x, enemyTarget.y, slavedObj.x, slavedObj.y);
				//Gets a direction that levels out into strafing left/right around the target at the desired engage range
				var tOffset = 90 - (90*tLerp);
				var tVec = GetCoord(tDir - (tOffset*ai_LRand));
				slavedObj.in_mx = tVec.x;
				slavedObj.in_my = tVec.y;
			}else ai_task_MoveIntoRange(self, enemyTarget, clamp(engageRange*4 , 0, 260));	
		}
	}
	return true;
}

function AI_MoveToEngageRange(a_ai)
{
	with a_ai
	{
		if(enemyTarget != undefined)
		{
			var engageRange =  140;
			if(chosenAbl != undefined) engageRange = chosenAbl.ai_engageRange;
			if(point_distance(enemyTarget.x, enemyTarget.y, slavedObj.x, slavedObj.y) < clamp(engageRange*3.6 , 0, 240))
			{
				var tDir = point_direction(enemyTarget.x, enemyTarget.y, slavedObj.x, slavedObj.y);
				var tVec = GetCoord(tDir-90);
				slavedObj.in_mx = tVec.x;
				slavedObj.in_my = tVec.y;
			}else ai_task_MoveIntoRange(self, enemyTarget, clamp(engageRange*4 , 0, 280));	
		}
	}
	return true;
}

function AI_FollowPlayer(a_ai)
{
	with a_ai
	{
		var tChar = Dummy;
		if(slavedObj.char_SmnOwner != undefined) tChar = slavedObj.char_SmnOwner;
		else
		{
			for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
			{
				var iChar = global.PlayerIDList[|i].slavedObj;
				if(iChar != tChar) tChar = iChar;
				break;
			}
		}
		
		if(tChar != undefined)
		{
			var engageRange =  48;
			ai_task_MoveIntoRange(self, tChar, engageRange);	
		}
	}
	return true;
}

function AI_MoveToFightRange(a_ai)
{
	with a_ai
	{
		if(enemyTarget != undefined)
		{
			var engageRange = chosenAbl.ai_engageRange;
			if(CheckExistance(enemyTarget)) ai_task_MoveIntoRange(self, enemyTarget, engageRange*0.8);
		}
	}
	return true;
}

function AI_LookAtTarget(a_ai)
{
	with a_ai
	{
		if(enemyTarget != undefined)
		{
			if(CheckExistance(enemyTarget)) ai_lookPoint = enemyTarget;	
		}
	}
	return true;
}

function AI_InvestigateTarget(a_ai)
{
	with a_ai
	{
		if(enemyTarget != undefined)
		{
			if(CheckExistance(enemyTarget)) ai_task_MoveIntoRange(self, enemyTarget, 32);
		}
	}
	return true;
}

function AI_PatrolRoute(a_ai)
{
	with a_ai
	{
		if(ai_patrolPath != undefined)
		{
			ai_debugTxt = "Patrolling";
			var tPnt = ai_patrolPath.ptrl_Points[ai_patrolIndex];
			if(point_distance(slavedObj.x, slavedObj.y, tPnt.x, tPnt.y)	<= 16)
			{
				tPnt.ptrl_task(a_ai, tPnt);
				if(tPnt.ptrl_TaskComplete)
				{
					tPnt.ptrl_TaskComplete = false;
					tPnt.ptrl_taskTmr = 0;
					ai_patrolIndex += 1;
					if(ai_patrolIndex >= array_length(ai_patrolPath.ptrl_Points)) ai_patrolIndex = 0;
				}
			}else
			{
				ai_task_MoveIntoRange(self, tPnt, 16);
			}
		}
	}
	return true;
}

function AI_FightEnemy(a_ai)
{
	var retVal = false;
	with a_ai
	{
		if(CheckExistance(enemyTarget) && chosenAbl != undefined)
			{
				if(chosenAbl.ai_CanAttack)
				{
					if(!slavedObj.isStunned)
					{
						if(ai_prepAttack == false)
						{
							ai_prepAttack = true;
							slavedObj.char_preppingAttack = true;
						}
					}
				}
								
				if(ai_prepAttack)
				{
					var prepVal = atkTimer/ai_atkDelay;
					if(prepVal <= 0.5) ai_atkDir = new Vector2(enemyTarget.x, enemyTarget.y);
					if(slavedObj.teamID == TEAMID.Ally) atkTimer += GetDeltaTime(timeScale)/2;	//So Allies can prep attacks slightly faster
					if(slavedObj.isStunned) atkTimer += GetDeltaTime(timeScale)/4;
					else atkTimer += GetDeltaTime(timeScale);
					slavedObj.char_isAttacking = true;
				}
				if(chosenAbl.ai_reqRelease) toggleAtk = !toggleAtk;
				else toggleAtk = true;
				if(atkTimer >= ai_atkDelay && toggleAtk) beginAttack = true;
				else beginAttack = false;
				if(ai_prepAttack) ai_lookPoint = ai_atkDir;
				
				
				if(beginAttack && !slavedObj.isStunned)
				{
					retVal = true;
					ai_prepAttack = false;
					beginAttack = false;
					AI_UseAbility(self, chosenAbl);
					//beginAttack = false;
					//atkTimer = 0;
					if(slavedObj.teamID == TEAMID.Enemy)
					{
						atkTimer = 0;
					}//else atkTimer *= 0.65;	
				}
				
				if(ai_prepAttack || beginAttack)
				{
					reqPath = false;
				}
				
		}	
	}
	return retVal;
}

function AI_HitAndRun(a_ai)
{
	with a_ai
	{
		ai_priorityTmr = 0;	
		ai_CurrentState = AI_STATE.ENGAGE;
	}
	return true;
}

function AI_UseAbility(a_ai, a_abl)
{
	with a_ai
	{
		a_abl.inPressed = true;
		AI_RunCompAILogic(a_abl, self);
	}
	return true;
}