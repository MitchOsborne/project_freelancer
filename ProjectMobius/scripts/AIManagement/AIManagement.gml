// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function cmd_CombatLog(a_targ, a_combArr) constructor
{
	cmbt_Target = a_targ;
	cmbt_CombatantsArr = a_combArr;
}

function cmd_CalcFightScore(a_Char, a_Targ)
{
	var retVal = infinity;
	if(a_Char.ai_priorityMod != -1)
	{
		var tDist = point_distance(a_Char.x, a_Char.y,a_Targ.x, a_Targ.y);
		retVal = tDist/(a_Char.ai_combatPriority);
	}
	return retVal;
}
