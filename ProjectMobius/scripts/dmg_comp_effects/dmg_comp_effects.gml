// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function dmg_comp_SpawnParticlePassive(a_parent, a_particle, a_spawnRate, a_amount) : base_DamagerComponent(a_parent) constructor
{
	spawnTimer = 0;
	spawnRate = 0.3;
	spawnAmount = 1;
	if(a_spawnRate != undefined) spawnRate = a_spawnRate;
	if(a_amount != undefined) spawnAmount = a_amount;
	
	dmg_comp_Update = function()
	{
		spawnTimer += GetDeltaTime(weapon.owner.timeScale);
		if(spawnTimer > spawnRate)
		{
			spawnTimer = 0;
			for(var i = 0; i < spawnAmount; i += 1)
			{
				//instance_create_layer(x,y,"Projectiles",spawnParticle);
			}
		}	
	}
}

function dmg_comp_SpawnParticleOnHit(a_parent, a_particle, a_spawnRate, a_amount) : base_DamagerComponent(a_parent) constructor
{
	spawnTimer = 0;
	spawnRate = 0.3;
	spawnAmount = 1;
	if(a_spawnRate != undefined) spawnRate = a_spawnRate;
	if(a_amount != undefined) spawnAmount = a_amount;
	
	
	dmg_comp_OnHit = function()
	{
		for(var i = 0; i < spawnAmount; i += 1)
		{
			//instance_create_layer(x,y,"Projectiles",spawnParticle);
		}
	}
}