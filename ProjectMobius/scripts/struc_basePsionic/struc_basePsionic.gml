// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum PSI_HOLDTYPE
{
	NONE,
	FOLLOW_FRONT,
	FOLLOW_BACK,
}


function base_Psionic() : base_Ability() constructor
{
	
	psiDist = 180;
	
	psi_PrimeObj = undefined;
	psi_TaggedArray = [];
	wepProjList = ds_list_create();
	wepNodeList = ds_list_create();
	
	psi_SearchRes = 0.3;	//How often to update the psi_ObjArray with valid objects
	psi_SearchTimer = 0;
	
	psi_TagFunc = psi_FindValidObjects;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"PsiObj");
	speedMod = 1;
	range = 200;
	
	psi_HoldType = PSI_HOLDTYPE.NONE;
	psi_ActivateAbility = psi_ThrowTagged;
	
	func_PreUpdateStep = PsiPreUpdateStep;
	func_UpdateStep = PsiUpdateStep;
	//func_InitAbility = InitialiseWeapon;
	
	psi_holdAngle = 90;
	psi_holdDist = 90;
	psi_holdLerp = 0.07;
	psi_maxTags = 3;
	psi_baseThrowSpeed = 15;
	
	abl_LockDur = 0.2;
	
	
	abl_Icon = spr_Psionics;
}

function PsiPreUpdateStep(a_psiID)
{
	with a_psiID
	{
		
		AbilityPreUpdateStep(self);
		psi_SanityCheckObjects(self);
		psi_SearchTimer += GetDeltaTime(global.timeScale);
		if(psi_SearchTimer >= psi_SearchRes)
		{
			psi_PrimeObj = psi_FindNearestValidObject(self)[0];
			psi_SearchTimer = 0;
			psi_TaggedArray = psi_TagFunc(self);
			if(array_length(psi_TaggedArray) >= psi_maxTags || psi_PrimeObj == undefined) staminaCost = -1;
			else staminaCost = baseStaminaCost;
		}
	}
}

function PsiUpdateStep(a_psiID)
{
	with a_psiID
	{
		AbilityUpdateStep(self);
		if(activate)
		{
			if(inPressed)
			{
				didActivate = true;
				psi_ActivateAbility(self);
			}
		}
		psi_HoldProjectiles(self);
	}
}

function psi_DrawDebug(a_psiID)
{
	with a_psiID
	{
		for(var i = 0; i < array_length(psi_TaggedArray); i += 1)
		{
			if(instance_exists(psi_TaggedArray[i]) && instance_exists(owner))
			{
				draw_line_color(owner.x, owner.y,psi_TaggedArray[i].x, psi_TaggedArray[i].y, c_purple, c_purple);
			}
		}
		if(instance_exists(owner)) draw_circle_color(owner.x, owner.y, psiDist, c_fuchsia, c_fuchsia, true);
		if(CheckExistance(psi_PrimeObj) && instance_exists(owner)) draw_line_color(owner.x, owner.y, psi_PrimeObj.x, psi_PrimeObj.y, c_fuchsia, c_fuchsia);
	
		psi_ObjArray = psi_FindValidObjects(self);
		
		for(var i = 0; i < array_length(psi_ObjArray); i += 1)
		{
			var tObj = psi_ObjArray[i];
			if(instance_exists(tObj) && instance_exists(owner))
			{
				DrawOutline(tObj, c_psiDPurple, 2);
			}
		}
	
	}
	
}

function psi_DrawPsiOutline(a_psiID)
{
	with a_psiID
	{
		if(array_length(psi_TaggedArray) > 0)
		{
			DrawOutline(a_psiID.owner, c_psiDPurple, 2);
		}
	}
}

function psi_GetObjectsFromPrimary(a_psiID)
{
	var tArr = a_psiID.package.psi_TaggedArray;
	return tArr;
}

function psi_GetObjectsFromOwner(a_psiID)
{
	var tArr = a_psiID.owner.dbf_taggedObjects;
	return tArr;
}

function psi_SanityCheckObjects(a_psiID)
{
	with a_psiID
	{
		for(var i = 0; i < array_length(psi_TaggedArray); i += 1)
		{
			var tProj = psi_TaggedArray[i];
			if(!CheckExistance(tProj))
			{
				array_delete(psi_TaggedArray, i, 1);
				i -= 1;
			}
		}
		if(!CheckExistance(psi_PrimeObj)) psi_PrimeObj = undefined;
	}
}

function psi_FindValidObjects(a_psiID)
{
	var objArr = [];
	var objCount = instance_number(base_Prop);
	for(var i = 0; i < objCount; i += 1)
	{
		var tObj = instance_find(base_Prop, i);
		if(point_distance(a_psiID.owner.x, a_psiID.owner.y, tObj.x, tObj.y) <= a_psiID.psiDist)
		{
			array_push(objArr, tObj);	
		}
	}
	return objArr;
}

function psi_FindPlaceHolder(a_psiID)
{
	return a_psiID.psi_TaggedArray;
}	

function psi_FindNearestValidObject(a_psiID)
{
	var retObj = undefined;
	var objCount = instance_number(base_Prop);
	for(var i = 0; i < objCount; i += 1)
	{
		var tObj = instance_find(base_Prop, i);
		var tDist = point_distance(a_psiID.owner.x, a_psiID.owner.y, tObj.x, tObj.y);
		if(tDist <= a_psiID.psiDist)
		{
			if(retObj != undefined)
			{
				var tDist2 = point_distance(a_psiID.owner.x, a_psiID.owner.y, retObj.x, retObj.y)	
				if(tDist < tDist2) retObj = tObj;
			}else retObj = tObj;
		}
	}
	var objArr = [retObj];
	return objArr;	
}

function psi_HoldProjectiles(a_psiID)
{
	
	var tArr = [];
	for(var i = 0; i < array_length(a_psiID.psi_TaggedArray); i += 1)
	{
		var tProj = a_psiID.psi_TaggedArray[i];
		if(object_is_ancestor(tProj.object_index, base_Projectile))
		{
			if(instance_exists(tProj) && tProj.isHeld)
			{
				if(!tProj.psi_isThrown)
				{
					var tDir = point_direction(tProj.x, tProj.y, a_psiID.owner.x, a_psiID.owner.y);
					tProj.direction = tDir;
					array_push(tArr, tProj);
					AttachMod(debuff_psiTouched, tProj, a_psiID.owner, 5);
				}else
				{
					array_delete(a_psiID.psi_TaggedArray, i, 1);
				}
			}
		}
	}
	
	switch (a_psiID.psi_HoldType)
	{
		case PSI_HOLDTYPE.NONE:
		break;
		case PSI_HOLDTYPE.FOLLOW_FRONT:
		psi_HoldProjFront(a_psiID, tArr);
		break;
		case PSI_HOLDTYPE.FOLLOW_BACK:
		psi_HoldProjBack(a_psiID, tArr);
		break;
		default:
		break;
	}
}

function psi_HoldProjFront(a_psiID, a_projArr)
{
	with a_psiID
	{
		var arrSize = array_length(a_projArr);
		for(var i = 0; i < arrSize; i += 1)
		{
			var arrPos = i/arrSize;
			var tProj = a_projArr[i];
			if(CheckExistance(tProj))
			{
				var tDir = (owner.direction + 90) + CalcFixedSpread(i,psi_holdAngle,arrSize);
				var tPos = new Vector2(lengthdir_x(psi_holdDist,tDir), lengthdir_y(psi_holdDist, tDir));
				tProj.x = lerp(tProj.x, owner.x + tPos.x, psi_holdLerp);
				tProj.y = lerp(tProj.y, owner.y + tPos.y, psi_holdLerp);
			}
		}
	}
}

function psi_HoldProjBack(a_psiID, a_projArr)
{
	with a_psiID
	{
		var arrSize = array_length(a_projArr);
		for(var i = 0; i < arrSize; i += 1)
		{
			var arrPos = i/arrSize;
			var tProj = a_projArr[i];
			if(CheckExistance(tProj))
			{
				var tDir = (owner.direction + 270) + CalcFixedSpread(i,psi_holdAngle,arrSize);
				var tPos = new Vector2(lengthdir_x(psi_holdDist,tDir), lengthdir_y(psi_holdDist, tDir));
				tProj.x = lerp(tProj.x, owner.x + tPos.x, psi_holdLerp);
				tProj.y = lerp(tProj.y, owner.y + tPos.y, psi_holdLerp);
			}
		}
	}
}

function psi_CreateProjectile(a_psiID)
{
	if(array_length(a_psiID.psi_TaggedArray) < psi_maxTags)
	{
		var tOwner = a_psiID.owner;
		var dir = tOwner.direction;
		var tInst = CreateProjectile(proj_psiObjSmall,a_psiID,tOwner.char_damageStat * a_psiID.damageMultiplier,new Vector2(tOwner.x, tOwner.y), dir, 1);
		//tInst.sprite_index = a_obj.sprite_index;
		//tInst.psi_OriginalObject = a_obj.object_index;
		array_push(a_psiID.psi_TaggedArray, tInst);
		psi_TagProjectile(tInst, tOwner);
	}
}

function psi_ConvertToProjectile(a_obj, a_psiID)
{
	var projType = undefined;
	var dmgMult = 2;
	switch(a_obj.psi_ProjectileType)
	{
		case PSI_PROJTYPE.SMALL:
		projType = proj_psiObjSmall;
		dmgMult = 5;
		break;
		case PSI_PROJTYPE.MEDIUM:
		projType = proj_psiObjMed;
		dmgMult = 10;
		break;
		case PSI_PROJTYPE.LARGE:
		projType = proj_psiObjLarge;
		dmgMult = 20;
		break;
		default:
		projType = proj_psiEnergy;
		break;
	}
	
	var dir = a_psiID.owner.direction;
	var tInst = CreateProjectile(projType,a_psiID,a_psiID.owner.char_damageStat * dmgMult,new Vector2(a_obj.x, a_obj.y), dir, 1);
	tInst.sprite_index = a_obj.sprite_index;
	tInst.psi_OriginalObject = a_obj.object_index;
	
	psi_TagProjectile(tInst, a_psiID.owner);

	
	for(var i = 0; i < array_length(a_psiID.psi_ObjArray); i += 1)
	{
		if(a_psiID.psi_ObjArray[i] = a_obj) array_delete(a_psiID.psi_ObjArray, i, 1);	
	}
	ds_list_add(wepProjList,tInst);
	
	
	instance_destroy(a_obj);
	return tInst;
}

function psi_ConvertToObject(a_proj)
{
	var pos = ConvertPosToGrid(new Vector2(a_proj.x, a_proj.y));
	
	a_proj.psi_OriginalObject.x = pos.x*global.gridSize;
	a_proj.psi_OriginalObject.y = pos.y*global.gridSize;
	instance_activate_object(a_proj.psi_OriginalObject);

	instance_destroy(a_proj);
}

function psi_TagNearestObject(a_psiID)
{
	var tObj = a_psiID.psi_PrimeObj;
	if(tObj != undefined)
	{
		if(array_length(a_psiID.psi_TaggedArray) < psi_maxTags)
		{
			var tProj = psi_ConvertToProjectile(tObj,a_psiID);
			a_psiID.psi_PrimeObj = undefined;
			array_push(a_psiID.psi_TaggedArray, tProj);
		}
	}
	
}

function psi_TagRadius(a_psiID)
{
	var tOwner = a_psiID.owner;
	var objList = ds_list_create();
	var projList = ds_list_create();
	collision_circle_list(tOwner.x, tOwner.y, a_psiID.psiDist, base_Object,false, true, objList, false);
	
	for(var i = 0; i < ds_list_size(objList); i += 1)
	{
		var tInst = objList[|i];
		if(object_is_ancestor(tInst.object_index, base_Character))
		{
			if(tInst.teamID != tOwner.teamID)
			{
				array_push(a_psiID.psi_TaggedArray, tInst);
				array_push(tOwner.dbf_taggedObjects, tInst);
				AttachMod(debuff_psiTouched, tInst, tOwner, 5);
				AttachMod(debuff_stunned, tInst, tOwner, 5);
			}
		}else if(object_is_ancestor(tInst.object_index, base_Projectile))
		{
			if(tInst.teamID != tOwner.teamID)
			{
				array_push(a_psiID.psi_TaggedArray, tInst);
				psi_TagProjectile(tInst, tOwner);
				
			}
		}else if(object_is_ancestor(tInst.object_index, base_Prop))
		{
			var tProj = psi_ConvertToProjectile(tInst,a_psiID);
			array_push(a_psiID.psi_TaggedArray, tProj);
			array_push(tOwner.dbf_taggedObjects, tInst);
		}
	}
}

function psi_ThrowTagged(a_psiID)
{
	tSize = array_length(a_psiID.psi_TaggedArray);
	for(var i = 0; i < array_length(a_psiID.psi_TaggedArray); i += 1)
	{
		var tObj = a_psiID.psi_TaggedArray[i];
		if(object_is_ancestor(tObj.object_index, base_Character))	psi_ThrowCharacter(a_psiID, i);
		else if (object_is_ancestor(tObj.object_index, base_Projectile))
		{
			psi_ThrowProjectile(a_psiID, i);
			i -= 1;
		}
	}
}

function psi_TagProjectile(a_proj, a_owner)
{
	AttachMod(debuff_psiTouched, a_proj, a_owner, 5);
	array_push(a_owner.dbf_taggedObjects, a_proj);
	tSize = array_length(a_owner.dbf_taggedObjects);
	
	a_proj.isStasis = true;
	a_proj.isHeld = true;
}

function psi_ThrowCharacter(a_psiID, a_iter)
{
	var tOwner = a_psiID.owner;
	var tTagged = a_psiID.psi_TaggedArray[a_iter];
	
	if(tTagged != undefined && instance_exists(tTagged))
	{
		var tDir = 0;
		if(tOwner.target != undefined && tOwner.inLockDir)
		{
			tDir = point_direction(tTagged.x, tTagged.y, tOwner.target.x, tOwner.target.y) - 90;
		}else
		{
			tDir = point_direction(0,0,tOwner.lx, tOwner.ly) - 90;
		}
		
		if(tOwner.target != tTagged)
		{
			AttachMod(debuff_Thrown, tTagged, tOwner, psi_ThrowDur);
			AttachLinearForce(tTagged, tOwner, psi_ThrowDur, tDir, psi_ThrowForce);
			AttachMod(debuff_stunned, tTagged, tOwner, psi_ThrowDur);
		}else
		{
			AttachMod(debuff_stunned, tTagged, tOwner, psi_ThrowDur);
			DamageAgent(tTagged, tOwner.char_damageStat * a_psiID.damageMultiplier);
		}
	}
}

function psi_ThrowProjectile(a_psiID, a_iter)
{
	
	var tOwner = a_psiID.owner;
	var tProj = a_psiID.psi_TaggedArray[a_iter];
	if(tProj != undefined && instance_exists(tProj))
	{
	
		var tDir = 0;
		if(tOwner.target != undefined)
		{
			tDir = point_direction(tProj.x, tProj.y, tOwner.target.x, tOwner.target.y) - 90;
		}else
		{
			tDir = point_direction(0,0,tOwner.lx, tOwner.ly) - 90;
		}
		tProj.weapon = a_psiID;
		tProj.teamID = tOwner.teamID;
		tProj.lifeTimer = 0;
		tProj.isStasis = false;
		tProj.isHeld = false;
		tProj.psi_isThrown = true;
		tProj.baseSpeed = psi_baseThrowSpeed;
		//tProj.forceStrength = a_psiID.abl_forceStrength * tProj.psi_ForceMult;
		tProj.stunDur = a_psiID.abl_stunDur;
		tProj.forceDur = a_psiID.abl_stunDur;
		tProj.direction = tDir;
		var tVec = GetCoord(tDir);
		tProj.mx = tVec.x;
		tProj.my = tVec.y;
		tProj.velX = 0;
		tProj.velY = 0;
		
		array_delete(a_psiID.psi_TaggedArray, a_iter, 1);
	}
	
}