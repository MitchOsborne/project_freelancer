// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function GetFullSight(a, b){
	
	if(Collision_Line_Tilemap(a,b,global.ProjectileTileCollisionClass) == false)
	{
	
		if(collision_line(a.bbox_left,a.bbox_top,b.x,b.y,base_PhysWall, false,true) == noone
		&& collision_line(a.bbox_right,a.bbox_top,b.x,b.y,base_PhysWall, false,true) == noone
		&& collision_line(a.bbox_left,a.bbox_bottom,b.x,b.y,base_PhysWall, false,true) == noone
		&& collision_line(a.bbox_right,a.bbox_bottom,b.x,b.y,base_PhysWall, false,true) == noone)
			return true;
	}
	return false;
}

function Collision_Line_Obj(a_Start, a_End, a_CollClass)
{
	
	for(var i = 0; i < array_length(a_CollClass); i += 1)
	{
		var tColl = a_CollClass[i];
		if(collision_line(a_Start.x, a_Start.y, a_End.x, a_End.y, tColl, false, true))
		{
			return true;	
		}
	}
	return false;
}

function Collision_Line_Tilemap(a_Start, a_End, a_CollClass)
{
	
	var tDir = point_direction(a_End.x, a_End.y, a_Start.x, a_Start.y);
	var tVec = GetCoord(tDir + 90);
	var tDist = point_distance(a_Start.x, a_Start.y, a_End.x, a_End.y);
	
	if(tDist > 0)
	{
		
		for(var i = 0; i < tDist/4; i += 1)
		{
			var tPos = new Vector2(a_Start.x + (tVec.x * (i*4)), a_Start.y + (tVec.y * (i*4)));
			if(TileMeetingPixel(tPos, a_CollClass))
			{
				return true;
			}
		}
	}
	
	return false;
}