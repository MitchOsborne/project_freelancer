// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
#region Pre-Update Logic
function Char_InitLocomotor(a_char)
{
	with (a_char)
	{
		//Sanity Check Locomotor
		if(locomotor == undefined || !instance_exists(locomotor))
		{
			if(instance_number(locomotorType) != 0)
			{
				locomotor = instance_find(locomotorType,0);
			}
			else
			{
				var tInst = instance_create_layer(0,0,"Instances", locomotorType);
				locomotor = tInst;
			}
		}
		//Moves Locomotor to Obj's position
		MoveLoco(locomotor, id);
	}
}

function Char_InitLight(a_char)
{
	with (a_char)
	{
		if(char_hasLight && !CheckExistance(char_ambLight)) 
		{
			var tLight = light_AmbChar;
			if(PlayerControlled) tLight = light_AmbPlayer;
			char_ambLight = instance_create_layer(x,y,"Instances", tLight);
			char_ambLight.owner = id;
		}
		
		if(CheckExistance(char_ambLight))
		{
			char_ambLight.x	= x;
			char_ambLight.y = y;
		}
	}
}

function Char_UpdateOperator(a_char)
{
	with (a_char)
	{
		mx = 0;
		my = 0;
		lx = 0;
		ly = 0;
		inFocus = 0;
		inLockDir = 0;
		inWeapon1 = 0;
		inUtility1 = 0;
		inAbility1 = 0;
		inCore = 0;


		if(!in_sequence)
		{
			script_execute(active_operator.func_PreUpdateStep, active_operator);
			script_execute(active_operator.func_UpdateStep, active_operator);
			script_execute(active_operator.func_EndUpdateStep, active_operator);
		}

		script_execute(animator.func_PreUpdateStep, animator);
		script_execute(animator.func_UpdateStep, animator);
		script_execute(animator.func_EndUpdateStep, animator);

		mx = in_mx;
		my = in_my;
		lx = in_mx;
		ly = in_my;
		if(in_lx != 0 || in_ly != 0)
		{
			lx = in_lx;
			ly = in_ly;
		}
	}
}

function Char_ProcessOutgoingHitReports(a_char)
{
	with (a_char)
	{
		for(var n = 0; n < array_length(char_hitReports); n += 1)
		{
			var tReport = char_hitReports[n];
			for(var i = 0; i < array_length(char_modArr); i += 1)
			{
				var tMod = char_modArr[i];
				tMod.mod_OnHit(tReport);
			}
		}

		for(var n = 0; n < array_length(char_killReports); n += 1)
		{
			var tReport = char_killReports[n];
			for(var i = 0; i < array_length(char_modArr); i += 1)
			{
				var tMod = char_modArr[i];
				tMod.mod_OnKill(tReport);
			}
		}

		array_clear(char_hitReports);
		array_clear(char_killReports);
	}
}

function Char_ProcessIncomingHitReports(a_char)
{
	with (a_char)
	{
		inDamage = 0;
		for(var n = 0; n < array_length(inHitArr); n += 1)
		{
			var tReport = inHitArr[n];
			wasHit = true;
			/* Process Damage Reductions Here
			----------------------
			----------------------
			----------------------
			*/
			if(ai_operator != undefined && ai_operator.ai_CurrentState < AI_STATE.SEARCH) ai_operator.ai_CurrentState = AI_STATE.SEARCH;
			if(tReport.hit_Weapon.abl_isSilent == false && global.alertMode == ALERT_STATE.Clear) global.alertMode = ALERT_STATE.Warning;
	
			array_push(tReport.hit_Owner.char_hitReports, tReport);
			array_push(tReport.hit_Weapon.abl_hitReports, tReport);
			
			if((totalHP < totalchar_maxHP*0.8) && (inDamage < totalHP) && (inDamage + tReport.hit_Dmg > totalHP))
			{
				array_push(tReport.hit_Owner.char_killReports, tReport);
				array_push(tReport.hit_Weapon.abl_killReports, tReport);
		
				for(var i = 0; i < array_length(char_modArr); i += 1)
				{
					var tMod = char_modArr[i];
					tMod.mod_OnDeath(tReport);
				}
			}
			for(var i = 0; i < array_length(char_modArr); i += 1)
			{
				var tMod = char_modArr[i];
				tMod.mod_OnHurt(tReport);
			}
	
	
	
	
	
			//Display Hit DMG Text
			/*var tInst = instance_create_layer(x,y,"Text",txt_dmg);
			tInst.damageVal = tReport.hit_Dmg;
			*/
			
			var dSoft	= tReport.hit_Dmg * tReport.hit_DmgType.dmg_Soft;
			var dHard	= tReport.hit_Dmg * tReport.hit_DmgType.dmg_Hard;
			var dTher	= tReport.hit_Dmg * tReport.hit_DmgType.dmg_Ther;
			var dEM		= tReport.hit_Dmg * tReport.hit_DmgType.dmg_EM;
			
			
			Char_ProcessDamage(a_char, dHard, DMG_TYPE.HARD);
			Char_ProcessDamage(a_char, dEM, DMG_TYPE.EM);
			Char_ProcessDamage(a_char, dTher, DMG_TYPE.THERMAL);
			Char_ProcessDamage(a_char, dSoft, DMG_TYPE.SOFT);
			
		}

		array_clear(inHitArr);
	}
}

function Char_ProcessDamage(a_char, a_dmgAmount, a_dmgType)
{
	with (a_char)
	{
		if(global.debug) tDmg = 0;
		
		if(a_dmgAmount != 0)
		{
					
			hitColTmr = 0;
			if(teamID = TEAMID.Ally) AttachMod(buff_hitInvul, id, id, 2);
			
			var tDmg = a_dmgAmount;
			
			//Shield damage calculation
			if(char_shdStat > 0)
			{
				if(a_dmgType == DMG_TYPE.HARD || a_dmgType == DMG_TYPE.EM) tDmg *= 1.1;
				
				if(tDmg >= char_shdStat)
				{
					tDmg -= char_shdStat;
					char_shdStat = 0;
				}else
				{
					char_shdStat -= tDmg;
					tDmg = 0;
				}
				char_shdDelayTimer = 0;
			}
			
			
			
			if(a_dmgType == DMG_TYPE.THERMAL || a_dmgType == DMG_TYPE.SOFT) tDmg *= 1.1;
			//Vitality Damage Calculation
			if(vitStat > 0)
			{
						
				if(tDmg >= vitStat)
				{
					tDmg -= vitStat;
					vitStat = 0;
				}else
				{
					vitStat -= tDmg;
					tDmg = 0;
				}
			}
			
			//Health Damage Calculation and 1-hit protection
			if(tDmg > 0) inDamage += tDmg;
			
			if(teamID == TEAMID.Ally)
			{
				if(inDamage > char_maxHP*char_dmgCap) 
				{
					var tDiff = tDmg - hp;
					var tVal = abs(round(char_maxHP / tDiff));
					AttachMod(new debuff_wounds(), id, id, infinity, tVal);
					tDmg = char_maxHP*char_dmgCap;
				}
			}
			
			if(a_dmgType == DMG_TYPE.EM || a_dmgType == DMG_TYPE.SOFT)
			{
				var tKO = (tDmg*0.5) * char_KORes;
				AttachMod(debuff_KO, id, id, 10, tKO, true)
				hp -= tDmg*0.5;
			}else hp -= tDmg;
			/*
			//Armour Damage Calculation
			if(char_armLvl > 0)
			{
				var amDmg = tDmg*0.75;
				if(amDmg >= char_armStat)
				{
					amDmg -= char_armStat;
					char_armStat = 0;
					AttachMod(debuff_stunned, id, id, 0.5, 1, false);
					tDmg = (tDmg*0.25)+amDmg;
				}else
				{
					char_armStat -= amDmg;
					tDmg *= 0.25;
				}
			
			}
			*/

		
		}
		//Update HitColor
			if(char_shdStat > 0)		hitCol = c_yellow;
			else if(vitStat > 0)		hitCol = c_red;
			else						hitCol = c_red;
	}
}

function Char_ProcessNonLethalDamage(a_char)
{
	with (a_char)
	{
		var tKO = inKO;
		if(global.debug) tKO = 0;
		
		if(inKO != 0)
		{
			if(tKO >= char_shdStat*2)
			{
				tKO -= char_shdStat*2;
				char_shdStat = 0;
				char_shdDelayTimer = 0;
			}else
			{
				char_shdStat -= tKO/2;
				tKO = 0;
				char_shdDelayTimer = 0;
			}
		
			if(char_armLvl > 0)
			{
				if(tKO >= char_armStat*4)
				{
					tKO -= char_armStat*4;
					char_armStat = 0;
					AttachMod(debuff_stunned, id, id, 5, 1, false);
				}else
				{
					char_armStat -= tKO/4;
					tKO = 0;
				}
			}

	
			if(teamID == TEAMID.Ally)
			{
				if(tKO > char_maxHP*char_dmgCap) 
				{
					tKO = char_maxHP*char_dmgCap;
				}
			}
	
			if(tKO > 0) AttachMod(debuff_KO, id, id, 10, tKO, true);
		}
	}
}

function Char_ProcessHitFX(a_char)
{
	with (a_char)
	{
		if(wasHit)
		{
			var sfx = undefined;
			var camStr = 0;
			if(PlayerControlled)
			{
				if(hp <= 0) 
				{
					sfx = sfx_player_incap;
					camStr = 100;
				}
				else 
				{
					sfx = sfx_player_hurt;
					camStr = 100 - ((hp / char_maxHP)*100);
				}
		
			}else
			{
				if(hp <= 0) 
				{
					sfx = sfx_explosion;
					camStr = 40;
				}
				else
				{
					sfx = sfx_hurt;
					camStr = 10;
				}
		
			}
			if(sfx != undefined) audio_play_sound(sfx,6,false);
			ShakeScreen(camStr);
		}	
	}
}

function Char_CalculateIncap(a_char)
{
	with (a_char)
	{
		if(hp - char_KO <= 0)
		{
			if(droppedLoot == false)
			{
				if(teamID == TEAMID.Enemy)
				{
					for(var i = 0; i < lootDrops; i += 1)
					{
						score += 10*(global.enemyLevel*global.enemyIntensity);
						instance_create_layer(x,y,"Instances", base_pickupSpawner);
					}
				}
				droppedLoot = true;
			}
			if(teamID == TEAMID.Enemy)
			{
				ds_list_delete(global.enmList,ds_list_find_index(global.enmList, id));
			}
			AttachMod(debuff_incap, id, id, 1);
			AttachMod(debuff_moveLocked, id, id, 1);
	
		}
		
		if(isIncap || isStunned)
		{
			char_preppingAttack = false;
			inFocus	= 0;
			inLockDir = 0;
			inWeapon1 = 0;
			inWeapon2 = 0;
			inUtility1 = 0;
			inUtility2 = 0;
			inAbility1 = 0;
			inAbility2 = 0;
			inCore = 0;
			inInteract = 0;
			mx = 0;
			my = 0;
		}
	}
}

function Char_ProcessHealthStat(a_char)
{
	with (a_char)
	{
		if(hp > char_healCap)
		{
			vitStat = hp - char_healCap;
			hp = char_healCap;
		}

		//Armour Regen Calculation
	
		if(char_armLvl > 0)
		{
			if(char_armStat > 0)
			{
				char_armStat += char_maxArm*(GetDeltaTime(timeScale)*0.1);
				char_armStat = clamp(char_armStat, 0, char_maxArm);
			}else
			{
				char_armTimer += GetDeltaTime(timeScale);
				if(char_armTimer >= char_armTime)
				{
					char_armTimer = 0;
					char_armStat = char_maxArm;
				}
			}
		}
		//Shield Regen Calculation
		if(char_shdStat < char_maxShd && char_shieldLvl > 0)
		{
			char_shdDelayTimer += GetDeltaTime(timeScale);
			if(char_shdDelayTimer >= char_shdDelayRate)
			{
				if(char_shdStat <= 0)
				{
					char_shdBuffer += ((char_shdChargeRate*2) * GetDeltaTime(timeScale));	
					if(char_shdBuffer >= char_shdBufferMax)
					{
						char_shdStat = char_shdBuffer;
						char_shdBuffer = 0;
					}
				}else
				{
					char_shdStat += (char_shdChargeRate * GetDeltaTime(timeScale));	
				}
			}
	
		}else if (char_shdStat > char_maxShd) char_shdStat = char_maxShd;

		if(char_armStat > char_maxArm)
		{
			char_armStat = char_maxArm;	
		}
		
		//Vitality Decay
		if(vitStat > 0)
		{
			vitStat -= maxVit*(GetDeltaTime(timeScale)/30);
			if(vitStat > maxVit)
			{
				vitStat = maxVit;	
			}
			if(vitStat < 0)
			{
				vitStat = 0	
			}
	
		}
		
		//Stamina Regen, probably not even used TBH
		staminaRegenTimer += GetDeltaTime(timeScale);
		if(staminaRegenTimer > staminaRegenDelay)
		{
			if(staminaStat < maxStamina)
			{
				var stamUp = staminaRegen*GetDeltaTime(timeScale);
				if(isSprinting)
				{
					stamUp *= 0.25;
				}
				staminaStat += stamUp;
			}
		}
		if(staminaStat > maxStamina) staminaStat = maxStamina;
		
	}
}

function Char_ProcessTarget(a_char)
{
	with (a_char)
	{
		if(!isIncap)
		{
			//if not locked onto target, or locking on for first time
			if((!inLockDir && (retargetTimer > retargetTime)) || (inLockDir && !isLocked))
			{
				retargetTimer = 0;
				var aimCap = autoAimCap;
				if(inFocus) aimCap = focusAimCap;
		
				var tTeam = TEAMID.Enemy;
				var tTarg = FindTargetInDir(x,y,GetDirection(x,y,mx,my,true, false), base_Character,id, aimCap, 2000, true,TEAMID.Other);
				if(CheckExistance(tTarg) == true)
				{
					target = tTarg;
				}else
				{
					target = undefined;	
				}
			}
			
			if(inLockDir) isLocked = true;
			else isLocked = false;
		}
	}
}
#endregion

#region Update Logic
function Char_ProcessAbilityInputs(a_char)
{
	with (a_char)
	{
		if(instance_exists(char_AC))
		{
			//Resets inputs
			char_AC.ac_activeLoadout.ld_slots.ld_util2.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_util1.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_abl1.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_wep1.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_abl2.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_wep2.inPressed =	false;
			char_AC.ac_activeLoadout.ld_slots.ld_core.inPressed =	false;
			
			//Pass input code to abilities/if usable
			//Process secondary abilities first, because on Controller they use the same face button + alt bind
			if(inUtility2)			char_AC.ac_activeLoadout.ld_slots.ld_util2.inPressed = true;
			else if(inUtility1)		char_AC.ac_activeLoadout.ld_slots.ld_util1.inPressed = true;
			else if (inAbility1)	char_AC.ac_activeLoadout.ld_slots.ld_abl1.inPressed = true;
			else if (inWeapon1)		char_AC.ac_activeLoadout.ld_slots.ld_wep1.inPressed = true;
			else if (inAbility2)	char_AC.ac_activeLoadout.ld_slots.ld_abl2.inPressed = true;
			else if (inWeapon2)		char_AC.ac_activeLoadout.ld_slots.ld_wep2.inPressed = true;
			else if(inCore)			char_AC.ac_activeLoadout.ld_slots.ld_core.inPressed = true;
			else if(inInteract)
			{
				if(CheckExistance(char_TargetIntr))
				{
					char_TargetIntr.intr_holdTmr += GetDeltaTime(timeScale);
					char_TargetIntr.intr_isActive = true;
				}
			}
		}
	}
}

function Char_ProcessLookDir(a_char)
{
	with (a_char)
	{
		//Handles locking on to targets and auto-aim
		if(CheckExistance(target))
		{
			//Calculate lookdir for Auto-Aim
			if(lx != 0 && ly != 0)
			{
				inDir = GetDirection(x,y,lx,ly,true, true);
			}else
			{
				inDir = direction;	
			}
			tarDir = GetDirection(x,y,target.x,target.y, false, true)
			
			var aimCap = autoAimCap;
			if(inFocus) aimCap = focusAimCap;
			aimCap *= 0.5;
	
			//If Lock-on input is 1, or looking within auto-aim dir, set lookdir to target
			if(inLockDir || abs(angle_difference(inDir,tarDir)) < aimCap)
			{
				var tAng = angle_difference(lookDir, GetDirection(x,y,target.x,target.y, false,true));
				clamp(tAng,-10,10);
				lookDir = lerp(lookDir, lookDir-tAng,0.8 * (timeScale * global.timeScale));	
			}
		}else
		{
			//if no LookInput, set LookInput to MoveInput
			if((lx == 0 && ly == 0) && (mx != 0 || my != 0))
			{
				lx = mx;
				ly = my;
			}else if (lx == 0 && ly == 0)
			{
				var tDir = GetCoord(direction);
				lx = tDir.x;
				ly = tDir.y;
			}	
		
			//Stops the character from looking to 0,0 when no look dir given
			if(lx != 0 || ly != 0)	//Only changes the characters direction if a key is pressed
			{
				if(inLockDir == 0)
				{	
					var tAng = angle_difference(lookDir, GetDirection(x,y,lx,ly, true,true));
					lookDir = lerp(lookDir, lookDir-tAng,0.2 * (timeScale * global.timeScale));
				}else
				{
					lookDir = direction;	
				}
	
			}
		}
		
		direction = lookDir;
	}
}

function Char_ProcessModifiers(a_char)
{
	with (a_char)
	{
		for(var i = 0; i < array_length(char_modArr); i += 1)
		{
			char_modArr[i].mod_PreStep();
			char_modArr[i].mod_Update();
			char_modArr[i].mod_EndStep();
			if(char_modArr[i].canDelete)
			{
				delete(char_modArr[i]);
				array_delete(char_modArr, i, 1);
				i -= 1;	
			}
		}	
	}
}
#endregion

#region End-Update Logic
function Char_ProcessIncap(a_char)
{
	with (a_char)
	{
		if((PlayerControlled || teamID == TEAMID.Ally) && isIncap)
		{
			if(!CheckExistance(char_rezBeacon))
			{
				var tInst = instance_create_layer(x,y,"Interactables", intr_ReviveChar);
				tInst.intr_resTarget = id;
				char_rezBeacon = tInst;
			}
		}

		if(hp - char_KO > 0 || PlayerControlled == true)
		{
			despawnTimer = 0;	
	
		}else
		{
			if(sprite_index == animator.anim_incap && image_index >= sprite_get_number(sprite_index)-1)
			{
				if(charData = undefined) charData = new CharData(char_Name, object_index, animator);
				var tType = DMG_TYPE.HARD;
				if(hp > 0) tType = DMG_TYPE.SOFT;
				array_push(global.currentRoom.rd_TombstoneArr, new Tombstone(charData, new Vector2(x,y), tType, ai_operator.ai_patrolID));
				if(hp <= 0)
				{
					//var tReg = GetRegion(global.currentRoom.rd_region);
					//tReg.rg_enmKilled += 1;
				}
		
				instance_deactivate_object(id);
			}
		}
	}
}

function Char_ProcessAnimationState(a_char)
{
	with (a_char)
	{
		if(animator != undefined)
		{
			if(inFocus && !isDashing) animState = ANIM_STATE.IDLE;
			if(mx != 0 || my != 0) animState = ANIM_STATE.WALK;
			else animState = ANIM_STATE.IDLE;
			if(isDashing || isSprinting) animState = ANIM_STATE.DASH;
			if(isStunned) animState = ANIM_STATE.STUNNED;
			if(isIncap)
			{
				if(hp > 0) animState = ANIM_STATE.REVIVE;
				else animState = ANIM_STATE.INCAP;
			}
		}	
	}
}

function Char_ProcessCombatWidth(a_char)
{
	with (a_char)
	{
		//Cleans up the Attacker Array of enemies who aren't actively fighting the character
		if(array_length(ai_atkArr) > global.cmd_combatWidth)
		{
			array_delete(ai_atkArr, global.cmd_combatWidth-1, infinity);	
		}
		for(var i = 0; i < array_length(ai_atkArr); i += 1)
		{
			var tEnm = ai_atkArr[i];
			if(CheckExistance(tEnm) == false || tEnm.ai_operator.enemyTarget != id || tEnm.isIncap)
			{
				array_delete(ai_atkArr, i, 1);
				i -= 1;
			}
		}	
	}
}
#endregion