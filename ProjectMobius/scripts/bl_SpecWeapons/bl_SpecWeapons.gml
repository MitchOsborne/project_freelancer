// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

with global.bp_Abilities.bp_Soulfire
{
	bp_Name = "Soul Fire";
	bp_Frame = base_wep_Soulfire;
	bp_Slot = LOADOUT_SLOT.ABILITY1;
	bp_AbilityType = ABILITY_TYPE.RANGED;
	
	bp_AttackArr = [atk_Soulfire];
	bp_AttackFin = undefined;
	bp_StayActive = false;
	with bp_WepStats
	{
		bp_Range = 1200; 
		bp_Spread = 45; 
		bp_DMGScale = 1;
		bp_RPM = 60;
		bp_Ammo = 1;
		bp_ReloadSpd = 2;
	}
	
	with bp_StatMods
	{
		bp_Range = 1; 
		bp_Spread = 1;
		bp_DMGScale = 1; 
		bp_RPM = 1;
		bp_Ammo = 1;
		bp_ReloadSpd = 1;
	}
}

/*
globalvar bp_Dragonwave;
bp_Dragonwave = new bp_BaseBlueprint()
with bp_Dragonwave
{
	bp_Name = "Dragonwave";
	bp_Frame = base_wep_Wave;
	bp_Slot = LOADOUT_SLOT.WEAPON2;
	bp_AbilityType = ABILITY_TYPE.RANGED;
	
	with bp_WepStats
	{
		bp_Range = 600; 
		bp_Spread = 7.5; 
		bp_DMGScale = 1;
		bp_RPM = 60;
		bp_Ammo = 5;
		bp_ReloadSpd = 5;
	}
	
	with bp_StatMods
	{
		bp_Range = 1; 
		bp_Spread = 1;
		bp_DMGScale = 1; 
		bp_RPM = 1;
		bp_Ammo = 1;
		bp_ReloadSpd = 1;
	}

	bp_SpriteSet = sprSet_RKBase;
	bp_SFX = sfx_shoot1;
}


globalvar bp_Dragonfire;
bp_Dragonfire = new bp_BaseBlueprint()
with bp_Dragonfire
{
	bp_Name = "Dragonfire";
	bp_Frame = base_wep_Shotgun;
	bp_Slot = LOADOUT_SLOT.WEAPON2;
	bp_AbilityType = ABILITY_TYPE.RANGED;
	
	with bp_WepStats
	{
		bp_Range = 600; 
		bp_Spread = 5; 
		bp_DMGScale = 1;
		bp_RPM = 1800;
		bp_Ammo = 125;
		bp_ReloadSpd = 1.5;
	}
	
	with bp_StatMods
	{
		bp_Range = 1; 
		bp_Spread = 1;
		bp_DMGScale = 1; 
		bp_RPM = 1;
		bp_Ammo = 1;
		bp_ReloadSpd = 1;
	}

	bp_SpriteSet = sprSet_RKBase;
	bp_SFX = sfx_shoot1;
}

*/

