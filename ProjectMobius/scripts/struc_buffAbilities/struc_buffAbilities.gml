// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function abl_Sprint(a_bp) : base_SelfBuff(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_sprint));
}

function base_FoodAbility(a_bp) : base_SelfBuff(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_FoodCooldown(self));
}

function base_AltLoadout(a_bp) : base_SelfBuff(a_bp) constructor
{
	if(a_bp.bp_AltLd != undefined) array_push(abl_Components, new abl_comp_LoadoutSwap(self, a_bp.bp_AltLd));
}

function base_UnequipLoadout(a_bp) : base_SelfBuff(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_UnequipLoadout(self));
}

function abl_RegenPop(a_bp) : base_FoodAbility(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_hpRegen, 10, 3));
	array_push(abl_Components, new abl_comp_SelfBuff(self, debuff_foodCD, 60/parentAbility.abl_CalcCDScale, 1));
	array_push(abl_Components, new abl_comp_AIHealSelf(self));
	abl_sfx = sfx_psi_On;
}

function abl_LifeSteal(a_bp) : base_SelfBuff(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_lifeSteal, 10, 20, true));	
	array_push(abl_Components, new abl_comp_AIHealSelf(self));
	abl_sfx = sfx_boom;
}


function abl_CritPop(a_bp) : base_FoodAbility(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_critBuff, 10, 1));
	array_push(abl_Components, new abl_comp_SelfBuff(self, debuff_foodCD, 60/parentAbility.abl_CalcCDScale, 1));
	abl_sfx = sfx_psi_On;
}

function abl_SelfDestruct(a_bp) : base_SelfBuff(a_bp) constructor
{
	array_push(abl_Components, new abl_comp_SelfBuff(self, debuff_death, 1, 1));
}

function abl_KobaldRegroup(a_bp) : base_SelfBuff(a_bp)  constructor
{
	array_push(abl_Components, new abl_comp_Firerate(self));
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_hpRegen, 1, 10, true));
	array_push(abl_Components, new abl_comp_SelfBuff(self, debuff_abilityLocked, 1, 1));
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_evade, 1, 1));
	array_push(abl_Components, new abl_comp_SelfBuff(self, buff_invis, 1, 1));
	array_push(abl_Components, new abl_comp_SelfBuff(self,buff_KobaldTunnel, 1));
	array_push(abl_Components, new abl_comp_KobaldRegroup(self));
	array_push(abl_Components, new abl_comp_AICantUse(self));
}