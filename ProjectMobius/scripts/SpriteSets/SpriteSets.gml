// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
globalvar sprSet_RKBase;
sprSet_RKBase = 
{
	sprSet_Idle : spr_wep_RK47_idle,
	sprSet_Attack1 : spr_wep_RK47_fire,
	sprSet_Attack2 : spr_wep_RK47_fire,
	sprSet_Attack3 : spr_wep_RK47_fire,
	sprSet_Attack4 : spr_wep_RK47_fire,
	sprSet_Reload : spr_wep_RK47_reload,
	sprSet_Equip : spr_wep_RK47_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_DomR;
sprSet_DomR = 
{
	sprSet_Idle : spr_wep_DomR_idle,
	sprSet_Attack1 : spr_wep_DomR_fire,
	sprSet_Attack2 : spr_wep_DomR_fire,
	sprSet_Attack3 : spr_wep_DomR_fire,
	sprSet_Attack4 : spr_wep_DomR_fire,
	sprSet_Reload : spr_wep_DomR_reload,
	sprSet_Equip : spr_wep_DomR_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_Hands;
sprSet_Hands = 
{
	sprSet_Idle : spr_None,
	sprSet_Attack1 : spr_wep_Hands_Atk1,
	sprSet_Attack2 : spr_wep_Hands_Atk2,
	sprSet_Attack3 : spr_wep_Hands_Atk4,
	sprSet_Attack4 : spr_wep_Hands_Atk4,
	sprSet_Reload : spr_None,
	sprSet_Equip : spr_None,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}

globalvar sprSet_Fists;
sprSet_Fists = 
{
	sprSet_Idle : spr_None,
	sprSet_Attack1 : spr_wep_Hands_Atk1,
	sprSet_Attack2 : spr_wep_Hands_Atk2,
	sprSet_Attack3 : spr_wep_Hands_Atk4,
	sprSet_Attack4 : spr_wep_Hands_Atk4,
	sprSet_Reload : spr_wep_Hands_Reload,
	sprSet_Equip : spr_wep_Hands_Reload,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}

globalvar sprSet_KnifeThrow;
sprSet_KnifeThrow = 
{
	sprSet_Idle : spr_wep_ComDag_idle,
	sprSet_Attack1 : spr_wep_Hands_Atk1,
	sprSet_Attack2 : spr_wep_Hands_Atk2,
	sprSet_Attack3 : spr_wep_Hands_Atk3,
	sprSet_Attack4 : spr_wep_Hands_Atk4,
	sprSet_Reload : spr_wep_ComDag_reload,
	sprSet_Equip : spr_wep_NezDag_equip,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}

globalvar sprSet_Rook443Base;
sprSet_Rook443Base = 
{
	sprSet_Idle : spr_wep_Rook443_idle,
	sprSet_Attack1 : spr_wep_Rook443_fire,
	sprSet_Attack2 : spr_wep_Rook443_fire,
	sprSet_Attack3 : spr_wep_Rook443_fire,
	sprSet_Attack4 : spr_wep_Rook443_fire,
	sprSet_Reload : spr_wep_Rook443_reload,
	sprSet_Equip : spr_wep_Rook443_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_Naga44Base;
sprSet_Naga44Base = 
{
	sprSet_Idle : spr_wep_Naga44_idle,
	sprSet_Attack1 : spr_wep_Naga44_fire,
	sprSet_Attack2 : spr_wep_Naga44_fire,
	sprSet_Attack3 : spr_wep_Naga44_fire,
	sprSet_Attack4 : spr_wep_Naga44_fire,
	sprSet_Reload : spr_wep_Naga44_reload,
	sprSet_Equip : spr_wep_Naga44_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_M9T;
sprSet_M9T = 
{
	sprSet_Idle : spr_wep_M9T_idle,
	sprSet_Attack1 : spr_wep_M9T_fire,
	sprSet_Attack2 : spr_wep_M9T_fire,
	sprSet_Attack3 : spr_wep_M9T_fire,
	sprSet_Attack4 : spr_wep_M9T_fire,
	sprSet_Reload : spr_wep_M9T_reload,
	sprSet_Equip : spr_wep_M9T_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_SP45Base;
sprSet_SP45Base = 
{
	sprSet_Idle : spr_wep_SP45_idle,
	sprSet_Attack1 : spr_wep_SP45_fire,
	sprSet_Attack2 : spr_wep_SP45_fire,
	sprSet_Attack3 : spr_wep_SP45_fire,
	sprSet_Attack4 : spr_wep_SP45_fire,
	sprSet_Reload : spr_wep_SP45_reload,
	sprSet_Equip : spr_wep_SP45_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_SP45r;
sprSet_SP45r = 
{
	sprSet_Idle : spr_wep_SP45r_idle,
	sprSet_Attack1 : spr_wep_SP45r_fire,
	sprSet_Attack2 : spr_wep_SP45r_fire,
	sprSet_Attack3 : spr_wep_SP45r_fire,
	sprSet_Attack4 : spr_wep_SP45r_fire,
	sprSet_Reload : spr_wep_SP45r_reload,
	sprSet_Equip : spr_wep_SP45r_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_DBSGBase;
sprSet_DBSGBase = 
{
	sprSet_Idle : spr_wep_DBSG_idle,
	sprSet_Attack1 : spr_wep_DBSG_fire,
	sprSet_Attack2 : spr_wep_DBSG_fire,
	sprSet_Attack3 : spr_wep_DBSG_fire,
	sprSet_Attack4 : spr_wep_DBSG_fire,
	sprSet_Reload : spr_wep_DBSG_reload,
	sprSet_Equip : spr_wep_DBSG_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}


globalvar sprSet_DBSGsBase;
sprSet_DBSGsBase = 
{
	sprSet_Idle : spr_wep_DBSGs_idle,
	sprSet_Attack1 : spr_wep_DBSGs_fire,
	sprSet_Attack2 : spr_wep_DBSGs_fire,
	sprSet_Attack3 : spr_wep_DBSGs_fire,
	sprSet_Attack4 : spr_wep_DBSGs_fire,
	sprSet_Reload : spr_wep_DBSGs_reload,
	sprSet_Equip : spr_wep_DBSGs_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_Mosin;
sprSet_Mosin = 
{
	sprSet_Idle : spr_wep_Mosin_idle,
	sprSet_Attack1 : spr_wep_Mosin_fire,
	sprSet_Attack2 : spr_wep_Mosin_fire,
	sprSet_Attack3 : spr_wep_Mosin_fire,
	sprSet_Attack4 : spr_wep_Mosin_fire,
	sprSet_Reload : spr_wep_Mosin_reload,
	sprSet_Equip : spr_wep_Mosin_equip,
	sprSet_RotEquip : true,
	sprSet_isMelee : false,
}

globalvar sprSet_Katana;
sprSet_Katana = 
{
	sprSet_Idle : spr_wep_Katana_Idle,
	sprSet_Attack1 : spr_wep_Katana_Atk1,
	sprSet_Attack2 : spr_wep_Katana_Atk2,
	sprSet_Attack3 : spr_wep_Katana_Atk3,
	sprSet_Attack4 : spr_wep_Katana_Atk4,
	sprSet_Reload : spr_wep_Katana_reload,
	sprSet_Equip : spr_wep_Katana_equip,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}

globalvar sprSet_ComDag;
sprSet_ComDag = 
{
	sprSet_Idle : spr_wep_ComDag_idle,
	sprSet_Attack1 : spr_wep_ComDag_Atk1,
	sprSet_Attack2 : spr_wep_ComDag_Atk2,
	sprSet_Attack3 : spr_wep_ComDag_Atk3,
	sprSet_Attack4 : spr_wep_Katana_Atk4,
	sprSet_Reload : spr_wep_ComDag_reload,
	sprSet_Equip : spr_wep_NezDag_equip,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}

globalvar sprSet_NezDag;
sprSet_NezDag = 
{
	sprSet_Idle : spr_wep_NezDag_idle,
	sprSet_Attack1 : spr_wep_NezDag_Atk1_dep,
	sprSet_Attack2 : spr_wep_NezDag_Atk2_dep,
	sprSet_Attack3 : spr_wep_NezDag_Atk3_dep,
	sprSet_Attack4 : spr_wep_NezDag_Atk1_dep,
	sprSet_Reload : spr_wep_ComDag_reload,
	sprSet_Equip : spr_wep_NezDag_equip,
	sprSet_RotEquip : false,
	sprSet_isMelee : true,
}