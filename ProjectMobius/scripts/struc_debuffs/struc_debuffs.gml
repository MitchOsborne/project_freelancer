// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_debuff() : base_Modifier() constructor
{
	isDebuff = true;	
}

function debuff_moveLocked() : base_debuff() constructor
{
	name = "Move Locked";
	stackLimit = 1;
	stackType = STACK_TYPE.NONE;
}

function debuff_harpooned() : base_debuff() constructor
{
	name = "Harpooned";
	stackLimit = 1;
	stackType = STACK_TYPE.OTHER;
	
	hrpn_range = 160;
	hrpn_inStr = 0.5;
	hrpn_outStr = 2;
	origin = undefined;
	mod_Update = function()
	{
		if(origin == undefined) origin = new Vector2(subject.x, subject.y);
		var tDist = point_distance(origin.x, origin.y, subject.x, subject.y);
		if(tDist <= hrpn_range)
		{
			var tDir = point_direction(origin.x, origin.y, subject.x, subject.y) + 90;
			var tForce = lerp(hrpn_inStr, hrpn_outStr, tDist/hrpn_range);
			AttachLinearForce(subject, owner, 0, tDir, tForce);
		}else durTimer += GetDeltaTime(subject.timeScale);
	}
	
	mod_Draw = function()
	{
		if(origin != undefined)
		{
			var tDist = point_distance(origin.x, origin.y, subject.x, subject.y);
			var tDir = point_direction(origin.x, origin.y, subject.x, subject.y);
			var tScale = tDist/sprite_get_width(spr_chain);
			draw_sprite(spr_Spike, -1, origin.x, origin.y);
			var tCol = merge_color(c_white, c_red, tDist/hrpn_range);
			draw_sprite_ext(spr_chain, -1, origin.x, origin.y, tScale, 1, tDir, tCol, 1);
			draw_sprite(spr_Spike, -1, subject.x, subject.y);
		}
	}
	
}

function debuff_hitPause() : base_debuff() constructor
{
	name = "Hit Pause";
	stackLimit = 1;
	mod_Update = function()
	{
		subject.timeScale *= 0.5;
	}
}

function debuff_exposedBack() : base_debuff() constructor
{
	name = "Exposed Back";
	stackLimit = 1;
	displayName = false;
	
	duration = infinity;
	mod_procCD = 5;
	mod_procTmr = 0;
	
	mod_Update = function()
	{
		mod_procTmr -= GetDeltaTime(subject.timeScale);	
	}
	
	mod_OnHurt = function(a_hitReport)
	{
		if(mod_procTmr <= 0)
		{
			if(abs(angle_difference(subject.direction, a_hitReport.hit_Proj.direction)) <= 90)
			{
				mod_procTmr = mod_procCD;
				AttachMod(debuff_stunned, subject, owner, 2, 1, false);
			}
		}
	}
}

function debuff_exposedAttack() : base_debuff() constructor
{
	name = "Exposed Attack";
	stackLimit = 1;
	displayName = false;
	
	duration = 1;
	
	mod_OnHurt = function(a_hitReport)
	{
		mod_canProc = false;
		AttachMod(debuff_stunned, subject, owner, 2, 1, false);
		audio_play_sound(sfx_coin_s, 1, false);
		duration = 0;
	}
	
	mod_Draw = function()
	{
		DrawOutline(subject, c_Gold,0.5);
	}
}

function debuff_wounds() : base_debuff() constructor
{
	name = "Wounds";
	stackLimit = 80;
	duration = infinity;
}

function debuff_tranq() : base_debuff() constructor
{
	name = "Tranquilised";
	stackType = STACK_TYPE.ALL;
	stackLimit = infinity;
	duration = 30;
	
	mod_Update = function()
	{
		if(subject.teamID == TEAMID.Ally) stacks = clamp(stacks,0,subject.char_maxHP*subject.char_dmgCap);
		
		AttachMod(debuff_KO, subject, owner, 180, (stacks/3)*GetDeltaTime(subject.timeScale), true);
	}
}

function debuff_KO() : base_debuff() constructor
{
	name = "Knockout";
	stackLimit = infinity;
	duration = 180;
	stackType = STACK_TYPE.ALL;
	animTmr = 0;
	mod_Update = function()
	{
		if(subject.teamID == TEAMID.Ally) duration = 10;
		else duration = clamp(180*(stacks/stackLimit),10,infinity);
		stackLimit = ceil(subject.char_maxHP);
		subject.timeScale = 1 - clamp((stacks/stackLimit),0,0.3);
		subject.char_KO = stacks;
	}
	
	
	mod_Draw = function()
	{
		if(!subject.isIncap)
		{
			//var tStr = (stacks/subject.hp)*100;
			//DrawTextOutline(subject.x, subject.y, 1, string(tStr), c_white, c_black);	
		}else
		{
			animTmr += GetDeltaTime(subject.timeScale)*10;
			if(animTmr >= sprite_get_number(spr_KO_3)) animTmr = 0;
			
			draw_sprite(spr_KO_3, floor(animTmr), subject.x, subject.y);
		}
	}
}

function debuff_abilityLocked() : base_debuff() constructor
{
	name = "Ability Locked";
	duration = 0.3;
	displayName = true;
	mod_Update = function()
	{
		subject.isAbilityLocked = true;
	}
	
	mod_Destroy = function()
	{
		//subject.isAbilityLocked = false;
		Destroy_Mod(self);
	}
	
}

function debuff_slowed() : base_debuff() constructor
{
	stackType = STACK_TYPE.ALL;
	name = "Slowed";
	stackLimit = 10;
	displayName = true;
}

function debuff_death() : base_debuff() constructor
{
	stackType = STACK_TYPE.NONE;
	name = "Death";
	stackLimit = 1;
	displayName = true;
	
	mod_Update = function()
	{
		subject.hp = 0;
		duration = 0;
	}
}


function debuff_foodCD() : base_debuff() constructor
{
	name = "Drink Cooldown";
	displayName = true;
}

function debuff_psiTouched() : base_debuff() constructor
{
	name = "Psi-Touched";
	duration = 3;
	displayName = true;
	stackLimit = 1;
	isDebuff = true;
	
	mod_Init = function()
	{
		Apply_Tagging(self);	
	}
	
	mod_Update = function()
	{
		subject.isPsiTouched = true;
		
	}
	
	mod_Destroy = function()
	{
		//subject.isPsiTouched = false;
		Remove_Tagging(self);
		Destroy_Mod(self);
	}
}

function debuff_Thrown() : base_debuff() constructor
{
	name = "Thrown";
	damageScale = 3;
	stunDur = 1;
	displayName = true;
	stackLimit = 1;
	isDebuff = true;
	hitList = ds_list_create();
	mod_Update = function()
	{
		
		if(collision_circle(subject.x, subject.y, 32, base_PhysObstacle, false, false))
		{
			DamageAgent(subject, owner.char_damageStat*damageScale);
			AttachMod(debuff_stunned, subject, owner, stunDur);
			durTimer = duration;
		}
		if(collision_circle_list(subject.x, subject.y, 32, base_Character, false, false, hitList, false))
		{
			for(var i = 0; i < ds_list_size(hitList); i += 1)
			{
				var tInst = hitList[|i];
				if(tInst != subject)
				{
					if(tInst.teamID != owner.teamID)
					{
						DamageAgent(subject, owner.char_damageStat*damageScale);
						DamageAgent(tInst, owner.char_damageStat*damageScale);
						AttachMod(debuff_stunned, subject, owner, stunDur);
						AttachMod(debuff_stunned, tInst, owner, stunDur);
						
						durTimer = duration;
					}
				}
			}
			
		}
	}
	
	mod_Destroy = function()
	{
		//subject.isPsiTouched = false;
		Destroy_Mod(self);
	}	
}

function debuff_stunned() : base_debuff() constructor
{
	name = "Stunned";
	stackType = STACK_TYPE.NONE;
	displayName = true;
	stackLimit = 1;
	isDebuff = true;
	duration = 1;
	mod_Update = function()
	{
		subject.isStunned = true;
		durTimer += (GetDeltaTime(subject.timeScale)*subject.char_stunRes)/5;
	}
	
	mod_Destroy = function()
	{
		//subject.isStunned = false;	
		Destroy_Mod(self);
	}
}


function debuff_incap() : base_debuff() constructor
{
	name = "Incapacitated";
	displayName = true;
	stackLimit = 1;
	isDebuff = true;
	
	incap_WoundTmr = 0;
	mod_Update = function()
	{
		subject.isIncap = true;
		subject.isPhased = true;
		
		if(subject.hp < 0) incap_WoundTmr += GetDeltaTime(subject.timeScale);
		if(incap_WoundTmr >= 1)
		{
			incap_WoundTmr = 0;
			AttachMod(new debuff_wounds(), subject, subject, infinity);
		}
	}
	mod_Destroy = function()
	{
		subject.isIncap = false;
		Destroy_Mod(self);
		if(subject.teamID == TEAMID.Enemy && subject.hp > 0)
		{
			if(ds_list_find_index(global.enmList, subject) == -1) ds_list_add(global.enmList, subject);
		}
	}
}