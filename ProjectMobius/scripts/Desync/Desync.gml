// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function InitDesync(callerID, a_objList, a_desyncFunction, a_threadCount){
	var tInst = instance_create_layer(callerID.x, callerID.y, "debug", Desynchroniser);
	tInst.desyncFunc = a_desyncFunction;
	tInst.threadCount = a_threadCount;
	
	var tSize = ds_list_size(a_objList);
	var ix = 0;
	var iy = 0;
	
	for(var t = 0; t < a_threadCount; t += 1)
	{
		ds_list_add(tInst.threadList,ds_list_create());
	}
	
	for(var n = 0; n < tSize; n += 1)
	{	
		
		tInst.threadList[|ix][|iy] = a_objList[|n];
		ix += 1;
		if(ix >= tInst.threadCount)
		{
			ix = 0;
			iy += 1;
		}
	}
	return tInst;
}

