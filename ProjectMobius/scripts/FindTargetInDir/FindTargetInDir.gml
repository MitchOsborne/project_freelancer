function FindTargetInDir(a_aX, a_aY, a_rot, a_targetObj, a_callerObj, a_dirCap, a_distCap, a_LoS, a_TargetTeam) {


	var dirCap = 90;		//Direction in degrees that it will consider targets viable
	var distWeight = 4;	//The weighting distance from the origin has on the score
	var dirWeight = 1;		//The weighting on score the difference in direction from the direction the origin is facing has.
	var distCap = 500;		//Affects the weighting calculation for distance to origin
	var tTeam = a_TargetTeam;

	var target = undefined;
	var topScore = -1;

	if(a_dirCap >= 0)
	{
		dirCap = a_dirCap;	
	}
	if(a_distCap >= 0)
	{
		distCap = a_distCap;	
	}

	for(i = 0; i < instance_number(a_targetObj); i++)
	{
	
		var tTarg = instance_find(a_targetObj,i);
		if(tTarg != a_callerObj)
		{
			if(!tTarg.isIncap && !tTarg.isInvis)
			{
				var targetDir = GetDirection(a_aX, a_aY, tTarg.x, tTarg.y, false, false);
				var dirDiff = abs(angle_difference(a_rot, targetDir));
				var distDiff = point_distance(a_aX, a_aY, tTarg.x, tTarg.y);
	
		
				if(tTeam == undefined ||
				(tTeam == TEAMID.Other && tTarg.teamID != a_callerObj.teamID) ||
				(tTeam == TEAMID.Same && tTarg.teamID == a_callerObj.teamID))
				{
					if(dirDiff < dirCap/2 && distDiff < distCap)
					{
						var distScore = (distCap / distDiff) * 100;
						var dirScore = ((dirCap/2) / dirDiff) * 100;
						var tScore = (dirScore * dirWeight) + (distScore * distWeight);
		
						if(a_LoS == true)
						{
			
								var res = collision_line(a_aX,a_aY,tTarg.x, tTarg.y,base_PhysWall,false,true);
								if(CheckExistance(res) || tTarg.isInvul || tTarg.isInvis || tTarg.isIncap)
								{
									tScore = -2;
								}
						}
		
				
						if(tScore > topScore)
						{
							target = tTarg;
							topScore = tScore;
						}
					}
				}
			}
		}
	}

	return target;




}
