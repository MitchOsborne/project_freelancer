// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function base_Summon() : base_Ability() constructor
{
	smn_SummonObj = undefined;	//The Character obj to be summoned
	smn_Minion = undefined;		//Holds the created minion
	smn_Master = undefined;	//The Character that summoned the Minion
	
	
	func_PreUpdateStep = SmnPreUpdateStep;
	func_UpdateStep = SmnUpdateStep;
	
	smn_ActivateAbility = smn_ActivateSummon;
	//func_InitAbility = InitialiseWeapon;
	
	smn_SummoningMinion = false;
	smn_InControl = false;
	
	abl_LockDur = 0.1;
	
	cooldown = 1;
	reqRelease = true;
	
}

function SmnPreUpdateStep(a_smnID)
{
	with a_smnID
	{
		AbilityPreUpdateStep(self);
		owner.smn_Minion = smn_Minion;
	}
}

function SmnUpdateStep(a_smnID)
{
	with a_smnID
	{
		AbilityUpdateStep(self);
		if(activate)
		{
			if(inPressed)
			{
				didActivate = true;
				smn_ActivateAbility(self);
			}
		}
	}
}

function smn_ActivateSummon(a_smnID)
{
	with a_smnID
	{
		if(smn_InControl) smn_ReleasingControl(self);
		else if(CheckExistance(smn_Minion))
		{
			if(!smn_Minion.isIncap)
			{
				smn_AssumingDirectControl(self);
			}
		}else
		{
			if(!smn_SummoningMinion) smn_SpawnSummon(self);
		}
	}
}

function smn_AssumingDirectControl(a_smnID)
{
	with a_smnID
	{
		smn_Minion.active_operator = owner.active_operator;	
		owner.active_operator.slavedObj = smn_Minion;
		smn_Minion.PlayerControlled = owner.PlayerControlled;
		smn_Minion.smn_Master = owner;
		smn_Minion.Core = self.package;
		self.abl_isOverride = true;
		smn_InControl = true;
		instance_deactivate_object(owner);
	}
}

function smn_ReleasingControl(a_smnID)
{
	with a_smnID
	{
		owner.active_operator = smn_Minion.active_operator;
		owner.active_operator.slavedObj = owner;
		smn_Minion.active_operator = smn_Minion.ai_operator;
		smn_Minion.PlayerControlled = false;
		smn_Minion.inCore = false;
		self.abl_isOverride = false;		
		smn_InControl = false;
		owner.x = smn_Minion.x;
		owner.y = smn_Minion.y;
		SanityCheckLoadout(smn_Minion);
		SanityCheckOperator(smn_Minion);
		instance_activate_object(owner);
		
	}
}

function smn_SpawnSummon(a_smnID)
{
	with a_smnID
	{
		smn_SummoningMinion = true;
		var smnBeacon = ally_SpawnBeacon;
		if(owner.teamID == TEAMID.Enemy) smnBeacon = enm_SpawnBeacon;
		var tPos = ConvertPosToGrid(new Vector2(owner.x, owner.y));
		tPos.x *= global.gridSize;
		tPos.y *= global.gridSize;
		var tInst = instance_create_layer(tPos.x,tPos.y,"Instances", smnBeacon);
		tInst.spawnObj = smn_SummonObj;
		tInst.smn_Owner = self;
	}
}