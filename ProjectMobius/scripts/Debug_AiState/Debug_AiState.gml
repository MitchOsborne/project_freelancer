///Debug_Log(Desc, Data, OverrideDebug)
function Debug_AiState(a_desc, a_data, a_OverrideDebug) {


	if(global.debug == true || a_OverrideDebug == true)
	{
		var text = "NyukNyukGoose";
		switch(a_data)
		{
			case 0:	
				text = "AISTATE.IDLE";
				break;
			case 1:
				text = "AISTATE.ROAM";
				break;
			case 2:
				text = "AISTATE.SEEK";
				break;
			case 3:
				text = "AISTATE.ENGAGE";
				break;
			case 4:
				text = "AISTATE.FOLLOW";
				break;
			default:
			break;
		}
		show_debug_message(a_desc + text);
	}


}
