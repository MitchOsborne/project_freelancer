// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_base_UI(a_playerIndex) constructor
{
	ui_PlayerIndex = a_playerIndex;
	ui_Size = new Vector2(360,360);
	ui_Pos = new Vector2(80,320);
 
 
	ui_HeaderText = "Header Text Goes Here"; 
	ui_Sprite = spr_MenuBkg;
	
	ui_Elements = [];
	ui_Options = [];
	ui_GridWidth = 1;
	ui_GridBorder = 10;
	ui_GridSpacingHeight = 60;
	ui_GridSpacingWidth = 80;
	
	ui_OptionIndex = 0;
	
	ui_DrawFunc = UI_Draw;
	ui_UpdateFunc = UI_Update;
	ui_FillElementsFunc = UI_FillElements;
	
	ui_SubMenu = false;	//If true, will render the menu underneath this one as well
	ui_CanClose = false;
  
}

function base_defunc_uiElement(a_PI) constructor
{
	defunc_uiEl_text = "Text Goes Here";
	defunc_uiEl_tooltipTxt = ""
	defunc_uiEl_sprite = spr_None;
	defunc_uiEl_OnActivateFunc = defunc_uiEl_OnActivate;
	defunc_uiEl_canSelect = true;
	
	defunc_uiEl_UpdateFunc = defunc_uiEl_Update;
	defunc_uiEl_PlayerIndex = a_PI;
}

function defunc_uiEl_TextElement(a_txt) : base_defunc_uiElement() constructor
{
	defunc_uiEl_text = a_txt;
	defunc_uiEl_canSelect = false;
}

function UI_Init(a_ui)
{
	with (a_ui)
	{
		array_clear(ui_Elements);
		ui_FillElementsFunc(self);
		UI_GetOptions(self);
		
		if(array_length(ui_Elements) > ui_GridWidth)
		{
			ui_GridSpacingWidth = ui_Size.x/ui_GridWidth;
		}else ui_GridSpacingWidth = ui_Size.x/array_length(ui_Elements);
		ui_GridSpacingWidth -= ui_GridBorder*2;
		ui_GridSpacingHeight -= ui_GridBorder*2;
		
	}
	return a_ui;
}

function UI_Update(a_ui)
{
	with a_ui
	{
		array_clear(ui_Elements);
		ui_FillElementsFunc(self);
		UI_GetOptions(self);
		
		if(global.uiUP == 1) ui_OptionIndex -= ui_GridWidth;
		else if(global.uiDOWN == 1) ui_OptionIndex += ui_GridWidth;
		else if(global.uiLEFT == 1) ui_OptionIndex -= 1;
		else if(global.uiRIGHT == 1) ui_OptionIndex += 1;
		
		ui_OptionIndex = clamp(ui_OptionIndex, 0, array_length(ui_Options)-1);
		
		if(global.uiYES == 1)
		{
			var tOption = ui_Options[ui_OptionIndex];
			if(tOption != undefined) tOption.defunc_uiEl_OnActivateFunc(tOption);
		}
		else if(global.uiNO == 1) ui_CanClose = true;
		
		
		
		for(var i = 0; i < array_length(ui_Elements); i += 1)
		{
			var tEl = ui_Elements[i];
			tEl.defunc_uiEl_UpdateFunc(tEl);
		}
	}
}

function UI_Draw(a_ui)
{
	with a_ui
	{
		var tSize = new Vector2(ui_Size.x/sprite_get_width(ui_Sprite), ui_Size.y/sprite_get_height(ui_Sprite));
		draw_sprite_ext(ui_Sprite, 0,ui_Pos.x, ui_Pos.y, tSize.x, tSize.y,0,c_white,1);
		DrawTextOutline(ui_Pos.x + (ui_GridSpacingWidth/2), ui_Pos.y + (ui_GridSpacingHeight/2) , 1, ui_HeaderText, c_white, c_black, 120, true);
			
		for(var i = 0; i < array_length(ui_Elements); i += 1)
		{
			var tOffX = ui_GridBorder + (ui_GridSpacingWidth * (i mod ui_GridWidth));
			var yPos = floor(i / ui_GridWidth);
			var tOffY = ui_GridBorder*(yPos) + (ui_GridSpacingHeight * yPos);
			var tE = ui_Elements[i];
			var tPos = new Vector2(ui_Pos.x + tOffX, ui_Pos.y + tOffY + ui_GridSpacingHeight);
			var tCol = c_white;
			if(ui_Elements[i] == ui_Options[ui_OptionIndex])
			{
				tCol = c_aqua;
				DrawTextOutline(ui_Pos.x + (ui_GridSpacingWidth/2), ui_Pos.y - (ui_GridSpacingHeight/2) , 1, ui_Elements[i].defunc_uiEl_tooltipTxt, c_white, c_black, 120, true);
		
			}
			draw_sprite_ext(tE.defunc_uiEl_sprite, 0,tPos.x + (ui_GridSpacingWidth/2), tPos.y, 1, 1,0,tCol,1);
			DrawTextOutline(tPos.x + (ui_GridSpacingWidth/2), tPos.y, 1, tE.defunc_uiEl_text, tCol, c_black, 38, true);
			
			if(global.debug)
			{
				draw_line_color(tPos.x, tPos.y, tPos.x + ui_GridSpacingWidth, tPos.y, c_white, c_white);
				draw_line_color(tPos.x, tPos.y, tPos.x, tPos.y+ ui_GridSpacingHeight, c_white, c_white);
				draw_line_color(tPos.x + ui_GridSpacingWidth, tPos.y, tPos.x + ui_GridSpacingWidth, tPos.y + ui_GridSpacingHeight, c_white, c_white);
				draw_line_color(tPos.x, tPos.y+ ui_GridSpacingHeight, tPos.x + ui_GridSpacingWidth, tPos.y + ui_GridSpacingHeight, c_white, c_white);
			}
		}
	}
}

function UI_FillElements(a_UI)
{
	with a_UI
	{
		//
	}
}

function UI_GetOptions(a_ui)
{
	with a_ui
	{
		array_clear(ui_Options);
		for(var i = 0; i < array_length(ui_Elements); i += 1)
		{
			var tE = ui_Elements[i];
			if(tE.defunc_uiEl_canSelect)
			{
				array_push(ui_Options,tE); 
			}
		}
	}
}

function defunc_uiEl_Update(a_defunc_uiEl)
{
	with a_defunc_uiEl
	{
		//do stuff as necessary
	}
}

function defunc_uiEl_OnActivate(a_defunc_uiEl)
{
	with a_defunc_uiEl
	{
		//do stuff	
	}
}



function UI_GetCharCard(a_PI)
{
	return a_PI.char_StatCard;
}