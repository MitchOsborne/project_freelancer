// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function abl_comp_Animation(a_parent, a_spriteSet) : base_AbilityComponent(a_parent) constructor{
	
	shdr_color = shader_get_uniform(shdr_ownerColor, "ownerColor");
	
	abl_comp_Sprite = a_spriteSet.sprSet_Equip;
	abl_comp_SpriteSet = undefined;
	abl_comp_AnimWep = false;
	
	abl_comp_anim_state = ABL_ANIM_STATE.NONE;
	abl_comp_lerp = -1;
	abl_comp_animIndex = 0;
	abl_comp_animTimer = 0;
	abl_comp_animCtr = 1;
	abl_comp_animType = spritespeed_framespersecond;
	
	abl_comp_prevFrame = -1;
	if(a_spriteSet != undefined) abl_comp_SpriteSet = a_spriteSet;
	
	
	abl_comp_Init = function(a_parent, a_comp)
	{
		InitAbilityComponent();
		abl_comp_animType = sprite_get_speed_type(abl_comp_Sprite);
		abl_comp_animCtr = sprite_get_speed(abl_comp_Sprite);	
	}
	
	abl_comp_Update = function()
	{
		
		if(abl_comp_anim_state != parentAbility.abl_animState)
		{
			abl_comp_animIndex = 0;
			abl_comp_prevFrame = -1;
			abl_comp_anim_state = parentAbility.abl_animState;
		}
		
		switch abl_comp_anim_state
		{
			case ABL_ANIM_STATE.NONE:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Idle;
			break;
			case ABL_ANIM_STATE.ATK1:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Attack1;
			break;
			case ABL_ANIM_STATE.ATK2:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Attack2;
			break;
			case ABL_ANIM_STATE.ATK3:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Attack3;
			break;
			case ABL_ANIM_STATE.ATK4:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Attack4;
			break;
			case ABL_ANIM_STATE.RELOAD:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Reload;
			break;
			case ABL_ANIM_STATE.EQUIP:
			abl_comp_Sprite = abl_comp_SpriteSet.sprSet_Equip;
			break;
			default:
			break;
			
		}
		
		abl_comp_animType = sprite_get_speed_type(abl_comp_Sprite);
		abl_comp_animCtr = sprite_get_speed(abl_comp_Sprite);	
		
		var tIndex = abl_comp_animIndex;
		if(abl_comp_animIndex >= sprite_get_number(abl_comp_Sprite)-1)
		{
			abl_comp_AnimWep = false;
		}
		
		if(parentAbility.abl_animLerp != -1)
		{
			abl_comp_animIndex = floor(sprite_get_number(abl_comp_Sprite)*parentAbility.abl_animLerp);	
		}else if(abl_comp_AnimWep)
		{
			var animCheck = 0;
			if(abl_comp_animType == spritespeed_framespersecond)
			{
				abl_comp_animTimer += GetDeltaTime(parentAbility.timeScale);	
				animCheck = 1/abl_comp_animCtr;
			}else
			{
				abl_comp_animTimer += 1;
				animCheck = abl_comp_animCtr;
			}
			if(abl_comp_animTimer >= animCheck) 
			{
				abl_comp_animIndex += 1;
				abl_comp_animTimer = 0;
			}
			if(abl_comp_animIndex >= sprite_get_number(abl_comp_Sprite)) abl_comp_animIndex = 1;	
		}
		
		if(tIndex != abl_comp_prevFrame)	//Stops it from triggering the same message on a frame multiple times
		{
			abl_comp_prevFrame = abl_comp_animIndex;
			var tArr = sprite_get_info(abl_comp_Sprite).messages;
		
			for(var i = 0; i < array_length(tArr); i += 1)
			{
				var tM = tArr[i];
				if((tM.frame >= tIndex || abl_comp_animIndex == 0)	//Checks in case the animIndex loops back to the start
				&& tM.frame <= abl_comp_animIndex)
				{
					array_push(parentAbility.abl_AnimAlerts, tM.message);
				}
			
			}
		}	
	}
	
	abl_comp_Draw = function()
	{
		var flip = 1;
		var tDir = 0;
		var tScale = 1.2;
		if(abl_comp_SpriteSet != undefined)
		{
			if(abl_comp_SpriteSet.sprSet_isMelee)
			{
				if(abl_comp_anim_state < ABL_ANIM_STATE.ATK1)
				{
					if(parentAbility.owner.direction <= 180) flip = -1
				}
			}else
			{
				if(parentAbility.owner.direction <= 180) flip = -1
				//tScale = 0.75 //Guns too big, scaled down by 0.75, will remove if I make smaller sprites for weapons	
			}
			if(abl_comp_SpriteSet.sprSet_RotEquip) tDir = parentAbility.abl_dirOffset * flip;
			
		}
		if(abl_comp_Sprite != undefined)
		{
			DrawOwnerColor(owner.ownerColor); 
			draw_sprite_ext(abl_comp_Sprite,abl_comp_animIndex,owner.char_offset.x + parentAbility.abl_posOffset.x,owner.char_offset.y + parentAbility.abl_posOffset.y,tScale,flip*tScale,parentAbility.owner.direction + 90 + tDir, c_white, 1);
			shader_reset();	
		}
		//else draw_sprite(spr_error,0,owner.x, owner.y);	
	}
	
	abl_comp_OnActivate = function()
	{
		abl_comp_AnimWep = true;
		abl_comp_animIndex = 1;
	}
	
} 


function abl_comp_DrawCasing(a_parent, a_sprite) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_sprite = a_sprite;
	abl_comp_spawnDelay = 0.5;	//This is relative to the animation cycle of the parent weapon
	abl_comp_prepFire = false;
	
	if(a_sprite != undefined) abl_comp_sprite = a_sprite;
	
	abl_comp_OnFireCycle = function()
	{
		var tVec = GetCoord(parentAbility.owner.direction + irandom_range(150, 210));
		array_push(Renderer.rndr_ParticleArr, new Ptl_BulletCasing(parentAbility.owner.x, parentAbility.owner.y, tVec.x, tVec.y, 0.5, abl_comp_sprite));
	}	
}

function abl_comp_Audio(a_parent) : base_AbilityComponent(a_parent) constructor{
	
	abl_comp_OnActivate = function()
	{
		with parentAbility
		{
			if(!is_undefined(abl_sfx)) audio_play_sound(abl_sfx,5,false);
		}
	}
}
