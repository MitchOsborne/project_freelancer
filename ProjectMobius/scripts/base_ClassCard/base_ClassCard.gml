// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_ClassCard() constructor{
	
	crd_ClassType = CLASS_TYPE.NONE;
	crd_Name = "No Class Name";
	
	crd_intrinsic = undefined;
	
	crd_wep1Slots = [SLOT_TYPE.MELEE_LIGHT, SLOT_TYPE.MELEE_MED];
	crd_wep2Slots = [SLOT_TYPE.GUN_LIGHT, SLOT_TYPE.GUN_MED];
	
	crd_util1Slots = [SLOT_TYPE.EVADE];
	crd_util2Slots = [SLOT_TYPE.UTILITY];
	crd_abl1Slots = [SLOT_TYPE.ABILITY1];
	crd_abl2Slots = [SLOT_TYPE.ABILITY2];
	crd_coreSlots = [SLOT_TYPE.CORE];
	crd_classSlots = [SLOT_TYPE.CLASS];
	
	crd_HPMod = 1;
	crd_SHDMod = 1;
	crd_DMGMod = 1;
	crd_SPDMod = 1;
	crd_CDMod = 1;
	crd_DURMod = 1;
	crd_CRITMod = 1;
	crd_PROCMod = 1;
	
}

function UpdateSlotRequirements(a_char)
{
	var tCard = a_char.char_StatArchCard;
	
	with a_char
	{
		//Clean up this code at some point, too much copy paste
		//-----------------
		//Probably move to store large collections of arrays in a custom struct, 
		//and pull them with variable_struct_get
		/*
		array_clear(char_ldSlots.ld_slots.ld_wep1);
		array_clear(char_ldSlots.ld_slots.ld_wep2);
		array_clear(char_ldSlots.ld_slots.ld_util1);
		array_clear(char_ldSlots.ld_slots.ld_util2);
		array_clear(char_ldSlots.ld_slots.ld_abl1);
		array_clear(char_ldSlots.ld_slots.ld_abl2);
		array_clear(char_ldSlots.ld_slots.ld_core);
		array_clear(char_ldSlots.ld_slots.ld_class);
		
		array_append(char_ldSlots.ld_slots.ld_wep1,tCard.crd_wep1Slots);
		array_append(char_ldSlots.ld_slots.ld_wep2,tCard.crd_wep2Slots);
		array_append(char_ldSlots.ld_slots.ld_util1,tCard.crd_util1Slots);
		array_append(char_ldSlots.ld_slots.ld_util2,tCard.crd_util2Slots);
		array_append(char_ldSlots.ld_slots.ld_abl1,tCard.crd_abl1Slots);
		array_append(char_ldSlots.ld_slots.ld_abl2,tCard.crd_abl2Slots);
		array_append(char_ldSlots.ld_slots.ld_core,tCard.crd_coreSlots);
		array_append(char_ldSlots.ld_slots.ld_class,tCard.crd_classSlots);
		
		
		var tArr = GetLoadoutArr(self);
		
		for(var i = 0; i < array_length(tArr); i += 1)
		{
			var tAbl = tArr[i];
			array_append(char_ldSlots.ld_slots.ld_wep1,tAbl.bp_wep1Slots);
			array_append(char_ldSlots.ld_slots.ld_wep2,tAbl.bp_wep2Slots);
			array_append(char_ldSlots.ld_slots.ld_util1,tAbl.bp_util1Slots);
			array_append(char_ldSlots.ld_slots.ld_util2,tAbl.bp_util2Slots);
			array_append(char_ldSlots.ld_slots.ld_abl1,tAbl.bp_abl1Slots);
			array_append(char_ldSlots.ld_slots.ld_abl2,tAbl.bp_abl2Slots);
			array_append(char_ldSlots.ld_slots.ld_core,tAbl.bp_coreSlots);	
			array_append(char_ldSlots.ld_slots.ld_class,tAbl.bp_classSlots);	
		}
		*/
	}	
}

function GetValidAbilities(a_Slot, a_AblArr)
{
	var retArr = []
	for(var i = 0; i < array_length(a_AblArr); i += 1)
	{
		var tAbl = a_AblArr[i];
		if(tAbl.bp_Slot == a_Slot || (tAbl.bp_Slot == LOADOUT_SLOT.ALL && a_Slot != LOADOUT_SLOT.CLASS))
		{
			array_push(retArr, tAbl);
		}
	}
	return retArr;
}

function crd_Enemy() : base_ClassCard() constructor
{
	crd_Name = "Enemy";	
}

function crd_Default() : base_ClassCard() constructor
{
	crd_Name = "Freelancer";
}

function crd_Fighter() : base_ClassCard() constructor
{
	crd_Name = "Fighter";
	crd_ClassType = CLASS_TYPE.FIGHTER;
	crd_HPMod = 1.2;
	crd_DMGMod = 1.2;
}

function crd_Hunter() : base_ClassCard() constructor
{
	crd_Name = "Hunter";
	crd_ClassType = CLASS_TYPE.HUNTER;
	crd_SPDMod = 1.2;
	crd_CRITMod = 1.2;
}


function crd_Engineer() : base_ClassCard() constructor
{
	crd_Name = "Engineer";
	crd_ClassType = CLASS_TYPE.ENGINEER;
	crd_SHDMod = 1.2;
	crd_DURMod = 1.2;
}

function crd_Dynamist() : base_ClassCard() constructor
{
	crd_Name = "Dynamist";
	crd_ClassType = CLASS_TYPE.DYNAMIST;
	crd_CDMod = 1.2;
	crd_PROCMod = 1.2;
}

function crd_Aegis_Paladin() : base_ClassCard() constructor
{
	array_push(crd_wep1Slots, SLOT_TYPE.GUN_MED, SLOT_TYPE.GUN_HEAVY, SLOT_TYPE.MELEE_MED, SLOT_TYPE.MELEE_HEAVY);
	array_push(crd_wep2Slots, SLOT_TYPE.GUN_MED, SLOT_TYPE.GUN_HEAVY, SLOT_TYPE.MELEE_MED, SLOT_TYPE.MELEE_HEAVY);
}


function crd_Ronin_Iaido() : base_ClassCard() constructor
{
	array_push(crd_wep1Slots, SLOT_TYPE.GUN_MED, SLOT_TYPE.MELEE_MED);
	array_push(crd_wep2Slots, SLOT_TYPE.GUN_MED, SLOT_TYPE.MELEE_MED);
}

function crd_Pilot_Flight() : base_ClassCard() constructor
{
	array_push(crd_wep1Slots, SLOT_TYPE.GUN_MED);
	array_push(crd_wep2Slots, SLOT_TYPE.GUN_MED);
}

function crd_Kobald_Hunter() : base_ClassCard() constructor
{
	crd_ClassType = CLASS_TYPE.HUNTER;
}




