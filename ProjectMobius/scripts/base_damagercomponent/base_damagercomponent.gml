// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_DamagerComponent(a_parent) constructor{
	
	parentDmg = a_parent;
	weapon = undefined;
	owner = undefined;
	
	dmg_comp_Init = function()
	{
		weapon = parentDmg.weapon;
		owner = weapon.owner;	
	}
	
	dmg_comp_Update = function ()
	{
		
	}
	dmg_comp_EndUpdate = function()
	{
		
	}
	dmg_comp_OnHit = function(a_hitReport)
	{
		
	}
	dmg_comp_OnKill = function(a_killReport)
	{
		
	}
	dmg_comp_Draw = function()
	{
			
	}
}