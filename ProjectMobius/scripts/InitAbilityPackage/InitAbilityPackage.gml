/*function InitAbilityPackage(a_pkg, a_ownerID) {
	
	var tPkg;
	
	if(is_undefined(a_pkg)) tPkg = new PKG_Empty();
	else
	{
		if(is_struct(a_pkg))
		{
			tPkg = a_pkg;
		}else
		{
			tPkg = new a_pkg();
			//tPkg = Copy_AbilityPackage(a_pkg);
		}
	}
	//var tnyku = typeof(tPkg);
	tPkg.owner = a_ownerID;
	if(tPkg.pkg_Ability != undefined)
	{
		var tInst;
		if(is_struct(tPkg.pkg_Ability)) tInst = tPkg.pkg_Ability;
		else tInst = new tPkg.pkg_Ability();
		tInst.owner = a_ownerID;	
		tInst.teamID = a_ownerID.teamID;
		script_execute(tInst.func_InitAbility, tInst);
		tPkg.pkg_Ability = tInst;
		tInst.package = tPkg;
		tInst.packageSlot = 0;
		tInst.cooldownTimer = tInst.cooldown;
	}
	if(tPkg.pkg_AltAbility != undefined)
	{
		var tInst;
		if(is_struct(tPkg.pkg_AltAbility)) tInst = tPkg.pkg_AltAbility;
		else tInst = new tPkg.pkg_AltAbility();
		tInst.owner = a_ownerID;	
		tInst.teamID = a_ownerID.teamID;
		script_execute(tInst.func_InitAbility, tInst);
		tPkg.pkg_AltAbility = tInst;
		tInst.package = tPkg;
		tInst.packageSlot = 1;
		tInst.cooldownTimer = tInst.cooldown;
	}
	
	return tPkg;
}*/

function InitAbility(a_bp, a_owner)
{
		var tInst = new a_bp.bp_Frame(a_bp);
		var tMercy = FindMod(a_owner.char_modArr, "Mercy");
	
		if(tMercy)
		{
			if(tInst.abl_DmgType != DMG_TYPE.EM) array_push(tInst.abl_Components, new abl_comp_KO(tInst));
		}
		
		
		tInst.owner = a_owner;	
		tInst.teamID = a_owner.teamID;
		script_execute(tInst.func_InitAbility, tInst);
		return tInst;
}
