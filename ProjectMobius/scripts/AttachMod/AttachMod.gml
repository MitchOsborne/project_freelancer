function AttachMod(a_Mod, a_Subject, a_Owner, a_Duration, a_Stacks, a_AddStack) {

	var rMod = undefined;
	var tStacks = 1;
	if(a_Stacks != undefined) tStacks = a_Stacks;
	var addStack = false;
	if(a_AddStack != undefined) addStack = a_AddStack;
	var oldMod = undefined;
	var tID = undefined;
	if(is_struct(a_Mod)) tID = a_Mod;
	else tID = new a_Mod();
	if(tID.stackType != STACK_TYPE.NONE)
	{
		var tBreak = false;
		for(var i = 0; i < array_length(a_Subject.char_modArr); i++)
		{
			var tMod = a_Subject.char_modArr[i];
			var tName = tMod.name;
						
			if(tName == tID.name)
			{
				if(tID.stackType == STACK_TYPE.SELF && tID.owner == tMod.owner)	
				{
					oldMod = tMod;
					tBreak = true;
				}
				else if(tID.stackType == STACK_TYPE.OTHER && tID.owner != tMod.owner)
				{
					oldMod = tMod;
					tBreak = true;
				}
				else if (tID.stackType == STACK_TYPE.ALL)
				{
					oldMod = tMod;
					tBreak = true;
				}
			}
			if(tBreak) break;
		}
	}
	
	delete(tID);
	
	var createNew = false;
	
	if(oldMod == undefined)	createNew = true;
	else if(oldMod.stackType == STACK_TYPE.NONE) createNew = true;
	else if(oldMod.stackType == STACK_TYPE.SELF && oldMod.owner == a_Owner) createNew = true;
	else if(oldMod.stackType == STACK_TYPE.OTHER && oldMod.owner != a_Owner) createNew = true;

	if(createNew)
	{
		var tInst;
		if(is_struct(a_Mod)) tInst = a_Mod;
		else tInst = new a_Mod();
		tInst.owner = a_Owner;
		tInst.subject = a_Subject;
		tInst.stacks = round(tStacks*10)/10;
		if(!is_undefined(a_Duration))
		{
			tInst.duration = a_Duration;
			tInst.durTimer = 0;
		}
	
		array_push(a_Subject.char_modArr, tInst);
		tInst.mod_Init();
		rMod = tInst;
	}else
	{
	/*
		if(oldMod.owner.char_damageStat < a_Owner.char_damageStat)
		{
			oldMod.owner = a_Owner;	
		}
	*/
		if(oldMod.stackLimit > oldMod.stacks)
		{
			if(!addStack)
			{
				if(oldMod.stacks < tStacks) oldMod.stacks = tStacks;
			}
			else oldMod.stacks += tStacks;
		}
			oldMod.durTimer = 0;
		rMod = oldMod;	
	}

	return rMod;
}


function FindMod(a_arr, a_modName)
{
	for(var i = 0; i < array_length(a_arr); i += 1)
	{
		var tMod = a_arr[i];
		if(tMod.name == a_modName) return tMod;
	}
	return undefined;
}



function AttachLinearForce(a_Subject, a_Owner, a_Duration, a_ForceDir, a_ForceStrength) {

	var tInst = AttachMod(new force_linear(), a_Subject, a_Owner, a_Duration, 1);
	
	if(a_Subject != a_Owner) a_ForceStrength -= (a_ForceStrength*a_Subject.char_pushRes);
	
	tInst.owner = a_Owner;
	tInst.subject = a_Subject;
	tInst.forceDirection = a_ForceDir;
	tInst.forceStrength = a_ForceStrength;
	tInst.duration = a_Duration;
	tInst.durTimer = 0;
	
	return tInst;
}

function AttachPositionalForce(a_Subject, a_Owner, a_Duration, a_ForceOrigin, a_ForceStrength, a_ForceMinRange, a_ForceMaxRange) {

	var tInst = AttachMod(new force_positional(), a_Subject, a_Owner, a_Duration, 1);
	
	tInst.owner = a_Owner;
	tInst.subject = a_Subject;
	tInst.forceOrigin = a_ForceOrigin;
	tInst.forceStrength = a_ForceStrength;
	tInst.duration = a_Duration;
	tInst.durTimer = 0;
	tInst.forceMinRange = a_ForceMinRange;
	tInst.forceMaxRange = a_ForceMaxRange;
	
	return tInst;
}
