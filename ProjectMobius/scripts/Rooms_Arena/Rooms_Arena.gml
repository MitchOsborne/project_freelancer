// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_ArenaRoom() : base_RoomData() constructor
{
	rd_region = REGION.YAKIBASE;
	rd_BGM = bgm_YakiBase;
	rd_Outdoors = false;
	rd_ambAlpha = 0;
	rd_ambColour = c_black;
}

function roomData_Arena_Lobby() : base_ArenaRoom() constructor
{
	rd_roomID = "ArenaLobby";
	rd_roomObj = room_arena_lobby;
	array_push(rd_tExit,roomData_Arena_Area1);
	array_push(rd_bExit,roomData_Desert1);
}

function roomData_Arena_Area1() : base_ArenaRoom() constructor
{
	rd_roomID = "ArenaArea1";
	rd_roomObj = room_arena_area1;
	rd_isCleared = true;
	rd_ambAlpha = 1;
	array_push(rd_bExit,roomData_Arena_Lobby);
}

