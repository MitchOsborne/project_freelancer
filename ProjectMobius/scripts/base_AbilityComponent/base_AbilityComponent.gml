// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum COMP_TAGS
{
	NONE,
	INPUT,
	STATS,
	CHARGE,
	AMMO,
	ANIMATION,
	
}

function base_AbilityComponent(a_parent) constructor{
	parentAbility = a_parent;
	owner = a_parent.owner;
	tags = [];
	
	abl_comp_showRes = false;
	abl_comp_hasProc = false;
	abl_comp_Init = InitAbilityComponent;
	abl_comp_Update = function ()
	{
		
	}
	abl_comp_EndUpdate = function()
	{
		
	}
	abl_comp_OnActivate = function ()
	{
		
	}
	abl_comp_OnFireStart = function()
	{
			
	}
	abl_comp_OnFireTrigger = function()
	{
			
	}
	abl_comp_OnFireCycle = function()
	{
			
	}
	abl_comp_OnFireEnd = function()
	{
			
	}
	abl_comp_OnHit = function(a_hitReport)
	{
		
	}
	abl_comp_OnKill = function(a_killReport)
	{
		
	}
	abl_comp_Draw = function()
	{
			
	}
	abl_comp_AIUpdate = function()
	{
		return 1;
	}
	abl_comp_AIOnFire = function(a_ai)
	{
			
	}
	abl_comp_OnAblEnd = function()
	{
		
	}
}


function abl_comp_WeaponStats(a_parent, a_range, a_spread, a_damage, a_RPM, a_projSize, a_projSpeed) : base_AbilityComponent(a_parent) constructor
{
	array_push(tags, COMP_TAGS.STATS);
	abl_comp_Range = 80;
	abl_comp_Spread = 10;
	abl_comp_Damage = 1;
	abl_comp_RPM = 180;
	abl_comp_ProjSize = 1;
	abl_comp_ProjSpeed = animcurve_get_channel(crv_ProjectileSpeed,"Default");
	
	if(a_range != undefined) abl_comp_Range = a_range;
	if(a_spread != undefined) abl_comp_Spread = a_spread;
	if(a_damage != undefined) abl_comp_Damage = a_damage;
	if(a_RPM != undefined) abl_comp_RPM = a_RPM;
	if(a_projSize != undefined) abl_comp_ProjSize = a_projSize;
	if(a_projSpeed != undefined) abl_comp_ProjSpeed = a_projSpeed;
	
	abl_comp_Init = function()
	{
		InitAbilityComponent();
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 1;
		var targ = owner.active_operator.enemyTarget;
			if(CheckExistance(targ))
			{
				var tDist = point_distance(owner.x, owner.y, targ.x, targ.y);
				if(tDist <= abl_comp_Range)
				{
					retVal *= abl_comp_Range/tDist;
				}
			}
		return retVal;
	}
}

function GetComponentsByTag(a_ability, a_Tag)
{
	var retArr = [];
	
	for(var i = 0; i < array_length(a_ability.abl_Components); i += 1)
	{
		var tComp = a_ability.abl_Components[i];
		if(array_find(tComp.tags, a_Tag) != -1) array_push(retArr, tComp);
	}
	return retArr;
}

function InitAbilityComponent()
{
	if(owner = undefined) owner = parentAbility.owner;
}