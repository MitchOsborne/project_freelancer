function FindTargetNearest(a_aX, a_aY, a_targetObj, a_callerObj, a_LoS, a_IgnoreFriendly) {


	var target = undefined;

	for(i = 0; i < instance_number(a_targetObj); i++)
	{
		var tTarg = instance_find(a_targetObj,i);
		var targetDir = GetDirection(a_aX, a_aY, tTarg.x, tTarg.y, false, false);
		if(tTarg.isIncap == false && tTarg.isInvis == false)
		{
			if(a_IgnoreFriendly && tTarg.teamID == a_callerObj.teamID)
			{
			
			}else
			{
				var newTarg = false;
				var tDist = point_distance(a_aX,a_aY, tTarg.x,tTarg.y)
			
				if(target == undefined)
				{
					newTarg = true;	
				}else
				{
					if(tDist < point_distance(a_aX,a_aY,target.x,target.y))
					{
						if(a_LoS)
						{
							var res = collision_line(a_aX,a_aY,tTarg.x, tTarg.y,base_PhysObstacle,false,true);
							if(CheckExistance(res) == false)
							{
								newTarg = true;	
							}
						}else
						{
							newTarg = true;
						}
					}
				}
			
				if(newTarg == true)
				{
					target = tTarg;	
				}
			}
		}
	
	}

	return target;




}
