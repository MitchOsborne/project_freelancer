// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function anim_Ronin() : base_Animator() constructor{
	
	anim_default = spr_Ronin_idle;

	anim_idle_front = spr_Ronin_idle;
	anim_idle_back = spr_Ronin_idle;

	anim_walk_front = spr_Ronin_walk;
	anim_walk_back = spr_Ronin_walk;

	anim_dash_front = spr_Ronin_dash;
	anim_dash_back = spr_Ronin_dash;

	anim_incap = spr_Ronin_incap;
	anim_stun_front = spr_Ronin_dash;
	anim_stun_back = spr_Ronin_dash;
}

function anim_Shepherd() : base_Animator() constructor{
	
	anim_default = spr_Shepherd_idle;

	anim_idle_front = spr_Shepherd_idle;
	anim_idle_back = spr_Shepherd_idle;

	anim_walk_front = spr_Shepherd_walk;
	anim_walk_back = spr_Shepherd_walk;

	anim_dash_front = spr_Shepherd_dash;
	anim_dash_back = spr_Shepherd_dash;

	anim_incap = spr_Shepherd_incap;
	anim_stun_front = spr_Shepherd_dash;
	anim_stun_back = spr_Shepherd_dash;
	
}

function anim_Kamui() : base_Animator() constructor{
	
	anim_default = spr_Kamui_idle;

	anim_idle_front = spr_Kamui_idle;
	anim_idle_back = spr_Kamui_idle;

	anim_walk_front = spr_Kamui_walk;
	anim_walk_back = spr_Kamui_walk;

	anim_dash_front = spr_Kamui_dash;
	anim_dash_back = spr_Kamui_dash;

	anim_incap = spr_Kamui_incap;
	anim_stun_front = spr_Kamui_dash;
	anim_stun_back = spr_Kamui_dash;
	
}

function anim_Pilot() : base_Animator() constructor{
	
	anim_default = spr_pilot_idle;

	anim_idle_front = spr_pilot_idle;
	anim_idle_back = spr_pilot_idle;

	anim_walk_front = spr_pilot_walk;
	anim_walk_back = spr_pilot_walk;

	anim_dash_front = spr_pilot_dash;
	anim_dash_back = spr_pilot_dash;

	anim_incap = spr_pilot_incap;
	anim_stun_front = spr_pilot_dash;
	anim_stun_back = spr_pilot_dash;
}

function anim_Kobald() : base_Animator() constructor{
	
	anim_default = spr_kobald_idle;

	anim_idle_front = spr_kobald_idle;
	anim_idle_back = spr_kobald_idle;

	anim_walk_front = spr_kobald_walk;
	anim_walk_back = spr_kobald_walk;

	anim_dash_front = spr_kobald_dash;
	anim_dash_back = spr_kobald_dash;

	anim_incap = spr_kobald_dash;
	anim_stun_front = spr_kobald_dash;
	anim_stun_back = spr_kobald_dash;
}