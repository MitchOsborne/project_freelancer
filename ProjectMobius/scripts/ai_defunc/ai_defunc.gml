// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information



function defunc_ai_DragonBoss() : base_AI() constructor{
	
	sinTimer = 0;
	
	attackPhase = 0;
	waitTime = 5;
	waitTimer = 0;
		
	func_UpdateStep = AI_UpdateDragonBoss;	
	
}

function defunc_AI_UpdateAdaptive(a_ai)
{
	with a_ai
	{
		if(!slavedObj.isIncap)
		{
			AI_UpdateStep(self);
			if(!ai_prepAttack) chosenAbl = slavedObj.char_AC.ac_activeLoadout.ld_slots.ld_wep1;
			AI_GetAbilityScores(chosenAbl);
			var engageRange = chosenAbl.ai_engageRange;
			
			if(CheckExistance(enemyTarget)) currentTarget = enemyTarget;
			else if (CheckExistance(allyTarget)) currentTarget = allyTarget;
			else currentTarget = undefined;
		
			//Check if the enemy is within vision range
			AI_CheckVision(self);
		
			switch ai_CurrentState
			{
				case AI_STATE.IDLE:
				{
					ai_debugTxt = "Lazing about";
					if(slavedObj.teamID != TEAMID.Ally) AI_PatrolRoute(self);
					else ai_task_MoveIntoRange(self, allyTarget, 16);
					break;
				}
				case AI_STATE.SEARCH:
				{
					
					ai_debugTxt = "Searching...";
				}break;
				case AI_STATE.INVESTIGATE:
				{
					ai_debugTxt = "Investigating...";
				}break;
				case AI_STATE.ENGAGE:
				{
					if(CheckExistance(enemyTarget))
					{
						
					}
					ai_debugTxt = "Engaging";
				}break;
				case AI_STATE.FIGHT:
				{
					if(!beginAttack && !ai_prepAttack)
					{
						if(CheckExistance(enemyTarget)) ai_task_MoveIntoRange(self, enemyTarget, engageRange*0.8);	
					}
					ai_debugTxt = "Fighting";
				}break;
			
			
			}
		
			if(ai_CurrentState == AI_STATE.FIGHT || ai_CurrentState == AI_STATE.ENGAGE)
			{
				if(CheckExistance(enemyTarget)) ai_lookPoint = new Vector2(enemyTarget.x, enemyTarget.y);
				if(global.alertMode != ALERT_STATE.Danger)
				{
					if(!slavedObj.isIncap)
					{
						if(slavedObj.isStunned) ai_alertTmr += GetDeltaTime(timeScale)/4;
						else if (global.alertMode == ALERT_STATE.Warning) ai_alertTmr += GetDeltaTime(timeScale)*4;
						else ai_alertTmr += GetDeltaTime(timeScale);
					}
					if(ai_alertTmr >= ai_alertDelay)
					{
						global.alertMode = ALERT_STATE.Danger;
						ai_alertTmr = 0;
					}
				}	
			}
		
			AI_FightEnemy(self);
		}
	}
	
}
