// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function SanityCheckBPLoadout(a_bpLd)
{
	if(a_bpLd.ld_slots.ld_class == undefined) a_bpLd.ld_slots.ld_class = global.bp_Abilities.bp_Class_Freelancer;
	var tArr = variable_struct_get_names(a_bpLd.ld_slots);
	
	UpdateLoadoutKeywords(a_bpLd);
	for(var i = 0; i < array_length(tArr); i += 1)	
	{
		var tB = variable_struct_get(a_bpLd.ld_slots, tArr[i]);
		var validKey = CheckLoadoutKeywords(a_bpLd, tB);
		if(tB == undefined || !validKey)
		{
			variable_struct_set(a_bpLd.ld_slots, tArr[i], global.bp_Abilities.bp_Empty);
		}
		if(validKey == false)
		{
			//Recheck the abilities to ensure valid keywords
			var tArr = variable_struct_get_names(a_bpLd.ld_slots);
			i = 0;
		}
	}
}


function UpdateLoadoutKeywords(a_bpLd)
{
	array_clear(a_bpLd.ld_Keywords);
	
	var tArr = variable_struct_get_names(a_bpLd.ld_slots);
	for(var n = 0; n < array_length(tArr); n += 1)
	{
		var tBP = variable_struct_get(a_bpLd.ld_slots, tArr[n]);
		array_append(a_bpLd.ld_Keywords, tBP.bp_Keywords);
	}	
}
//Checks to see if the loadout has the required keyword
function CheckLoadoutKeywords(a_bpLd, a_bp)
{
	
	var bp_Key = a_bp.bp_ReqKeywords;
	var ld_Key = a_bpLd.ld_Keywords;
	
	
	//Makes sure it returns true if there are no required keywords
	if(array_length(bp_Key) == 0) return true;
	
	for(var ix = 0; ix < array_length(bp_Key); ix += 1)
	{
		for(var iy = 0; iy < array_length(ld_Key); iy += 1)
		{
			var tX = bp_Key[ix];
			var tY = ld_Key[iy];
			if(tX == tY)
			{
				return true
			}
		}	
	}
	return false;
}

function SanityCheckLoadout(charID){
	with charID
	{
		if(instance_exists(char_AC) == false)
		{
			char_AC = instance_create_layer(x,y,"Instances", base_AbilityController);
			char_AC.slavedObj = id;
		}
		SanityCheckBPLoadout(char_AC.ac_bpLd);
		if(!char_hasAltLoadout)
		{
			char_AC.ac_activeLoadout = CleanLoadout(char_AC.ac_activeLoadout, char_AC.ac_bpLd, id);
			if(char_AC.ac_activeWeapon == undefined) char_AC.ac_activeWeapon = char_AC.ac_activeLoadout.ld_slots.ld_wep1;
		}else
		{
			char_AC.ac_activeLoadout = CleanLoadout(char_AC.ac_activeLoadout, char_AC.ac_altbpLd, id);
			if(char_AC.ac_activeWeapon == undefined) char_AC.ac_activeWeapon = char_AC.ac_activeLoadout.ld_slots.ld_wep1;
		}
		var w1 = char_AC.ac_activeLoadout.ld_slots.ld_wep1.abl_bpStats.bp_WeightBonus;
		var w2 = char_AC.ac_activeLoadout.ld_slots.ld_wep2.abl_bpStats.bp_WeightBonus;
		char_ldWeightBonus = (w1+w2)/2;
		//}
	}
}

function ClearAltLoadout(charID)
{
	with charID
	{
		alt_char_bpLd.ld_slots.ld_wep1  	= undefined;
		alt_char_bpLd.ld_slots.ld_wep2	= undefined;
		alt_char_bpLd.ld_slots.ld_util1	= undefined;
		alt_char_bpLd.ld_slots.ld_util2	= undefined;
		alt_char_bpLd.ld_slots.ld_abl1	= undefined;
		alt_char_bpLd.ld_slots.ld_abl2	= undefined;
		alt_char_bpLd.ld_slots.ld_core	= undefined;
	}
}

function RefreshLoadout(charID)
{
	with charID
	{
		var tMod = FindMod(char_modArr, "Alt Loadout");
		if(tMod != undefined) tMod.duration = 0;
		char_hasAltLoadout = false;
		char_AC.ac_activeWeapon = undefined;
		weapon1 =  undefined;
		weapon2 =  undefined;
		utility1 = undefined;
		utility2 = undefined;
		ability1 = undefined;
		ability2 = undefined;
		Core = undefined;
	}
	SanityCheckLoadout(charID);
}

function CompareLoadout(charA, charB)
{
	var tArr = variable_struct_get_names(charA.char_AC.ac_bpLd.ld_slots);
	var retVal = true;
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var tA = variable_struct_get(charA.char_AC.ac_bpLd.ld_slots, tArr[i]);
		var tB = variable_struct_get(charB.char_AC.ac_bpLd.ld_slots, tArr[i]);
		
		if(tA != tB) retVal = false;
	}
	return retVal;
}

function CopyLoadout(charA, charB)
{
	
	var tArr = variable_struct_get_names(charA.char_AC.ac_bpLd.ld_slots);
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var tA = variable_struct_get(charA.char_AC.ac_bpLd.ld_slots, tArr[i]);
		variable_struct_set(charB.char_AC.ac_bpLd.ld_slots, tArr[i], tA);
	}
}

function CheckOverride(a_abl)
{
	if(a_abl != undefined)
	{
		with a_abl
		{
			if(is_struct(self))
			{
				if(abl_isOverride) return true;	
			}
		}
	}
	return false;
}


function CheckLoadout(a_ability, a_loadout, a_Caller){
	
	if(a_ability != undefined)
	{
		a_ability = InitAbility(a_loadout,a_Caller);
	}
	return a_ability;
	
}



function CleanLoadout(a_ablLd, a_bpLd, a_Caller)
{
	var tArr = variable_struct_get_names(a_ablLd.ld_slots);
	
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var tA = variable_struct_get(a_ablLd.ld_slots, tArr[i]);
		var tB = variable_struct_get(a_bpLd.ld_slots, tArr[i]);
		
		
		if(tB != undefined)
		{
			if(tA == undefined || tA.abl_blueprint != tB)
			{
				variable_struct_set(a_ablLd.ld_slots, tArr[i], InitAbility(tB, a_Caller));
				a_Caller.char_AC.ac_activeWeapon = undefined;
			}
		}else
		{
			if(tA == undefined) tA = global.bp_Abilities.bp_Empty;	
		}
	}
	
	return a_ablLd;
}

function GetAblFromSlot(a_slot, a_ld)
{
	var retVal = undefined;
	switch a_slot
	{
		case LOADOUT_SLOT.WEAPON1:
		retVal = a_ld.ld_slots.ld_wep1;
		break;
		case LOADOUT_SLOT.WEAPON2:
		retVal = a_ld.ld_slots.ld_wep2;
		break;
		case LOADOUT_SLOT.UTILITY1:
		retVal = a_ld.ld_slots.ld_util1;
		break;
		case LOADOUT_SLOT.UTILITY2:
		retVal = a_ld.ld_slots.ld_util2;
		break;
		case LOADOUT_SLOT.ABILITY1:
		retVal = a_ld.ld_slots.ld_abl1;
		break;
		case LOADOUT_SLOT.ABILITY2:
		retVal = a_ld.ld_slots.ld_abl2;
		break;
		case LOADOUT_SLOT.CORE:
		retVal = a_ld.ld_slots.ld_core;
		break;
		default:
		break;
	}
	return retVal;
}

function GetLoadoutArr(a_char)
{
	var retArr = [];
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_wep1);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_wep2);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_util1);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_util2);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_abl1);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_abl2);
	array_push(retArr,a_char.char_AC.ac_bpLd.ld_slots.ld_core);
	
	return retArr;

}
