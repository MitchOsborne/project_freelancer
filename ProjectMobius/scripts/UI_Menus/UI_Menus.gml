// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Menu_CharSelect(a_PI) : defunc_base_UI() constructor
{
	ui_Size = new Vector2(320,320);
	ui_Pos = new Vector2(80,320);
	ui_HeaderText = "Character Selection";
	ui_PlayerIndex = a_PI;
	
	
	ui_FillElementsFunc = function(a_ui)
	{
		with a_ui
		{
			ui_Elements[0] = new defunc_uiEl_TextElement("Player " + string(ui_PlayerIndex) + ", Choose Your Character");
			ui_Elements[1] = new defunc_uiEl_CharSelectShepherd(ui_PlayerIndex);
			ui_Elements[2] = new defunc_uiEl_CharSelectKamui(ui_PlayerIndex);
			ui_Elements[3] = new defunc_uiEl_CharSelectPilot(ui_PlayerIndex);
			ui_Elements[4] = new defunc_uiEl_CharSelectDoggo(ui_PlayerIndex);
			ui_Elements[5] = new defunc_uiEl_CharSelectYakiMelee(ui_PlayerIndex);
			ui_Elements[6] = new defunc_uiEl_CharSelectYakiGun(ui_PlayerIndex);
		}
	}
	
}

function Menu_PlayerInit() : defunc_base_UI() constructor
{
	ui_HeaderText = "Player Input Selection";
	ui_GridSpacingHeight = 60;
	ui_FillElementsFunc = function(a_ui)
	{
		with a_ui
		{
			ui_Elements[0] = new defunc_uiEl_P1InputSelect();
			ui_Elements[1] = new defunc_uiEl_P2InputSelect();
		}
	}
	ui_UpdateFunc = function(a_ui)
	{
		with a_ui
		{
			var canComplete = false;
			UI_Update(self);
			//InitInput();
			if(global.uiYES == 1) canComplete = true;
		}
			
		if(canComplete)
		{
			ui_CanClose = true;
			var tMenu = new Menu_CharSelect("1");
			tMenu = UI_Init(tMenu);
			array_push(global.ui_MenuStack, tMenu);
		}
		
	}
}

function Menu_CharEdit(a_PI) : defunc_base_UI(a_PI) constructor
{
	ui_CharCard = UI_GetCharCard(a_PI);
	
	ui_HeaderText = "Editting Character";
}

function Menu_CharEditOptions(a_PI) : defunc_base_UI(a_PI) constructor
{
	ui_CharCard = UI_GetCharCard(a_PI);
	
	ui_HeaderText = "Editting Character";
	
	
	ui_FillElementsFunc = function(a_ui)
	{
		with a_ui
		{
			ui_Elements[0] = new defunc_uiEl_CharEditAbl(ui_PlayerIndex);
			ui_Elements[1] = new defunc_uiEl_CharEditStat(ui_PlayerIndex);
		}
	}
	
}

function Menu_CharEditAbl(a_PI) : Menu_CharEdit(a_PI) constructor
{
	
}

function Menu_CharEditStat(a_PI) : Menu_CharEdit(a_PI) constructor
{
	
	ui_GridWidth = 5;
	
	ui_FillElementsFunc = function(a_ui)
	{
		with a_ui
		{
			for(var i = 0; i < array_length(ui_CharCard.crd_StatSlotArr); i += 1)
			{
				ui_Elements[i] = new defunc_uiEl_CharEditStatSlot(ui_PlayerIndex, i);
			}
		}
	}

}

function Menu_CharSelectStatMod(a_PI, a_StatIndex) : Menu_CharEdit(a_PI) constructor
{
	ui_StatSlotIndex = a_StatIndex;
	ui_ModArr = [];
	
	ui_GridWidth = 5;
	
	ui_FillElementsFunc = function(a_ui)
	{
		with a_ui
		{
			array_clear(ui_ModArr);
			for(var i = 0; i < array_length(global.inv_StatMods); i += 1)
			{
				var invMod = global.inv_StatMods[i];
				var canSlot = true;
				for(var n = 0; n < array_length(ui_CharCard.crd_StatSlotArr); n += 1)
				{
					var charMod = ui_CharCard.crd_StatSlotArr[n];
					if(charMod == invMod) canSlot = false;
					if(ui_StatSlotIndex < global.SlotRange.ATK)
					{
						if(invMod.stat_MainType != STAT_CAT.ATTACK && invMod.altStatType != STAT_CAT.ATTACK) canSlot = false;
					}
					else if(ui_StatSlotIndex < global.SlotRange.DEF)
					{
						if(invMod.stat_MainType != STAT_CAT.DEFENSE && invMod.altStatType != STAT_CAT.DEFENSE) canSlot = false;
					}
					else if(ui_StatSlotIndex < global.SlotRange.ABL) 
					{
						if(invMod.stat_MainType != STAT_CAT.ABILITY && invMod.altStatType != STAT_CAT.ABILITY) canSlot = false;
					}
					else if(ui_StatSlotIndex < global.SlotRange.LUCK) 
					{
						if(invMod.stat_MainType != STAT_CAT.LUCK && invMod.altStatType != STAT_CAT.LUCK) canSlot = false;
					}
				
				
				
				}
				if(canSlot) array_push(ui_ModArr, invMod);
			}
	
			for(var z = 0; z < array_length(ui_ModArr); z += 1)
			{
				var tMod = ui_ModArr[z];
				ui_Elements[z] = new defunc_uiEl_CharSocketMod(ui_PlayerIndex, ui_StatSlotIndex, tMod);	
			}
		}
	}
}
