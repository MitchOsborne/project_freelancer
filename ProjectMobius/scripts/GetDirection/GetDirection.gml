function GetDirection(a_aX, a_aY, a_bX, a_bY, isOffset, rotateBack) {


	var tRot = 0;
	if(rotateBack)
	{
		tRot = -90;
	}

	var rot = -infinity;

	if(isOffset)
	{
		rot = point_direction(a_aX,a_aY, a_aX+a_bX, a_aY+a_bY) + tRot;	
	}else
	{
		rot = point_direction(a_aX,a_aY, a_bX, a_bY) + tRot;
	}

	return rot;




}
