// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_Animator() : base_CharacterComponent() constructor{
	
	anim_default = spr_DummyBlue;
	anim_scale = 1;
	anim_idle_front = anim_default;
	anim_idle_back = anim_default;

	anim_walk_front = anim_default;
	anim_walk_back = anim_default;

	anim_dash_front = anim_default;
	anim_dash_back = anim_default;

	anim_incap = anim_default;
	anim_wake = anim_default;
	anim_stun_front = anim_default;
	anim_stun_back = anim_default;

	hasIncap = false;
	
	
	func_PreUpdateStep = Anim_PreUpdateStep;
	func_UpdateStep = Anim_UpdateStep;
	func_EndUpdateStep = Anim_EndUpdateStep;
	func_DrawStep = Anim_DrawStep;
}

function Anim_PreUpdateStep(a_anim)
{
	with a_anim
	{
		char_comp_PreUpdateStep(self);
	}
}

function Anim_UpdateStep(a_anim)
{
	with a_anim
	{
		char_comp_UpdateStep(self);
		slavedObj.char_animScale = anim_scale;
		hasIncap = false;
		var faceBack = false;
		var activeSprite = anim_idle_front;
	
		var lookDir = GetCoord(slavedObj.direction);
	
		if(lookDir.y >= 0)
		{
			faceBack = true;	
		}
		if(slavedObj.animState == ANIM_STATE.INCAP)
		{
		
			activeSprite = anim_incap;
			if(floor(slavedObj.image_index) == sprite_get_number(slavedObj.sprite_index)-1)
			{
				hasIncap = true;
			}
			if(hasIncap) slavedObj.image_index = sprite_get_number(slavedObj.sprite_index)-1;
		}
		else if(slavedObj.animState == ANIM_STATE.REVIVE)
		{
			activeSprite = anim_incap;
			slavedObj.image_speed *= -1;
			if(floor(slavedObj.image_index) == sprite_get_number(slavedObj.sprite_index)-1)
			{
				hasIncap = true;
			}
			if(hasIncap) slavedObj.image_index = sprite_get_number(slavedObj.sprite_index)-1;
	
		}else if(slavedObj.animState == ANIM_STATE.STUNNED)
		{
			if(faceBack) activeSprite = anim_stun_back;
			else activeSprite = anim_stun_front;
			
			if(floor(slavedObj.image_index) == sprite_get_number(slavedObj.sprite_index)-1)
			{
				hasIncap = true;
			}
			if(hasIncap) slavedObj.image_index = sprite_get_number(slavedObj.sprite_index)-1;
		}
		else if(slavedObj.animState == ANIM_STATE.IDLE)
		{
			if(faceBack) activeSprite = anim_idle_back;
			else activeSprite = anim_idle_front;
	
		}else if(slavedObj.animState == ANIM_STATE.WALK)
		{
			if(faceBack) activeSprite = anim_walk_back;
			else activeSprite = anim_walk_front;	
		}else if(slavedObj.animState == ANIM_STATE.DASH)
		{
			if(faceBack) activeSprite = anim_dash_back;
			else activeSprite = anim_dash_front;	
		}
	
		if(slavedObj.sprite_index != activeSprite)
		{
			slavedObj.sprite_index = activeSprite;
			slavedObj.image_index = 0;
		}
		
	}	
}

function Anim_EndUpdateStep(a_anim)
{
	with a_anim
	{
		char_comp_EndUpdateStep(self);
	}
}

function Anim_DrawStep(a_anim)
{
	with a_anim
	{
		char_comp_DrawStep(self);
	}
}