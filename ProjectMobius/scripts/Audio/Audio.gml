// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayBGM(){
	if(global.activeBGM != undefined) audio_stop_sound(global.activeBGM);
	global.currBGM = global.BGM;
	global.activeBGM = audio_play_sound(global.BGM,50,true);
}