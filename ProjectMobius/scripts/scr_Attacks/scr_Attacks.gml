// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function atk_Bullet() : base_Attack() constructor
{
	
	atk_projectile = dmg_Bullet;
	atk_projectileCount = 1;
	
	atk_duration = 0.1;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = 0;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0;
	
	atk_resourceCost = 1;
	atk_sfx = sfx_shoot1;
}

function atk_Tranq() : base_Attack() constructor
{
	
	atk_projectile = dmg_Tranq;
	atk_projectileCount = 1;
	
	atk_dmgMod = 0;
	
	atk_duration = 0.1;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = 0;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0;
	
	atk_resourceCost = 1;
	atk_sfx = sfx_saber1;
}

function atk_GravLance1() : base_Attack() constructor
{
	
	atk_projectile = proj_GravLanceSmall;
	atk_projectileCount = 1;
	
	atk_duration = 0.2;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = -0.1;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0.1;
	
	atk_resourceCost = 1;
	atk_sfx = sfx_shoot1;
}

function atk_GravLance2() : atk_GravLance1() constructor
{
	atk_duration = 0.3;
	atk_projectile = proj_GravLanceSmall;
	atk_projectileCount = 3;
	atk_resourceCost = 2;
}

function atk_GravLance3() : base_Attack() constructor
{
	
	atk_projectile = proj_GravLanceLarge;
	atk_projectileCount = 1;
	
	atk_duration = 0.5;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = -0.5;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0.1;
	
	atk_dmgMod = 15;
	
	atk_resourceCost = 3;
	atk_sfx = sfx_shoot1;
}

function atk_GravLance4() : atk_GravLance3() constructor
{
	
	atk_projectile = proj_GravLanceLarge;
	atk_projectileCount = 3;
	
	atk_duration = 0.5;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = -1;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0.1;
	
	atk_resourceCost = 6;
	atk_sfx = sfx_shoot1;
}

function atk_Grenade(a_GrenadeType, a_ProjCount) : base_Attack() constructor
{
	
	atk_projectile = proj_Grenade;
	if(a_GrenadeType != undefined) atk_projectile = a_GrenadeType;
	atk_projectileCount = 1;
	if(a_ProjCount != undefined) atk_projectileCount = a_ProjCount;
	atk_duration = 0.5;
	atk_parryType = PARRY_TYPE.NONE;
	atk_lungeForce = 0;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0;
	
	atk_resourceCost = 1;
	atk_sfx = sfx_shoot1;
}

function atk_EnmBullet() : base_Attack() constructor
{
	
	atk_projectile = tEnmBullet;
	atk_duration = 0.1;
	atk_parryType = PARRY_TYPE.NONE;
	
	atk_lungeForce = 0;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0;
	
	atk_resourceCost = 1;
	
	atk_sfx = sfx_psi_Throw;
}


function atk_KnifeThrow1() : base_Attack() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK1;
	atk_projectile = dmg_KnifeThrow;
	atk_projectileCount = 1;
	
	atk_duration = 0.1;
	atk_parryType = PARRY_TYPE.NONE;

	
	atk_resourceCost = 1;
	atk_sfx = sfx_sword1;
}

function atk_KnifeThrow2() : atk_KnifeThrow1() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK2;
	atk_sfx = sfx_sword2;
}

function atk_KnifeThrow3() : atk_KnifeThrow1() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK3;
	atk_projectileCount = 3;
	atk_projectileSpread = 45;
	
	atk_dmgMod = 1.2;
	atk_spdMod = 0.75;
	
	atk_sfx = sfx_sword_long;
}


function atk_Melee() : base_Attack() constructor 
{
	
	atk_projectile = dmg_SlashLeft;
	
	atk_parryType = PARRY_TYPE.BLOCK;
	
	atk_lungeForce = 2;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0.1;
	atk_forceStrength = 0;
	
}



function atk_Slash1() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK1;
	atk_projectile = dmg_SlashLeft;
	atk_sfx = sfx_sword1;
}
function atk_Slash2() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK2;
	atk_projectile = dmg_SlashRight;
	atk_sfx = sfx_sword2;
}
function atk_Slash3() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK3;
	
	atk_projectile = dmg_SlashSpin;
	
	atk_lungeForce = 4;
	atk_lungeDur = 0.1;
	
	atk_forceStrength = 15;
	
	atk_dmgMod = 1.2;
	atk_spdMod = 0.75;
	
	atk_sfx = sfx_sword_long;
}

function atk_Thrust() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK4;
	atk_projectile = dmg_BashCharge;
	atk_sfx = sfx_sword1;
}

function atk_BurstSlash() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK3;
	atk_duration = 1;
	atk_projectile = dmg_SlashSpin;
	
	atk_burstCount = 3;
	atk_lungeForce = 2;
	atk_lungeDur = 0.3;
	
	atk_forceStrength = 1;
	
	atk_dmgMod = 0.75;
}

function atk_Bash1() : atk_Melee() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK1;
	atk_lungeForce = 4;	//Negative numbers for kncokback/recoil
	atk_lungeDur = 0.2;
	atk_forceStrength = 1;
	atk_sfx = sfx_sword1;
}
function atk_Bash2() : atk_Bash1() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK2;
	atk_projectile = dmg_SlashRight;
	atk_lungeDur = 0.2;
	atk_forceStrength = 1;
	atk_sfx = sfx_sword2;
}

function atk_Bash3() : atk_Bash1() constructor
{
	atk_anim = ABL_ANIM_STATE.ATK3;
	atk_spdMod = 0.7;
	atk_projectile = dmg_BashCharge;
	
	atk_recoilOnHit = true;
	
	atk_lungeForce = 6;
	atk_lungeDur = 0.3;
	
	atk_forceStrength = 15;
	
	atk_dmgMod = 1.2;
	atk_sfx = sfx_sword_long;
}

function atk_LungeBite() : atk_Melee() constructor
{
	atk_duration = 0.8;
	atk_projectile = dmg_Bite;
	
	atk_lungeForce = 6;
	atk_lungeDur = 0.25;
	
	atk_forceStrength = 6;
	
	atk_dmgMod = 1.2;
	atk_sfx = sfx_sword_long;
}

function atk_Bite() : atk_Melee() constructor
{
	atk_duration = 0.8;
	atk_projectile = dmg_Bite;
	
	
	atk_dmgMod = 1.2;
	atk_sfx = sfx_sword1;
}

function atk_BladeDash() : atk_Melee() constructor
{
	atk_duration = 0.5;
	atk_projectile = dmg_SlashSpin;
	
	atk_lungeForce = 3;
	atk_lungeDur = 0.25;
	
	atk_forceStrength = 6;
	atk_dmgMod = 1.2;
	atk_sfx = sfx_sword_long;
}

function atk_Shotgun() : base_Attack() constructor
{
	atk_duration = 0.3;
	atk_projectile = dmg_Pellet;
	
	atk_lungeForce = 0;
	atk_lungeDur = 0;
	
	atk_forceStrength = 15;
	
	atk_projectileCount = 5;
	atk_projectileSpread = 45;
	atk_randomSpread = false;
	
	atk_dmgMod = 1.2;
	
	atk_sfx = sfx_boom;
	
	atk_burstCount = 1;
	atk_burstDelay = 0.1;
}

function atk_Soulfire() : base_Attack() constructor
{
	atk_duration = 0.3;
	atk_projectile = dmg_SoulFire;
		
	atk_forceStrength = 15;
	
	atk_projectileCount = 3;
	atk_projectileSpread = 8;
	atk_randomSpread = false;
	
	atk_dmgMod = 1;
	
	atk_sfx = sfx_boom;
	
	atk_burstCount = 1;
	atk_burstDelay = 0.1;
}

function atk_Flamer() : base_Attack() constructor
{
	atk_duration = 0.3;
	atk_projectile = dmg_FlameBall;
		
	atk_forceStrength = 15;
	
	atk_projectileCount = 5;
	atk_projectileSpread = 8;
	atk_randomSpread = false;
	
	atk_dmgMod = 1;
	
	atk_sfx = sfx_dash;
	
	atk_burstCount = 1;
	atk_burstDelay = 0.1;
}

function atk_SniperBullet() : atk_Bullet() constructor
{
	atk_projectile = dmg_Bullet;
	atk_sfx = sfx_explosion;
}

function atk_Wave() : base_Attack() constructor
{
	atk_duration = 1;
	atk_projectile = dmg_Bullet;
	
	atk_lungeForce = 0;
	atk_lungeDur = 0;
	
	atk_forceStrength = 15;
	
	atk_projectileCount = 36;
	atk_projectileSpread = 10;
	atk_randomSpread = false;
	
	atk_dmgMod = 1.2;
	
	atk_burstCount = 1;
	atk_burstDelay = 0.1;	
}
