function InitMeleeAttack(a_attackObj, a_weaponID) {


	var tInst = undefined;
	var createInst = false;
	if(CheckExistance(a_attackObj) == false)
	{
		createInst = true;
	}else
	{
		if(a_attackObj.weapon == undefined)
		{
			a_attackObj.weapon = a_weaponID;	
		}else if (a_attackObj.weapon != a_weaponID)
		{
			createInst = true;
		}
	}
	
	if(createInst)
	{
		tInst = instance_create_layer(x,y,"Projectiles",a_attackObj);
		tInst.enabled = false;
		tInst.weapon = id;
		tInst.teamID = a_weaponID.teamID;
	}

	return tInst;



}
