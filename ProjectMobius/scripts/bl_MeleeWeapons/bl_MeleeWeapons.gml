// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Internal_bp_Init_Melee()
{
	with global.bp_Abilities.bp_BaseMeleeStats
	{
		bp_Range = 48; 
		bp_Spread = 1; 
		bp_DMGScale = 1; 
		bp_RPM = 120;
		bp_Ammo = 4;
		bp_ReloadSpd = 0.5;
		bp_AutoReload = false;
		bp_EquipTime = 0.8;
		bp_WeightBonus = 0.8;
	}

	with global.bp_Abilities.bp_Katana
	{
		bp_Name = "Katana";
		bp_Frame = base_wep_Melee;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
	
		bp_AttackArr = [atk_Slash1, atk_Slash2];
		bp_AttackFin = atk_Slash3;

		bp_SpriteSet = sprSet_Katana;
	}
	
	
	with global.bp_Abilities.bp_Shortsword
	{
		bp_Name = "Shortsword";
		bp_Frame = base_wep_Melee;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
	
		with bp_WepStats
		{
			bp_Ammo = 3;
			bp_WeightBonus = 1.1;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 0.9; 
			bp_RPM = 1.2;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
	
		bp_AttackArr = [atk_Slash1, atk_Slash2];
		bp_AttackFin = atk_Slash3;

		bp_SpriteSet = sprSet_Katana;
	}
	
	with global.bp_Abilities.bp_CombatKnife
	{
		bp_Name = "Combat Knife";
		bp_FlexType = "Knife";
		bp_Frame = base_wep_Melee;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
	
		with bp_WepStats
		{
			bp_Ammo = 4;
			bp_WeightBonus = 1.2;
			bp_AutoReload = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 0.8; 
			bp_RPM = 1.4;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
	
		bp_AttackArr = [atk_Slash1, atk_Slash2];

		bp_SpriteSet = sprSet_ComDag;
	}
	
		with global.bp_Abilities.bp_KnifeThrow 
	{

		bp_Name = "Knife Throw";
		bp_FlexType = "Knife";
		bp_Frame = base_wep_KnifeThrow;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_ReqKeywords = [LDKEYWORDS.HUNTER, LDKEYWORDS.FIGHTER];
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
		
		
		with bp_WepStats
		{
			bp_Ammo = 3;
			bp_WeightBonus = 1.2;
			bp_AutoReload = true;
		}
		
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 0.8; 
			bp_RPM = 1.4;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_AttackArr = [atk_KnifeThrow1, atk_KnifeThrow2];
		bp_AttackFin = undefined;
		
		bp_SpriteSet = sprSet_KnifeThrow;
		bp_SFX = sfx_shoot1;
	}
	
	with global.bp_Abilities.bp_NezumiBlade
	{
		bp_Name = "Nezumi Blade";
		bp_FlexType = "NezKnife";
		bp_Frame = base_NezumiKnife;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		bp_DMGType = DMG_TYPE.EM;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
	
		with bp_WepStats
		{
			bp_Ammo = 3;
			bp_WeightBonus = 1.2;
			bp_AutoReload = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 0.6; 
			bp_RPM = 1.4;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
	
		bp_AttackArr = [atk_Slash1, atk_Slash2];
		bp_AttackFin = atk_Slash3;

		bp_SpriteSet = sprSet_NezDag;
	}



	with global.bp_Abilities.bp_ShieldBash
	{
		bp_Name = "Shield Bash";
		bp_FlexType = "Shield";
		bp_Frame = base_wep_Melee;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
		with bp_WepStats 
		{
			bp_Range = 48; 
			bp_Spread = 1; 
			bp_DMGScale = 2.5; 
			bp_RPM = 1;
			bp_Ammo = 3;
			bp_ReloadSpd = 0.5;
			bp_EquipTime = 0;
			bp_AutoReload = true;
		}
	
		bp_AttackArr = [atk_Bash1, atk_Bash2];
		bp_AttackFin = atk_Bash3;

		bp_SpriteSet = spr_wep_Shield_atkM;	
	}



	with global.bp_Abilities.bp_LungeBite
	{
		bp_Name = "Lunge Bite";
		bp_Frame = base_wep_LungeBite;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
		with bp_WepStats
		{
			bp_Range = 90; 
			bp_Spread = 1; 
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 0.5;
			bp_EquipTime = 0;
			bp_AutoReload = true;
		}
		bp_AttackArr = [atk_Bite];
		bp_AttackFin = atk_LungeBite;

		bp_SpriteSet = undefined;
	}
	
	with global.bp_Abilities.bp_Bite
	{
		bp_Name = "Bite";
		bp_Frame = base_wep_LungeBite;
		bp_Slot = LOADOUT_SLOT.WEAPON1;
		bp_AbilityType = ABILITY_TYPE.MELEE;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
		with bp_WepStats
		{
			bp_Range = 90; 
			bp_Spread = 1; 
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 0.5;
			bp_EquipTime = 0;
			bp_AutoReload = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		bp_AttackArr = [atk_LungeBite];
		bp_AttackFin = undefined;

		bp_SpriteSet = undefined;
	}
	
		with global.bp_Abilities.bp_BladeDash
	{
		bp_Name = "Blade Dash";
		bp_Frame = base_wep_Melee;
		bp_Slot = LOADOUT_SLOT.ABILITY1;
		bp_AbilityType = ABILITY_TYPE.ABILITY;
		bp_StayActive = false;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseMeleeStats);
		with bp_WepStats
		{
			bp_Range = 90; 
			bp_Spread = 1; 
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 2;
			bp_EquipTime = 0;
			bp_CDScale = 1;
			bp_AutoReload = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		bp_AttackArr = [atk_BladeDash];
		bp_AttackFin = undefined;

		bp_SpriteSet = sprSet_Katana;
	}
}
