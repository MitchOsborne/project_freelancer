// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Agent_Locomote(a_obj, a_loco){
	with a_loco
	{
		var tm = layer_tilemap_get_id("Tiles_Physics");
		var tileHitArr = [TILE_PHYS.WALL, TILE_PHYS.GAP];
		
		
		
		slavedObj = a_obj;
		if(instance_exists(slavedObj))
		{
		
			MoveLoco(id, slavedObj);
			velX = a_obj.velX;
			velY = a_obj.velY;
			velLerp = a_obj.velLerp;
			timeScale = slavedObj.timeScale;
			var mx = slavedObj.mx;
			var my = slavedObj.my;
			var moveSpeed = slavedObj.moveSpeed;
			var moveLock = false;
			var sprint = false;
			
			collisionClass = global.AgentCollisionClass;
			if(slavedObj.isSprinting) collisionClass = global.AgentCollisionClassSprinting;
			if(slavedObj.isDashing) collisionClass = global.AgentCollisionClassDashing;
			
			var movVec = new Vector2(0,0);
			movVec.x = mx * moveSpeed;
			movVec.y = my * moveSpeed;
			
			
			var forceVector = new Vector2(0,0);
			var tModArr = slavedObj.char_modArr
			for (var i = 0; i < array_length(tModArr); i++)
			{
				var tMod = tModArr[i];
				var tN = instanceof(tMod);
				if(tN == "debuff_moveLocked")
				{
					moveLock = true;	
				}
				if(tN == "buff_sprint")
				{
					sprint = true;
				}
				if(tN == "force_linear" || tN == "force_positional")
				{
					var fVecX = tMod.forceVector.x;
					var fVecY = tMod.forceVector.y;
				
					forceVector.x += fVecX;
					forceVector.y += fVecY;
				
				}
				if(tN == "debuff_slowed")
				{
					movVec.x *= abs(1-(tMod.stacks/10));
					movVec.y *= abs(1-(tMod.stacks/10));
				}
				
			}
			
			forceVector.x = clamp(forceVector.x, -global.forceCap,global.forceCap);
			forceVector.y = clamp(forceVector.y, -global.forceCap,global.forceCap);
					
			if(moveLock || slavedObj.isIncap) movVec = new Vector2(0,0);
			else if(sprint) movVec = new Vector2(movVec.x*1.6, movVec.y*1.6);
			var distX = movVec.x + (forceVector.x * slavedObj.forceMult);
			var distY = movVec.y + (forceVector.y * slavedObj.forceMult);
			
			distX *= GetDeltaTime(timeScale)*global.targetFPS;
			distY *= GetDeltaTime(timeScale)*global.targetFPS;
			
			
			if(GetPhysOverlap(x,y,collisionClass) || TileMeeting(self, 0, 0))
			{
				
				x = slavedObj.validX;
				y = slavedObj.validY;
					
			}
			
			velX = lerp(velX, distX, velLerp);
			velY = lerp(velY, distY, velLerp);
			slavedObj.velX = velX;
			slavedObj.velY = velY;
			
			if(!GetPhysOverlap(x,y,collisionClass) && !TileMeeting(self, 0, 0))
			{
				slavedObj.validX = x;
				slavedObj.validY = y;
			
				//Debug_Log("Char Mov.x: " , distX, true);
				//Debug_Log("Char Mov.y: " , distY, true);
				
				var tDistX = 0;
				var tDistY = 0;
				var xPool = velX;
				var yPool = velY;
				var xRet = -1;
				var yRet = -1;
				var tOffset = new Vector2(0,0);
								
				xRet = Loco_MoveCheck(self, xPool, true);
				xPool -= xRet;
				tDistX += xRet;
				tOffset.x += xRet;
									
				yRet = Loco_MoveCheck(self, yPool, false);
				yPool -= yRet;
				tDistY += yRet;
				tOffset.y += yRet;
					
				velX = tDistX;
				velY = tDistY;
				
			}
			
			
			
			
			
			x += velX;
			y += velY;
			
			if(GetPhysOverlap(x,y,collisionClass) || TileMeeting(self, 0, 0))
			{
				
				//x = slavedObj.validX;
				//y = slavedObj.validY;
					
			}
			
			slavedObj.locoBBoxTL = new Vector2(bbox_left, bbox_top);
			slavedObj.locoBBoxBR = new Vector2(bbox_right, bbox_bottom);
			
			
			slavedObj.x = x - slavedObj.locoOffset.x;
			slavedObj.y = y - slavedObj.locoOffset.y;
			
			var tInst = instance_place(x,y,base_TriggerVolume);
			if(tInst != noone) tInst.func_OnCharTouch(slavedObj);	
			
		}
		slavedObj = undefined;
	}
}

function Loco_MoveCheck(a_loco, a_dist, a_checkX)
{
	
	var distCap = 1;
	var distPool = 0;
	distPool = Round_Ext(a_dist, 0.001);
	var distSeg = ceil(abs(distPool)/distCap);
	var distCheck = 0;
	var checkVec = 0;
	var coll = false;
	
	var retDist = 0;
	with a_loco
	{
		for(var i = 0; i <= distSeg; i += 1)
		{
			if(distCap > abs(distPool)) distCheck = distPool;
			else 
			{
				if(distPool < 0) distCheck = -distCap;
				else distCheck = distCap;
			}
			
			distPool -= distCheck;
			
			if(a_checkX) checkVec = new Vector2(distCheck + retDist, 0);
			else checkVec = new Vector2(0, distCheck + retDist);
			
			//Avoids sub-pixel collision detection, which can cause false reports
			if(abs(distCheck) < 1)
			{
				if(a_checkX)
				{
					checkVec = new Vector2(1 + abs(retDist),0);
				}
				else
				{
					checkVec = new Vector2(0,1 + abs(retDist));
				}
				//Negative check for distCheck
				if(distCheck < 0) checkVec = new Vector2(checkVec.x * -1, checkVec.y * -1);
			}
			
			for(var c = 0; c < array_length(collisionClass); c += 1)
			{
				var tObj = collisionClass[c];
				
				
				if(place_meeting(x + checkVec.x, y + checkVec.y, tObj) 
				|| TileMeeting(self, checkVec.x, checkVec.y) 
				|| (slavedObj.PlayerControlled && OutsideBoundry(new Vector2(x + checkVec.x, y + checkVec.y))))
				{
					coll = true;
					
					var tVec = new Vector2 (0,0);
					while (true)
					{
						if(a_checkX) tVec.x = retDist;
						else tVec.y = retDist;
					
						if(!place_meeting(x + tVec.x, y + tVec.y, tObj) 
						&& !TileMeeting(self, tVec.x, tVec.y))
						{
							return retDist;
						}else
						{
							if(distCheck < 0) retDist += 1;
							else retDist -= 1;
						}
						if ((a_dist < 0 && retDist > 0) || (a_dist > 0 && retDist < 0)) return 0;
					}
					
				}
				
			}
			if(coll == false)
			{
				retDist += distCheck;	
			}
		}
	}
	return retDist;
	
}

function Projectile_Locomote(a_obj, a_loco)
{
	with a_loco
	{
		
		var tileHitArr = global.ProjectileTileCollisionClass;
		slavedObj = a_obj;
		x = slavedObj.x;
		y = slavedObj.y;
		timeScale = slavedObj.timeScale;
		
		var destroyObj = false;
		var mx = slavedObj.mx;
		var my = slavedObj.my;
		var moveSpeed = slavedObj.moveSpeed;	
		


		var movVec = new Vector2(0,0);
		movVec.x = mx * moveSpeed;
		movVec.y = my * moveSpeed;


		var forceVector = new Vector2(0,0);
		var tModArr = slavedObj.dmg_modArr;
		for (var i = 0; i < array_length(tModArr); i++)
		{
			var tMod = tModArr[i];
			var tN = instanceof(tMod);
			if(tN == "force_linear" || tN == "force_positional")
			{
				var fVecX = tMod.forceVector.x;
				var fVecY = tMod.forceVector.y;
				forceVector.x += fVecX;
				forceVector.y += fVecY;
			}
		}


		var distX = movVec.x + forceVector.x;
		var distY = movVec.y + forceVector.y;

		distX *= GetDeltaTime(timeScale)*global.targetFPS;
		distY *= GetDeltaTime(timeScale)*global.targetFPS;
	

		if(!GetPhysOverlap(x,y,collisionClass) && !TileMeeting_Arr(self, 0, 0, tileHitArr) 
		|| slavedObj.ignoreObstacles)
		{
			//lastX = x;
			//lastY = y;
		
		/*	Stops projectiles from passing through walls if they're travelling too fast
			var tRes = Collision_Line_Point(x, y, x+distX, y+distY,collisionClass, false, true);
			if(tRes[0] != noone)
			{
				var hitX = tRes[1];
				var hitY = tRes[2];
				var tDist = point_distance(x, y, hitX, hitY);
				var tDir = point_direction(x, y, hitX, hitY) - 90;
				var tNorm = GetCoord(tDir);
				distX = tNorm.x * tDist;
				distY = tNorm.y * tDist;
			
			}
		*/
		
			var tDistX = distX;
			var tDistY = distY;
		
		
			var coll = true;
			var hasCollided = false;
		
			while(coll == true)
			{
							
				
				if(GetPhysOverlap(x + tDistX, y, collisionClass) || TileMeeting_Arr(self, tDistX, 0, tileHitArr))
				{
					if(distX > 0) tDistX -= 1
					else if(distX < 0) tDistX += 1;
					hasCollided = true;
				}else
				{
					distX = tDistX;
				}
		
				if(GetPhysOverlap(x, y + tDistY, collisionClass) || TileMeeting_Arr(self, 0, tDistY, tileHitArr))
				{
					if(distY > 0) tDistY -= 1
					else if(distY < 0) tDistY += 1;
					hasCollided = true;
				}else
				{
					distY = tDistY;
				}
		
				if(GetPhysOverlap(x + tDistX, y + tDistY, collisionClass) || TileMeeting_Arr(self, tDistX, tDistY, tileHitArr))
				{
					if(distX > 0) tDistX -= 1
					else if(distX < 0) tDistX += 1;
					if(distY > 0) tDistY -= 1
					else if(distY < 0) tDistY += 1;
					hasCollided = true;
				}else
				{
					distX = tDistX;
					distY = tDistY;
				}

				if(!GetPhysOverlap(x + distX, y + distY, collisionClass) || !TileMeeting_Arr(self, tDistX, tDistY, tileHitArr))
				{
					coll = false;	
				}
		
			}
		
			if(hasCollided)
			{
				
				var maxBounces = slavedObj.dmg_maxBounces;
				if(bounceCount < maxBounces)
				{
					bounceCount += 1;
					var wallNormal = GetCollisionNormalCombined(x,y,collisionClass,tileHitArr, 32, 2);
					if(wallNormal != -1)
					{
						direction = (2*wallNormal) - direction - 180;
					}	
				}else
				{
					destroyObj = true;
				}
				
			}
		
		}else if (!slavedObj.ignoreObstacles)
		{
			destroyObj = true;
		}
		slavedObj.distTravelled += point_distance(x,y,x+distX, y+distY);
		
		x += distX;
		y += distY;
		
	
		if(destroyObj)
		{
			slavedObj.hp = -1;
		}
		//slavedObj = undefined;
	}
}


function Loco_NavPath(a_loco, a_path, a_startPos, a_endPos)
{
	var canPath = false;
	with a_loco
	{
		canPath = mp_grid_path(global.walkNavGrid, a_path, a_startPos.x,a_startPos.y,a_endPos.x, a_endPos.y, true);
	}
	return canPath;	
}