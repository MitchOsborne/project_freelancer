// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function ui_GetSlotArr(a_targSlot, a_caller)
{
	with a_caller
	{/*
		switch a_targSlot
		{
			
			case LOADOUT_SLOT.WEAPON1:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_wep1;
			break;
			case LOADOUT_SLOT.WEAPON2:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_wep2;
			break;
			case LOADOUT_SLOT.UTILITY1:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_util1;
			break;
			case LOADOUT_SLOT.UTILITY2:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_util2;
			break;
			case LOADOUT_SLOT.ABILITY1:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_abl1;
			break;
			case LOADOUT_SLOT.ABILITY2:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_abl2;
			break;
			case LOADOUT_SLOT.CORE:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_core;
			break;
			case LOADOUT_SLOT.CLASS:
			ui_slotArr = ui_targetChar.char_ldSlots.ld_slots.ld_class;
			break;
			default:
			break;
			
		}*/
	}
	
}

function ui_SetLoadoutSlot(a_targSlot, a_char, a_abl)
{
	with a_char
	{
		switch a_targSlot
		{
			case LOADOUT_SLOT.WEAPON1:
			char_AC.ac_bpLd.ld_slots.ld_wep1 = a_abl;
			break;
			case LOADOUT_SLOT.WEAPON2:
			char_AC.ac_bpLd.ld_slots.ld_wep2 = a_abl;
			break;
			case LOADOUT_SLOT.UTILITY1:
			char_AC.ac_bpLd.ld_slots.ld_util1 = a_abl;
			break;
			case LOADOUT_SLOT.UTILITY2:
			char_AC.ac_bpLd.ld_slots.ld_util2 = a_abl;
			break;
			case LOADOUT_SLOT.ABILITY1:
			char_AC.ac_bpLd.ld_slots.ld_abl1 = a_abl;
			break;
			case LOADOUT_SLOT.ABILITY2:
			char_AC.ac_bpLd.ld_slots.ld_abl2 = a_abl;
			break;
			case LOADOUT_SLOT.CORE:
			char_AC.ac_bpLd.ld_slots.ld_core = a_abl;
			break;
			case LOADOUT_SLOT.CLASS:
			char_AC.ac_bpLd.ld_slots.ld_class = a_abl;
			break;
			default:
			break;
		}
	}
}