function GetOffsetPos(a_xOff, a_yOff, a_Angle) {

	a_Angle *= (pi/180);


	var rXOff = (a_xOff * cos(a_Angle) - a_yOff * sin(a_Angle));
	var rYOff = (a_xOff * sin(a_Angle) + a_yOff * cos(a_Angle));


	return new Vector2(rXOff, rYOff);


}

function GetOffsetVec(a_vecA, a_vecB)
{
	var tVal = GetDirection(a_vecA.x, a_vecA.y, a_vecB.x, a_vecB.y, false, true);
	var retVal = GetCoord(tVal);
	return retVal;
}
