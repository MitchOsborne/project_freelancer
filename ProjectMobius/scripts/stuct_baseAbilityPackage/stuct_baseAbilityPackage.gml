// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum PACKAGE_TYPE
{
	SINGLE,
	DOUBLE,
	DOUBLE_INV,
	DOUBLE_EXCLUSIVE,
	PASSIVE,
}

enum PACKAGE_OUTPUT_TYPE
{
	AMMO,
	COOLDOWN,
	CHARGE,
	RELOAD,
	STAMINA,
}

/*
function base_AbilityPackage() constructor{

	owner = undefined;

	AbilityType = ABILITY_TYPE.PACKAGE;
	PackageType = PACKAGE_TYPE.SINGLE;
	pkg_Ability = undefined;
	pkg_AltAbility = undefined;
	
	pkg_AbilityDown = false;
	pkg_AbilityUp = false;
	
	inDown = false;
	
	pkg_Icon = spr_error;
	pkg_Name = "Unnamed";
	pkg_outputType = PACKAGE_OUTPUT_TYPE.COOLDOWN;
	pkg_output1 = 0;
	pkg_output2 = 0;
	
	//ai_AbilityProcess = a_Abl1.ai_Process;
	//ai_AltAbilityProcess = a_Abl2.ai_Process;
	
	
	//pkg_Ability2 = undefined;
	//pkg_AltAbility2 = undefined;
	
	//Ability2Down = false;
	//Ability2Up = false;
	
	pkg_ExcActivate = false;
	pkg_DefaultSlot = "None";
}
*/

function Update_AbilityLoadout(a_ablLd)
{
	var tArr = variable_struct_get_names(a_ablLd.ld_slots);
	
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var tAbl = variable_struct_get(a_ablLd.ld_slots, tArr[i]);
		if(tAbl != undefined)
		{
			with tAbl
			{
				script_execute(func_PreUpdateStep, self);
				script_execute(func_UpdateStep, self);
				script_execute(func_EndUpdateStep, self);
			}
			
			
		}
	}
}
/*
function Update_AbilityPackage(a_pkg)
{
	
	if(a_pkg != undefined)
	{
		with a_pkg
		{
			if(!owner.isStunned)
			{
				if(pkg_Ability != undefined)
				{
					if(PackageType != PACKAGE_TYPE.PASSIVE)
					{
						if(didActivate) pkg_ExcActivate = true;
						if(pkg_AbilityDown) inDown = true;
						if(!pkg_AbilityDown && inDown) 
						{
							inDown = false;
							pkg_AbilityUp = true;
						}
					
						inPressed = pkg_AbilityDown;
					
						if(pkg_AltAbility != undefined)
						{
							if(PackageType == PACKAGE_TYPE.DOUBLE)
							{
								pkg_AltAbility.inPressed = pkg_AbilityDown;	
							}else if (PackageType == PACKAGE_TYPE.DOUBLE_INV)
							{
								pkg_AltAbility.inPressed = pkg_AbilityUp;	
							}else if (PackageType == PACKAGE_TYPE.DOUBLE_EXCLUSIVE)
							{
								if(!pkg_ExcActivate)
								{
									pkg_AltAbility.inPressed = pkg_AbilityUp;	
								}
							}
						}
					}else
					{
						inPressed = !inPressed;
						if(is_struct(pkg_AltAbility))pkg_AltAbility.inPressed = !pkg_AltAbility.inPressed;
					}
				
					if(pkg_AbilityUp) pkg_ExcActivate = false;
				}
			
			
				pkg_AbilityDown = false;
				pkg_AbilityUp = false;
			}
		
			if(pkg_Ability != undefined)
			{
				script_execute(func_PreUpdateStep, pkg_Ability);
				script_execute(func_UpdateStep, pkg_Ability);
				script_execute(func_EndUpdateStep, pkg_Ability);
			
			}
			if(pkg_AltAbility != undefined)
			{
				script_execute(pkg_AltAbility.func_PreUpdateStep, pkg_AltAbility);
				script_execute(pkg_AltAbility.func_UpdateStep, pkg_AltAbility);
				script_execute(pkg_AltAbility.func_EndUpdateStep, pkg_AltAbility);
			}
		/*
			switch pkg_outputType
			{
			
				case PACKAGE_OUTPUT_TYPE.AMMO:
					if(pkg_Ability != undefined)	pkg_output1 = (ammoCount/ammoMax) * 100;
					if(pkg_AltAbility != undefined)	pkg_output2 = (pkg_AltAbility.ammoCount/pkg_AltAbility.ammoMax) * 100;
					break;
				case PACKAGE_OUTPUT_TYPE.COOLDOWN:
					if(pkg_Ability != undefined)	pkg_output1 = (cooldownTimer/cooldown) * 100;
					if(pkg_AltAbility != undefined)	pkg_output2 = (pkg_AltAbility.cooldownTimer/pkg_AltAbility.cooldown) * 100;
					break;
				case PACKAGE_OUTPUT_TYPE.CHARGE:
					if(pkg_Ability != undefined)	pkg_output1 = (abl_chargeTimer/abl_chargeTime) * 100;
					if(pkg_AltAbility != undefined)	pkg_output2 = (abl_chargeTimer/abl_chargeTime) * 100;
					break;
				case PACKAGE_OUTPUT_TYPE.RELOAD:
					if(pkg_Ability != undefined)	pkg_output1 = (reloadTimer/reloadTime) * 100;
					if(pkg_AltAbility != undefined)	pkg_output2 = (pkg_AltAbility.reloadTimer/pkg_AltAbility.reloadTime) * 100;
					break;
				case PACKAGE_OUTPUT_TYPE.STAMINA:
					if(pkg_Ability != undefined)	pkg_output1 = (owner.staminaStat/owner.maxStamina) * 100;
					if(pkg_AltAbility != undefined)	pkg_output2 = (owner.staminaStat/owner.maxStamina) * 100;
					break;
				default:
					break;
			
			}
		*/
			
			//if(pkg_Name == "Unnamed") if(is_struct(pkg_Ability)) pkg_Name = ablName;
	//	}
	//}
	
//}
/*
function Draw_AbilityPackage(a_pkg)
{
	if(a_pkg != undefined)
	{
		with a_pkg
		{
			if(pkg_Ability != undefined)
			{
				script_execute(func_DrawStep, pkg_Ability);
			}
			if(pkg_AltAbility != undefined)
			{
				script_execute(pkg_AltAbility.func_DrawStep, pkg_AltAbility);
			}
		}
	}
}
*/
function Draw_Ability(a_abl)
{
	with a_abl
	{
		script_execute(func_DrawStep, self);
	}
}

function Copy_Struct(a_target, a_source)
{
	var tArr = variable_struct_get_names(a_source);
	
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var data = variable_struct_get(a_source, tArr[i]);
		variable_struct_set(a_target, tArr[i], data);	
	}
}
