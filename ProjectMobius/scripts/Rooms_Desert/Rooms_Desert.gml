// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_DesertRoom() : base_RoomData() constructor
{
	rd_region = REGION.YAKIBASE;
	rd_BGM = bgm_Desert;
}

function roomData_Desert_Entry() : base_WoodsRoom() constructor
{

}

function roomData_Desert1() : base_DesertRoom() constructor
{
	rd_roomID = "Desert1";
	rd_roomObj = t_room_desert_1;
	
}

function roomData_Desert2() : base_DesertRoom() constructor
{
	rd_roomID = "Desert2";
	rd_roomObj = t_room_desert_2;
	
}


