// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_RoomData() constructor{
	
	rd_roomID = "Base Room Data";
	rd_roomObj = undefined;
	rd_spawnPool = undefined;
	rd_region = REGION.NONE;
	
	
	rd_hasEncounter = false;
	rd_isCleared = false;
	
	rd_dynamicBGM = false;
	rd_BGM = undefined;
	rd_BGM_quiet = undefined;
	rd_BGM_tension = undefined;
	rd_BGM_caution = undefined;
	rd_BGM_danger = undefined;
	
	rd_ambAlpha = 0;
	rd_ambColour = c_black;
	
	//Used to determine if it uses the Day/Night Cycle system or not
	rd_Outdoors = true;
	
	rd_IsLit = true;
	rd_EnemyLevel = -1;
	rd_TombstoneArr = [];
	rd_ChestArr = [];
	
	rd_events = [];
}

enum REGION
{
	NONE,
	DESERT,
	STRUT_F_INNER,
	STRUT_F_OUTER,
	DESERT_EAST,
	CAVE,
	YAKIBASE,
	YAKIBASEBOSS,
}