// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum AI_STATE
{
	IDLE = 0,
	SEARCH = 1,
	INVESTIGATE = 2,
	ENGAGE = 3,
	FIGHT = 4,
}

function base_AI() : base_Operator() constructor{
	
	currentTarget = undefined;
	enemyTarget = undefined;
	lastTargetPos = new Vector2(-infinity, -infinity);
	retargetTime = 1;			//How often to recalculate the optimal target
	targetTimer = 1;
	
	ai_lookPoint = new Vector2(0,0);
	ai_patrolPath = undefined;
	ai_patrolIndex = 0;
	ai_patrolID = -1;
	ai_canEvade = false;
	ai_LRand = 1 - ((current_time mod 2)*2);
	
	ai_useAlertMode = false;
	
	ai_searchDelay = 1;
	ai_searchTmr = 0;
	
	
	
	ai_alertDelay = 1;
	ai_alertTmr = 0;
	
	ai_targetVisible = false;
	ai_directPath = false;
	
	
	allyTarget = undefined;
	ai_CurrentState = AI_STATE.IDLE;
	ai_debugTxt = "NyukNyuk";
	
	idleTime = 3;
	idleTimer = 0;
	roamPos = new Vector2(-infinity,-infinity);
	
	movX = 0;
	movY = 0;
	
	reqPath = false;
	navPos = new Vector2(0,0);
	
	navResolution = 20; //How many frames it should wait before recalculating the path, lower values equal more responsive pathing, but slower performance
	navCounter = 0;		//Timer for navResolution calculating

	ai_fightDelay = 0;
	ai_fightTmr = 0;
	ai_fightLock = false;
	
	ai_prepAttack = false;
	beginAttack = false;
	ai_atkDelay = 1;
	atkTimer = 0;
	atkDecay = 1;
	atkTmrCheck = 0;
	ai_atkDir = 0;
	ai_priorityMod = 1;
	ai_priorityDelay = 5;
	ai_priorityTmr = 5;
	
	ai_logic_idle = AI_Idle;
	ai_logic_search = AI_Idle;
	ai_logic_investigate = AI_Idle;
	ai_logic_engage = AI_Idle;
	ai_logic_moveEngage = AI_Idle;
	ai_logic_fight = AI_Idle;
	ai_logic_moveFight = AI_Idle;
	ai_logic_postFight = AI_Idle;
	
	//Abilities to use under certain circumstances
	ai_abl_fight = LOADOUT_SLOT.NONE;
	ai_abl_engage = 0;
	ai_abl_evade = 0;
	ai_abl_spEvade = 0;
	ai_abl_spFight = 0;
	ai_abl_spEngage = 0;
	
	ai_abl_evadeRes = 0.2;
	ai_abl_evadeTmr = 0;
	ai_abl_spCD = 10;
	ai_abl_spTmr = 0;
		
	ai_bhv_Offensive = 1;
	ai_bhv_Defensive = 1;
	ai_bhv_Support = 1;
	
	ai_Path = path_add();
	ai_Path_Res = 20;
	ai_Path_Ctr = 0;
	
	ai_hasInit = false;
	func_Init = AI_BaseInit;
	func_PreUpdateStep = AI_PreUpdateStep;
	func_UpdateStep = AI_UpdateStep;
	func_EndUpdateStep = AI_EndUpdateStep;
	func_DrawStep = AI_DrawStep;
}

function AI_PreUpdateStep(a_ai)
{
	with a_ai
	{
		slavedObj.PlayerControlled = false;
		Oper_PreUpdateStep(self);
		
		atkTimer = clamp(atkTimer, 0, ai_atkDelay);
		if(CheckExistance(slavedObj))
		{
			if(slavedObj.teamID == TEAMID.Enemy)color = c_enemyRed;
			else if(slavedObj.teamID == TEAMID.Ally) color = c_allyGreen;
			
			slavedObj.in_mx = 0;
			slavedObj.in_my = 0;
			slavedObj.in_lx = 0;
			slavedObj.in_ly = 0;
			ai_lookPoint = new Vector2(0,0);
			
			//Reset Path every X frames to rebuild Pathing
			ai_Path_Ctr += 1;
			if(ai_Path_Ctr >= ai_Path_Res)
			{
				path_clear_points(ai_Path);
				ai_Path_Ctr = 0;
			}
			
			//object_set_mask(id, slavedObj.locomotor.image_index);
			
			targetTimer += GetDeltaTime(slavedObj.timeScale);
			
			if(targetTimer >= retargetTime)
			{
				targetTimer = 0;
				enemyTarget = FindTargetNearest(slavedObj.x,slavedObj.y,base_Character,slavedObj,true,TEAMID.Other);
				AI_CheckTargetPos(self);
			}
			
			if(CheckExistance(allyTarget) && global.Player1ID != undefined)
			{
				allyTarget = global.Player1ID.slavedObj;	
			}
			
			if(CheckExistance(enemyTarget))
			{
				if(collision_line(slavedObj.x,slavedObj.y,enemyTarget.x,enemyTarget.y,base_PhysWall, false, true) == noone)
				{
					lastTargetPos = new Vector2(enemyTarget.x, enemyTarget.y);
				}
			}
			
		}	
	}
}

function AI_UpdateStep(a_ai)
{
	with a_ai
	{
		Oper_UpdateStep(self);	
		slavedObj.char_atkCharge = clamp(ai_atkDelay - atkTimer,0,1);
	}
}

function AI_EndUpdateStep(a_ai)
{
	with a_ai
	{
		Oper_EndUpdateStep(self);
		if(ai_lookPoint.x != 0 || ai_lookPoint.y != 0)
		{
			var tVec = GetOffsetVec(slavedObj, ai_lookPoint);
			slavedObj.in_lx = tVec.x;
			slavedObj.in_ly = tVec.y;
		}
	}
}

function AI_DrawStep(a_ai)
{
	with a_ai
	{
		Oper_DrawStep(self);	
		//draw_sprite_ext(enm_AimDir, 0, slavedObj.x, slavedObj.y, 1,1,slavedObj.direction, c_white, 1);
		if(ai_CurrentState == AI_STATE.SEARCH)draw_sprite(spr_Search, -1, slavedObj.x, slavedObj.y-8);
		else if (ai_CurrentState == AI_STATE.INVESTIGATE) draw_sprite(spr_Investigate, -1, slavedObj.x, slavedObj.y-8);
		if(global.debug)
		{
			if(ai_searchTmr > 0) DrawTextOutline(slavedObj.x, slavedObj.y-10, 0.5, string(ai_searchDelay-ai_searchTmr), c_black, c_white, 64, true);
		}
	}
}

function AI_SetAILoadout(a_ai, a_fight, a_engage, a_evade, a_spFight, a_spEngage, a_spEvade)
{
	with a_ai
	{
		ai_abl_fight = a_fight;
		ai_abl_engage = a_engage;
		ai_abl_evade = a_evade;
		ai_abl_spFight = a_spFight;
		ai_abl_spEngage = a_spEngage;
		ai_abl_spEvade = a_spEvade;
	}
}

function AI_GetUtilityScores(a_char)
{
	with a_char
	{
		var chosenAbl = 0;
		var ablArr = [utility1, utility2];
		var highestSc = -1;
		for(var i = 0; i < array_length(ablArr); i += 1)
		{
			var sc = AI_GetAbilityScores(ablArr[i]);
			if(sc > highestSc)
			{
				chosenAbl = i;
				highestSc = sc;
			}
		}
		return ablArr[chosenAbl];
	}	
}

function AI_GetHighestAbility(a_char)
{
	with a_char
	{
		var chosenAbl = 0;
		var ablArr = variable_struct_get_names(char_AC.ac_activeLoadout.ld_slots);
		var highestSc = -1;
		for(var i = 0; i < array_length(ablArr); i += 1)
		{
			var tAbl = variable_struct_get(char_AC.ac_activeLoadout.ld_slots, ablArr[i])
			var sc = AI_GetAbilityScores(tAbl);
			if(sc > highestSc)
			{
				if(highestSc != -1)
				{
					if(tAbl.ai_CanAttack)
					{
						chosenAbl = i;
						highestSc = sc;
					}
				}else
				{
					chosenAbl = i;
					highestSc = sc;
				}
			}
		}
		return variable_struct_get(char_AC.ac_activeLoadout.ld_slots,ablArr[chosenAbl]);
	}
}

function AI_GetAbilityScores(a_abl)
{
	with a_abl
	{
		var retScore = a_abl.ai_Score;
		for(var i = 0; i < array_length(a_abl.abl_Components); i += 1)
		{
			var tComp = a_abl.abl_Components[i];
			var sc = tComp.abl_comp_AIUpdate();
			retScore *= sc;
		}
	}
	return retScore;
}

function AI_RunCompAILogic(a_abl, a_ai)
{
	with a_abl
	{
		for(var i = 0; i < array_length(a_abl.abl_Components); i += 1)
		{
			var tComp = a_abl.abl_Components[i];
			tComp.abl_comp_AIOnFire(a_ai);
		}
	}
}
