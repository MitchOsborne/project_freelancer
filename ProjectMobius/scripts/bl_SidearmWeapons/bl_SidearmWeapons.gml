// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
	
function Internal_bp_Init_Sidearm()
{
	with global.bp_Abilities.bp_BaseSidearmStats
	{
		bp_Range = 320;
		bp_Spread = 1;
		bp_DMGScale = 0.8;
		bp_RPM = 240;
		bp_Ammo = 8;
		bp_ReloadSpd = 1.3;
		bp_EquipTime = 0.7;
		bp_AutoReload = false;
		bp_SpeedCurve = "Sidearm";
		bp_WeightBonus = 1.2;
	}
	
	with global.bp_Abilities.bp_M9T 
	{

		bp_Name = "M9-Tranq";
		bp_Frame = base_wep_Tranq;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_DMGType = DMG_TYPE.EM;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseSidearmStats);
		
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_RPM = 45;	
		}
		bp_SpriteSet = sprSet_M9T;
		bp_SFX = sfx_shoot1;
	}
	
	with global.bp_Abilities.bp_Rook443 
	{

		bp_Name = "Rook-443";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseSidearmStats);

		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		bp_SpriteSet = sprSet_Rook443Base;
		bp_SFX = sfx_shoot1;
	}
	
	

	with global.bp_Abilities.bp_Rook443H 
	{


		bp_Name = "Rook-443 Carbine";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = global.bp_Abilities.bp_Rook443.bp_WepStats;

		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 1.2;
			bp_Spread = 0.8;
			bp_DMGScale = 1.1;
			bp_RPM = 0.9;
			bp_Ammo = 1.2;
			bp_ReloadSpd = 1.1;
		}

		bp_SpriteSet = sprSet_Rook443Base;
		bp_SFX = sfx_shoot1;
	}

	with global.bp_Abilities.bp_Naga44 
	{
		bp_Name = "Naga-44";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseSidearmStats);

		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Range = 480;
			bp_Spread = 1;
			bp_DMGScale = 8;
			bp_RPM = 180;
			bp_Ammo = 5;
			bp_ReloadSpd = 0.4;
			bp_ReloadAmount = 1;
		}

		bp_SpriteSet = sprSet_Naga44Base;
		bp_SFX = sfx_shoot1;
	}

	with global.bp_Abilities.bp_Naga44H 
	{
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		bp_Name = "Naga-44H";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = Copy_Struct(bp_WepStats,global.bp_Abilities.bp_Naga44.bp_WepStats);
		with bp_StatMods
		{
			bp_Range = 1.2;
			bp_Spread = 0.8;
			bp_DMGScale = 1.2;
			bp_RPM = 0.8;
			bp_Ammo = 1;
			bp_ReloadSpd = 1.3;
		}

		bp_SpriteSet = sprSet_Naga44Base;
		bp_SFX = sfx_shoot1;
	}


	with global.bp_Abilities.bp_Naga44SG {



		bp_Name = "Naga-44SG";
		bp_Frame = base_wep_Shotgun;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = Copy_Struct(bp_WepStats,global.bp_Abilities.bp_Naga44.bp_WepStats);
		
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_DMGScale = 0.8;
			bp_Ammo = 3;	
		}
		
		with bp_StatMods
		{
			bp_Range = 0.5;
			bp_Spread = 4;
			bp_DMGScale = 1;
			bp_RPM = 1.6;
			bp_Ammo = 1;
			bp_ReloadSpd = 1.3;
		}

		bp_SpriteSet = sprSet_Naga44Base;
		bp_SFX = sfx_shoot1;
	}



	with global.bp_Abilities.bp_Storm 
	{


		bp_Name = "Storm";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseSidearmStats);
		
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Spread = 1;
			bp_DMGScale = 2;
			bp_RPM = 260;
			bp_Ammo = 12;
			bp_ReloadSpd = 0.7;
		}

		bp_SpriteSet = sprSet_Rook443Base;
		bp_SFX = sfx_shoot1;
		array_push(bp_Components, abl_comp_Storm);

	}

	with global.bp_Abilities.bp_Drake
	{

		bp_Name = "Drake";
		bp_Frame = base_wep_Sidearm;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;

		bp_WepStats = global.bp_Abilities.bp_Storm.bp_WepStats;

		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 2;
			bp_Spread = 1;
			bp_DMGScale = 3;
			bp_RPM = 0.5;
			bp_Ammo = 0.5;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_Rook443Base;
		bp_SFX = sfx_shoot2;
		array_push(bp_Components, abl_comp_Drake);

	}
	
	with global.bp_Abilities.bp_BossFlamer
	{

		bp_Name = "Boss_Flamer";
		bp_Frame = base_wep_Flamer;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseSidearmStats);

		bp_AttackArr = [atk_Flamer];
		bp_AttackFin = undefined;
		
		bp_SpriteSet = sprSet_Rook443Base;
		bp_SFX = sfx_dash;
		
		with bp_WepStats
		{
			bp_Ammo = infinity;
			bp_RPM = 600;
			bp_Spread = 45;
			bp_ReloadSpd = 0;
			bp_Range = 640;
			bp_ProjectileCount = 5;
		}
	}
}