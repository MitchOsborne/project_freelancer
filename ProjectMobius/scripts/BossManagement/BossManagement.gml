// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function BossManager() constructor{
	
	bossChar = undefined;
	camera = instance_find(CameraMarker,0);
	startFight = false;
	initComplete = false;
	
	hpBarWidth = 100;
	hpBarHeight = 10;
	posOffset = new Vector2(0,0);
	
}

/*
function DrawBossHP()
{
	with global.bm
	{
		if(CheckExistance(bossChar))
		{
			var hpVal = (bossChar.hp / bossChar.char_maxHP) * 100;
			posOffset = new Vector2(global.cam_width/2 - hpBarWidth, global.cam_height/5); 
		
			draw_healthbar(posOffset.x - hpBarWidth/2,posOffset.y - hpBarHeight/2,posOffset.x + hpBarWidth/2,posOffset.y + hpBarHeight/2 ,hpVal, c_black, c_yellow, c_lime, 0, true, true);
		}
	}
}
*/

function InitBoss()
{
	array_clear(global.spawnPool);
	array_push(global.spawnPool,enemy_DragonBoss);
	//ds_list_add(global.spawnPool,enemy_Dragon);
	//ds_list_add(global.spawnPool,enemy_Dragon);
	//ds_list_add(global.spawnPool,enemy_Dragon);
	//ds_list_add(global.spawnPool,enemy_Dragon);
	
	room_SpawnEnemies();	
	
}

/*
function Update_Boss()
{
	with global.bm
	{
		if(layer_exists("Boss"))
		{
			instance_deactivate_layer("BossPhase_3");
			instance_deactivate_layer("BossPhase_2");
			instance_deactivate_layer("BossPhase_1");
		}
		bossChar = instance_find(enemy_DragonBoss,0);
		if(CheckExistance(bossChar))
		{
			camera.override = true;
			camera.overrideTarget = bossChar;
			var bossHPScale = bossChar.hp / bossChar.char_maxHP;
			bossChar.diffScaling = 1;
			var phase = 0;
			if(layer_exists("Boss"))
			{
				if(bossHPScale == 0) phase = 0;
				else if(bossHPScale < 0.33) phase = 3;
				else if(bossHPScale < 0.66) phase = 2;
				else phase = 1;
						
				if(phase == 1) 
				{
					instance_activate_layer("BossPhase_1");
				}
				else if (phase == 2) 
				{
					instance_activate_layer("BossPhase_2");
				}
				else if (phase == 3)
				{
					instance_activate_layer("BossPhase_3");
					bossChar_diffScaling = 3;
				}
			}
			
		}else
		{
			camera.override = false;	
		}
	}
}
*/