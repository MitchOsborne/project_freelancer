// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function RoomManager() constructor
{

	global.rm = self;
	spawnTimer = 0;
	spawnDelay = 0.25;
	spawnCount = 0;
	
	hasCleared = true;
	
	//if(!global.currentRoom.rd_isCleared)
	//{
		//room_InitRoomContents();
		room_SpawnPatrols();
	//}
	room_InitPlayerPos();
	if(global.BGM != global.currentRoom.rd_BGM) 
	{
		if(global.currBGM != undefined && audio_is_playing(global.currBGM)) audio_stop_sound(global.currBGM);
		global.BGM = global.currentRoom.rd_BGM;
		PlayBGM();
	}

}

function room_CreateRoom(a_roomData)	//Manages a Singleton system for rooms so data is persistent between room transfers
{
	var newRoom = true;
	var aRm = new a_roomData();
	var retVar = undefined;
	for(var i = 0; i < array_length(Overlord.roomArr); i += 1)
	{
		var tRm = Overlord.roomArr[i];
		if(Internal_CompareRoom(aRm, tRm)) 
		{
			newRoom = false;
			retVar = tRm;
			break;
		}
	}
	if(newRoom)
	{
		array_push(Overlord.roomArr, aRm);
		retVar = aRm;
	}
	return retVar;
}

function TransitionRoom()
{
	if(!global.cp_Respawn)
	{
		if(global.targetRoom != undefined)
		{
			global.nextRoom = room_CreateRoom(global.targetRoom);				
		}
	}
	
	global.exitRoom = false;
	
	if(global.currentRoom.rd_region != global.nextRoom.rd_region) global.alertMode = ALERT_STATE.Clear;
	if(global.previousRoom != undefined)
	{
		//if(global.previousRoom != global.nextRoom) global.previousRoom.rd_isCleared = false;
	}
	global.previousRoom = global.currentRoom;
	global.currentRoom = global.nextRoom;
	room_goto(global.nextRoom.rd_roomObj);
	startRoom = true;
}

function Internal_CompareRoom(a_roomA, a_roomB)
{
	return a_roomA.rd_roomID == a_roomB.rd_roomID;	
}

function room_InitPlayerPos()
{
	var entry = room_GetEntrancePoint(global.entryPoint, global.entryIndex);
	
	
	if(CheckExistance(entry))
	{
		for(var i = 0; i < ds_list_size(global.PlayerIDList); i += 1)
		{
			var tPlayer = global.PlayerIDList[|i];
			MovePlayer(tPlayer, entry.x, entry.y);
		
			CameraMarker.x = entry.x;
			CameraMarker.y = entry.y;
		}
	}
	//var tInst = instance_create_layer(entry.x, entry.y, "Instances", ally_SpawnBeacon);
	//tInst.spawnObj = char_Medic;
	global.entryPoint = entry_default;	
	
}


function room_GetEntrancePoint(a_entPoint, a_entIndex)
{
	with global.rm
	{
		var tInst = instance_find(entry_default, 0);
		for(var i = 0; i < instance_number(base_entry); i += 1)
		{
			var tExit = instance_find(base_entry,i);
			if(tExit.ent_Index == a_entIndex)
			{
				tInst = tExit;
				break;
			}
		}
	}
	return tInst;
}


function defunc_room_GetExitDoor(a_exitDoor, a_exitIndex)
{
	with global.rm
	{
		var tInst;
		switch (a_exitDoor)
		{
			case DOOR_SIDE.TOP:
			a_exitIndex = clamp(a_exitIndex, 0, array_length(botEntryArr)-1);
			tInst = instance_find(botEntryArr[a_exitIndex],0);
			if(tInst == noone)tInst = instance_find(base_entry,0);
			break;
			case DOOR_SIDE.RIGHT:
			a_exitIndex = clamp(a_exitIndex, 0, array_length(leftEntryArr)-1);
			tInst = instance_find(leftEntryArr[a_exitIndex],0);
			if(tInst == noone)tInst = instance_find(base_entry_left,0);
			break;
			case DOOR_SIDE.BOTTOM:
			a_exitIndex = clamp(a_exitIndex, 0, array_length(topEntryArr)-1);
			tInst = instance_find(topEntryArr[a_exitIndex],0);
			if(tInst == noone)tInst = instance_find(base_entry_top,0);
			break;
			case DOOR_SIDE.LEFT:
			a_exitIndex = clamp(a_exitIndex, 0, array_length(rightEntryArr)-1);
			tInst = instance_find(rightEntryArr[a_exitIndex],0);
			if(tInst == noone)tInst = instance_find(base_entry_right,0);
			break;
			default:
			tInst = instance_find(entry_default, 0);
			break;
		}
		if(tInst == noone) tInst = instance_find(entry_default, 0);
	}
	return tInst;
}

function room_GetExitCheckpoint(a_exitDoor)
{
	with global.rm
	{
		var retVal = DOOR_SIDE.NONE;
		switch (a_exitDoor)
		{
			case DOOR_SIDE.TOP:
			retVal = DOOR_SIDE.BOTTOM
			break;
			case DOOR_SIDE.RIGHT:
			retVal = DOOR_SIDE.LEFT;
			break;
			case DOOR_SIDE.BOTTOM:
			retVal = DOOR_SIDE.TOP;
			break;
			case DOOR_SIDE.LEFT:
			retVal = DOOR_SIDE.RIGHT;
			break;
			default:
			break;
		}
	}
	return retVal;
}


function room_InitRoomContents()
{
	global.currRegion = GetRegion(global.currentRoom.rd_region);
	var enmArr = global.currRegion.rg_enmArr;
	
	if(instance_number(region_EnemySpawn) > 0)
	{
		if(global.currRegion.rg_randSpawn)
		{
			for(var i = 0; i < (global.difficulty) * 3 + ceil((sqrt(global.enemyLevel))); i += 1)
			//for(var i = 0; i < global.spawnCap; i += 1)
			{
				var tEnm = enmArr[round(random_range(0,array_length(enmArr)-1))];
				var tVal = GetRegionEnmPool(global.currRegion);
				if(tVal >= i) array_push(global.spawnPool, tEnm);
			}
		}else
		{
			for(var i = 0; i < array_length(enmArr); i += 1)
			{
				array_push(global.spawnPool,enmArr[i]);	
			}
		}
	}

	/*
	if(global.currentRoom.roomType == t_room_loot)
	{
		var tInst = new Pkup_sKey();
		tInst.x = entry_default.x;
		tInst.y = entry_default.y;
		ds_list_add(global.pkup_List, tInst);	
	}
	*/
	for(var i = 0; i < instance_number(region_PropSpawn); i += 1)
	{
		var propRate = 15;
		var propPool = 3;
	
		var tReg = instance_find(region_PropSpawn,i);
		for(var ix = tReg.bbox_left; ix < tReg.bbox_right; ix += global.gridSize)
		{
			for(var iy = tReg.bbox_top; iy < tReg.bbox_bottom; iy += global.gridSize)
			{
				var spawnRoll = irandom_range(0,100);
				if(spawnRoll <= propRate && propPool > 0)
				{
					instance_create_layer(ix,iy,"Instances", prop_Box);
					propPool -= 1;
				}
			}
		}
	}
}

function room_Update()
{
	room_CheckCleared();
	if((!global.currentRoom.rd_isCleared) && global.alertMode == ALERT_STATE.Danger) room_SpawnEnemies();
}

function room_SpawnBoss()
{
		
}


function room_SpawnPatrols()
{
	with global.rm
	{
		var patrolCount = instance_number(PatrolMarker_Start);
		if(patrolCount > 0)
		{
			for(var i = 0; i < patrolCount; i += 1)
			{
				var canSpawn = true;
				var spPoint = instance_find(PatrolMarker_Start, i);
				for(var n = 0; n < array_length(global.currentRoom.rd_TombstoneArr); n += 1)
				{
					var tN = global.currentRoom.rd_TombstoneArr[n];
					if(tN.tmb_patrolID == spPoint.ptrl_id) 
					{
						canSpawn = false;
					}
				}
				
				array_push(spPoint.ptrl_Points, spPoint);
				if(canSpawn)
				{
					var tInst = SpawnCharacter(spPoint.ptrl_spawnData, spPoint, undefined, TEAMID.Enemy)
					tInst.char_patrolPath = spPoint;
					spPoint.ptrl_owner = tInst;
				}
				//Initialise Patrol Paths
				var patrolPoints = instance_number(PatrolMarker);
				for(var n = 0; n < patrolPoints; n += 1)
				{
					var tPoint = instance_find(PatrolMarker, n);
					if(tPoint != spPoint)
					{
						if(tPoint.ptrl_id == spPoint.ptrl_id) array_push(spPoint.ptrl_Points, tPoint);
					}
				}
				
				array_sort(spPoint.ptrl_Points, func_SortPatrolPoints);
			}
		}	
	}
}


function defunc_room_SpawnPatrols()
{
	with global.rm
	{
		var enmArr = global.currRegion.rg_enmArr;
		var patrolCount = instance_number(PatrolMarker_Start);
		if(patrolCount > 0)
		{
			for(var i = 0; i < patrolCount; i += 1)
			{
				var spPoint = instance_find(PatrolMarker_Start, i);
				array_push(spPoint.ptrl_Points, spPoint);
				if(array_length(global.spawnPool) > 0)
				{
					var tInst = instance_create_layer(spPoint.x, spPoint.y, "Instances", enm_SpawnBeacon);
					tInst.spn_PatrolPath = spPoint;
					tInst.spawnObj = global.spawnPool[0];
					array_delete(global.spawnPool, 0,1);
				}
				//Initialise Patrol Paths
				var patrolPoints = instance_number(PatrolMarker);
				for(var n = 0; n < patrolPoints; n += 1)
				{
					var tPoint = instance_find(PatrolMarker, n);
					if(tPoint != spPoint)
					{
						if(tPoint.ptrl_id == spPoint.ptrl_id) array_push(spPoint.ptrl_Points, tPoint);
					}
				}
				
				array_sort(spPoint.ptrl_Points, func_SortPatrolPoints);
			}
		}	
	}
}


function room_SpawnEnemies()
{
		
	with global.rm
	{
		spawnTimer += GetDeltaTime(global.timeScale);
		if(global.spawnEnm && spawnTimer >= spawnDelay)
		{
			spawnTimer = 0;
			if(instance_number(region_EnemySpawn) > 0)
			{
				var spawnerCount = instance_number(region_EnemySpawn);
				var sp = instance_find(region_EnemySpawn,random(spawnerCount));
		
				var spVec = new Vector2(floor(random_range(sp.bbox_left, sp.bbox_right)/global.gridSize), floor(random_range(sp.bbox_top, sp.bbox_bottom)/global.gridSize));
				spVec.x = spVec.x*global.gridSize + global.gridSize/2;
				spVec.y = spVec.y*global.gridSize + global.gridSize/2;
				var tInst = SpawnCharacter(global.spawnPool[0], spVec, undefined, TEAMID.Enemy);
				array_delete(global.spawnPool, 0,1);
			}
	
		}
	}
		
}

function room_CheckCleared()
{
	
	var enmCount = ds_list_size(global.enmList);
	var poolSize = array_length(global.spawnPool);
	
	if(enmCount <= global.spawnCap/2 && poolSize>0)
	{
		global.spawnEnm = true	
	}
	if(enmCount >= global.spawnCap || poolSize<= 0)
	{
		global.spawnEnm = false;	
	}
	
	
	
	if((enmCount + poolSize) > 0)
	{
		global.roomClear = false;	
		global.currentRoom.rd_isCleared = false;
	}else
	{
		global.roomClear = true;
		
		global.currentRoom.rd_isCleared = true;
		
	}
	if(global.roomClear == false)
	{
		global.rm.hasCleared = false;	
	}
	
	if(global.rm.hasCleared == false && global.roomClear == true)
	{
		var tInst = instance_create_layer(CameraMarker.x, CameraMarker.y,"Text",txt_dmg);
		audio_play_sound(sfx_boom, 20,false);
		
		if(global.alertMode == ALERT_STATE.Danger) global.alertMode = ALERT_STATE.Warning;
		ShakeScreen(50);
		tInst.damageVal = "ROOM CLEARED";
		tInst.colA = c_lime;
		tInst.colB = c_green;
		global.rm.hasCleared = true;
	}
}
		

function room_DoorCheck()
{
	
	if(global.roomClear)
	{
		instance_deactivate_layer("Doors");
	}else
	{
		instance_activate_layer("Doors");	
	}
}