// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function array_find(a_arr, a_data){
	var i = 0;
	for(i = 0; i < array_length(a_arr); i += 1)
	{
		var tData = a_arr[i];
		if(tData == a_data) return i;	
	}
	return -1;
}

function array_clear(a_arr)
{
	array_delete(a_arr, 0, array_length(a_arr));	
}

function array_append(a_arr1, a_arr2)
{
	array_copy(a_arr1, array_length(a_arr1), a_arr2, 0, array_length(a_arr2));	
}

function array_fill(a_arr, a_data, a_count)
{
	array_clear(a_arr);
	for(var i = 0; i < a_count; i += 1)
	{
		array_push(a_arr, a_data);	
	}
}

function func_SortPatrolPoints(a_ptA, a_ptB)
{
	if(a_ptA.ptrl_index > a_ptB.ptrl_index) return 1;
	else return -1;
}

function func_SortLightByAlpha(a_ltA, a_ltB)
{
	var tA = a_ltA.lt_sprite;
	var tB = a_ltB.lt_sprite;
	if (tA > tB) return 1;
	else return -1;
}

function func_SortStatMods(a_Mod1, a_Mod2)
{
	if(a_Mod1.stat_currLvl > a_Mod2.stat_currLvl) return -1;
	else if (a_Mod1.stat_currLvl < a_Mod2.stat_currLvl) return 1;
	else if (a_Mod1.mainStat < a_Mod2.mainStat) return -1;
	else if (a_Mod1.mainStat > a_Mod2.mainStat) return 1;
	//if Riven Mod
	else if(a_Mod1.mainStat != a_Mod1.altStat)
	{
		//if Mod2 is also a Riven Mod, return equal, otherwise return Non-Riven Mod
		if(a_Mod2.mainStat != a_Mod2.altStat) return 0;
		else return 1;
	}
	else if(a_Mod2.mainStat != a_Mod2.altStat) return -1 
	else return 0;
}

function func_SortCombatWidth(a_charA, a_charB)
{
	
	if(a_charA.ai_fightScore <= a_charB.ai_fightScore) return -1;
	else if (a_charA.ai_fightScore == a_charB.ai_fightScore)
	{
		if(a_charA.id <= a_charB.id) return -1;	
	}
	return 1;
	
}
