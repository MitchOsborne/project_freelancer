// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function AI_Method(a_offScore, a_defScore, a_supScore, a_taskList) constructor
{
	//Scores
	ai_mtd_OffensiveScore = a_offScore;
	ai_mtd_DefensiveScore = a_defScore;
	ai_mtd_SupportScore = a_supScore;
	//Tasks List
	mtd_TaskList = a_taskList;
	
}

