// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum CONT_MODE
{
	NONE = 0,
	KB_M = 1,
	GAMEPAD = 2,
	AI = 3,
}


function base_Player() : base_Operator() constructor{

	inputSource = undefined;
	
	color = c_white;
	
	aimReticle = undefined;
	
	
	teamID = TEAMID.Ally;
	uiOffset = 0;
	
	nodeList = ds_list_create();
	
	followerDist = 80;
	
	xp = 0;
	powerLvl = 1;
	levelCost = 100;
	
	func_PreUpdateStep = Player_PreUpdateStep;
	func_UpdateStep = Player_UpdateStep;
	func_EndUpdateStep = Player_EndUpdateStep;
	func_DrawStep = Player_DrawStep;
	
}

function Player_PreUpdateStep(a_player)
{
	
	with a_player
	{
		if(CheckExistance(slavedObj))
		{
			
			slavedObj.PlayerControlled = true;
			slavedObj.ownerColor = color;
			
			slavedObj.in_mx = 0
			slavedObj.in_my = 0
			slavedObj.in_lx = 0
			slavedObj.in_ly = 0
			slavedObj.inFocus = 0;
			slavedObj.inLockDir = 0;
			slavedObj.inWeapon1 = 0;
			slavedObj.inWeapon2 = 0;
			slavedObj.inUtility1 = 0;
			slavedObj.inUtility2 = 0;
			slavedObj.inAbility1 = 0;
			slavedObj.inAbility2 = 0;
			slavedObj.inCore = 0;
			
			if(inputSource != undefined)
			{
				var inLLeft		= inputSource.in_LLEFT;
				var inLRight	= inputSource.in_LRIGHT;
				var inLUp		= inputSource.in_LUP;
				var inLDown		= inputSource.in_LDOWN;
				if(!global.uiFocus)
				{
						slavedObj.inFocus	= inputSource.in_ALT;
						slavedObj.inLockDir	= inputSource.in_LOCK;
						slavedObj.inWeapon1	= inputSource.in_WEAPON1;
						slavedObj.inWeapon2	= inputSource.in_WEAPON2;
						slavedObj.inUtility1 = inputSource.in_UTILITY1;
						slavedObj.inUtility2 = inputSource.in_UTILITY2;
						slavedObj.inAbility1 = inputSource.in_ABILITY1;
						slavedObj.inAbility2 = inputSource.in_ABILITY2;
						slavedObj.inCore = inputSource.in_CORE;
						slavedObj.inInteract = inputSource.in_INTERACT;
				
					slavedObj.in_mx = (inLRight - abs(inLLeft));
					slavedObj.in_my = (inLDown - abs(inLUp));
				
					if(inputSource.in_InputType == CONT_MODE.KB_M)
					{
						var mDir = point_direction(slavedObj.x, slavedObj.y, mouse_x, mouse_y) - 90;
						var lVec = GetCoord(mDir);
						slavedObj.in_lx = lVec.x;
						slavedObj.in_ly = lVec.y;
					}else
					{
						var inRLeft		= inputSource.in_RLEFT;
						var inRRight	= inputSource.in_RRIGHT;
						var inRUp		= inputSource.in_RUP;
						var inRDown		= inputSource.in_RDOWN;
					
						slavedObj.in_lx = (inRRight - inRLeft);
						slavedObj.in_ly = (inRDown - inRUp);
					}
				}
			}
			
		}else
		{
			SpawnCharacter(slavedObj,instance_find(entry_default, 0),self, teamID);	
		}
	}

}

function Player_UpdateStep(a_player)
{
	with a_player
	{
		Oper_UpdateStep(self);
				
		
		if(global.worldLvl > powerLvl)
		{
			powerLvl = global.worldLvl;
			var tInst = instance_create_layer(slavedObj.x,slavedObj.y,"Text",txt_dmg);
			audio_play_sound(sfx_lvlup, 20,false);
			ShakeScreen(50);
			tInst.damageVal = "LEVEL UP";
			tInst.colA = c_aqua;
			tInst.colB = c_navy;
			SanityCheckCharStats(slavedObj);
			if(CheckExistance(slavedObj.smn_Minion))
			{
				UpdateSummonStats(slavedObj.smn_Minion);
				SanityCheckCharStats(slavedObj.smn_Minion);
				slavedObj.smn_Minion.hp = slavedObj.smn_Minion.char_maxHP;
			}
		}
	}
}

function Player_EndUpdateStep(a_player)
{
	
	with a_player
	{
		Oper_EndUpdateStep(self);
		
		with slavedObj
		{
			//var retDist = weapon1.range;
			var retDist = 100;
			
			var reticleX = char_offset.x+lengthdir_x(retDist,direction+90);
			var reticleY = char_offset.y+lengthdir_y(retDist,direction+90);
			if(active_operator.controlMode == CONT_MODE.KB_M)
			{
				if(point_distance(char_offset.x, char_offset.y, mouse_x, mouse_y) < retDist)
				{
					reticleX = mouse_x;
					reticleY = mouse_y;
				}
				
			}
		
			var retPosObs = GetCollisionPoint(char_offset.x,char_offset.y,reticleX, reticleY, base_PhysWall,true,true);
			var retPosEnemy = [infinity, infinity];
			if(target != undefined && instance_exists(target))
			{
				var retPosEnemy = [target.x, target.y];
			}
			var retPos;
			if(point_distance(x,y,retPosObs[0],retPosObs[1]) < 
				point_distance(x,y,retPosEnemy[0],retPosEnemy[1]))
			{
				retPos = retPosObs;	
			}else
			{
				retPos = retPosEnemy;	
			}
		}
		aimReticle.x = retPos[0];
		aimReticle.y = retPos[1];
	}
}

function Player_DrawHud(a_player)
{
	with a_player
	{
		if(instance_exists(slavedObj))
		{
			draw_set_color(c_white);
			draw_set_alpha(1);
			draw_set_font(fnt_default);
			draw_text(uiOffset.x,uiOffset.y+10,"Char Class: " + string(slavedObj.char_StatArchCard.crd_Name));
			draw_text(uiOffset.x,uiOffset.y+20,"Char Level: " + string(slavedObj.charLvl));
			draw_text(uiOffset.x,uiOffset.y+30,"Class Level: " + string(slavedObj.classLvl));
			draw_text(uiOffset.x,uiOffset.y+40,"credits: " + string(global.credits));
			draw_text(uiOffset.x,uiOffset.y+50,"EXP to Next Level: " + string(global.WorldExpPool - xp));
			draw_text(uiOffset.x,uiOffset.y+60,"Primary Ammo: " + string(slavedObj.char_AC.ac_activeWeapon.resourceDisplay));
			draw_text(uiOffset.x,uiOffset.y+70,"Total Armour: " + string(slavedObj.char_armStat));
			draw_text(uiOffset.x,uiOffset.y+80,"Stamina: " + string(slavedObj.staminaStat));
		
			
		
			draw_text(uiOffset.x,uiOffset.y+260,"Damage Lvl: " + string(slavedObj.char_damageLvl));
			draw_text(uiOffset.x,uiOffset.y+270,"Damage Stat: " + string(slavedObj.char_damageStat));
			draw_text(uiOffset.x,uiOffset.y+280,"AtkSpd Stat: " + string(slavedObj.char_spdStat));
			draw_text(uiOffset.x,uiOffset.y+300,"Shield Lvl: " + string(slavedObj.char_shieldLvl));
			draw_text(uiOffset.x,uiOffset.y+320,"AblDur Stat: " + string(slavedObj.char_durStat));
			draw_text(uiOffset.x,uiOffset.y+330,"AblCD Stat: " + string(slavedObj.char_CDStat));
			
			draw_text(uiOffset.x,uiOffset.y+340,"Crit Rate:" + string(slavedObj.char_critLvl * slavedObj.char_critPerLvl));
			draw_text(uiOffset.x,uiOffset.y+350,"Proc Rate: " + string(slavedObj.char_procLvl * slavedObj.char_procPerLvl));
			
			DrawHPBar(slavedObj, new Vector2(uiOffset.x + 128/2, uiOffset.y+120), new Vector2(128,24), true);
			
			
			
			
			var modSlotSize = 10;
			var modSlotPos = 140;
			for(var i = 0; i < array_length(slavedObj.char_modArr); i += 1)
			{
				var tMod = slavedObj.char_modArr[i];
				if(tMod.displayName && !tMod.canDelete)
				{
					var mName = tMod.name;
					var mStacks = tMod.stacks;
					draw_set_color(c_white);
					draw_text(uiOffset.x, uiOffset.y+modSlotPos + (modSlotSize*i), mName + " x" + string(mStacks) + " <> (" + string(tMod.duration-tMod.durTimer) + ")");
				}
			}
			
		}
	}	
}

function Player_DrawStep(a_player)
{
	with a_player
	{
		Oper_DrawStep(self);
		var posOffset = new Vector2(slavedObj.x, slavedObj.y + slavedObj.sprite_height/2);
		var barWidth = 64;
		var barHeight = 6;
			
		
		if(slavedObj.staminaStat != slavedObj.maxStamina)
		{
			var stamVal = (slavedObj.staminaStat / slavedObj.maxStamina)*100;
			//DrawBar(stamVal, barWidth, barHeight, 0, posOffset, c_yellow);
		}
		/*
		var wep = undefined;
		if(!slavedObj.inFocus) wep = slavedObj.weapon1.pkg_Ability;
		else wep = slavedObj.weapon2.pkg_Ability;
		if(wep != undefined)
		{
			if(wep.ammoCount != wep.ammoMax)
			{
				var ammoVal = wep.ammoCount/wep.ammoMax * 100;
				var reloadVal = wep.reloadTimer/wep.reloadTime * 100;	
				DrawBar(ammoVal, barWidth, barHeight, 1, posOffset, c_ltgrey);
				DrawBar(reloadVal, barWidth, barHeight, 2, posOffset, c_ltgrey);
			}
		}
		wep = undefined;
		if(!slavedObj.inFocus) wep = slavedObj.weapon1.pkg_AltAbility;
		else wep = slavedObj.weapon2.pkg_AltAbility;
		if(wep != undefined)
		{
			if(wep.ammoCount != wep.ammoMax)
			{
				
				var ammoVal = wep.ammoCount/wep.ammoMax * 100;
				var reloadVal = wep.reloadTimer/wep.reloadTime * 100;	
				DrawBar(ammoVal, barWidth, barHeight, 3, posOffset, c_ltgrey);
				DrawBar(reloadVal, barWidth, barHeight, 4, posOffset, c_ltgrey);
			}
		}
		*/
		//draw_line_color(slavedObj.char_offset.x, slavedObj.char_offset.y, aimReticle.x, aimReticle.y,color,color);
	}
}

/*
function Internal_DrawPrimaries(a_player, a_uiHeight, a_iconOffset)
{
	with a_player.slavedObj
	{
		
		//HUD Borders
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x, a_player.uiOffset.y+ a_uiHeight);
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + a_iconOffset.x,a_player.uiOffset.y+a_uiHeight - a_iconOffset.y);
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y);
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight);
		
		//Weapon 1
		var outVal = weapon1.pkg_output1;
		var hudPos = new Vector2(a_player.uiOffset.x, a_player.uiOffset.y+ a_uiHeight); 
		
		draw_sprite(weapon1.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(weapon1.pkg_Icon)/2;
		var c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(weapon1.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Ability 1
		outVal = ability1.pkg_output1;
		hudPos = new Vector2(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight - a_iconOffset.y); 
		draw_sprite(ability1.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(ability1.pkg_Icon)/2;
		c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(ability1.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Utility 1
		outVal = utility1.pkg_output1;
		hudPos = new Vector2(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y); 
		draw_sprite(utility1.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(utility1.pkg_Icon)/2;
		c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(utility1.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Interact
		draw_sprite(spr_Interact,0,a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight);
		
		
		
		draw_set_font(fnt_dmg);
		draw_set_color(c_aqua);				
		draw_text(a_player.uiOffset.x, a_player.uiOffset.y+a_uiHeight, "X");
		draw_set_color(c_yellow);
		draw_text(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight - a_iconOffset.y, "Y");
		draw_set_color(c_lime);
		draw_text(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y, "A");
		draw_set_color(c_red);
		draw_text(a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight, "B");
				
	}		
}
*/
/*
function Internal_DrawSecondaries(a_player, a_uiHeight, a_iconOffset)
{
	with a_player.slavedObj
	{
		//HUD BORDERS
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x, a_player.uiOffset.y+a_uiHeight);
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight - a_iconOffset.y);
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y);	
		draw_sprite(spr_tTargetP1,0,a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight);
	
		//Weapon 2
		var outVal = weapon2.resourceDisplay;
		var hudPos = new Vector2(a_player.uiOffset.x, a_player.uiOffset.y+ a_uiHeight); 
		
		draw_sprite(weapon2.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(weapon2.pkg_Icon)/2;
		var c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(weapon2.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Ability 2
		outVal = ability2.pkg_output1;
		hudPos = new Vector2(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight - a_iconOffset.y); 
		draw_sprite(ability2.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(ability2.pkg_Icon)/2;
		c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(ability2.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Utility 2
		outVal = utility2.pkg_output1;
		hudPos = new Vector2(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y); 
		draw_sprite(utility2.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(utility2.pkg_Icon)/2;
		c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(utility2.pkg_Icon), 10, 0, hudPos, c_col);		
		
		//Ability 3
		outVal = Core.pkg_output1;
		hudPos = new Vector2(a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight); 
		draw_sprite(Core.pkg_Icon,0,hudPos.x, hudPos.y);
		
		hudPos.y += sprite_get_height(Core.pkg_Icon)/2;
		c_col = c_orange;
		if(outVal >= 100) c_col = c_aqua;
		DrawBar(outVal, sprite_get_width(Core.pkg_Icon), 10, 0, hudPos, c_col);		
		
		
	
		draw_set_font(fnt_dmg);
		draw_set_color(c_aqua);				
		draw_text(a_player.uiOffset.x, a_player.uiOffset.y+a_uiHeight, "X");
		draw_set_color(c_yellow);
		draw_text(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight - a_iconOffset.y, "Y");
		draw_set_color(c_lime);
		draw_text(a_player.uiOffset.x + a_iconOffset.x, a_player.uiOffset.y+a_uiHeight + a_iconOffset.y, "A");
		draw_set_color(c_red);
		draw_text(a_player.uiOffset.x + (a_iconOffset.x*2), a_player.uiOffset.y+a_uiHeight, "B");
		
	}
}
*/

function Player_DrawAbilityHud(a_player)
{
	
	
	
	with a_player
	{
		var iconOffset = new Vector2(64,64);
		var uiHeight = 300;
		
		with slavedObj
		{
			if(!inFocus)
			{
				draw_set_alpha(0.5);
				//Internal_DrawSecondaries(a_player, uiHeight + 20, iconOffset);
				draw_set_alpha(1);
				//Internal_DrawPrimaries(a_player, uiHeight, iconOffset);
									
			}else
			{
				draw_set_alpha(0.5);
				//Internal_DrawPrimaries(a_player, uiHeight + 20, iconOffset);
				draw_set_alpha(1);
				//Internal_DrawSecondaries(a_player, uiHeight, iconOffset);
			}
				
				
		}
		
		
	}
}