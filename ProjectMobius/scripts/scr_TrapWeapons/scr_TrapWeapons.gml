// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function wep_WaveShot() : defunc_base_wep_Shotgun() constructor {
	ablCost = 50;
	ablName = "WaveTrap"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 0;
	
	range *= 5;
	
	damageMultiplier *= 1;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapSlow");
	wep_randomSpread = false;
	wep_projectileSpread = 360;
	wep_projectileCount = 24;
	RPM = 60;
}

function wep_BulletTrap() : defunc_base_wep_Sidearm() constructor {
	
	ablCost = 50;
	ablName = "BulletTrap"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 0;
	
	range *= 5;
	
	damageMultiplier *= 1;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapFast");
	wep_randomSpread = false;
	wep_projectileSpread = 0;
	wep_projectileCount = 1;
	RPM = 60;
}

function wep_TurnTrap() : defunc_base_wep_Sidearm() constructor {
	
	ablCost = 50;
	ablName = "TurnTrap"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	reloadTime = 0;
	
	range *= 5;
	
	damageMultiplier *= 1;
	weaponSprite = spr_None;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"TrapMed");
	wep_randomSpread = false;
	wep_projectileSpread = 360;
	wep_projectileCount = 4;
	RPM = 720;
}