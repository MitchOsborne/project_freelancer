function InitEnemyWep(a_loadout, a_ownerID) {


	for(var i = 0; i < array_length(a_loadout); i++)
	{
		var createInst = false;
		if(CheckExistance(a_loadout[i]) == false)
		{
			createInst = true;
		}else
		{
			if(a_loadout[i].owner == undefined)
			{
				a_loadout[i].owner = a_ownerID;	
			}else if (a_loadout[i].owner != a_ownerID)
			{
				createInst = true;
			}
		}
	
		if(createInst)
		{
			var tInst = instance_create_layer(x,y,"Instances", a_loadout[i]);
			tInst.owner = a_ownerID;	
			tInst.teamID = a_ownerID.teamID;
		}
	}


}
