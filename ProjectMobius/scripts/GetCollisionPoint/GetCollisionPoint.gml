function GetCollisionPoint() {
	var a_aX = argument[0];
	var a_aY = argument[1];
	var a_bX = argument[2];
	var a_bY = argument[3];
	var a_object = argument[4];

	var pointData = Collision_Line_Point(a_aX, a_aY, a_bX, a_bY, a_object, true, true);

	var pointVec = [];
	pointVec[0] = pointData[1];
	pointVec[1] = pointData[2];

	return pointVec;


}
