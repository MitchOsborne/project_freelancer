function DamageAgent(a_Agent, a_Damage) {


	//Perform Damage Calculations Here
	if(a_Agent.isInvul == false)
	{
		
		if(a_Agent.hp <= a_Damage)
		{
			return true;	
		}
		
	}
	
	return false;


}

function ReportHit(a_proj, a_weapon, a_subject, a_dmg, a_dir, a_dmgType)
{
	var tReport = new HitReport(a_proj, a_weapon, a_subject, a_dmg, a_dir, a_dmgType);
	array_push(a_subject.inHitArr, tReport);
}
