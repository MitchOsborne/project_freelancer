// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function GetPhysOverlap(a_X, a_Y,physHitArr)
{
	var tRes = false;
	for(var i = 0; i < array_length(physHitArr); i += 1)
	{
		if(place_meeting(a_X, a_Y,physHitArr[i])) tRes = true;
	}
	return tRes;
}