// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function abl_PsiThrow() : base_Psionic() constructor
{
	
	AbilityType = ABILITY_TYPE.RANGED;
	
	abl_sfx = sfx_psi_Throw;
	psiDist = 360;
	damageMultiplier = 2;
	
	psi_ObjArray = [];
	psi_ProjArray = [];
	
	psi_SearchRes = 0.3;	//How often to update the psi_ObjArray with valid objects
	psi_SearchTimer = 0;
	
	psi_TagFunc = psi_GetObjectsFromPrimary;
	psi_PrimeObj = undefined;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"PsiObj");
	
	psi_ActivateAbility = psi_ThrowTagged;
	
	func_PreUpdateStep = PsiPreUpdateStep;
	func_UpdateStep = PsiUpdateStep;
	//func_InitAbility = InitialiseWeapon;
	psi_HoldType = PSI_HOLDTYPE.NONE;
	
	abl_forceStrength = 10;
	abl_stunDur = 1;
	
	psi_ThrowForce = 50;
	psi_ThrowDur = 0.5;
	
}

function wep_PsiWave() : defunc_base_wep_Shotgun() constructor {
	
	ds_list_clear(projectileList);
	ds_list_add(projectileList, tPsiWave);
	abl_sfx = sfx_psi_On;
	ablCost = 1000;
	ablName = "PsiWave"
	ammoMax = 1;
	ammoCount = ammoMax;
	reloadAmount = ammoMax;
	
	cooldown = 5;
	
	reqRelease = true;
	
	range = 80;
	damageMultiplier *= 0;
	weaponSprite = spr_None;
	
	abl_forceStrength = 75;
	
	wep_projectileSpread = 90;
	wep_projectileCount = 5;
	wep_randomSpread = false;
	RPM = 120;
}

function abl_PsiGlobalThrow() : abl_PsiThrow() constructor
{
	cooldown = 0.5;
	AbilityType = ABILITY_TYPE.RANGED;
	psi_TagFunc = psi_GetObjectsFromOwner;
	
}

function abl_PsiSpawnProj() : base_Psionic() constructor
{
	
	staminaCost = 0;
	baseStaminaCost = staminaCost;
	psiDist = 360;
	
	damageMultiplier = 2;
	
	cooldown = 1;
	abl_LockDur = 0;
	
	psi_ObjArray = [];
	psi_ProjArray = [];
	
	psi_maxTags = 5;
	
	psi_SearchRes = 0.3;	//How often to update the psi_ObjArray with valid objects
	psi_SearchTimer = 0;
	
	psi_TagFunc = psi_FindPlaceHolder;
	psi_PrimeObj = undefined;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"PsiObj");
	
	psi_ActivateAbility = psi_CreateProjectile;
	
	func_PreUpdateStep = PsiPreUpdateStep;
	func_UpdateStep = PsiUpdateStep;
	//func_InitAbility = InitialiseWeapon;	
	func_DrawStep = psi_DrawPsiOutline;
	psi_HoldType = PSI_HOLDTYPE.FOLLOW_BACK;
}

function abl_PsiTagObjects() : base_Psionic() constructor
{
	
	staminaCost = 10;
	baseStaminaCost = staminaCost;
	psiDist = 360;
	
	cooldown = 0.3;
	
	psi_ObjArray = [];
	psi_ProjArray = [];
	
	psi_SearchRes = 0.3;	//How often to update the psi_ObjArray with valid objects
	psi_SearchTimer = 0;
	
	psi_TagFunc = psi_FindPlaceHolder;
	psi_PrimeObj = undefined;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"PsiObj");
	
	psi_ActivateAbility = psi_TagNearestObject;
	
	func_PreUpdateStep = PsiPreUpdateStep;
	func_UpdateStep = PsiUpdateStep;
	//func_InitAbility = InitialiseWeapon;	
	func_DrawStep = psi_DrawPsiOutline;
	psi_HoldType = PSI_HOLDTYPE.FOLLOW_FRONT;
}

function abl_PsiTagRadius() : base_Psionic() constructor
{
	abl_sfx = sfx_psi_On;
	staminaCost = 10;
	baseStaminaCost = staminaCost;
	psiDist = 360;
	
	cooldown = 20;
	
	psi_ObjArray = [];
	psi_ProjArray = [];
	
	psi_SearchRes = 0.3;	//How often to update the psi_ObjArray with valid objects
	psi_SearchTimer = 0;
	
	psi_TagFunc = psi_FindPlaceHolder;
	psi_PrimeObj = undefined;
	
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"PsiObj");
	
	psi_ActivateAbility = psi_TagRadius;
	
	func_PreUpdateStep = PsiPreUpdateStep;
	func_UpdateStep = PsiUpdateStep;
	//func_InitAbility = InitialiseWeapon;	
	func_DrawStep = psi_DrawPsiOutline;
	psi_HoldType = PSI_HOLDTYPE.NONE;
}