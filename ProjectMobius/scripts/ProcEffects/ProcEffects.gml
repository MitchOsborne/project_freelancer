// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Proc_HealDmg_1(a_report)
{
	owner = a_report.hit_Owner;
	dmg = a_report.hit_Dmg;
	
	owner.hp += dmg*0.1;
}

function Proc_Healchar_maxHP_25(a_report)
{
	owner = a_report.hit_Owner;
	dmg = a_report.hit_Dmg;
	
	owner.hp += owner.char_maxHP * 0.25;
}

function Proc_RefreshStamina(a_report)
{
	owner = a_report.hit_Owner;
	dmg = a_report.hit_Dmg;
	
	owner.staminaStat = owner.maxStamina;
}

function Proc_Reload(a_report)
{
	weapon = a_report.hit_Weapon;
	weapon.ammoCount = ammoMax;
}

function Proc_RefundShot(a_report)
{
	weapon = a_report.hit_Weapon;
	weapon.ammoCount += 1;
}

function Proc_BulletBurst(a_report)
{
	with a_report
	{
		for(var i = 0; i < 12; i += 1)
		{
			var dmg = hit_Owner.char_damageStat * hit_Weapon.damageMultiplier;
			var tRot = lerp(0,360,(i/12));
			var tInst = CreateProjectile(proj_Bullet, hit_Weapon, dmg, global.dm, new Vector2(hit_Subject.x, hit_Subject.y),tRot, 1); 
			tInst.lifeTime = 0.5;
		}
	}
}

function Proc_ExplosiveBurst(a_report)
{
	with a_report
	{
		var dmg = hit_Owner.char_damageStat * hit_Weapon.damageMultiplier;
		var tRot = irandom_range(0,360);
		var tInst = CreateProjectile(tExplosion, hit_Weapon, hit_Dmg*5, new Vector2(hit_Subject.x, hit_Subject.y),tRot, 1); 
		tInst.lifeTime = 1;
	}
}


function Proc_GravitonBurst(a_report)
{
	with a_report
	{
		var dmg = hit_Owner.char_damageStat * hit_Weapon.damageMultiplier;
		var tRot = irandom_range(0,360);
		var tInst = CreateProjectile(tExplosion, hit_Weapon, hit_Dmg*2, new Vector2(hit_Subject.x, hit_Subject.y),tRot, 1); 
		tInst.lifeTime = 1;
	}
}

function Proc_GravitonBolt(a_report)
{
	with a_report
	{
		for(var i = 0; i < 4; i += 1)
		{
			var dmg = hit_Owner.char_damageStat * hit_Weapon.damageMultiplier;
			var tRot = lerp(0,360,(i/4));
			var tInst = CreateProjectile(proj_GravitonBolt, hit_Weapon, dmg * 0.8, new Vector2(hit_Subject.x, hit_Subject.y),tRot, 1); 
			tInst.lifeTime = 2;
		}
	}
}