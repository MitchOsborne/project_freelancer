// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function base_Evade(a_bp) : base_Ability(a_bp) constructor
{
	ai_Score = 50;	//So that is isn't sharing priority with offensive abilities
	array_push(abl_Components, new abl_comp_OnPress(self));
	array_push(abl_Components, new abl_comp_ReqRelease(self));
	array_push(abl_Components, new abl_comp_Audio(self));
	//array_push(abl_Components, new abl_comp_AIKeepClose(self));
	array_push(abl_Components, new abl_comp_AICharges(self));
}