// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Internal_bp_InitLoadouts()
{
	/*with global.enmLD
	{
		Doggo = new Loadout();
		Bat = new Loadout();
		Boomer = new Loadout();
		Goomer = new Loadout();
		Neko_M = new Loadout();
		Neko_RL = new Loadout();
		Neko_RS = new Loadout();
		Nezumi_M = new Loadout();
		Nezumi_R = new Loadout();
		Kitsune_M = new Loadout();
		Kitsune_R = new Loadout();
	}
	
	
	global.enmLD.Doggo.ld_wep1 = global.bp_Abilities.bp_LungeBite;
	global.enmLD.Bat.ld_wep1 = global.bp_Abilities.bp_LungeBite;

	global.enmLD.Neko_M.ld_wep1 = global.bp_Abilities.bp_Shortsword;
	global.enmLD.Neko_RL.ld_wep1 = global.bp_Abilities.bp_Mosin;
	global.enmLD.Neko_RS.ld_wep1 = global.bp_Abilities.bp_DBSG;

	global.enmLD.Nezumi_M.ld_wep1 = global.bp_Abilities.bp_NezumiBlade;
	global.enmLD.Nezumi_M.ld_wep2 = global.bp_Abilities.bp_M9T;

	global.enmLD.Kitsune_M.ld_wep1 = global.bp_Abilities.bp_Katana;
	global.enmLD.Kitsune_M.ld_util1 = global.bp_Abilities.bp_Evade;

	global.enmLD.Kitsune_R.ld_wep2 = global.bp_Abilities.bp_Soulfire;	
	*/
}

function Internal_Init_CharData()
{
	/*
	with global.EnmData
	{
		Shepherd = new CharData("Shepherd", SPAWN_POOL.None, anim_Shepherd, global.enmLD.Nezumi_M);
		Kamui = new CharData("Kamui", SPAWN_POOL.None, anim_Kamui, global.enmLD.Kitsune_M);
	
		Doggo = new CharData("Wolf", SPAWN_POOL.None, anim_Doggo, global.enmLD.Doggo);
		Bat = new CharData("Bat", SPAWN_POOL.None, anim_Bat, global.enmLD.Bat);
		Boomer = new CharData("Boomer", SPAWN_POOL.None, anim_Boomer, global.enmLD.Doggo);
		Goomer = new CharData("Goomer", SPAWN_POOL.None, anim_Goomer, global.enmLD.Doggo);
		Neko_M = new CharData("Neko Swordsman", SPAWN_POOL.Neko_M, anim_YakiHat, global.enmLD.Neko_M);
		Neko_R = new CharData("Neko Gunner", SPAWN_POOL.Neko_R, anim_YakiHood, global.enmLD.Neko_RL);
		Neko_RS = new CharData("Neko Gunner", SPAWN_POOL.Neko_R, anim_YakiHood, global.enmLD.Neko_RS);
		Nezumi_M = new CharData("Nezumi Rogue", SPAWN_POOL.Nezumi_M, anim_NezumiRogue, global.enmLD.Nezumi_M);
		Nezumi_R = new CharData("Nezumi Engineer", SPAWN_POOL.Nezumi_R, anim_NezumiRogue, global.enmLD.Nezumi_M);
		Kitsune_M = new CharData("Kitsune Samurai", SPAWN_POOL.Kitsune_M, anim_KitsuneSamurai, global.enmLD.Kitsune_M);
		Kitsune_R = new CharData("Kitsune Oracle", SPAWN_POOL.Kitsune_R, anim_KitsuneOracle, global.enmLD.Kitsune_R);

	}
	*/
}


function Internal_bp_Init_Blueprints()
{
	Internal_bp_Init_Classes();
	Internal_bp_Init_Ability();
	Internal_bp_Init_SelfBuffs()
	Internal_bp_Init_Evade();
	Internal_bp_Init_Rifle();
	Internal_bp_Init_Shotgun();
	Internal_bp_Init_Sidearm();
	Internal_bp_Init_Melee();
	
	Internal_bp_InitLoadouts();
	Internal_Init_CharData();
}


