function CreateProjectile(a_proj, a_wepID, a_dmg, a_pos, a_rot, a_size) {

var proj = a_proj;
	if(a_wepID.abl_AbilityType != ABILITY_TYPE.MELEE && a_wepID.teamID == TEAMID.Enemy)
	{
		proj = tEnmBullet;			
	}
	
	var tInst = instance_create_layer(a_pos.x,a_pos.y,"Projectiles",proj);
	tInst.direction = a_rot + 90;
	var tVec2 = GetCoord(tInst.direction - 90);
	tInst.mx = tVec2.x;
	tInst.my = tVec2.y;
	tInst.image_angle = tInst.direction;
	tInst.teamID = a_wepID.teamID;
	tInst.dmg_speedCurve = a_wepID.abl_bpStats.bp_SpeedCurve;
	tInst.weapon = a_wepID;
	tInst.owner = a_wepID.owner;
	//tInst.speedMod = 1;
	tInst.damage = a_dmg;
	tInst.range = a_wepID.abl_bpStats.bp_Range;
	//tInst.moveSpeed = tInst.baseSpeed * tInst.speedMod;
	tInst.forceStrength = a_wepID.abl_forceStrength;
	tInst.stunDur = a_wepID.abl_stunDur;
	tInst.forceDur = 0.1;
	tInst.image_xscale *= a_size;
	tInst.image_yscale *= a_size;
	
	
	if(tInst.teamID == TEAMID.Ally)
	{
		ds_list_add(global.allyProjArr,tInst);
	}
	else if(tInst.teamID == TEAMID.Enemy)
	{
		array_push(enmProjArr, tInst);
	}
	
	for(var i = 0; i < array_length(tInst.dmg_Components); i += 1)
	{
		var tComp = tInst.dmg_Components[i];
		tComp.dmg_comp_Init();
	}

	return tInst;
}

function CreateLiteProjectile(a_proj, a_wepID, a_dmg, a_pos, a_rot, a_stats) {

	var tInst = new a_proj();
	
	tInst.x = a_pos.x;
	tInst.y = a_pos.y;
	tInst.direction = a_rot + 90;
	var tVec2 = GetCoord(tInst.direction - 90);
	tInst.mx = tVec2.x;
	tInst.my = tVec2.y;
	tInst.image_angle = tInst.direction;
	tInst.teamID = a_wepID.teamID;
	tInst.dmg_speedCurve = a_wepID.abl_bpStats.bp_SpeedCurve;
	tInst.weapon = a_wepID;
	tInst.owner = a_wepID.owner;
	//tInst.speedMod = 1;
	tInst.damage = a_dmg;
	tInst.range = a_wepID.abl_bpStats.bp_Range;
	//tInst.moveSpeed = tInst.baseSpeed * tInst.speedMod;
	tInst.forceStrength = a_wepID.abl_forceStrength;
	tInst.stunDur = a_wepID.abl_stunDur;
	tInst.forceDur = 0.1;
	
	if(a_stats != undefined)
	{
		tInst.dmg_speedCurve = a_stats.bp_SpeedCurve;
		tInst.dmg_maxBounces = a_stats.bp_ProjectileBounces;
		tInst.hp = a_stats.bp_ProjectileHP;
		tInst.image_xscale *= a_stats.bp_ProjectileSize;
		tInst.image_yscale *= a_stats.bp_ProjectileSize;
	}
	
	if(tInst.dmg_isCore) array_push(global.coreProjArr, tInst);
	if(tInst.dmg_hitOthers) array_push(global.counterProjArr, tInst);
	
	if(tInst.teamID == TEAMID.Ally)
	{
		array_push(global.allyProjArr,tInst);
	}
	else if(tInst.teamID == TEAMID.Enemy)
	{
		array_push(global.enmProjArr, tInst);
	}
	
	for(var i = 0; i < array_length(tInst.dmg_Components); i += 1)
	{
		var tComp = tInst.dmg_Components[i];
		tComp.dmg_comp_Init();
	}

	return tInst;
}

function AssignProjomotor(a_proj)
{
	if(CheckExistance(global.projomotor) == false) global.projomotor = instance_create_layer(0,0,"debug",base_Projomotor);
	with a_proj
	{
		global.projomotor.slavedObj = a_proj;
		global.projomotor.sprite_index = dmg_sprite;	
		global.projomotor.direction = direction;
		global.projomotor.image_angle = direction;
		global.projomotor.x = x;
		global.projomotor.y = y;	
	}
}

function AssignAltProjomotor(a_proj)
{
	if(CheckExistance(global.alt_projomotor) == false) global.alt_projomotor = instance_create_layer(0,0,"debug",base_Projomotor);
	with a_proj
	{
		global.projomotor.slavedObj = a_proj;
		global.alt_projomotor.sprite_index = dmg_sprite;	
		global.alt_projomotor.direction = direction;
		global.alt_projomotor.image_angle = direction;
		global.alt_projomotor.x = x;
		global.alt_projomotor.y = y;	
	}
}

function ProjoCollideCheck(a_projA, a_projB)
{
	AssignProjomotor(a_projA);
	AssignAltProjomotor(a_projB);
	
	if(place_meeting(global.projomotor.x, global.projomotor.y, global.alt_projomotor))
	{
		//Insret Melee-Proj onhit logic
		a_projB.hp = 0;
	}
}

function InitProjGrid()
{
	array_resize(global.projGrid, ceil(room_width/projGridSize));
	for(var i = 0; i < array_length(global.projGrid); i += 1)
	{
		if(global.projGrid[i] == 0) global.projGrid[i] = [];
		array_resize(global.projGrid[i], ceil(room_height/projGridSize));
		for(var n = 0; n < array_length(global.projGrid[i]); n += 1)
		{
			if(global.projGrid[i][n] == 0) global.projGrid[i][n] = [];	
		}
	}	
}

function ClearProjGrid()
{
	for(var ix = 0; ix < array_length(global.projGrid); ix += 1)
	{
		for(var iy = 0; iy < array_length(global.projGrid[ix]); iy += 1)
		{
			if(array_length(global.projGrid[ix][iy]) > 0) global.projGrid[ix][iy] = [];
		}
	}
}

function PushProjToGrid(a_proj, a_gridPos)
{
	for(var ix = -1; ix <= 1; ix += 1)
	{
		for(var iy = -1; iy <= 1; iy += 1)
		{
			var tx = a_gridPos.x + ix;
			var tLen = array_length(global.projGrid);
			if(tx > 0 && tx < array_length(global.projGrid))
			{
				var ty = a_gridPos.y + iy;
				if(ty > 0 && ty < array_length(global.projGrid[tx]))	
				{
					array_push(global.projGrid[tx][ty], a_proj);	
				}
			}
		}
	}
}

function PullProjFromGrid(a_gridPos)
{
	var retArr = [];
	for(var ix = -1; ix <= 1; ix += 1)
	{
		for(var iy = -1; iy <= 1; iy += 1)
		{
			var tx = a_gridPos.x + ix;
			if(tx > 0 && tx < array_length(global.projGrid))
			{
				var ty = a_gridPos.y + iy;
				if(ty > 0 && ty < array_length(global.projGrid[tx]))	
				{
					for(var i = 0; i < array_length(global.projGrid[tx][ty]); i += 1)
					{
						var tProj = global.projGrid[tx][ty][i];
						array_push(retArr, tProj);
					}
				}
			}
		}
	}
	return retArr;
}

function ProjTeamCheck(a_proj, a_other)
{
	if(a_proj.teamID != a_other.teamID || a_proj.friendlyFire || a_proj.friendlyOnly) return true	
	else return false;
	
}

function DesyncUpdateCluster(proj)
{
	/*
	with proj
	{
		
		if(CheckExistance(leadProjectile))
		{
			if(leadProjectile.leadProjectile != leadProjectile)
			{
				leadProjectile = undefined;	
			}else if(point_distance(x,y,leadProjectile.x,leadProjectile.y) > global.pm_projClusterDist)
			{
				leadProjectile = undefined;	
			}
		}
		if(CheckExistance(leadProjectile) == false)
		{
			if(	(teamID == TEAMID.Ally &&	ds_list_find_index(global.l_allyProjArr,id) == -1) ||
				(teamID == TEAMID.Enemy &&	ds_list_find_index(global.l_enmProjArr, id) == -1))
			{
				var l_projList;
				var hasLead = false;
				if(teamID == TEAMID.Ally) l_projList = global.l_allyProjArr;
				else l_projList = global.l_enmProjArr;
				
				for(var l = 0; l < ds_list_size(l_projList); l += 1)
				{
					var tLead = l_projList[|l];
					var tDist = point_distance(x,y,tLead.x, tLead.y);
					if(tDist < global.pm_projClusterDist)
					{
						if(CheckExistance(leadProjectile))
						{
							if(point_distance(x,y, leadProjectile.x, leadProjectile.y) > tDist)
							{
								leadProjectile = tLead;
								hasLead = true;
							}
						}else
						{
							leadProjectile = tLead;
							hasLead = true;
						}
					}
				}
				
				
				if(hasLead == false)
				{
					
					if(weapon != undefined)
					{
						var projList = weapon.wepProjList;
					
						for(var p = 0; p < ds_list_size(projList); p += 1)
						{
							var tProj = projList[|p];
							if(CheckExistance(tProj))
							{
								if(point_distance(x,y,tProj.x,tProj.y) < global.pm_projClusterDist)
								{
									if(tProj.leadProjectile == undefined)
									{
										tProj.leadProjectile = id;
										leadProjectile = id;
									}
								}
							}
						}
				
						if(leadProjectile == id)
						{
							if(teamID == TEAMID.Ally) ds_list_add(global.l_allyProjArr, id);
							else if (teamID == TEAMID.Enemy) ds_list_add(global.l_enmProjArr, id);	
						}
					}
				}
			}
		}
	}
	*/
}
