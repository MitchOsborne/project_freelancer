// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function CreateMenu(a_pos, a_menu, a_caller){
	if(!layer_exists("Menu")) layer_create(-1000, "Menu");
	global.menuUser = a_caller;
	
	var tInst = instance_create_layer(a_pos.x,a_pos.y,"Menu", a_menu);
	array_push(global.ui_MenuStack, tInst);
	return tInst;
}