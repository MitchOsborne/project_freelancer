// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Internal_bp_Init_Rifle()
{
	
	with global.bp_Abilities.bp_BaseRifleStats
	{
		bp_Range = 720; 
		bp_Spread = 5; 
		bp_DMGScale = 0.75;
		bp_RPM = 360;
		bp_Ammo = 12;
		bp_ReloadSpd = 1.8;
		bp_AutoReload = false;
		bp_EquipTime = 0.7;
		bp_SpeedCurve = "Rifle"
		bp_WeightBonus = 0.8;
	}


	with global.bp_Abilities.bp_RK
	{
		bp_Name = "RK-47";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseRifleStats);
		
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	}


	with global.bp_Abilities.bp_Straya88
	{
		bp_Name = "Straya-88";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseRifleStats);
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		with bp_WepStats
		{
			bp_Spread = 4; 
			bp_RPM = 450;
			bp_ReloadSpd = 1.7;
		}
	
		with bp_StatMods
		{
			bp_Range = 0.8; 
			bp_Spread = 1;
			bp_DMGScale = 0.8; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	}
	
		with global.bp_Abilities.bp_DomR
	{
		bp_Name = "Dominator-R";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_DMGType = DMG_TYPE.SOFT;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseRifleStats);
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Spread = 1; 
			bp_RPM = 280;
			bp_ReloadSpd = 2;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_DomR;
		bp_SFX = sfx_saber2;
	}



	with global.bp_Abilities.bp_RKL {
	
	
		bp_Name = "RK-47 Light";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = global.bp_Abilities.bp_RK.bp_WepStats;
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 0.8;
			bp_Spread = 2; 
			bp_DMGScale = 0.9;
			bp_RPM = 1.1;
			bp_Ammo = 1;
			bp_ReloadSpd = 0.8;
			bp_EquipTime = 0.9;
		}
		//bp_Components : [new abl_comp_Ammo(self, 31, 1, 1.5, 31)],

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	
	
	}


	with global.bp_Abilities.bp_RKH {
	
	
		bp_Name = "RK-47 global.statArch_Heavy";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = global.bp_Abilities.bp_RK.bp_WepStats;
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 1.2; 
			bp_Spread = 0.8;
			bp_DMGScale = 1.25; 
			bp_RPM = 0.8;
			bp_Ammo = 0.8;
			bp_ReloadSpd = 1.2;
			bp_EquipTime = 1.2;
		}
		//bp_Components : [new abl_comp_Ammo(self, 31, 1, 1.5, 31)],

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot2;
	
	
	}


	with global.bp_Abilities.bp_RKR {
	
	
		bp_Name = "RK-47 Rapid";
		bp_Frame = base_wep_Rifle;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_WepStats = global.bp_Abilities.bp_RK.bp_WepStats;
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 0.8; 
			bp_Spread = 3; 
			bp_DMGScale = 0.85;
			bp_RPM = 1.3;
			bp_Ammo = 0.8;
			bp_ReloadSpd = 0.7;
			bp_EquipTime = 0.9;
		}
		//bp_Components : [new abl_comp_Ammo(self, 31, 1, 1.5, 31)],

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	
	}

	with global.bp_Abilities.bp_Mosin {
	
	
		bp_Name = "Mosin";
		bp_Frame = base_wep_Sniper;
		bp_Slot = LOADOUT_SLOT.CORE;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseRifleStats);
	
		bp_AttackArr = [atk_SniperBullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Range = 960; 
			bp_Spread = 5; 
			bp_DMGScale = 8;
			bp_RPM = 120;
			bp_Ammo = 1;
			bp_ReloadSpd = 2;
			bp_EquipTime = 1.2
			bp_SpeedCurve = "Sniper";
			bp_ProjectileHP = 5;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 1;
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		//bp_Components : [new abl_comp_Ammo(self, 31, 1, 1.5, 31)],

		bp_SpriteSet = sprSet_Mosin;
		bp_SFX = sfx_shoot2;
	
	}

	with global.bp_Abilities.bp_TheEnd {
	
	
		bp_Name = "The End";
		bp_Frame = base_wep_Tranq;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseRifleStats);
	
		bp_AttackArr = [atk_SniperBullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Range = 960; 
			bp_Spread = 5; 
			bp_DMGScale = 6;
			bp_RPM = 120;
			bp_Ammo = 1;
			bp_ReloadSpd = 2;
			bp_EquipTime = 1.2
			bp_SpeedCurve = "Sniper";
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1; 
			bp_DMGScale = 1;
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		//bp_Components : [new abl_comp_Ammo(self, 31, 1, 1.5, 31)],

		bp_SpriteSet = sprSet_Mosin;
		bp_SFX = sfx_shoot2;
	
	}





	with global.bp_Abilities.bp_DragnetRifle
	{
		bp_Name = "Dragnet Rifle";
		bp_Frame = base_wep_DragnetRifle;
		bp_Slot = LOADOUT_SLOT.ABILITY1;
		bp_AbilityType = ABILITY_TYPE.ABILITY;
	
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Ammo = 1;
			bp_Range = 240; 
			bp_Spread = 6; 
			bp_DMGScale = 0;
			bp_RPM = 60;
			bp_ReloadSpd = 3;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 0; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	}


	with global.bp_Abilities.bp_HarpoonRifle
	{
		bp_Name = "Harpoon Rifle";
		bp_Frame = base_wep_HarpoonRifle;
		bp_Slot = LOADOUT_SLOT.ABILITY1;
		bp_AbilityType = ABILITY_TYPE.ABILITY;
	
		bp_AttackArr = [atk_Bullet];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_Ammo = 1;
			bp_Range = 480; 
			bp_Spread = 0; 
			bp_DMGScale = 4;
			bp_RPM = 60;
			bp_ReloadSpd = 2;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}

		bp_SpriteSet = sprSet_RKBase;
		bp_SFX = sfx_shoot1;
	}
}