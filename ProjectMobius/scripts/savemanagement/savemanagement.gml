// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SaveGame(){
	
	var outArr = [];
	
	var tArr = sv_SaveCharSelect();
	array_append(outArr, tArr);
	tArr = sv_SaveWepUnlocks();
	array_append(outArr, tArr);
	tArr = sv_SaveStatInv();
	array_append(outArr, tArr);
	tArr = sv_SaveLoadouts();
	array_append(outArr, tArr);
	
	var tStr = json_stringify(outArr);
	
	var tBuffer = buffer_create(string_byte_length(tStr) + 1, buffer_fixed, 1);
	buffer_write(tBuffer, buffer_string, tStr);
	buffer_save(tBuffer, "tSave.save");
}

function LoadGame()
{
	if(file_exists("tSave.save"))
	{
		var tBuffer = buffer_load("tSave.save");
		var tStr = buffer_read(tBuffer, buffer_string);
		buffer_delete(tBuffer);
		
		var tImport = json_parse(tStr);
		
		for(var i = 0; i < array_length(tImport); i += 1)
		{
			var tData = tImport[i];
			if(tData == "CharSelect")
			{
				//Get the following data until *end* is found
				for(var n = i + 1; n < array_length(tImport); n += 1)
				{
					var tNode = tImport[n];
					if(tNode != "*end*")
					{
						global.sv_Player1Char = tNode.sv_char1;
						global.sv_PlayerLvl = tNode.sv_charLvl;
					}else
					{
						i = n;
						break;
					}
				}
			}else if(tData == "WepUnlocks")
			{
				for(var n = i + 1; n < array_length(tImport); n += 1)
				{
					var tNode = tImport[n];
					if(tNode != "*end*")
					{
						variable_struct_get(global.ablArchUnlocks, tNode.sv_archName).arch_Level = tNode.sv_archLvl;
					}else
					{
						i = n;
						break;
					}
				}
			}else if (tData == "StatMods")
			{
				array_clear(global.sv_StatInv);
				for(var n = i + 1; n < array_length(tImport); n += 1)
				{
					var tNode = tImport[n];
					if(tNode != "*end*")
					{
						with tNode
						{
							var tMod = new Stat_EmptySlot();
							tMod.mainStat = sv_mainStat;
							tMod.altStat = sv_altStat;
							Init_StatSlot(tMod);
							Stat_SetSlotStats(tMod, sv_dmgStat, sv_spdStat, sv_hpStat, sv_shdStat, 
												sv_durStat, sv_cdStat, sv_critStat, sv_procStat);
							tMod.stat_currLvl = tNode.sv_modLvl;
							array_push(global.sv_StatInv, tMod);
						}
						
					}else
					{
						i = n;
						break;
					}
				}	
			}else if(tData == "Loadouts")
			{
				for(var n = i + 1; n < array_length(tImport); n += 1)
				{
					var tNode = tImport[n];
					if(tNode != "*end*")
					{
						if(tNode == "Player1Loadout")
						{
							tNode = tImport[n+1];
							sv_ImportLoadout(global.sv_Player1Data, tNode);	
							n += 1
						}else if (tNode == "Player2Loadout")
						{
							tNode = tImport[n+1];
							sv_ImportLoadout(global.sv_Player2Data, tNode);
							n += 1
						}
						
					}else
					{
						i = n;
						break;
					}
				}	
			}
		}
	}
}

function sv_SaveCharSelect()
{
	var outArray = ["CharSelect"];
	var tData = new sv_CharacterData(global.Player1Char.object_index,  global.Player1ID.powerLvl);
	array_push(outArray, tData);
	array_push(outArray, "*end*");
	return outArray;
}

function sv_LoadCharSelect(a_Str)
{
		
}

function sv_SaveWepUnlocks()
{
	var outArray = ["WepUnlocks"];
	array_push(outArray, new sv_WepUnlockData("RKRifle", global.ablArchUnlocks.RKRifle.arch_Level));
	array_push(outArray, new sv_WepUnlockData("RKLight", global.ablArchUnlocks.RKLight.arch_Level));
	array_push(outArray, new sv_WepUnlockData("Swords", global.ablArchUnlocks.Swords.arch_Level));
	array_push(outArray, new sv_WepUnlockData("Evades", global.ablArchUnlocks.Evades.arch_Level));
	array_push(outArray, "*end*");
	return outArray;
}

function sv_SaveStatInv()
{
	var outArray = ["StatMods"];
	for (var i = 0; i < array_length(global.inv_StatMods); i += 1)
	{
		var tMod = global.inv_StatMods[i];
		with tMod
		{
			var tData = new sv_StatModData
			(
				tMod.mainStat,
				tMod.altStat,
				tMod.stat_currLvl,
				tMod.stat_DMGMod,
				tMod.stat_SPDMod,
				tMod.stat_HPMod,
				tMod.stat_SHDMod,
				tMod.stat_DURMod,
				tMod.stat_CDMod,
				tMod.stat_CRITMod,
				tMod.stat_PROCMod,
			)		
			array_push(outArray, tData);
		}
		
	}
	array_push(outArray, "*end*");
	return outArray;
}

function sv_SaveLoadouts()
{
	var outArray = ["Loadouts"];
	with global.Player1Char
	{
		array_push(outArray,"Player1Loadout");
		var tData = new sv_LoadoutData(global.Player1Char.object_index, char_AC.ac_bpLd.ld_slots.ld_wep1, char_AC.ac_bpLd.ld_slots.ld_wep2, char_AC.ac_bpLd.ld_slots.ld_util1, char_AC.ac_bpLd.ld_slots.ld_util2, char_AC.ac_bpLd.ld_slots.ld_abl1, char_AC.ac_bpLd.ld_slots.ld_abl2, char_AC.ac_bpLd.ld_slots.ld_core);
		array_push(outArray, tData);	
	}
	
	array_push(outArray, "*end*");
	return outArray;
}
function sv_LoadoutData(a_char, a_wep1, a_wep2, a_util1, a_util2, a_abl1, a_abl2, a_core) constructor
{
	sv_charID = a_char;
	sv_wep1 = a_wep1.bp_Name;
	sv_wep2 = a_wep2.bp_Name;
	sv_util1 = a_util1.bp_Name;
	sv_util2 = a_util2.bp_Name;
	sv_abl1 = a_abl1.bp_Name;
	sv_abl2 = a_abl2.bp_Name;
	sv_core = a_core.bp_Name;
}

function sv_CharacterData(a_char1, a_charLvl) constructor
{
	sv_char1 = a_char1;
	sv_char2 = a_char2;
	sv_charLvl = a_charLvl;
}

function sv_StatModData(a_mainStat, a_altStat, a_lvl, a_dmg, a_spd, a_hp, a_shd, a_dur, a_cd, a_crit, a_proc) constructor
{
	sv_mainStat = a_mainStat;
	sv_altStat = a_altStat;
	sv_modLvl = a_lvl;
	sv_dmgStat = a_dmg;
	sv_spdStat = a_spd;
	sv_hpStat = a_hp;
	sv_shdStat = a_shd;
	sv_durStat = a_dur;
	sv_cdStat = a_cd;
	sv_critStat = a_crit;
	sv_procStat = a_proc;
}

function sv_WepUnlockData(a_name, a_lvl) constructor
{
	sv_archName = a_name;
	sv_archLvl = a_lvl;
}

function sv_ImportLoadout(a_player, a_node)
{
	with a_player
	{
		a_player.sv_charID = a_node.sv_charID;
		a_player.sv_wep1 = a_node.sv_wep1;
		a_player.sv_wep2 = a_node.sv_wep2;
		a_player.sv_util1 = a_node.sv_util1;
		a_player.sv_util2 = a_node.sv_util2;
		a_player.sv_abl1 = a_node.sv_abl1;
		a_player.sv_abl2 = a_node.sv_abl2;
		a_player.sv_core = a_node.sv_core;
	}
}

function sv_GetBlueprintFromName(a_str)
{
	for(var i = 0; i < array_length(global.bp_BlueprintArr); i += 1)
	{
		var tBP = global.bp_BlueprintArr[i];
		if(tBP.bp_Name == a_str) return tBP;
	}
	return undefined;
}

function sv_UpdateLoadout(a_P1, a_Data)
{
	with a_P1
	{
		char_AC.ac_bpLd.ld_slots.ld_wep1 = sv_GetBlueprintFromName(a_Data.sv_wep1);
		char_AC.ac_bpLd.ld_slots.ld_wep2 = sv_GetBlueprintFromName(a_Data.sv_wep2);
		char_AC.ac_bpLd.ld_slots.ld_util1 = sv_GetBlueprintFromName(a_Data.sv_util1);
		char_AC.ac_bpLd.ld_slots.ld_util2 = sv_GetBlueprintFromName(a_Data.sv_util2);
		char_AC.ac_bpLd.ld_slots.ld_abl1 = sv_GetBlueprintFromName(a_Data.sv_abl1);
		char_AC.ac_bpLd.ld_slots.ld_abl2 = sv_GetBlueprintFromName(a_Data.sv_abl2);
		char_AC.ac_bpLd.ld_slots.ld_core = sv_GetBlueprintFromName(a_Data.sv_core);
	}
}

