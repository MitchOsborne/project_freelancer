// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


function defunc_base_Weapon() : base_Ability() constructor {
	
	AbilityType = ABILITY_TYPE.RANGED;
	wep_parryType = PARRY_TYPE.NONE;
	wep_SlotType = SLOT_TYPE.GUN;
	animWep = 0;
	ablName = "base_Weapon";
	ablCost = 100;
	
	abl_Icon = spr_Weapon;
	
	abl_sfx = sfx_shoot1;
	
	weaponSprite = spr_error;
	animIndex = 0;
	animTimer = 0;
	animCtr = 1;
	animType = 0;
	

	damageMultiplier = 1;

	projectileList = ds_list_create();
	projIndex = 0;
	resetIndexOnReload = true;

	

	
	RPM = 0;
	fireRate = 0;

	range = 60;


	wep_projectileCount = 0;
	wep_projectileSpread = 0;
	wep_randomSpread = false;
	
	
	projSizeScale = 1;

	speedMod = 1;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Default");

	ai_engageRange = range;
	ai_engagePriority = 10;


	wepProjList = ds_list_create();
	wepNodeList = ds_list_create();


	func_PreUpdateStep = WeaponPreUpdateStep;
	func_UpdateStep = WeaponUpdateStep;
	func_EndUpdateStep = WeaponEndUpdateStep;
	func_InitAbility = InitialiseWeapon;
	func_DrawStep = DrawWeapon;
}


function InitialiseWeapon(a_weapon)
{
	with a_weapon
	{
		ammo = ammoMax;
		if(reloadAmount == 0) reloadAmount = ammoMax;
		animType = sprite_get_speed_type(weaponSprite);
		animCtr = sprite_get_speed(weaponSprite);
	}
}

function WeaponPreUpdateStep(a_weapon)
{
	with a_weapon
	{
		AbilityPreUpdateStep(a_weapon);
		//ai_engageRange = range*0.9;
		
		if(ammoCount == 0) abl_isDepleted = true;		
		resourceDisplay = ammoCount;
		cdDisplay = reloadTime - reloadTimer;
		animCtr = sprite_get_speed(weaponSprite);


		ReloadWeaponCheck(self);

		// Stops the index from moving outside the bounds of the projectile list
		if(projIndex >= ds_list_size(projectileList) || projIndex < 0)
		{
			projIndex = 0;
		}
	}
	
}

function WeaponUpdateStep(a_weapon)
{
	with a_weapon
	{
		if (activate)
		{
			FireWeapon(self);
		}
		AbilityUpdateStep(self);
		
	}
	
	
}

function WeaponEndUpdateStep(a_weapon)
{
	with a_weapon
	{
		AbilityEndUpdateStep(self);
		
		
	}
}

function ReloadWeaponCheck(a_weapon)
{
	with a_weapon
	{
		if(abl_canReload && ammoCount < ammoMax)
		{
			if(in_Released || autoReload || ammoCount <= 0)
			{
				if(teamID == TEAMID.Enemy)
				{
					reloadTimer += GetDeltaTime(timeScale);
				}else
				{
					reloadTimer += GetDeltaTime(timeScale);	
				}
			}else
			{
				reloadTimer = 0;	
			}
	
			if(reloadTimer > reloadTime)
			{
				if(resetIndexOnReload || ammoCount == 0)
				{
					projIndex -= reloadAmount;
					sfxIndex -= reloadAmount;
				}
				activate = false;
			}
			if(projIndex < 0) projIndex = 0;
		}
	}
}

function FireWeapon(a_weapon)
{
	with a_weapon
	{
		if(activate)
		{
			var tRot = 0;
			if(fireRateTimer <= 0 && ammoCount > 0)
			{
				didActivate = true;
				
				fireRateTimer = fireRate;
				
					var tPos = new Vector2(owner.char_offset.x + abl_posOffset.x, owner.char_offset.y + abl_posOffset.y);
			
					tInst = CreateProjectile(projectileList[| projIndex],self,owner.char_damageStat * damageMultiplier ,tPos,tRot, projSizeScale);
					tInst.weapon = self;
					projIndex += 1;
					listSize = ds_list_size(projectileList);
					projIndex = clamp(projIndex,0,listSize-1);
					ds_list_add(wepProjList,tInst);
				
				
			}
		}
	}
}


function DrawWeapon(a_weapon)
{
	with a_weapon
	{
		var flip = 1;
		if(owner.direction <= 180) flip = -1;
		flip *= 0.75;
		
		
		
		if(weaponSprite != undefined) draw_sprite_ext(weaponSprite,animIndex,owner.char_offset.x + abl_posOffset.x,owner.char_offset.y + abl_posOffset.y,0.75,flip,owner.direction + 90, c_white, 1);
		else draw_sprite(spr_error,0,owner.x, owner.y);
	}
}