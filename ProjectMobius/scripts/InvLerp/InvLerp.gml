// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function InvLerp(a, b, v)	// Gets the Decimal of two numbers and their Lerp'd Value (E.G: A=0, B=2, V=1, LerpVal=0.5)
{
	var t = (v - a) / (a - b);
	return abs(t);
}

function Round_Ext (a_Val, a_Dec)
{
	if(a_Val != 0)
	{
		return round(a_Val / a_Dec) * a_Dec;
	}else return 0;
}