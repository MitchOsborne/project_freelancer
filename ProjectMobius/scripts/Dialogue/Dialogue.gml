// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DialogueLine(a_spr, a_txt) constructor
{
	dlg_face = spr_None;
	dlg_text = "Text Goes Here";
	if(a_spr != undefined) dlg_face = a_spr;
	if(a_txt != undefined) dlg_text = a_txt;
}

function InitDialogueChain(a_dlgArr)
{
	var retArr = [];
	for(var i = 0; i < array_length(a_dlgArr); i += 1)
	{
		array_push(retArr, new DialogueLine(a_dlgArr[i][0],a_dlgArr[i][1]));
	}
	return retArr;
}

function DialogueChain(a_dlgArr, a_Caller) constructor
{
	dlgCh_LineIndex = 0;
	dlgCh_EventCaller = a_Caller;
	dlgCh_LineArr = [];
	
	dlgCh_LineArr = InitDialogueChain(a_dlgArr);
	//if(a_dlgArr != undefined) array_copy(dlgCh_LineArr, 0, a_dlgArr, 0, array_length(a_dlgArr));
	
	dlgCh_CurrDB = undefined;
	
	func_InitDB = function()
	{
		dlgCh_CurrDB = CreateMenu(global.screen_middle, base_DialogueBox, global.Player1ID);
		dlgCh_CurrDB.db_charFace = dlgCh_LineArr[dlgCh_LineIndex].dlg_face;
		dlgCh_CurrDB.db_text = dlgCh_LineArr[dlgCh_LineIndex].dlg_text;	
		dlgCh_CurrDB.db_DialogueChain = self;
	}
	func_InitDB();
	
	func_NextLine = function()
	{
		instance_destroy(dlgCh_CurrDB);
		dlgCh_LineIndex += 1;
		if(dlgCh_LineIndex < array_length(dlgCh_LineArr))
		{
			func_InitDB();	
		}else
		{
			dlgCh_EventCaller.rmEvent_OnEventEnd();	
		}
	}
}

global.DialogueChains = 
{
	dlg_Opening : 
	[
		[spr_face_kobald_temp, "Kamui, this is Kidney, have you arrived on-site"],
		[spr_face_kamui_temp, "Kamui here, Kept you waiting... huh?"],
		[spr_face_kobald_temp, "I'd tell you your mission, but its not in the game yet"],
		[spr_face_kobald_temp, "Just go beat up some pirates"],
		[spr_face_kamui_temp, "Gotcha, heading out"],
	],
	dlg_Opening_T : 
	[
		[spr_face_kobald_temp, "Kamui, this is Kidney, have you arrived on-site"],
		[spr_face_kamui_temp, "Kamui here, Kept you waiting... huh?"],
		[spr_face_kobald_temp, "Your mission is to clear out the relic Terran Site: 'Titan Shell'"],
		[spr_face_kobald_temp, "The local pirates, the 'Sand Kraits' have been using it as a base of operations."],
		[spr_face_kobald_temp, "Lady Auresh said Solaris have sent a team to help with clearing the area of pirates and bringing the base back online"],
		[spr_face_kobald_temp, "Meet up with them at the abandoned salvagers camp, just head through that cave and you should reach there soon"],
		[spr_face_kamui_temp, "Gotcha, heading out"],
	],
	dlg_Sand : 
	[
		[spr_face_kamui_temp, "Thats alot of sand..."],
	],
	dlg_Test : 
	[
		[spr_face_kamui_temp, "I don't like sand, it's coarse, rough, irritating, and gets everywhere."],
	],
}

