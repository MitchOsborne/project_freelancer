// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SanityCheckOperator(charID){
	with charID
	{
		if(active_operator == undefined || is_method(active_operator))
		{
			if(PlayerControlled == true)
			{
				if(is_method(player_operator)) player_operator = new player_operator();	
				active_operator = player_operator;
				active_operator.slavedObj = self;	
			}else
			{
				if(ai_operator == undefined || is_method(ai_operator)) ai_operator = new ai_defaultOperator();
				active_operator = ai_operator;
				ai_operator.slavedObj = self;
				ai_operator.func_Init(ai_operator);
			}
			
		}
	}
}