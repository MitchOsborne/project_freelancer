// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_Attack() constructor{
	
	
	atk_anim = ABL_ANIM_STATE.ATK1;
	atk_projectile = undefined;
	atk_duration = 0.2;
	atk_dmgMod = 1;
	atk_spdMod = 1;
	atk_parryType = PARRY_TYPE.NONE;
	
	
	atk_lungeForce = 0;	//Negative numbers for knockback/recoil
	atk_lungeDur = 0;
	
	atk_forceStrength = atk_lungeForce;
	atk_stunDur = atk_duration;
	
	
	atk_resourceCost = 1;
	
	atk_projectileCount = 1;
	atk_projectileSpread = 1;
	atk_randomSpread = -1;
	
	atk_rotOffset = 0;
	atk_spreadOffset = new Vector2(0,0);
	
	atk_parentAbl = undefined;
	owner = undefined;
	
	executeAttack = false;
	atk_persist = false;
	atk_projList = ds_list_create();
	atk_sfx = undefined;
	
}

function atk_InitAttack(a_wep, a_atk)
{
	a_atk.owner = a_wep.owner;
	a_atk.atk_parentAbl = a_wep;
	
	a_atk.atk_stunDur = a_atk.atk_duration * 1.1;
}

function atk_UpdateAtk(a_attack)
{
	with (a_attack)
	{
		for(var i = 0; i < ds_list_size(atk_projList); i += 1)
		{
			if(instance_exists(atk_projList[|i]) == false) 
			{
				ds_list_delete(atk_projList,i);
				i -= 1;
			}
		}
		
	}

}

function atk_CancelAttack(a_attack)
{
	
}

function atk_PlaySFX(a_attack)
{
	with a_attack
	{
		if(!is_undefined(atk_sfx)) audio_play_sound(atk_sfx,5,false);
	}
}

function atk_ExecuteAttack(a_attack)
{
	with a_attack
	{
		var tRot = 0;
		for(var i = 1; i <= atk_projectileCount; i++)
		{
			
			var tSpread = atk_parentAbl.abl_bpStats.bp_Spread * atk_projectileCount;		
			var tRot = owner.direction + (CalcFixedSpread(i,tSpread,atk_projectileCount));
			tRot += irandom_range(-atk_parentAbl.abl_bpStats.bp_Spread/2,atk_parentAbl.abl_bpStats.bp_Spread/2);
			var tPos = new Vector2(owner.char_offset.x + atk_parentAbl.abl_posOffset.x, owner.char_offset.y + atk_parentAbl.abl_posOffset.y);
			var tDmg = atk_parentAbl.abl_CalcPowerScale * a_attack.atk_dmgMod;
			tInst = CreateLiteProjectile(atk_projectile,atk_parentAbl,tDmg ,tPos,tRot, atk_parentAbl.abl_bpStats);
			tInst.weapon = atk_parentAbl;
			tInst.stunDur = atk_stunDur;
			if(atk_parentAbl.teamID == TEAMID.Enemy) tInst.stunDur *= 0.5;
			ds_list_add(atk_parentAbl.abl_Instances,tInst);
		
		}
		
		if(atk_lungeForce != 0)
		{
			var tForce = AttachLinearForce(owner, owner, atk_lungeDur, owner.direction, atk_lungeForce);
		}
		var lockDur = 0;
		if(atk_lungeDur != 0) lockDur = atk_duration + 0.1;
		if(lockDur != 0) AttachMod(debuff_moveLocked, owner, owner, lockDur, 1, 1);
	}
}
