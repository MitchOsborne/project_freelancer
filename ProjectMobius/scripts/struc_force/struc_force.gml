enum FORCE_TYPE
{
	LINEAR,
	POSITIONAL,
	UNDEFINED,
}

function base_ForceMod() : base_Modifier() constructor
{
	name = "base_Force";
	displayName = true;
	stackLimit = 1;
	forceType = FORCE_TYPE.UNDEFINED;
	stackType = STACK_TYPE.NONE;
	forceDirection = 0;
	forceStrength = 0;
	forceMultiplier = 1;
	forceVector = new Vector2(0,0);
	clearOnExit = true;
	
}

function force_linear() : base_ForceMod() constructor
{
	name = "Linear Force";
	forceType = FORCE_TYPE.LINEAR;
	stackType = STACK_TYPE.NONE;
	mod_Update = function()
	{
		UpdateLinearForce(self);
	}
}


function force_positional() : base_ForceMod() constructor
{
	name = "Positional Force";
	forceType = FORCE_TYPE.POSITIONAL;
	forceOrigin = undefined;
	forceMinRange = 0;
	forceMaxRange = 0;
	forceMultiplier = 1;
	mod_Update = function()
	{
		UpdatePositionalForce(self);
	}
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function UpdateLinearForce(a_force)
{
	with a_force
	{
		forceVector = GetCoord(forceDirection);
		forceVector.x *= (forceStrength * forceMultiplier);
		forceVector.y *= (forceStrength * forceMultiplier);
	}
}


function UpdatePositionalForce(a_force)
{
	with a_force
	{
		var tDist = point_distance(forceOrigin.x, forceOrigin.y, subject.x, subject.y);
		var distClamp = clamp(tDist, forceMinRange, forceMaxRange);
		
		forceMultiplier = 1 - (InvLerp(forceMinRange, forceMaxRange, distClamp));
		forceDirection = point_direction(forceOrigin.x, forceOrigin.y, subject.x, subject.y) + 90;
		forceVector = GetCoord(forceDirection);
		forceVector.x *= (forceStrength * forceMultiplier);
		forceVector.y *= (forceStrength * forceMultiplier);
	}
}