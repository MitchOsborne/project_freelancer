// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_base_XPPickup() : base_Pickup() constructor{
		
		
	lifeTime = 10;
	blinkTime = 4;
	
	
	moveSpeed = 2;
	pickupRange = 300;
	
	xpAmount = 1;
	func_EndUpdateStep = XP_EndUpdateStep;
}

function XP_EndUpdateStep(a_pkup)
{
	with a_pkup
	{	
		if(isCollected)
		{
			score += xpAmount;
			//if(global.Player1ID != undefined) global.Player1ID.xp += xpAmount;
			//if(global.Player2ID != undefined) global.Player2ID.xp += xpAmount;
			lifeTimer = lifeTime;
		}
		Pickup_EndUpdate(self);
	}
}