// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ReflectProjectile(a_proj, a_other)
{
	if(!a_other.dmg_unblockable)
	{
		var tOwner = a_proj.owner;
		var tDir = point_direction(x,y,other.x,other.y) - 90;
		
		var angleDiff = angle_difference(a_proj.direction,tDir);
		var rand = random_range(-4,4)
		
		if(other.lifeTimer > 0.2)
		{
			other.teamID = teamID;
			other.weapon = weapon;
			//other.damage += damage*(weapon.damageMultiplier/4);
		
			if(weapon.wep_parryType == PARRY_TYPE.REFLECT)
			{
				other.direction = a_proj.direction - (5*rand);
				
			}else if (weapon.wep_parryType == PARRY_TYPE.DEFLECT)
			{
				other.direction += (rand*10);
			}else if (weapon.wep_parryType == PARRY_TYPE.BLOCK)
			{
				if(other.hp != infinity)other.hp = 0;	
			}
			var mVec = GetCoord(other.direction - 90);
			other.mx = mVec.x;
			other.my = mVec.y;
			
			array_push(Renderer.rndr_ParticleArr, new Ptl_SwordHit(other.x, other.y, 0,0));
		}else if(other.hp != infinity)other.hp = 0;
	}		
}