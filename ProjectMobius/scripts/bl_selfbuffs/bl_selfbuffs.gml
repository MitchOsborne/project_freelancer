// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Internal_bp_Init_SelfBuffs()
{

	with global.bp_Abilities.bp_RegenPop
	{
		bp_Name = "Stamune";
		bp_Frame = abl_RegenPop;
		bp_Slot = LOADOUT_SLOT.UTILITY1;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_EquipTime : 0,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}

	with global.bp_Abilities.bp_CritPop
	{
		bp_Name = "Criticola";
		bp_Frame = abl_CritPop;
		bp_Slot = LOADOUT_SLOT.UTILITY1;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	
	with global.bp_Abilities.bp_CombatJunkie
	{
		bp_Name = "Combat Junkie";
		bp_Frame = abl_LifeSteal;
		bp_Slot = LOADOUT_SLOT.CORE;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
			bp_DURScale : 0.5,
			bp_CDScale : 0.5,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	
	
	with global.bp_Abilities.bp_EquipMosin
	{
		bp_Name = "Mosin";
		bp_Frame = base_AltLoadout;
		bp_Slot = LOADOUT_SLOT.CORE;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		bp_DisplayActive = false;
		bp_StayActive = false;
		
		bp_AltLd = new Loadout(global.bp_Abilities.bp_Mosin,undefined, undefined, undefined, undefined, undefined, global.bp_Abilities.bp_Unequip);
		
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	
		with global.bp_Abilities.bp_Unequip
	{
		bp_Name = "Unequip Loadout";
		bp_Frame = base_UnequipLoadout;
		bp_Slot = LOADOUT_SLOT.CORE;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		
		bp_DisplayActive = false;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	

	with global.bp_Abilities.bp_KobaldRegroup
	{
		bp_Name = "Kobald Regroup";
		bp_Frame = abl_KobaldRegroup;
		bp_Slot = LOADOUT_SLOT.CORE;
		bp_DisplayActive = false;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 61,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	
	with global.bp_Abilities.bp_SelfDestruct
	{
		bp_Name = "SelfDestruct";
		bp_Frame = abl_SelfDestruct;
		bp_Slot = LOADOUT_SLOT.UTILITY1;
		bp_AbilityType = ABILITY_TYPE.UTILITY;
		bp_DisplayActive = false;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 8, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_EquipTime : 0,
			bp_ReloadSpd : 1,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
	
}