// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Internal_bp_Init_Ability()
{
	with global.bp_Abilities.bp_Empty
	{
		bp_Slot = LOADOUT_SLOT.ALL;	
	}
	
	with global.bp_Abilities.bp_BaseGrenadeStats
	{
		bp_Range = 160; 
		bp_Spread = 15; 
		bp_DMGScale = 1;
		bp_RPM = 60;
		bp_Ammo = 1;
		bp_ReloadSpd = 5;
		bp_EquipTime = 0;
		bp_SpeedCurve = "Grenade";
	}


	with global.bp_Abilities.bp_SpikeTraps
	{
		bp_Name = "Spike Traps";
		bp_Frame = base_wep_SpikeTrap;
		bp_Slot = LOADOUT_SLOT.ABILITY1;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		bp_StayActive = false;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseGrenadeStats);
	
		with bp_WepStats
		{
			bp_Range = 160; 
			bp_Spread = 15; 
			bp_DMGScale = 0.3;
			bp_RPM = 240;
			bp_Ammo = 2;
			bp_ReloadSpd = 5;
			bp_EquipTime = 0;
			bp_ReloadAmount = 1;
			bp_ProjectileCount = 3;
			bp_ProjectileType = proj_SpikeTrap;
			bp_SpeedCurve = "GrenadeTrap";
			bp_ChargeBased = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		bp_SpriteSet = sprSet_Hands;
	}


	with global.bp_Abilities.bp_FragGrenade
	{
		bp_Name = "Frag Grenade";
		bp_Frame = base_Grenade;
		bp_Slot = LOADOUT_SLOT.ABILITY1;
		bp_AbilityType = ABILITY_TYPE.RANGED;
	
		bp_StayActive = false;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseGrenadeStats);
	
		with bp_WepStats
		{
			bp_Range = 160; 
			bp_Spread = 15; 
			bp_DMGScale = 4;
			bp_RPM = 240;
			bp_Ammo = 2;
			bp_ReloadSpd = 5;
			bp_ReloadAmount = 1;
			bp_EquipTime = 0;
			bp_ProjectileCount = 3;
			bp_ProjectileType = proj_Grenade;
			bp_SpeedCurve = "Grenade";
			bp_ChargeBased = true;
		}
	
		with bp_StatMods
		{
			bp_Range = 1; 
			bp_Spread = 1;
			bp_DMGScale = 1; 
			bp_RPM = 1;
			bp_Ammo = 1;
			bp_ReloadSpd = 1;
		}
		bp_SpriteSet = sprSet_Hands;
	}
}