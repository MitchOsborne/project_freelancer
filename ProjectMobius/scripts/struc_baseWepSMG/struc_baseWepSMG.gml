// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_base_wep_SMG() : defunc_base_Weapon() constructor{
	
	ds_list_add(projectileList,proj_Bullet);
	
	abl_canReload = true;
	autoReload = false;
	reloadTime = 1.5;
	reloadTimer = 0;
	reloadAmount = ammoMax;	//Amount of ammo added when the gun reloads

	fireRate = 0.1;
	fireRateTimer = 0;

	wep_projectileCount = 1;
	wep_projectileSpread = 5;
	wep_randomSpread = true;
	
	range = 120;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"SMG");
	RPM = 800;
	
	abl_forceStrength = 0.5;
	abl_stunDur = 0.05;
	
}