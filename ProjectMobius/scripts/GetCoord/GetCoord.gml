function GetCoord(a_Rot) {
	
	var vec2 = new Vector2(0,0);
	var tempRot = a_Rot+270;


	vec2.x = cos(tempRot*pi/180)*-1;
	vec2.y = sin(tempRot*pi/180);


	return vec2;


}
