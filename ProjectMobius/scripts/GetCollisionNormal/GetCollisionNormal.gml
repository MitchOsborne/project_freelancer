function GetCollisionNormal(a_X, a_Y, a_Target, a_Radius) {


	var wallNormal = Collision_Normal(a_X,a_Y,a_Target,a_Radius,2);

	return wallNormal;


}
function GetCollisionNormalTileMap(a_X, a_Y,a_Target, a_Radius)
{
	var wallNormal = TileMap_Collision_Normal(a_X,a_Y,a_Target,a_Radius,2);

	return wallNormal;
}

function GetCollisionNormalCombined(a_X, a_Y, a_PhysArr, a_TileArr, a_Rad, a_Res)
{
	var wallNormal = Collision_Normal_Combined(a_X, a_Y, a_PhysArr, a_TileArr, a_Rad, a_Res);
	
	return wallNormal;
}
