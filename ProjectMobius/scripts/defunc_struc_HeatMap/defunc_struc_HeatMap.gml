// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function heatNode(a_pos, a_gridCoord) constructor{

gridCoord = a_gridCoord;
x = a_pos.x;
y = a_pos.y;


regions = ds_list_create();
hasRegions = false;

//Dynamic Data

//Ally Heat Data Input
ally_pThreatIn = 0;			//Danger from projectiles
ally_sThreatIn = 0;			//Danger from enemy potential Line of Sight
ally_vThreatIn = 0;			//Danger from enemy actual Line of Sight
ally_otherIn = 0;			//Positive modifier from effects

//Ally Heat Data Output
ally_favour = 0;			//How safe or optimal the cells position is
ally_pThreatOut = 0;		//Danger from projectiles
ally_sThreatOut = 0;		//Danger from enemy potential Line of Sight
ally_vThreatOut = 0;		//Danger from enemy actual Line of Sight
ally_otherOut = 0;			//Positive modifier from effects

//Enemy Heat Data Input
enm_pThreatIn = 0;			//Danger from projectiles
enm_sThreatIn = 0;			//Danger from enemy potential Line of Sight
enm_vThreatIn = 0;			//Danger from enemy actual Line of Sight
enm_otherIn = 0;			//Positive modifier from effects

//Enemy Heat Data Output
enm_favour = 0;				//How safe or optimal the cells position is
enm_pThreatOut = 0;			//Danger from projectiles
enm_sThreatOut = 0;			//Danger from enemy potential Line of Sight
enm_vThreatOut = 0;			//Danger from enemy actual Line of Sight
enm_otherOut = 0;			//Positive modifier from effects

//Static Data
stc_totalSight = 0;			//Total visible cells in all directions
stc_dirSight = 0;			//Highest visible cells in fixed direction
stc_proxSight = 0;			//Total visible to nearby cells


coldColour = c_blue
ambientColour = c_green;
hotColour = c_red;

drawHeat = 0;

awake = true;
awakeAlly = true;
awakeEnm = true;

bbox_bottom = y;
bbox_top = y + (global.gridSize);
bbox_left = x + (global.gridSize);
bbox_right = x;


for(var i = 0; i < instance_number(HeatRegion); i += 1)
{
	var tRegion = instance_find(HeatRegion,i)
	if(instance_position(x,y,tRegion) != noone)
	{
		ds_list_add(regions,tRegion);	
	}
}

}

function heatCell(a_pos, a_gridCoord) : heatNode(a_pos, a_gridCoord) constructor
{
	
}

function CheckNodeStatus(a_node)	
{
	with a_node
	{
		awake = false;
		awakeAlly = false;
		awakeEnm = false;
		/* Checks to see if it is in range of a subscriber, 
		and attachs to the subscribers AI node reading list */
		for(var i = 0; i < ds_list_size(regions); i += 1)
		{
			var reg = regions[|i];
			for(var s = 0; s < ds_list_size(reg.subscribers); s += 1)
			{
				var sub = reg.subscribers[|s];
				if(sub.canRead)
				{
					if(point_distance(x,y,sub.x,sub.y) < sub.heatMapRange)
					{
						if(point_distance(x,y,sub.x,sub.y) < sub.heatMapAllyRange)
						{
							if(ds_list_find_index(sub.active_operator.nodeList, self) == -1)
							{
								ds_list_add(sub.active_operator.nodeList,self);	
							}
						}
						if(sub.teamID == TEAMID.Ally) awakeAlly = true;
						else if(sub.teamID == TEAMID.Enemy)	awakeEnm = true;
					}else if(ds_list_find_index(sub.active_operator.nodeList, self) != -1)
					{
						ds_list_delete(sub.active_operator.nodeList,ds_list_find_index(sub.active_operator.nodeList,self));	
					}
				}
			}
		}
		//Checks if it is within reading range, if not, removes itself from the active node list
		if(awakeAlly || awakeEnm) awake = true;

		if(awake)
		{
			if(awakeAlly)
			{
				if(ds_list_find_index(allyAwakeNodes,self) == -1)
				{
					ds_list_add(allyAwakeNodes,self);	
				}
			}else if(ds_list_find_index(allyAwakeNodes,self) != -1)
			{
				ds_list_delete(allyAwakeNodes,ds_list_find_index(allyAwakeNodes,self));	
			}

			if(awakeEnm)
			{
				if(ds_list_find_index(enmAwakeNodes,self) == -1)
				{
					ds_list_add(enmAwakeNodes,self);	
				}
			}else if(ds_list_find_index(enmAwakeNodes,self) != -1)
			{
				ds_list_delete(enmAwakeNodes,ds_list_find_index(enmAwakeNodes,self));	
			}
		}
	}
}

function DesyncUpdateHeatNode(node){
	with node
	{
		CheckNodeStatus(node);
		if(node.awake)
		{
			
			for(var r = 0; r < ds_list_size(regions); r += 1)
			{
				var tRegion = regions[|r];
				for(var i = 0; i < ds_list_size(tRegion.subscribers); i += 1)
				{
					var tInst = tRegion.subscribers[|i];
					if(tInst.canWrite)
					{
						if((node.awakeAlly && tInst.teamID == TEAMID.Enemy) 
						|| node.awakeEnm && tInst.teamID == TEAMID.Ally)
						{
							var fDist = base_Player.followerDist;
							var tDir = point_direction(x,y,tInst.x,tInst.y) - 90;
							var tDist = point_distance(x,y, tInst.x, tInst.y);
							var pDist = point_distance(x,y,Player1.x,Player1.y);
							var distScore = clamp(pDist - fDist,0,fDist); 
							ally_otherIn += (fDist - distScore)/fDist;
							if(tDist <= tInst.sightRadius)
							{
								if(collision_line(x,y,tInst.x,tInst.y,base_PhysObstacle,false,true) == noone)
								{
									if(tInst.teamID == TEAMID.Ally)
									{
										enm_vThreatIn = 1;
									}else
									{
										ally_vThreatIn = 1;
									}
				
									if(abs(angle_difference(tDir, tInst.direction)) > tInst.sightAngle)
									{
										if(tInst.teamID == TEAMID.Ally)
										{
											enm_sThreatIn = 1;	
										}else
										{
											ally_sThreatIn = 1;
										}
									}
								}
							}
						}
					}
				}
		
			}
		}
		
	}
	DesyncUpdateHeatNodeProjectiles(node)
	DesyncEndUpdateHeatNode(node);
}

function DesyncUpdateHeatNodeProjectiles(node)
{
	
	with node
	{
		if(node.awake)
		{
			if(node.awakeAlly)
			{
				for(var i = 0; i < ds_list_size(global.l_enmProjArr); i += 1)
				{
					var proj = global.l_enmProjArr[|i];
					if(CheckExistance(proj))
					{
						var tDir = GetDirection(proj.x,proj.y,x,y,false,false);
						var pDir = proj.direction;
						if(abs(angle_difference(tDir, pDir) <= 10))
						{
							var tDist = point_distance(x,y,proj.x,proj.y);
							var projRange = proj.range - proj.distTravelled;
							if(tDist < projRange)
							{
								var tDirBL = GetDirection(proj.x,proj.y,bbox_left,bbox_bottom,false,false);
								var tDirTL = GetDirection(proj.x,proj.y,bbox_left,bbox_top,false,false);
								var tDirBR = GetDirection(proj.x,proj.y,bbox_right,bbox_bottom,false,false);
								var tDirTR = GetDirection(proj.x,proj.y,bbox_right,bbox_top,false,false);
								var angBL = abs(angle_difference(tDirBL,pDir));
								var angTL = abs(angle_difference(tDirTL,pDir));
								var angBR = abs(angle_difference(tDirBR,pDir));
								var angTR = abs(angle_difference(tDirTR,pDir));
								if(angBL <= 5 || angTL <= 5 || angBR <= 5 || angTR <= 5 || tDist < global.gridSize*2)
								{
									if(collision_line(x,y,proj.x,proj.y,base_PhysWall,false,true) == noone)
									{
										ally_pThreatIn = 1;
										break;
									}
								}else
								{
									//ds_list_delete(node.projList, i);	
								}
							}
						}
					}else 
					{
						//ds_list_delete(node.projList, i);
					}
				}
			}
			if(node.awakeEnm)
			{
				for(var i = 0; i < ds_list_size(global.l_allyProjArr); i += 1)
				{
					var proj = global.l_allyProjArr[|i];
					if(CheckExistance(proj))
					{
						var tDir = GetDirection(proj.x,proj.y,x,y,false,false);
						var pDir = proj.direction;
						if(abs(angle_difference(tDir, pDir) <= 10))
						{
							var tDist = point_distance(x,y,proj.x,proj.y);
							var projRange = proj.range - proj.distTravelled;
							if(tDist < projRange)
							{
								var tDirBL = GetDirection(proj.x,proj.y,bbox_left,bbox_bottom,false,false);
								var tDirTL = GetDirection(proj.x,proj.y,bbox_left,bbox_top,false,false);
								var tDirBR = GetDirection(proj.x,proj.y,bbox_right,bbox_bottom,false,false);
								var tDirTR = GetDirection(proj.x,proj.y,bbox_right,bbox_top,false,false);
								var angBL = abs(angle_difference(tDirBL,pDir));
								var angTL = abs(angle_difference(tDirTL,pDir));
								var angBR = abs(angle_difference(tDirBR,pDir));
								var angTR = abs(angle_difference(tDirTR,pDir));
								if(angBL <= 5 || angTL <= 5 || angBR <= 5 || angTR <= 5 || tDist < global.gridSize*2)
								{
									if(collision_line(x,y,proj.x,proj.y,base_PhysWall,false,true) == noone)
									{
										enm_pThreatIn = 1;
										break;
									}
								}else
								{
									//ds_list_delete(node.projList, i);	
								}
							}
						}
					}else 
					{
						//ds_list_delete(node.projList, i);
					}
				}
			}
			
		}
	}
}

function DesyncEndUpdateHeatNode(node)
{
	with node
	{
			//Ally Heat Data Output	
			ally_pThreatOut = ally_pThreatIn;	
			ally_sThreatOut = ally_sThreatIn;	
			ally_vThreatOut = ally_vThreatIn;	
			ally_otherOut =	ally_otherIn;	

			ally_pThreatIn = 0;	
			ally_sThreatIn = 0;	
			ally_vThreatIn = 0;	
			ally_otherIn = 0;

			ally_favour = ally_otherOut - (ally_sThreatOut + ally_vThreatOut + ally_pThreatOut);
		
			//Enemy Heat Data Output
			enm_pThreatOut = enm_pThreatIn;	
			enm_sThreatOut = enm_sThreatIn;	
			enm_vThreatOut = enm_vThreatIn;	
			enm_otherOut =	enm_otherIn;	

			enm_pThreatIn = 0;	
			enm_sThreatIn = 0;	
			enm_vThreatIn = 0;	
			enm_otherIn = 0;

			enm_favour = enm_otherOut - (enm_sThreatOut + enm_vThreatOut + enm_pThreatOut);	
	}
}

function DesyncUpdateHeatCell(cell)
{
	with cell
	{
		CheckNodeStatus(cell);
		var uNode = undefined;
		var dNode = undefined;
		var lNode = undefined;
		var rNode = undefined;
		
		if(gridCoord.y - 1 > 0)
		{
			var uNode = ds_grid_get(heatMap,gridCoord.x, gridCoord.y - 1);
		}
		if(gridCoord.x - 1 > 0)
		{
			var lNode = ds_grid_get(heatMap,gridCoord.x - 1, gridCoord.y);
		}
		if(gridCoord.y + 1 < ds_grid_height(heatMap))
		{
			var dNode = ds_grid_get(heatMap,gridCoord.x, gridCoord.y + 1);
		}
		if(gridCoord.x + 1 < ds_grid_width(heatMap))
		{
			var rNode = ds_grid_get(heatMap,gridCoord.x + 1, gridCoord.y);
		}
		
		validNodes = 0;
		if(uNode != undefined && uNode != 0)
		{
			if(uNode.awake)
			{
				ally_pThreatIn += uNode.ally_pThreatOut;
				ally_sThreatIn += uNode.ally_sThreatOut;
				ally_vThreatIn += uNode.ally_vThreatOut;
				ally_otherIn += uNode.ally_otherOut;
			
				enm_pThreatIn += uNode.enm_pThreatOut;
				enm_sThreatIn += uNode.enm_sThreatOut;
				enm_vThreatIn += uNode.enm_vThreatOut;
				enm_otherIn += uNode.enm_otherOut;
				validNodes += 1;
			}
		}
		
		if(dNode != undefined && dNode != 0)
		{
			if(dNode.awake)
			{
			ally_pThreatIn += dNode.ally_pThreatOut;
			ally_sThreatIn += dNode.ally_sThreatOut;
			ally_vThreatIn += dNode.ally_vThreatOut;
			ally_otherIn += dNode.ally_otherOut;
			
			enm_pThreatIn += dNode.enm_pThreatOut;
			enm_sThreatIn += dNode.enm_sThreatOut;
			enm_vThreatIn += dNode.enm_vThreatOut;
			enm_otherIn += dNode.enm_otherOut;
			validNodes += 1;
			}
		}
		
		if(lNode != undefined && lNode != 0)
		{
			if(lNode.awake)
			{
				ally_pThreatIn += lNode.ally_pThreatOut;
				ally_sThreatIn += lNode.ally_sThreatOut;
				ally_vThreatIn += lNode.ally_vThreatOut;
				ally_otherIn += lNode.ally_otherOut;
			
				enm_pThreatIn += lNode.enm_pThreatOut;
				enm_sThreatIn += lNode.enm_sThreatOut;
				enm_vThreatIn += lNode.enm_vThreatOut;
				enm_otherIn += lNode.enm_otherOut;
				validNodes += 1;
			}
		}
		
		if(rNode != undefined && rNode != 0)
		{
			if(rNode.awake)
			{
				ally_pThreatIn += rNode.ally_pThreatOut;
				ally_sThreatIn += rNode.ally_sThreatOut;
				ally_vThreatIn += rNode.ally_vThreatOut;
				ally_otherIn += rNode.ally_otherOut;
			
				enm_pThreatIn += rNode.enm_pThreatOut;
				enm_sThreatIn += rNode.enm_sThreatOut;
				enm_vThreatIn += rNode.enm_vThreatOut;
				enm_otherIn += rNode.enm_otherOut;
				validNodes += 1;
			}
		}
		
		ally_pThreatOut = ally_pThreatIn/validNodes
		ally_sThreatOut = ally_sThreatIn/validNodes
		ally_vThreatOut = ally_vThreatIn/validNodes
		ally_otherOut = ally_otherIn/validNodes
		
		enm_pThreatOut = enm_pThreatIn/validNodes
		enm_sThreatOut = enm_sThreatIn/validNodes
		enm_vThreatOut = enm_vThreatIn/validNodes
		enm_otherOut = enm_otherIn/validNodes
		
		ally_pThreatIn = 0;	
		ally_sThreatIn = 0;	
		ally_vThreatIn = 0;	
		ally_otherIn = 0;

		ally_favour = ally_otherOut - (ally_sThreatOut + ally_vThreatOut + ally_pThreatOut);
		
		
		enm_pThreatIn = 0;	
		enm_sThreatIn = 0;	
		enm_vThreatIn = 0;	
		enm_otherIn = 0;
		
		enm_favour = enm_otherOut - (enm_sThreatOut + enm_vThreatOut + enm_pThreatOut);
	}
}

function ConvertPosToGrid(a_pos)
{
	return new Vector2(floor(a_pos.x/global.gridSize), floor(a_pos.y/global.gridSize));
}
