// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SanityCheckCharStats(charID){
	with charID
	{
		if(char_StatArchCard == undefined || char_StatArchCard != char_AC.ac_bpLd.ld_slots.ld_class) char_StatArchCard = char_AC.ac_bpLd.ld_slots.ld_class.bp_Class;
		if(PlayerControlled) 
		{
			charLvl = clamp(global.Player1ID.powerLvl,0,100);
		}else if(teamID == TEAMID.Enemy) charLvl = global.enemyLevel + global.worldLvl;
		
		var xPos = charLvl/global.LevelCap;
		var lvlScale = animcurve_channel_evaluate(char_StatGrowths, xPos);
		
		char_baseDamage = char_StatArch.dmg * 10;
		char_baseHealth = char_StatArch.HP * 10;
		//char_damagePerLvl = char_StatArch.dmg + char_Race.dmg;
		//char_healthPerLvl = char_StatArch.HP + char_Race.HP;
		
		char_damagePerLvl = char_StatArch.dmg;
		char_healthPerLvl = char_StatArch.HP;
		if(char_Elite)
		{
			char_damagePerLvl *= 1.5;
			char_healthPerLvl *= 10;
		}
		char_woundLvl = 0;
		
		var tWounds = FindMod(char_modArr, "Wounds");
		if(tWounds != undefined)
		{
			char_woundLvl = floor(tWounds.stacks);	
		}
		
		
		if(char_StatCard != undefined)
		{
			if(char_StatArchCard != undefined)
			{
				char_StatCard.crd_DMGMod = char_StatArchCard.crd_DMGMod;
				char_StatCard.crd_SPDMod = char_StatArchCard.crd_SPDMod;
				char_StatCard.crd_HPMod = char_StatArchCard.crd_HPMod;
				char_StatCard.crd_SHDMod = char_StatArchCard.crd_SHDMod;
				char_StatCard.crd_CDMod = char_StatArchCard.crd_CDMod;
				char_StatCard.crd_DURMod = char_StatArchCard.crd_DURMod;
				char_StatCard.crd_CRITMod = char_StatArchCard.crd_CRITMod;
				char_StatCard.crd_PROCMod = char_StatArchCard.crd_PROCMod;
			}
			
			if(char_UpdateCard)	Update_StatCard(id);
			classLvl = char_StatCard.crd_CardLvl;
			char_damageLvl = char_StatCard.crd_DMGLvl;
			char_healthLvl = char_StatCard.crd_HPLvl;
			char_shieldLvl = char_StatCard.crd_SHDLvl;
			char_spdLvl = char_StatCard.crd_SPDLvl;
			char_durLvl = char_StatCard.crd_DURLvl;
			char_CDLvl = char_StatCard.crd_CDLvl;
			char_critLvl = char_StatCard.crd_CRITLvl;
			char_procLvl = char_StatCard.crd_PROCLvl;
		}
		
		//Calculates Stats to prevent issues with negative levels and scaling
		var spdVal = animcurve_channel_evaluate(animcurve_get_channel(crv_StatGrowths, "AtkSpd"),abs(char_spdLvl/100));
		if(char_spdLvl < 0) char_spdStat = 1 / spdVal;
		else char_spdStat = spdVal;
		
		
		var durVal = animcurve_channel_evaluate(animcurve_get_channel(crv_StatGrowths, "AblDur"),abs(char_durLvl/100));
		if(char_durLvl < 0) char_durStat = 1 / durVal;
		else char_durStat = durVal;
		
		var CDVal = animcurve_channel_evaluate(animcurve_get_channel(crv_StatGrowths, "AblCD"),abs(char_CDLvl/100));
		if(char_CDLvl < 0) char_CDStat = 1 / CDVal;
		else char_CDStat = CDVal;
		
		
		var hpCalc = (char_healthLvl + charLvl)*char_healthPerLvl;
		
		if(teamID == TEAMID.Enemy) hpCalc *= global.enemyHPScale;
		var tDmgLvl = char_damageLvl + charLvl;
		if(tDmgLvl < 0) char_damageStat = char_baseDamage/abs(tDmgLvl);
		else char_damageStat = char_baseDamage + ((char_damageLvl + charLvl)*char_damagePerLvl);
		//Makes sure MaxHP can't go below 1 (prevent player from dying if they equip too much negative HP Mods)
		
		var tHPLvl = char_healthLvl + charLvl;
		if(tHPLvl < 0) char_maxHP = char_baseHealth/abs(tHPLvl);
		else char_maxHP = clamp(char_baseHealth + (hpCalc + (hpCalc * lvlScale)),1,infinity);
		
		maxVit = char_maxHP/2;
		
		var tShdScale = ((char_shieldLvl + char_baseShdLvl)*char_shieldPerLvl)*0.01;
		char_maxShd = clamp(char_maxHP * tShdScale, char_maxHP * -0.8, char_maxHP*10);	//char_shieldLvl = X% of char_maxHP per level
		char_shdBufferMax = char_maxShd/2;
		char_shdBufferMax = clamp(char_shdBufferMax, 0, char_maxHP/2);
		char_shdChargeRate = char_maxHP/10;
		
		if(char_shieldLvl < 0) 
		{
			char_woundLvl += abs(char_shieldLvl);
			
			char_shdStat = 0;
		}
		char_woundLvl = clamp(char_woundLvl, 0, 80);
		var woundAmount = char_maxHP * (char_woundLvl/100);
		char_healCap = char_maxHP - woundAmount;
		
		//hp = clamp(hp, -infinity, char_healCap);
		
		//var armVal = 0;
		//if(char_armStat != 0) armVal = char_maxArm / char_armStat;
		char_maxArm = char_armLvl * (charLvl*2);
		//char_armStat *= armVal;
		
		totalHP = hp + char_shdStat + vitStat + char_armStat;
		totalchar_maxHP = char_maxHP + char_maxShd + maxVit + char_maxArm;
		
		//Adds modifier stat multipliers to base stats
		for(var i = 0; i < array_length(char_modArr); i += 1)
		{
			var tMod = char_modArr[i];
			char_damageStat *= tMod.mod_DMG;	
			char_spdStat *= tMod.mod_SPD;
			char_durStat *= tMod.mod_DUR;
			char_CDStat *= tMod.mod_CD;
			char_critLvl *= tMod.mod_CRIT;
			char_procLvl *= tMod.mod_PROC;
		}
		char_CDStat *= char_ldWeightBonus;
		char_critLvl *= char_ldWeightBonus;
		char_procLvl *= char_ldWeightBonus;
	}
}

function UpdateSummonStats(charID)
{
	with charID
	{
		charLvl = smn_Master.charLvl;
		char_healthLvl = smn_Master.char_healthLvl;
		char_damageLvl = smn_Master.char_damageLvl;
		char_shieldLvl = smn_Master.char_shieldLvl;
	}
}

function HealParty(a_woundCap)
{
	var woundVal = 0;
	if(a_woundCap != undefined) woundVal = a_woundCap;
	for(var i = 0; i < ds_list_size(global.coreAllyList); i += 1)
	{
		var tChar = global.coreAllyList[|i];
		tChar.char_woundLvl = 0;
		var tMod = FindMod(tChar.char_modArr, "Wounds");
		if(tMod != undefined)
		{
			if(tMod.stacks >= woundVal)
			{
				tMod.stacks = woundVal;
			}
		}
		tChar.hp = tChar.char_maxHP;
	
	}
	
}