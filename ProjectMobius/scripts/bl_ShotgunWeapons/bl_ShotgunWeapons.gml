// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Internal_bp_Init_Shotgun()
{

	with global.bp_Abilities.bp_BaseShotgunStats
	{
		bp_Range = 240;
		bp_Spread = 4;
		bp_DMGScale = 0.5;
		bp_RPM = 70;
		bp_Ammo = 6;
		bp_ReloadSpd = 0.8;
		bp_ReloadAmount = 1;
		bp_AutoReload = false;
		bp_EquipTime = 0.7;
		bp_SpeedCurve = "Shotgun";
		bp_WeightBonus = 0.8;
	}

	with global.bp_Abilities.bp_SP45 {
	
		bp_Name = "SP45";
		bp_Frame = base_wep_Shotgun;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseShotgunStats);
	
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		bp_SpriteSet = sprSet_SP45Base;
		bp_SFX = sfx_shoot1;
		bp_WepStats.bp_DMGType = new DMGReport(1,0,0,0);
	}
	
	with global.bp_Abilities.bp_SP45r {
	
		bp_Name = "SP45-Riot";
		bp_Frame = base_wep_Shotgun;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseShotgunStats);
		
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		bp_DMGType = DMG_TYPE.SOFT;
		bp_StatMods.bp_DMGScale = 0.8;
	
		bp_SpriteSet = sprSet_SP45r;
		bp_SFX = sfx_shoot1;
	}
	
	with global.bp_Abilities.bp_DBSG {
	
		bp_Name = "DB-SG";
		bp_Frame = base_wep_Shotgun;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_BaseShotgunStats);
		
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		with bp_WepStats
		{
			bp_RPM = 120;
			bp_Ammo = 2;
			bp_ReloadAmount = 2;
			bp_ReloadSpd = 1.5;
		}
		with bp_StatMods
		{
			bp_Spread = 0.8;
			bp_Range = 1.25;
			bp_DMGScale = 2;	
		}
	
		bp_SpriteSet = sprSet_DBSGBase;
		bp_SFX = sfx_shoot1;
	}
	
		with global.bp_Abilities.bp_DBSGs {
	
		bp_Name = "Sawed-Off DB-SG";
		bp_Frame = base_wep_Shotgun;
		bp_Slot = LOADOUT_SLOT.WEAPON2;
		bp_AbilityType = ABILITY_TYPE.RANGED;
		Copy_Struct(bp_WepStats, global.bp_Abilities.bp_DBSG.bp_WepStats);
		
		bp_AttackArr = [atk_Shotgun];
		bp_AttackFin = undefined;
		
		with bp_StatMods
		{
			bp_Range = 0.75
			bp_Spread = 1.3;
		}
	
		bp_SpriteSet = sprSet_DBSGsBase;
		bp_SFX = sfx_shoot1;
	}
}