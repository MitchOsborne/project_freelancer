// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

global.spawnPool = [];

function GetRegion(a_id)
{
	var retVal = undefined;
	switch a_id
	{
		case REGION.NONE:
		break;
		case REGION.DESERT_EAST:
		retVal = global.regions.rg_EastDesert;
		break;
		case REGION.STRUT_F_OUTER:
		retVal = global.regions.rg_FreightDock;
		break;
		case REGION.STRUT_F_INNER:
		retVal = global.regions.rg_FreightPlatform;
		break;
	}
	return retVal;
}

function GetRegionEnmPool(a_region)
{
	with a_region
	{
		return rg_enmPool - (rg_enmCaptured	+ rg_enmKilled);
	}
}

function GetRegionWildlifePool(a_region)
{
	with a_region
	{
		return rg_wildlifePool - (rg_wildlifeCaptured + rg_wildlifeKilled);	
	}
}

function base_Region() constructor{
	rg_randSpawn = true;
	rg_enmArr = [];
	rg_enmPool = 0;
	rg_enmCaptured = 0;
	rg_enmKilled = 0;
	rg_wildlifePool = 0;
	rg_wildlifeCaptured = 0;
	rg_wildlifeKilled = 0;
}

function base_Biome() : base_Region() constructor
{
	rg_dungeonType = undefined;	
}

function Region_EasternDesert() : base_Region() constructor
{
	rg_enmPool = 24;
	rg_wildlifePool = 80;
}

function Region_FreightDock() : base_Region() constructor
{
	rg_enmPool = 24;
}

function Region_FreightPlatform() : base_Region() constructor
{
	rg_enmPool = 40;	
	rg_enmArr = [enemy_Yaki, enemy_Yaki_Gun];
}

function WoodsRegion() : base_Region() constructor
{
	rg_enmArr = [enemy_Kitsune, enemy_Kitsune_Oracle];
}

function CaveRegion() : base_Region() constructor
{
	rg_enmArr = [enemy_Boomer, enemy_Bat];
}

function YakiBaseRegion() : base_Region() constructor
{
	rg_enmArr = [enemy_Yaki, enemy_Yaki_Gun, enemy_Nezumi, enemy_Nezumi, enemy_Kitsune, enemy_Kitsune_Oracle];
}
function YakiBaseBossRegion() : base_Region() constructor
{
	rg_randSpawn = false;
	rg_enmArr = [defunc_enemy_Drake];
}

function RuinBiome() : base_Biome() constructor
{
	rg_enmArr = [enemy_Kitsune, enemy_Kitsune_Oracle];
	rg_dungeonType = GenerateDungeonFloorBranch;
}

function BeachBiome() : base_Biome() constructor
{
	rg_enmArr = [enemy_Goon, enemy_Doggo, enemy_Dragon];
	rg_dungeonType = GenerateDungeonFloorGrid;	
}