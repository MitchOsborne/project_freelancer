// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function abl_comp_Effects(a_parent) : base_AbilityComponent(a_parent) constructor
{

}

function abl_comp_Projectile(a_parent, a_proj, a_projCount) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_proj = a_proj;
	abl_comp_projCount = 1;
	if(a_projCount != undefined) abl_comp_projCount = a_projCount;
	
	
	abl_comp_OnActivate = function()
	{
		for(var i = 1; i <= abl_comp_projCount; i++)
		{
			var tSpread = parentAbility.abl_bpStats.bp_Spread * abl_comp_projCount;		
			var tRot = owner.direction + (CalcFixedSpread(i,tSpread,abl_comp_projCount));
			tRot += irandom_range(-parentAbility.abl_bpStats.bp_Spread/2,parentAbility.abl_bpStats.bp_Spread/2);
			var tPos = new Vector2(owner.char_offset.x + parentAbility.abl_posOffset.x, owner.char_offset.y + parentAbility.abl_posOffset.y);
		
			tInst = CreateLiteProjectile(abl_comp_proj,parentAbility,parentAbility.abl_CalcPowerScale ,tPos,tRot, parentAbility.abl_bpStats);
			ds_list_add(parentAbility.abl_Instances,tInst);		
		}
	}
}

function abl_comp_Attack(a_parent, a_attack) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_atk = a_attack;
	
	abl_comp_Init = function()
	{
		atk_InitAttack(parentAbility, abl_comp_atk);
	}
	
	abl_comp_Update = function()
	{
		parentAbility.abl_sfx = abl_comp_atk.atk_sfx;
		atk_UpdateAtk(abl_comp_atk);
	}
	
	abl_comp_OnActivate = function()
	{
		atk_ExecuteAttack(abl_comp_atk);
		parentAbility.abl_currAtk = abl_comp_atk;
	}
}

function abl_comp_SelfBuff(a_parent, a_modifier, a_duration, a_stacks, a_addStack) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_stacks = 1;
	abl_comp_ModToApply = undefined;
	abl_comp_dur = 0;
	abl_comp_addStack = false;
	if(a_modifier != undefined) abl_comp_ModToApply = a_modifier;
	if(a_duration != undefined) abl_comp_dur = a_duration;
	if(a_stacks != undefined) abl_comp_stacks = a_stacks;
	if(a_addStack != undefined) abl_comp_addStack = a_addStack;
	abl_comp_OnActivate = function()
	{
		if(!is_undefined(abl_comp_ModToApply))
		{
			var tDur = abl_comp_dur*parentAbility.abl_CalcDurScale;
			AttachMod(abl_comp_ModToApply,owner,owner,tDur, abl_comp_stacks, abl_comp_addStack);
		}
	}
}

function abl_comp_LoadoutSwap(a_parent, a_loadout) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_loadout = a_loadout;
		
	abl_comp_OnActivate = function()
	{
		var tMod = AttachMod(buff_AltLoadout, owner, owner, infinity);
		Copy_Struct(tMod.mod_ld, abl_comp_loadout);
	}
}

function abl_comp_UnequipLoadout(a_parent) : abl_comp_Effects(a_parent) constructor
{
	
	abl_comp_OnActivate = function()
	{
		var tMod = FindMod(owner.char_modArr, "Alt Loadout");
		if(tMod != undefined) tMod.duration = 0;
		owner.char_hasAltLoadout = false;
	}
}

function abl_comp_Evade(a_parent, a_rollDur, a_rollDist, a_evadeDur) : abl_comp_Effects(a_parent) constructor
{
	
	abl_comp_rollDur = 1;
	abl_comp_rollTimer = 0
	abl_comp_rollDist = 40;
	abl_comp_evadeDur = 1;
	
	abl_comp_hitList = ds_list_create();
	
	if(a_rollDur != undefined) abl_comp_rollDur = a_rollDur;
	if(a_rollDist != undefined) abl_comp_rollDist = a_rollDist;
	if(a_evadeDur != undefined) abl_comp_evadeDur = a_evadeDur;
	else abl_comp_evadeDur = abl_comp_rollDur;
	abl_comp_rollSpeed = ((abl_comp_rollDist * (1/abl_comp_rollDur)))/global.targetFPS;

	abl_comp_rollDirection = 0;
	
	
	abl_comp_OnActivate = function()
	{
		
		if(owner.mx == 0 && owner.my == 0)	//Check if movement input is present, otherwise dodge backwards
			{
				abl_comp_rollDirection = owner.direction - 180;	
			}else
			{
				abl_comp_rollDirection = GetDirection(owner.x,owner.y,owner.mx,owner.my,true, true);
			}
			rollTimer = 0;
			//Caps the maximum I-Frames to 10% longer than the roll duration
			var evadeDur = clamp(abl_comp_evadeDur * parentAbility.abl_CalcDurScale, 0, abl_comp_rollDur*1.1);
			var tForce = abl_comp_rollSpeed;// * GetFixedDeltaTime();
			AttachLinearForce(owner, owner, abl_comp_rollDur, abl_comp_rollDirection, tForce);
			AttachMod(buff_evade, owner, owner, evadeDur);
			AttachMod(debuff_moveLocked, owner, owner, abl_comp_rollDur);	
			AttachMod(debuff_abilityLocked, owner, owner, abl_comp_rollDur);	
	}
	
	abl_comp_Update = function()
	{
		abl_comp_rollTimer += GetDeltaTime(owner.timeScale);
		if(abl_comp_rollTimer < abl_comp_rollDur) return false;
		else return true;
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 0;
		parentAbility.ai_CanAttack = false;
		
		//Check for incoming hostile hitboxes
		ds_list_clear(abl_comp_hitList);
		collision_circle_list(owner.x, owner.y, abl_comp_rollDist/4, defunc_base_DamageBox,false, true, abl_comp_hitList, false);
		for(var i = 0; i < ds_list_size(abl_comp_hitList); i += 1)
		{
			var hitObj = abl_comp_hitList[|i];
			if(hitObj.teamID != owner.teamID)
			{
				if(hitObj.dmg_canDamage)
				{
					parentAbility.ai_CanAttack = true;
					retVal += 4;
				}
			}
		}
		/*
		ds_list_clear(abl_comp_hitList);
		//Check for nearby hostiles that are attacking
		collision_circle_list(owner.x, owner.y, abl_comp_rollDist/4, base_Character,false, true, abl_comp_hitList, false);
		
		for(var i = 0; i < ds_list_size(abl_comp_hitList); i += 1)
		{
			var hitObj = abl_comp_hitList[|i];
			if(hitObj.teamID != owner.teamID)
			{
				if(hitObj.char_preppingAttack && hitObj.char_atkCharge < 0.1) retVal += 4;
			}
		}
		*/
		if(retVal >= 2) parentAbility.ai_forceUsage = true;
		else parentAbility.ai_forceUsage = false;
		return retVal;
	}
	
	abl_comp_AIOnFire = function(a_ai)
	{
		with a_ai
		{
			
			var tDir = point_direction(slavedObj.x, slavedObj.y, currentTarget.x, currentTarget.y)- 90;
			if(currentTarget != allyTarget && ai_CurrentState != AI_STATE.IDLE)
			{
				tDir += choose(-45,45);
				if(ai_CurrentState == AI_STATE.FIGHT && slavedObj.char_Ranged == true) tDir -= 180;	//Dash away from the target
			}
			var tVec = GetCoord(tDir);
			slavedObj.in_mx = tVec.x;
			slavedObj.in_my = tVec.y;
		}
	}
}

function abl_comp_Storm(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnKill = function(a_hitReport)
	{
		AttachMod(buff_StormDrake, parentAbility.owner, parentAbility.owner);	
	}
	
	abl_comp_Update = function()
	{
		var tMod = FindMod(parentAbility.owner.char_modArr, "Storm Drake");
		if(tMod != undefined)
		{
			parentAbility.abl_CalcSpeedScale += 0.2+(tMod.stacks/50);
		}
	}
}

function abl_comp_Drake(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_Update = function()
	{
		var tMod = FindMod(parentAbility.owner.char_modArr, "Storm Drake")
		if(tMod != undefined)
		{
			parentAbility.abl_CalcPowerScale *= 4* clamp(tMod.stacks,1, 10);
			tMod.duration = 60;
		}
	}
	
	abl_comp_OnActivate = function()
	{
		var tMod = FindMod(parentAbility.owner.char_modArr, "Storm Drake");
		if(tMod != undefined)
		{
			tMod.stacks -= 5;
			if(tMod.stacks <= 0) tMod.duration = 0;
			audio_play_sound(sfx_sword_long, 5, false);
		}
	}
}

function abl_comp_Harpoon(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnHit = function(a_hitReport)
	{
		var tMod = AttachMod(debuff_harpooned, a_hitReport.hit_Subject, parentAbility.owner, 3 * parentAbility.abl_CalcDurScale, 1);	
		tMod.origin = new Vector2(parentAbility.owner.x, parentAbility.owner.y);
	}
}

function abl_comp_Dragnet(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnHit = function(a_hitReport)
	{
		var tMod = AttachMod(debuff_harpooned, a_hitReport.hit_Subject, parentAbility.owner, 3 * parentAbility.abl_CalcDurScale, 1);
	}	
}

function abl_comp_Tranq(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnHit = function(a_hitReport)
	{
		var tMod = AttachMod(debuff_tranq, a_hitReport.hit_Subject, parentAbility.owner, 30, parentAbility.abl_CalcPowerScale, true);
	}		
}

function abl_comp_KO(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_Update = function()
	{
		parentAbility.abl_DmgType = DMG_TYPE.SOFT;	
	}
	
	abl_comp_OnHit = function(a_hitReport)
	{
		//var tMod = AttachMod(debuff_KO, a_hitReport.hit_Subject, parentAbility.owner, 10, parentAbility.abl_CalcPowerScale, true);
	}		
}

function abl_comp_ExposedAttack(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnAblEnd = function()
	{
		AttachMod(debuff_exposedAttack, parentAbility.owner, parentAbility.owner, 0.5, 1, false);	
	}
}

function abl_comp_KobaldRegroup(a_parent) : abl_comp_Effects(a_parent) constructor
{
	abl_comp_OnActivate = function()
	{
		var tMod = FindMod(parentAbility.owner.char_modArr, "Kobald Trio");
		if(tMod != undefined)
		{
			for(var i = 0; i < array_length(tMod.kbld_Chars); i += 1)
			{
				var tChar = tMod.kbld_Chars[i];
				tChar.x = parentAbility.owner.x;
				tChar.y = parentAbility.owner.y;
				
				AttachMod(buff_hpRegen, tChar, parentAbility.owner, 1, 10, true);
				AttachMod(debuff_abilityLocked, tChar, parentAbility.owner, 1);
				AttachMod(buff_evade, tChar, parentAbility.owner, 1);
				AttachMod(buff_invis, tChar, parentAbility.owner, 1);
				AttachMod(buff_KobaldTunnel, tChar, parentAbility.owner, 1);
			}
		}
	}
}
