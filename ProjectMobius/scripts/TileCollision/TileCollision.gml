// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
enum TILE_PHYS
{
	EMPTY = 0,
	GAP = 1,
	WALL = 2,
	BARRIER = 3,
}

function TileMeetingPixel(a_vec, a_hitList)
{
	isColl = false;
	var tm = layer_tilemap_get_id("Tiles_Physics");
	if(a_hitList != undefined)
	{
		var tmPos = tilemap_get_at_pixel(tm, a_vec.x, a_vec.y);
		for(var i = 0; i < array_length(a_hitList); i += 1)
		{
			var tRes = a_hitList[i];
			if(tmPos == tRes) isColl = true;
		}
	}else if(tilemap_get_at_pixel(tm, a_vec.x,a_vec.y) != 0) isColl = true;
	return isColl;
}

function TileMeetingBox(bbL,bbR,bbT,bbB, a_hitList)
{
	isColl = false;
	var tm = layer_tilemap_get_id("Tiles_Physics");
	if(a_hitList != undefined)
	{
		var tmLT = tilemap_get_at_pixel(tm, bbL, bbT);
		var tmLB = tilemap_get_at_pixel(tm, bbL, bbB);
		var tmRT = tilemap_get_at_pixel(tm, bbR, bbT);
		var tmRB = tilemap_get_at_pixel(tm, bbR, bbB);
		for(var i = 0; i < array_length(a_hitList); i += 1)
		{
			var tRes = a_hitList[i];
			if(tmLT == tRes || tmLB == tRes || tmRT == tRes || tmRB = tRes)	isColl = true;
		}
	}else
	{
	if(	tilemap_get_at_pixel(tm, bbL,bbT) != 0 ||
			tilemap_get_at_pixel(tm,bbR,bbT) != 0 ||
			tilemap_get_at_pixel(tm, bbL,bbB) != 0 ||
			tilemap_get_at_pixel(tm, bbR,bbB) != 0)
		isColl = true;
	}
	return isColl;
	
}

function OutsideBoundry(a_pos)
{
	if(a_pos.x < 0 || a_pos.x >= room_width	|| a_pos.y < 0 || a_pos.y >= room_height) return true;
	else return false;
}


function TileMeeting(a_obj,a_distX, a_distY)
{
	var isColl = false;
	var dX = a_distX;
	var dY = a_distY;
	var tm = layer_tilemap_get_id("Tiles_Physics");
	
	if(dX < 0) dX = floor(dX);
	else dX = ceil(dX);
	if(dY < 0) dY = floor(dY);
	else dY = ceil(dY);
	
	
	with a_obj
	{
		if(!(bbox_left < 0 || bbox_right >= room_width) && !(bbox_top < 0 || bbox_bottom >= room_height))	//Ensures the obj is within the boundries of the map, ignores collisions if not
		{
		if(	tilemap_get_at_pixel(tm, bbox_left + dX, bbox_top + dY) != 0 ||
			tilemap_get_at_pixel(tm, bbox_right+ dX, bbox_top + dY) != 0 ||
			tilemap_get_at_pixel(tm, bbox_left+ dX, bbox_bottom + dY) != 0 ||
			tilemap_get_at_pixel(tm, bbox_right+ dX, bbox_bottom + dY) != 0)
		isColl = true;
		}
		
	}
	return isColl;
}

function TileMeeting_Arr(a_obj,a_distX, a_distY, a_hitList)
{
	var isColl = true;
	var dX = a_distX;
	var dY = a_distY;
	var tm = layer_tilemap_get_id("Tiles_Physics");
	with a_obj
	{
		for(var i = 0; i < array_length(a_hitList); i += 1)
		{
			if(	tilemap_get_at_pixel(tm, bbox_left + dX, bbox_top + dY) == a_hitList[i] ||
				tilemap_get_at_pixel(tm, bbox_right+ dX, bbox_top + dY) == a_hitList[i] ||
				tilemap_get_at_pixel(tm, bbox_left+ dX, bbox_bottom + dY) == a_hitList[i] ||
				tilemap_get_at_pixel(tm, bbox_right+ dX, bbox_bottom + dY) == a_hitList[i])
			isColl = false;
		}
		
	}
	return !isColl;
}