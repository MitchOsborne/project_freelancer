// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Pkup_sCredit() : base_CreditPickup() constructor
{
	credAmount = 1;
	pkup_sprite = spr_bCred;
	pkup_sfx = sfx_exp_s;
}
function Pkup_mCredit() : base_CreditPickup() constructor
{
	credAmount = 25;
	pkup_sprite = spr_sCred;
	pkup_sfx = sfx_exp_m;
}
function Pkup_lCredit() : base_CreditPickup() constructor
{
	credAmount = 100;
	pkup_sprite = spr_gCred;
	pkup_sfx = sfx_exp_l;
}

function Pkup_sXP() : defunc_base_XPPickup() constructor
{
	xpAmount = 1;
	pkup_sprite = spr_sXP;
	pkup_sfx = sfx_exp_s;
}
function Pkup_mXP() : defunc_base_XPPickup() constructor
{
	xpAmount = 25;
	pkup_sprite = spr_mXP;
	pkup_sfx = sfx_exp_m;
}
function Pkup_lXP() : defunc_base_XPPickup() constructor
{
	xpAmount = 225;
	pkup_sprite = spr_lXP;
	pkup_sfx = sfx_exp_l;
}

function Pkup_sKey() : base_KeyPickup() constructor
{
	keyType = "small"
	pkup_sprite = spr_sKey;
	pkup_sfx = sfx_key;
}

function Pkup_HPBox() : base_Pickup() constructor
{
	
	pkup_sprite = spr_MedBox;
	pkup_sfx = sfx_key;
	function OnHPPickup(a_pkup)
	{
		with a_pkup
		{
			if(isCollected)
			{
				for(var i = 0; i < instance_number(base_Character); i += 1)
				{
					var tInst = instance_find(base_Character,i);
					if(tInst.teamID == TEAMID.Ally)tInst.hp += tInst.char_maxHP*0.25;
				}
				//if(global.Player1ID != undefined && global.Player1ID.slavedObj != undefined)
				//{
				//	global.Player1ID.slavedObj.hp += global.Player1ID.slavedObj.char_maxHP*0.25;	
				//	var tMin = global.Player1ID.slavedObj.smn_Minion;
				//	if(CheckExistance(tMin)) tMin.hp += tMin.char_maxHP*0.25;
				//}
				//
				//if(global.Player2ID != undefined && global.Player2ID.slavedObj != undefined)
				//{
				//	global.Player2ID.slavedObj.hp += global.Player2ID.slavedObj.char_maxHP*0.25;
				//	var tMin = global.Player2ID.slavedObj.smn_Minion;
				//	if(CheckExistance(tMin)) tMin.hp += tMin.char_maxHP*0.25;
				//}
				
				lifeTimer = lifeTime;
			}
			Pickup_EndUpdate(self);
		}
	}
	
	func_EndUpdateStep = OnHPPickup;
}

enum STAT_CORE
{
	ATK,
	DEF,
	ABL,
	LUCK,
	RIVEN,
}

function Pkup_ATKCore() : Pkup_StatCore() constructor
{
	pkup_sprite = spr_ATKUp;
	pkup_statObj = STAT_CORE.ATK;	
}

function Pkup_DEFCore() : Pkup_StatCore() constructor
{
	pkup_sprite = spr_HPUp;
	pkup_statObj = STAT_CORE.DEF;	
}

function Pkup_ABLCore() : Pkup_StatCore() constructor
{
	pkup_sprite = spr_ABLUp;
	pkup_statObj = STAT_CORE.ABL;		
}

function Pkup_LUCKCore() : Pkup_StatCore() constructor
{
	pkup_sprite = spr_CRITUp;
	pkup_statObj = STAT_CORE.LUCK;	
}


function Pkup_RivenCore() : Pkup_StatCore() constructor
{
	pkup_sprite = spr_RANDUp;
	pkup_statObj = STAT_CORE.RIVEN;	
}

function Pkup_StatCore() : base_Pickup() constructor
{
	pkup_sprite = spr_RANDUp;
	pkup_statObj = undefined;
	pkup_sfx = sfx_key;
	function OnStatPickup(a_pkup)
	{	
		with a_pkup
		{
			if(pkup_statObj != undefined)
			{
				if(isCollected)
				{
					switch pkup_statObj
					{
					case STAT_CORE.ATK:
					global.inv_upgradeParts.ATK += 5;
					break;
					case STAT_CORE.DEF:
					global.inv_upgradeParts.DEF += 5;
					break;
					case STAT_CORE.ABL:
					global.inv_upgradeParts.ABL += 5;
					break;
					case STAT_CORE.LUCK:
					global.inv_upgradeParts.LUCK += 5;
					break;
					case STAT_CORE.RIVEN:
					global.inv_upgradeParts.RIVEN += 5;
					break;
					default:
					break;
					}
					lifeTimer = lifeTime;
				}
				Pickup_EndUpdate(self);
			}
		}
	}
	
	func_EndUpdateStep = OnStatPickup;
}