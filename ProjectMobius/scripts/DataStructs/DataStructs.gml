// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function CharData(a_name, a_obj, a_anim, a_ld) constructor{
	
	charData_Name = "Unnamed Char";
	charData_Object = undefined;
	charData_Animator = undefined;
	charData_Loadout = new Loadout();
	
	if(a_name != undefined) charData_Name = a_name;
	if(a_obj != undefined) charData_Object = a_obj;
	if(a_anim != undefined) charData_Animator = a_anim;
	if(a_ld != undefined) charData_Loadout = a_ld;
}

function DMGReport (a_dmgSoft, a_dmgHard, a_dmgTher, a_dmgEM) constructor
{
	dmg_Soft = a_dmgSoft;
	dmg_Hard = a_dmgHard;
	dmg_Ther = a_dmgTher;
	dmg_EM = a_dmgEM;
}

function HitReport(a_proj, a_weapon, a_subject, a_dmg, a_dir, a_dmgType) constructor
{
	hit_Proj = a_proj;
	hit_Weapon = a_weapon;
	hit_Owner = a_weapon.owner;
	hit_Subject = a_subject;
	hit_Dmg = a_dmg;
	hit_DmgType = a_dmgType;
	hit_Dir = a_dir;
}

function GetStructVariables(a_struct)
{
	var tArr = variable_struct_get_names(a_struct);
	var retArr = [];
	
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		array_push(retArr,variable_struct_get(a_struct, tArr[i]));
	}
	return retArr;
}

function Loadout(a_w1, a_w2, a_u1, a_u2, a_a1, a_a2, a_core, a_class) constructor
{
	ld_slots =
	{
		ld_wep1 : a_w1,
		ld_wep2 : a_w2,
		ld_util1 : a_u1,
		ld_util2 : a_u2,
		ld_abl1 : a_a1,
		ld_abl2 : a_a2,
		ld_core : a_core,
		ld_class : a_class,
	}
	
	ld_Keywords = [];
}

enum LDKEYWORDS
{
	FREELANCER,
	FIGHTER,
	HUNTER,
	SUPPORT,
	SPECIALIST,
	SHEPHERD,
	KAMUI,
	YAKI,
	WILDLIFE,
}

enum RENDERTYPE
{
	SPRITE,
	RAY,
}

function RenderCall(a_type, a_sprite, a_shader, a_Data) constructor
{
	rend_Type = a_type;
	rend_Sprite = a_sprite;
	rend_Shader = a_shader;
	rend_Data = a_Data;
}
	
function RenderData(a_SprIndex, a_Position, a_Scale, a_Rot, a_Col, a_RayEnd) constructor
{
	rendData_SprIndex = a_SprIndex;
	rendData_Position = a_Position;
	rendData_Scale = a_Scale;
	rendData_Rotation = a_Rot;
	rendData_Colour = a_Col;
	rendData_RayEnd = a_RayEnd;
}

function Tombstone(a_charData, a_pos, a_type, a_ptrlID) constructor
{
	tmb_pos = a_pos;
	tmb_charData = a_charData;
	tmb_type = a_type;
	tmb_patrolID = a_ptrlID;
	
	tmb_KODur = 60;
	tmb_Tmr = 0;
	tmb_Delete = false;
	tmb_Update = function()
	{
		if(tmb_type != DMG_TYPE.HARD)
		{
			tmb_Tmr += GetDeltaTime(1);	
			if(tmb_Tmr >= tmb_KODur) 
			{
				tmb_Delete = true;
			}
		}
	}
	
	array_push(global.TombstoneArr, self);
}



