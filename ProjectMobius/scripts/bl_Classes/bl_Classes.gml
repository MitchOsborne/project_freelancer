// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Internal_bp_Init_Classes()
{
	with global.bp_Abilities.bp_Class_Freelancer
	{
		bp_Name = "Freelancer";
		bp_Slot = LOADOUT_SLOT.CLASS;
		bp_Class = global.ClassCards.crd_Freelancer;
		bp_Keywords = [LDKEYWORDS.FREELANCER];
	}
	
	with global.bp_Abilities.bp_Class_Fighter
	{
		bp_Name = "Gladiator";
		bp_Slot = LOADOUT_SLOT.CLASS;
		bp_Class = global.ClassCards.crd_Gladiator;
		bp_Keywords = [LDKEYWORDS.FIGHTER];
	}
	
	with global.bp_Abilities.bp_Class_Hunter
	{
		bp_Name = "Rogue";
		bp_Slot = LOADOUT_SLOT.CLASS;
		bp_Class = global.ClassCards.crd_Rogue;
		bp_Keywords = [LDKEYWORDS.HUNTER];
	}
}