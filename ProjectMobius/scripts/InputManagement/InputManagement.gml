// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function InputSource(a_contIndex) constructor
{
	
	in_InputType = CONT_MODE.NONE;
	in_InputIndex = a_contIndex;
	in_PlayerIndex = -1;
	
	in_LUP = 0;
	in_LDOWN = 0;
	in_LRIGHT = 0;
	in_LLEFT = 0;
	
	in_RUP = 0;
	in_RDOWN = 0;
	in_RRIGHT = 0;
	in_RLEFT = 0;
	
	in_ACCEPT = 0;
	in_CANCEL = 0;
	in_WEAPON1 = 0;
	in_WEAPON2 = 0;
	in_UTILITY1 = 0;
	in_UTILITY2 = 0;
	in_ABILITY1 = 0;
	in_ABILITY2 = 0;
	in_CORE = 0;
	in_INTERACT = 0;
	
	in_PAUSE = 0;
	
	in_LOCK = 0;
	in_ALT = 0;
	
	func_InputUpdate = Input_Update;
}

function Input_None(a_pIndex) : InputSource(a_pIndex) constructor
{
	in_InputType = CONT_MODE.NONE;
}

function Input_KBM(a_contIndex) : InputSource(a_contIndex) constructor
{
	in_InputType = CONT_MODE.KB_M;
	func_InputUpdate = Input_UpdateKB;
}

function Input_Controller(a_contIndex) : InputSource(a_contIndex) constructor
{
	in_InputType = CONT_MODE.GAMEPAD;
	func_InputUpdate = Input_UpdateCont;
	gamepad_set_axis_deadzone(in_InputIndex,0.3);
}

function Input_Update(a_input)
{
	with a_input
	{
		//do stuff here	
	}
}

function Input_UpdateKB(a_input)
{
	with a_input
	{
		Input_ResetInputs(self);
		
		window_set_cursor(cr_none);
		in_LLEFT	= keyboard_check(ord("A"));
		in_LRIGHT	= keyboard_check(ord("D"));
		in_LUP	= keyboard_check(ord("W"));
		in_LDOWN	= keyboard_check(ord("S"));
		
		in_ALT	= (keyboard_check(vk_shift) || keyboard_check(vk_lcontrol));
		in_LOCK	= keyboard_check(vk_rshift);
			
		in_WEAPON1	= mouse_check_button(mb_left);
		in_ACCEPT = keyboard_check(vk_return) || in_WEAPON1;
		in_CANCEL = keyboard_check(vk_escape) || keyboard_check(vk_backspace);
		in_UTILITY1 = (keyboard_check(ord("V")) || keyboard_check(vk_space));
		in_ABILITY1 = mouse_check_button(mb_right);
		in_INTERACT = keyboard_check(ord("F")) || keyboard_check(ord("E"));
		
		in_PAUSE = keyboard_check(vk_escape);
		
		in_CORE = (in_INTERACT && in_ALT);
		
	}
}

function Input_UpdateCont(a_input)
{
	with a_input
	{
		Input_ResetInputs(self);
		
		in_LLEFT = clamp(gamepad_axis_value(in_InputIndex,gp_axislh), -1, 0);
		in_LRIGHT= clamp(gamepad_axis_value(in_InputIndex,gp_axislh), 0, 1);
		in_LUP = clamp(gamepad_axis_value(in_InputIndex,gp_axislv), -1, 0);
		in_LDOWN = clamp(gamepad_axis_value(in_InputIndex,gp_axislv), 0, 1);
		
		in_RLEFT = clamp(gamepad_axis_value(in_InputIndex,gp_axisrh), -1, 0);
		in_RRIGHT = clamp(gamepad_axis_value(in_InputIndex,gp_axisrh), 0, 1);
		in_RUP = clamp(gamepad_axis_value(in_InputIndex,gp_axisrv), -1, 0);
		in_RDOWN = clamp(gamepad_axis_value(in_InputIndex,gp_axisrv), 0, 1);
		
		in_ALT = gamepad_button_check(in_InputIndex,gp_shoulderrb)
		in_LOCK = gamepad_button_check(in_InputIndex,gp_shoulderlb);
		in_WEAPON1 = (gamepad_button_check(in_InputIndex,gp_face3) || gamepad_button_check(in_InputIndex,gp_shoulderl));
		in_WEAPON2 = gamepad_button_check(in_InputIndex,gp_face4);
		in_UTILITY1 = (gamepad_button_check(in_InputIndex,gp_face1) || gamepad_button_check(in_InputIndex,gp_shoulderr));
		in_UTILITY2 = in_ALT && in_UTILITY1;
		in_ABILITY1 = in_ALT && in_WEAPON1;
		in_ABILITY2 = in_ALT && in_WEAPON2;
		in_INTERACT = gamepad_button_check(in_InputIndex,gp_face2);
		in_CORE = (in_INTERACT && in_ALT);
		
		in_ACCEPT = in_UTILITY1;
		in_CANCEL = in_INTERACT;
		
		in_PAUSE = gamepad_button_check(in_InputIndex,gp_start);
		
	}
}

function Input_ResetInputs(a_input)
{
	with a_input
	{	
		in_LUP = 0;
		in_LDOWN = 0;
		in_LRIGHT = 0;
		in_LLEFT = 0;
	
		in_RUP = 0;
		in_RDOWN = 0;
		in_RRIGHT = 0;
		in_RLEFT = 0;
	
		in_ACCEPT = 0;
		in_CANCEL = 0;
		in_WEAPON1 = 0;
		in_UTILITY1 = 0;
		in_ABILITY1 = 0;
		in_CORE = 0;
		in_INTERACT = 0;
	
		in_PAUSE = 0;
	
		in_LOCK = 0;
		in_ALT = 0;
	}
}


function PollNewInputs()
{
	var contCount = gamepad_get_device_count();
	var retVal = undefined;
	for(var i = 0; i < contCount; i += 1)
	{
		if(gamepad_is_connected(i))
		{
			if(gamepad_button_check_pressed(i,gp_face1))
			{
				var freeCont = CheckInput(CONT_MODE.GAMEPAD, i);
				if(freeCont)
				{
					retVal = new Input_Controller(i);
					break;
				}
			}
		}
	}
	
	if(retVal == undefined)
	{
		if(keyboard_check_pressed(vk_enter))
		{
			var freeCont = CheckInput(CONT_MODE.KB_M, 0);
			if(freeCont) retVal = new Input_KBM(0);
		}
	}
	
	return retVal;
}

function CheckInput(a_InputType, a_InputIndex)
{
	
	var freeCont = true;
	for(var n = 0; n < ds_list_size(global.PlayerIDList); n += 1)
	{
		var tP = global.PlayerIDList[|n];
		if(tP.inputSource.in_InputType == a_InputType && tP.inputSource.in_InputIndex == a_InputIndex) freeCont = false;
	}
	return freeCont;
	

	
	/*
	var contCount = gamepad_get_device_count();
	for(var i = 0; i < contCount; i += 1)
	{
		if(gamepad_is_connected(i))
		{
			if(gamepad_button_check_pressed(i,gp_face1))
			{
				
			}
		}
	}
	
	//Makes sure Player 2 isn't using the KB_M before assigning it to Player 1
	if(global.p2Cont == undefined || global.p2Cont.in_InputType != CONT_MODE.KB_M)
	{
		if(keyboard_check_pressed(vk_enter))
		{
			global.p1Cont = new Input_KBM(1);
		}
	}
	
	
	if(global.p2Cont == undefined)
	{
		var contCount = gamepad_get_device_count();
		for(var i = 0; i < contCount; i += 1)
		{
			if(gamepad_is_connected(i))
			{
				if(global.p1Cont == undefined ||
				(global.p1Cont.in_InputType != CONT_MODE.GAMEPAD || i != global.p1Cont.in_InputIndex))
				{
					if(gamepad_button_check_pressed(i,gp_face1))
					{
						global.p2Cont = new Input_Controller(2, i);
						break;
					}
				}
			}
		}
		if(global.p1Cont == undefined || global.p1Cont.in_InputType != CONT_MODE.KB_M)
		{
			if(keyboard_check_pressed(vk_enter))
			{
				global.p2Cont = new Input_KBM(1);
			}
		}
	}
	*/
}

