// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_CharacterComponent() constructor{	
	
	x = 0;
	y = 0;
	timeScale = 1;
	
	slavedObj = undefined;
	
	func_PreUpdateStep = char_comp_PreUpdateStep;
	func_UpdateStep = char_comp_UpdateStep;
	func_EndUpdateStep = char_comp_EndUpdateStep;
	func_DrawStep = char_comp_DrawStep;
	
}

function char_comp_PreUpdateStep(a_comp)
{
	x = slavedObj.x;
	y = slavedObj.y;
	//timeScale = slavedObj.timeScale;
}

function char_comp_UpdateStep(a_comp)
{
	
}

function char_comp_EndUpdateStep(a_comp)
{
		
}

function char_comp_DrawStep(a_comp)
{
	
}