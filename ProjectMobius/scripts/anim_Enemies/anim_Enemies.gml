// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function anim_YakiHat() : base_Animator() constructor{
	
	anim_default = spr_Yaki_idle;

	anim_idle_front = spr_Yaki_idle;
	anim_idle_back = spr_Yaki_idle;

	anim_walk_front = spr_Yaki_walk;
	anim_walk_back = spr_Yaki_walk;

	anim_dash_front = spr_Yaki_dash;
	anim_dash_back = spr_Yaki_dash;

	anim_incap = spr_Yaki_incap;
	anim_stun_front = spr_Yaki_dash;
	anim_stun_back = spr_Yaki_dash;
}

function anim_YakiHood() : base_Animator() constructor{
	
	anim_default = spr_YakiHood_idle;

	anim_idle_front = spr_YakiHood_idle;
	anim_idle_back = spr_YakiHood_idle;

	anim_walk_front = spr_YakiHood_walk;
	anim_walk_back = spr_YakiHood_walk;

	anim_dash_front = spr_YakiHood_dash;
	anim_dash_back = spr_YakiHood_dash;

	anim_incap = spr_YakiHood_incap;
	anim_stun_front = spr_YakiHood_dash;
	anim_stun_back = spr_YakiHood_dash;
}

function anim_NezumiRogue() : base_Animator() constructor{
	
	anim_default = spr_NezumiRogue_idle;

	anim_idle_front = spr_NezumiRogue_idle;
	anim_idle_back = spr_NezumiRogue_idle;

	anim_walk_front = spr_NezumiRogue_walk;
	anim_walk_back = spr_NezumiRogue_walk;

	anim_dash_front = spr_NezumiRogue_dash;
	anim_dash_back = spr_NezumiRogue_dash;

	anim_incap = spr_NezumiRogue_incap;
	anim_stun_front = spr_NezumiRogue_dash;
	anim_stun_back = spr_NezumiRogue_dash;
}

function anim_NezumiLeader() : base_Animator() constructor{
	
	anim_default = spr_NezumiLeader_idle;
	anim_scale = 1.5;
	anim_idle_front = spr_NezumiLeader_idle;
	anim_idle_back = spr_NezumiLeader_idle;

	anim_walk_front = spr_NezumiLeader_walk;
	anim_walk_back = spr_NezumiLeader_walk;

	anim_dash_front = spr_NezumiLeader_dash;
	anim_dash_back = spr_NezumiLeader_dash;

	anim_incap = spr_NezumiLeader_incap;
	anim_stun_front = spr_NezumiLeader_dash;
	anim_stun_back = spr_NezumiLeader_dash;
}

function anim_KitsuneSamurai() : base_Animator() constructor{
	
	anim_default = spr_KitsuneSamurai_idle;

	anim_idle_front = spr_KitsuneSamurai_idle;
	anim_idle_back = spr_KitsuneSamurai_idle;

	anim_walk_front = spr_KitsuneSamurai_walk;
	anim_walk_back = spr_KitsuneSamurai_walk;

	anim_dash_front = spr_KitsuneSamurai_dash;
	anim_dash_back = spr_KitsuneSamurai_dash;

	anim_incap = spr_KitsuneSamurai_incap;
	anim_stun_front = spr_KitsuneSamurai_dash;
	anim_stun_back = spr_KitsuneSamurai_dash;
}

function anim_KitsuneOracle() : base_Animator() constructor{
	
	anim_default = spr_KitsuneOracle_idle;

	anim_idle_front = spr_KitsuneOracle_idle;
	anim_idle_back = spr_KitsuneOracle_idle;

	anim_walk_front = spr_KitsuneOracle_walk;
	anim_walk_back = spr_KitsuneOracle_walk;

	anim_dash_front = spr_KitsuneOracle_dash;
	anim_dash_back = spr_KitsuneOracle_dash;

	anim_incap = spr_KitsuneOracle_incap;
	anim_stun_front = spr_KitsuneOracle_dash;
	anim_stun_back = spr_KitsuneOracle_dash;
}

function anim_Doggo() : base_Animator() constructor{
	
	anim_default = spr_doggo_idle;

	anim_idle_front = spr_doggo_idle;
	anim_idle_back = spr_doggo_idle;

	anim_walk_front = spr_doggo_walk;
	anim_walk_back = spr_doggo_walk;

	anim_dash_front = spr_doggo_dash;
	anim_dash_back = spr_doggo_dash;

	anim_incap = spr_doggo_incap;
	anim_stun_front = spr_doggo_dash;
	anim_stun_back = spr_doggo_dash;
}

function anim_Bat() : base_Animator() constructor{
	
	anim_scale = 4;
	anim_default = spr_bat;

	anim_idle_front = spr_bat;
	anim_idle_back = spr_bat;

	anim_walk_front = spr_bat;
	anim_walk_back = spr_bat;

	anim_dash_front = spr_bat;
	anim_dash_back = spr_bat;

	anim_incap = spr_bat;
	anim_stun_front = spr_bat;
	anim_stun_back = spr_bat;
}

function anim_FlamerTurret() : base_Animator() constructor
{
	anim_scale = 1;
	anim_default = spr_FlamerTurret_Idle;

	anim_idle_front = spr_FlamerTurret_Idle;
	anim_idle_back = spr_FlamerTurret_Idle;

	anim_walk_front = spr_FlamerTurret_Idle;
	anim_walk_back = spr_FlamerTurret_Idle;

	anim_dash_front = spr_FlamerTurret_Idle;
	anim_dash_back = spr_FlamerTurret_Idle;

	anim_incap = spr_FlamerTurret_Incap;
	anim_stun_front = spr_FlamerTurret_Stun;
	anim_stun_back = spr_FlamerTurret_Stun;
}


function anim_Vespid() : base_Animator() constructor
{
	anim_scale = 2;
	anim_default = spr_vespid;

	anim_idle_front = spr_vespid;
	anim_idle_back = spr_vespid;

	anim_walk_front = spr_vespid;
	anim_walk_back = spr_vespid;

	anim_dash_front = spr_vespid;
	anim_dash_back = spr_vespid;

	anim_incap = spr_vespid;
	anim_stun_front = spr_vespid;
	anim_stun_back = spr_vespid;
}

function anim_Zeemer() : base_Animator() constructor{
	
	anim_scale = 1;
	anim_default = spr_Zeemer_idle;

	anim_idle_front = spr_Zeemer_idle;
	anim_idle_back = spr_Zeemer_idle;

	anim_walk_front = spr_Zeemer_idle;
	anim_walk_back = spr_Zeemer_idle;

	anim_dash_front = spr_Zeemer_idle;
	anim_dash_back = spr_Zeemer_idle;

	anim_incap = spr_Zeemer_incap;
	anim_stun_front = spr_Zeemer_idle;
	anim_stun_back = spr_Zeemer_idle;
}

function anim_Goomer() : base_Animator() constructor{
	
	anim_scale = 1;
	anim_default = spr_Goomer_idle;

	anim_idle_front = spr_Goomer_idle;
	anim_idle_back = spr_Goomer_idle;

	anim_walk_front = spr_Goomer_idle;
	anim_walk_back = spr_Goomer_idle;

	anim_dash_front = spr_Goomer_idle;
	anim_dash_back = spr_Goomer_idle;

	anim_incap = spr_Goomer_incap;
	anim_stun_front = spr_Goomer_idle;
	anim_stun_back = spr_Goomer_idle;
}


function anim_Boomer() : base_Animator() constructor{
	
	anim_scale = 1;
	anim_default = spr_Boomer_idle;

	anim_idle_front = spr_Boomer_idle;
	anim_idle_back = spr_Boomer_idle;

	anim_walk_front = spr_Boomer_idle;
	anim_walk_back = spr_Boomer_idle;

	anim_dash_front = spr_Boomer_idle;
	anim_dash_back = spr_Boomer_idle;

	anim_incap = spr_Boomer_incap;
	anim_stun_front = spr_Boomer_idle;
	anim_stun_back = spr_Boomer_idle;
}