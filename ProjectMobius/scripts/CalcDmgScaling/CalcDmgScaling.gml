// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function CalcDmgScaling(a_char1, a_char2){
	//((ABS(c1-c2)/10)/2)+1
	var c1 = a_char1.charLvl + a_char1.classLvl;
	var c2 = a_char2.charLvl + a_char2.classLvl;
	var diff = abs(c1-c2);
	var diffMult = ((diff/10)/2)+1;
	
	var retMult = sqrt(diffMult);
	return retMult;
}