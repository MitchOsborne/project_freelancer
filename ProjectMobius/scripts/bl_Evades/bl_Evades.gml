// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function Internal_bp_Init_Evade()
{
	with global.bp_Abilities.bp_Evade
	{
		bp_Name = "Evade Dash";
		bp_Frame = evade_Dash;
		bp_Slot = LOADOUT_SLOT.UTILITY1;
		bp_DisplayActive = false;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 3,
			bp_ReloadSpd : 1,
			bp_SPDScale : 0,
			bp_EquipTime : 0,
			bp_CDScale : 0,
			bp_DurScale : 0.5,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}


	with global.bp_Abilities.bp_BlitzEvade
	{
		bp_Name = "Blitz Dash";
		bp_Frame = evade_Blitz;
		bp_Slot = LOADOUT_SLOT.UTILITY1;
		bp_DisplayActive = false;
		bp_StayActive = false;
		bp_WepStats = 
		{
			bp_Range : 10000, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 2,
			bp_ReloadSpd : 1,
			bp_EquipTime : 0,
			bp_SPDScale : 0,
		};
	
		bp_StatMods =
		{
			bp_Range : 1, 
			bp_Spread : 1, 
			bp_DMGScale : 1, 
			bp_RPM : 1,
			bp_Ammo : 1,
			bp_ReloadSpd : 1,
		};
		bp_Icon = spr_error;
	}
}
