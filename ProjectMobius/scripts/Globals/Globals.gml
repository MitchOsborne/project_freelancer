// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.EntryRoom = roomData_CaveCamp;

global.Dummy = undefined;

global.targetFPS = 60;

global.forceCap = 500;

global.LevelCap = 100;

global.credits = 10000;


global.cam_width = 420;
global.cam_height = 420;
global.view_width = 420;
global.view_height = 420;
global.window_width = 1280;
global.window_height = 720;
global.game_Surface = -1;
global.resolution_Scale = 4;

global.radar_Surface = -1;
global.radarEnabled = true;
global.radar_Pos = new Vector2(900,100);
global.radar_Size = new Vector2(210,210);

global.drawEnmHP = true;


global.screen_middle = new Vector2(0,0);

global.timeScale = 1;

global.lt_ambColour = c_black;
global.lt_ambAlpha = 1;
global.lt_lightStr = 1;
global.lt_extColour = c_black;


global.uiFocus = false;

global.inv_StatMods = [new Stat_EmptySlot()];


global.SlotRange = {
	ATK : 5,
	DEF : 10,
	ABL : 15,
	LUCK : 20,
}

global.gameState = GAME_STATE.NONE;
enum GAME_STATE
{
	NONE,
	CUSTOM,
	PAUSED,
	LOADING,
}

enum KEY_TYPES
{
	NONE,
	YAKI,
	RED,
	BLUE,
	YELLOW,
	INTERACT,
}

enum DOOR_SIDE
{
	NONE,
	TOP,
	RIGHT,
	BOTTOM,
	LEFT,
}

global.inv_keys = 
{
	YAKI : false,
	RED : false,
	BLUE: false,
	YELLOW: false,
}

global.inv_upgradeParts = 
{
	ATK : 0,
	DEF : 0,
	ABL : 0,
	LUCK : 0,
	RIVEN : 0,
	WEP : 0,
}


// Misc Enums
enum DAMAGETYPE
{
	PHYSICAL,
	PYRO,
	CRYO,
	VOLT,
	ENTROPY,
	PURE,
}





//In-Game Management

global.enmList = ds_list_create();

global.trapList = ds_list_create();

global.allyList = ds_list_create();

global.coreAllyList = ds_list_create();

global.PlayerIDList = ds_list_create();

global.TombstoneArr = [];


global.alertMode = ALERT_STATE.Clear;


//Overlord

global.gameRunning = false;

global.SequenceArr = [];

//Room Management
global.currentRoom = undefined;
global.nextRoom =  undefined;
global.previousRoom = undefined;

global.exitRoom = false;
global.entryPoint = entry_default;
global.entryIndex = -1;
global.targetRoom = undefined;

global.cp_ExitSide = -1;
global.cp_ExitIndex = -1;
global.cp_Room = undefined;


random_set_seed(current_time);

global.spawnEnm = false;
global.difficulty = 1;
global.enemyIntensity = 0;
global.worldLvl = 1;


//Regional Management Information
global.regions = 
{
	rg_None : new WoodsRegion(),
	rg_EastDesert : new Region_EasternDesert(),
	rg_FreightPlatform : new Region_FreightPlatform(),
	rg_FreightDock : new Region_FreightDock(),
}

/*
ds_map_add(global.regionDSMap, REGION.NONE, new WoodsRegion());
ds_map_add(global.regionDSMap, REGION.DESERT, new WoodsRegion());
ds_map_add(global.regionDSMap, REGION.CAVE, new CaveRegion());
ds_map_add(global.regionDSMap, REGION.YAKIBASE, new YakiBaseRegion());
ds_map_add(global.regionDSMap, REGION.YAKIBASEBOSS, new YakiBaseBossRegion());
*/
global.currRegion = global.regions.rg_None;
//if(room == t_room_Camp) global.currentRoom = room_CreateRoom(roomData_Woods1);
global.roomClear = false;


global.enemyLevel = 1;

global.spawnCap = 12;
global.enemyHPScale = 2;


global.dm = undefined;

global.rm = undefined;

global.bm = undefined;


global.Player1ID = undefined;

global.P2Enabled = false;

global.AITeammateEnabled = false;

global.editChar = undefined;

global.Player1Char = undefined;


global.BGM = undefined;
global.currBGM = undefined;
global.activeBGM = undefined;

global.pkup_List = ds_list_create();

global.initPlayers = true;

global.partyWipe = false;

global.scoreScreen = false;

global.p1Cont = undefined;
global.p2Cont = undefined;

global.canLoad = false;

//Debugger
global.debug = 0;

enum DEBUG_MODES
{
	NONE = 0,
	PHYSICS = 1,
	AI = 2,
	PROJECTILES = 3,
}


//Commander


global.CMDR = undefined;

global.walkNavGrid = undefined
global.crawlNavGrid = undefined;
global.flyNavGrid = undefined;
global.gridSize = 16;


global.cmd_combatWidth = 2;
global.cmd_combatantArr = [];

global.alertTxt = "NyukNyuk";
