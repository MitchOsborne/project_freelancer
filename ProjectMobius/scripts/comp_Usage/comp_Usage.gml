// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function abl_comp_Empty(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Update = function()
	{
		return false;	//Stops Empty Abilities from activating every frame	
	}
}


function abl_comp_Ammo(a_parent, a_ammoMax, a_ammoCost, a_reloadTime, a_reloadAmount, a_resDisplay) : base_AbilityComponent(a_parent) constructor{
	
	abl_reloadDelay = 0.5;
	abl_reloadDelayTmr = 0;
	ammoMax = 0;
	calcAmmoMax = 0;
	ammoCount = 0;
	ammoCost = 1;
	ammoIndex = 0;
	abl_lastAmmo = false;
	
	fireCD = false;
	abl_canReload = true;
	autoReload = true;
	reloadTime = 1;
	reloadTimer = 0;
	reloadAmount = 1;	//Amount of ammo added when the gun reloads
	calcReloadAmount = 1;
	
	
	
	if(a_ammoMax != undefined) ammoMax = a_ammoMax;
	if(a_ammoCost != undefined) ammoCost = a_ammoCost;
	if(a_reloadTime != undefined) reloadTime = a_reloadTime;
	if(a_reloadAmount != undefined)
	{
		if(a_reloadAmount == 0) reloadAmount = ammoMax;
		else reloadAmount = a_reloadAmount;
	}
	autoReload = a_parent.abl_bpStats.bp_AutoReload;
	if(a_resDisplay != undefined) abl_comp_showRes = a_resDisplay;
	else reloadAmount = ammoMax;
	
	
	abl_comp_Init = function()
	{
		InitAbilityComponent();
		parentAbility.abl_ammo = self;
	}
	
	//ammoCount = ammoMax;
	abl_comp_Update = function()
	{
		calcAmmoMax = ammoMax*parentAbility.abl_CalcDurScale;
		calcReloadAmount = reloadAmount * parentAbility.abl_CalcDurScale;
		if(!fireCD)	//Prevents guns beginning to reload between shots
		{
			
			if(autoReload == true || 
			(owner.char_AC.ac_activeWeapon == parentAbility ||
			owner.char_AC.ac_activeAbility == parentAbility))
			{
				//Only adjust reload timer if ammo is less than max
				if(ammoCount < floor(calcAmmoMax)) 
				{
					abl_reloadDelayTmr += GetDeltaTime(parentAbility.timeScale);
			
				}
				if (abl_reloadDelayTmr >= abl_reloadDelay)
				{
					reloadTimer += (GetDeltaTime(parentAbility.timeScale) * parentAbility.abl_CalcCDScale);	
				}
				if(reloadTimer >= reloadTime)
				{
					ammoCount += clamp(calcReloadAmount, 1, calcAmmoMax);
					ammoIndex = 0;
					reloadTimer = 0;
					if(ammoCount >= floor(calcAmmoMax)) abl_reloadDelayTmr = 0;
				}else if (reloadTimer > 0)
				{
					parentAbility.abl_animState = ABL_ANIM_STATE.RELOAD;
					parentAbility.abl_animLerp = reloadTimer/reloadTime;	
				}
			}
			else
			{
				reloadTimer = 0;
				abl_reloadDelayTmr = 0;
			}
		}
		
		//Keep Ammo within sane limits
		ammoCount = floor(clamp(ammoCount,0,calcAmmoMax));
		ammoIndex = floor(clamp(ammoCount,0,calcAmmoMax));
		
		
		if(ammoCount >= ammoCost)
		{
			if(ammoCount < ammoCost*2) abl_lastAmmo = true;
			else abl_lastAmmo = false;
			return true;
		}
		else 
		{
			parentAbility.ai_CanAttack = false;
			return false;
		}
	}
	
	abl_comp_OnActivate = function()
	{
		ammoCount -= ammoCost;
		ammoIndex += 1;
		if(parentAbility.abl_bpStats.bp_ChargeBased == false)
		{
			reloadTimer = 0;
			abl_reloadDelayTmr = 0;
		}
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 0;
		if(ammoCount >= ammoMax/2) retVal = 1;
		else
		{
			if(reloadTimer >= reloadTime*0.75)
			{
				retVal = reloadTimer/reloadTime;	
			}else retVal = ammoCount/ammoMax;
		}
		return clamp(retVal,0,1);
	}
	
	abl_comp_Draw = function()
	{
		if(abl_comp_showRes)
		{
			parentAbility.resourceDisplay = string(ammoCount) + "/" + string(calcAmmoMax);	
		}
	}
}

function abl_comp_AbilityLock(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Update = function()
	{
		if(owner.isAbilityLocked && owner.char_activeAbility != parentAbility) return false;
		else return true;
	}
}

function abl_comp_EquipWeapon(a_parent, a_delay) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_equipTime = 0.5;
	abl_comp_equipTmr = 0;
	abl_comp_equipWep = false;
	
	if(a_delay != undefined) abl_comp_equipTime = a_delay;
	
	abl_comp_Update = function()
	{
		var retVar = false;
		
		if(parentAbility.inPressed)
		{
			if(owner.char_AC.ac_activeWeapon != parentAbility)
			{
				if(parentAbility.abl_blueprint.bp_FlexType != "None" &&
				parentAbility.abl_blueprint.bp_FlexType == owner.char_AC.ac_activeWeapon.abl_blueprint.bp_FlexType)
				{
					abl_comp_equipTmr = abl_comp_equipTime;
				}
				else abl_comp_equipTmr = 0;
				if(parentAbility.abl_StayActive) owner.char_AC.ac_activeWeapon = parentAbility;
				owner.char_AC.ac_activeAbility = parentAbility;
			}
		}
		
		abl_comp_equipTmr += GetDeltaTime(owner.timeScale)* parentAbility.abl_CalcSpeedScale;	
		if(abl_comp_equipTmr < abl_comp_equipTime)
		{
			parentAbility.abl_animLerp = abl_comp_equipTmr/abl_comp_equipTime;
			parentAbility.abl_animState = ABL_ANIM_STATE.EQUIP;
			parentAbility.ai_CanAttack = false;
			var tLerp = clamp(abl_comp_equipTmr/(abl_comp_equipTime/2),0,1)
			var rotLerp = 90;
			if(parentAbility.abl_blueprint.bp_Slot == SLOT_TYPE.GUN_LIGHT) rotLerp = -90;
			parentAbility.abl_dirOffset += lerp(rotLerp,0,tLerp);
		}else
		{
			parentAbility.ai_CanAttack = true;
			retVar = true;	
		}
		return retVar;	
	}
}

function abl_comp_Cooldown(a_parent, a_CDTime) : base_AbilityComponent(a_parent) constructor{
	
	abl_comp_CDTime = 1;
	abl_comp_CDTimer = 0;
	
	if(a_CDTime != undefined) abl_comp_CDTime = a_CDTime;

	abl_comp_Update = function()
	{
		if(abl_comp_CDTimer >= abl_comp_CDTime)
		{
			if(abl_comp_hasProc)
			{	
				abl_comp_hasProc = false;
				parentAbility.abl_endProc = true;
			}
			return true;
		}
		else 
		{
			parentAbility.ai_CanAttack = false;
			abl_comp_CDTimer += GetDeltaTime(parentAbility.timeScale) * parentAbility.abl_CalcCDScale;
			return false;
		}
	}
	
	abl_comp_OnActivate = function()
	{
		abl_comp_hasProc = true;
		abl_comp_CDTimer = 0;	
	}
	
	abl_comp_AIUpdate = function()
	{
		var retVal = 1;
		if(abl_comp_CDTimer >= abl_comp_CDTime*0.75)
		{
			retVal = abl_comp_CDTimer/abl_comp_CDTime;	
		}
		return clamp(retVal,0,1);
	}
}
function abl_comp_Firerate(a_parent) : base_AbilityComponent(a_parent) constructor{
	
	
	fireRateTimer = 0;
	fireRate = 0;
	fireRateMod = 1;
	abl_comp_Update = function()
	{
		fireRate = 1/(parentAbility.abl_bpStats.bp_RPM/60);
		
		calcFireRate = fireRate * fireRateMod;
		
		if(fireRateTimer < calcFireRate) fireRateTimer += GetDeltaTime(parentAbility.timeScale) * parentAbility.abl_CalcSpeedScale;
		if(fireRateTimer >= calcFireRate)
		{
			if(abl_comp_hasProc) 
			{
				abl_comp_hasProc = false;
				parentAbility.abl_endProc = true;
			}
			parentAbility.abl_ammo.fireCD = false;
			return true;
		}
		else
		{
			if(parentAbility.abl_currAtk != undefined)
			{
				if(parentAbility.abl_animState != parentAbility.abl_currAtk.atk_anim)
				{
					parentAbility.abl_animState = parentAbility.abl_currAtk.atk_anim;
					parentAbility.abl_animLerp = fireRateTimer / calcFireRate;
				}
			}
			parentAbility.abl_ammo.fireCD = true;
			parentAbility.ai_CanAttack = false;
			return false;
		}
	}
	
	abl_comp_OnActivate = function()
	{
		AttachMod(debuff_abilityLocked, owner, owner, calcFireRate);
		fireRateTimer = 0;	
		abl_comp_hasProc = true;
	}
}

function abl_comp_FoodCooldown(a_parent) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_Update = function()
	{
		var retVar = true;
		if(FindMod(parentAbility.owner.char_modArr, "Drink Cooldown")) 
		{
			parentAbility.ai_CanAttack = false;
			retVar = false;
		}
		return retVar;
	}
}

function abl_comp_Stamina(a_parent, a_cost) : base_AbilityComponent(a_parent) constructor{
	
	
	staminaCost = 0;
	if(a_cost != undefined) staminaCost = a_cost;

	
	abl_comp_Update = function()
	{
		if((staminaCost*0.5) <= owner.staminaStat) return true;
		else
		{
			parentAbility.ai_CanAttack = false;
			return false;
		}
	}
	
	abl_comp_OnActivate = function()
	{
		owner.staminaStat -= staminaCost;
		owner.staminaRegenTimer = 0;
	}
}

function abl_comp_ChargeAttack(a_parent, a_attackArr, a_chargeTime) : base_AbilityComponent(a_parent) constructor
{
	abl_comp_initAttackArr = a_attackArr;
	abl_comp_attackArr = [];
	abl_comp_chargeTimer = 0;
	abl_comp_chargeTime = a_chargeTime;
	abl_comp_chargeIndex = 0;
	abl_comp_resetTimer = 0;
	abl_comp_resetTime = 0.5;
	abl_comp_firePrep = false;
	abl_comp_lockIndex = false;
	
	abl_comp_Init = function()
	{
		for(var i = 0; i < array_length(abl_comp_attackArr); i += 1)
		{
			abl_comp_attackArr[i] = new abl_comp_attackArr[i]();
			atk_InitAttack(parentAbility, abl_comp_attackArr[i]);
			
		}
	}
	
	abl_comp_Update = function()
	{
		var retVal = false;
		if(parentAbility.inPressed)
		{
			AttachMod(debuff_slowed, parentAbility.owner, parentAbility.owner, 0.1, 5, true);
			abl_comp_firePrep = true;
			if(!abl_comp_lockIndex) abl_comp_chargeTimer += GetDeltaTime(parentAbility.owner.timeScale);
			abl_comp_chargeTimer = clamp(abl_comp_chargeTimer, 0, array_length(abl_comp_attackArr));
			abl_comp_chargeIndex = floor(abl_comp_chargeTimer/abl_comp_chargeTime);
			abl_comp_chargeIndex = clamp(abl_comp_chargeIndex, 0, array_length(abl_comp_attackArr)-1);
			abl_comp_resetTimer = 0;
		}else
		{
			
			abl_comp_resetTimer += GetDeltaTime(parentAbility.owner.timeScale);
			if(abl_comp_resetTimer >= abl_comp_resetTime)
			{
				abl_comp_chargeIndex = 0;
				abl_comp_chargeTimer = 0;
				abl_comp_resetTimer = 0;
				abl_comp_lockIndex = false;
			}
			if(abl_comp_firePrep == true)
			{
				abl_comp_lockIndex = true;
				abl_comp_firePrep = false;
				retVal = true;
			}
		}
		abl_comp_nextAtk = abl_comp_attackArr[abl_comp_chargeIndex];
		parentAbility.abl_ammo.ammoCost = abl_comp_nextAtk.atk_resourceCost;
	   
		return retVal;
	}
	
	abl_comp_Draw = function()
	{
		if(abl_comp_chargeTimer != 0)
		{
			var tVal =  (abl_comp_chargeTimer mod abl_comp_chargeTime)*100;
			var tPos = new Vector2(parentAbility.owner.x, parentAbility.owner.y);
			draw_healthbar(tPos.x - 6, tPos.y + 6, tPos.x + 6, tPos.y + 4, tVal, c_black, c_purple, c_white, 0, true, true);
		}
	}
	
	abl_comp_OnActivate = function()
	{
		atk_ExecuteAttack(abl_comp_nextAtk);
	}
}

function abl_comp_AttackData(a_parent, a_attackArr, a_comboFinisher) : base_AbilityComponent(a_parent) constructor 
{
	
	abl_comp_initAttackArr = a_attackArr;
	abl_comp_attackArr = [];
	abl_comp_comboFinisher = a_comboFinisher;
	
	abl_comp_nextAtk = undefined;
	abl_comp_activeAtk = undefined;
	owner = a_parent;
	
		
	abl_comp_Init = function()
	{
		//Initialises attacks
		for(var i = 0; i < array_length(abl_comp_initAttackArr); i += 1)
		{
			abl_comp_attackArr[i] = new abl_comp_initAttackArr[i]();
			atk_InitAttack(parentAbility, abl_comp_attackArr[i]);
		}
		if(abl_comp_comboFinisher != undefined)
		{
			abl_comp_comboFinisher = new abl_comp_comboFinisher();
			atk_InitAttack(parentAbility, abl_comp_comboFinisher);
		}
	}
	
	abl_comp_Update = function()
	{
		var comboIndex = parentAbility.abl_ammo.ammoIndex;
		var attackIndex = comboIndex mod array_length(abl_comp_attackArr);
		if(abl_comp_activeAtk != undefined)
		{
			parentAbility.abl_fireRate.fireRateMod = abl_comp_activeAtk.atk_spdMod;
			atk_UpdateAtk(abl_comp_activeAtk);
		}
		
	    //Prep finisher if reached max combo count
	    if(abl_comp_comboFinisher != undefined && parentAbility.abl_ammo.abl_lastAmmo)
	    {
	        abl_comp_nextAtk = abl_comp_comboFinisher;
	    }
		//Else equip next attack in combo chain
	    else abl_comp_nextAtk = abl_comp_attackArr[attackIndex];
		
		parentAbility.abl_sfx = abl_comp_nextAtk.atk_sfx;
	    parentAbility.abl_ammo.ammoCost = abl_comp_nextAtk.atk_resourceCost;
	   
	}
	
	abl_comp_OnActivate = function()
	{
		
	    if(!is_undefined(abl_comp_nextAtk) && is_struct(abl_comp_nextAtk))
	    {
			abl_comp_activeAtk = abl_comp_nextAtk;
			parentAbility.abl_currAtk = abl_comp_activeAtk;
	    }
	}
	
	abl_comp_OnFireTrigger = function()
	{
		atk_ExecuteAttack(abl_comp_activeAtk);
	}
}