// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

global.ablArchUnlocks = 
{
	arch_Fighter : new Arch_Fighter(),
	arch_Hunter : new Arch_Hunter(),
	arch_Engineer : new Arch_Engineer(),
	arch_Dynamist : new Arch_Dynamist(),
	arch_Freelancer : new Arch_Freelancer(),
	arch_SolTech : new Arch_SolTech(),		
};
global.archDefault = new base_AblArchetype();

global.ablUnlockArr = [];

function UnlockBlueprint(a_bp)
{
	if(CheckUnlock(a_bp) != true)
	{
		array_push(global.ablUnlockArr, a_bp);	
	}
}

function CheckUnlock(a_bp)
{
	var retVal = false;
	if (array_find(global.ablUnlockArr, a_bp) != -1) retVal = true;
	return retVal;	
}

function arch_Init()
{

	
	var tArr = variable_struct_get_names(global.ablArchUnlocks);
	for(var i = 0; i < array_length(tArr); i += 1)
	{
		var tArch = variable_struct_get(global.ablArchUnlocks,tArr[i]);
		for(var ix = 0; ix < array_length(tArch.arch_BaseWeapons); ix += 1)
		{
			var tBP = tArch.arch_BaseWeapons[ix];
			tBP.bp_Archetype = tArch;
		}
		for(var ix = 0; ix < array_length(tArch.arch_AdvWeapons); ix += 1)
		{
			var tBP = tArch.arch_AdvWeapons[ix];
			tBP.bp_Archetype = tArch;
		}
		for(var ix = 0; ix < array_length(tArch.arch_SpcWeapons); ix += 1)
		{
			var tBP = tArch.arch_SpcWeapons[ix];
			tBP.bp_Archetype = tArch;
		}
		for(var ix = 0; ix < array_length(tArch.arch_ExoWeapons); ix += 1)
		{
			var tBP = tArch.arch_ExoWeapons[ix];
			tBP.bp_Archetype = tArch;
		}
		
	}
}

function base_AblArchetype() constructor{
	
	
	arch_Name = "archDefault";
	arch_Level = 0;
	arch_BaseWeapons = [];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
	
	arch_BaseLevel = 1;
	arch_AdvLevel = 5;
	arch_SpcLevel = 10;
	arch_ExoLevel = 15;
	arch_LevelCap = 25;
			
}

function GetArchUnlockedAbilities()
{
	var retArr = [];
	var checkArr = variable_struct_get_names(global.ablArchUnlocks);
	for(var i = 0; i < array_length(checkArr); i += 1)
	{
		var tStr = checkArr[i];
		
		var tArch = variable_struct_get(global.ablArchUnlocks, tStr);
		with tArch
		{
			if(arch_Level >= arch_BaseLevel)array_copy(retArr, array_length(retArr), arch_BaseWeapons, 0, array_length(arch_BaseWeapons));
			if(arch_Level >= arch_AdvLevel) array_copy(retArr, array_length(retArr), arch_AdvWeapons, 0, array_length(arch_AdvWeapons));
			if(arch_Level >= arch_SpcLevel) array_copy(retArr, array_length(retArr), arch_SpcWeapons, 0, array_length(arch_SpcWeapons));
			if(arch_Level >= arch_ExoLevel) array_copy(retArr, array_length(retArr), arch_ExoWeapons, 0, array_length(arch_ExoWeapons));
		}
	}
	
	return retArr;
}

function GetUnlockedAbilities()
{
	var retArr = GetArchUnlockedAbilities();
	array_append(retArr, global.ablUnlockArr);
	return retArr;	
}

function Arch_Fighter() : base_AblArchetype() constructor
{
	arch_Name = "Fighter";
	arch_Level = 1;
	
	arch_BaseWeapons = [global.bp_Abilities.bp_BladeDash, global.bp_Abilities.bp_CombatJunkie];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
}

function Arch_Hunter() : base_AblArchetype() constructor
{
	arch_Name = "Hunter";
	arch_Level = 1;
	
	arch_BaseWeapons = [global.bp_Abilities.bp_EquipMosin];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
}

function Arch_Engineer() : base_AblArchetype() constructor
{
	
	arch_Name = "Engineer";
	arch_Level = 0;	
	
	arch_BaseWeapons = [global.bp_Abilities.bp_RK, global.bp_Abilities.bp_EquipMosin];
	arch_AdvWeapons = [global.bp_Abilities.bp_RKL, global.bp_Abilities.bp_RKH, global.bp_Abilities.bp_DBSG, global.bp_Abilities.bp_DBSGs];
	arch_SpcWeapons = [global.bp_Abilities.bp_RKR];
	arch_ExoWeapons = [global.bp_Abilities.bp_TheEnd];
	
}

function Arch_Dynamist() : base_AblArchetype() constructor
{
	arch_Name = "Dynamist";
	arch_Level = 0;	
	
	arch_BaseWeapons = [];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
	
} 


function Arch_Freelancer() : base_AblArchetype() constructor
{
	arch_Name = "Freelancer";
	arch_Level = 1;	
	
	arch_BaseWeapons = [global.bp_Abilities.bp_Empty, global.bp_Abilities.bp_Shortsword, global.bp_Abilities.bp_CombatKnife, global.bp_Abilities.bp_KnifeThrow, global.bp_Abilities.bp_Rook443, global.bp_Abilities.bp_Evade, global.bp_Abilities.bp_Class_Fighter, global.bp_Abilities.bp_Class_Hunter, global.bp_Abilities.bp_Class_Freelancer];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
	
}

function Arch_SolTech() : base_AblArchetype() constructor
{
	
	arch_Name = "SolTech";
	arch_Level = 0;	
	
	arch_BaseWeapons = [global.bp_Abilities.bp_M9T, global.bp_Abilities.bp_Rook443];
	arch_AdvWeapons = [];
	arch_SpcWeapons = [];
	arch_ExoWeapons = [];
	
}