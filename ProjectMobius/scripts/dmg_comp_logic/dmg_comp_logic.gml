// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function dmg_comp_Locomote(a_parent, a_locoType) : base_DamagerComponent(a_parent) constructor
{	
	a_parent.locomotorType = ploco_1;
	if(a_locoType != undefined) a_parent.locomotorType = a_locoType;
	AssignProjomotor(a_parent);
	dmg_comp_Update = function()
	{
		with parentDmg
		{
			if(instance_find(locomotorType, 0) != noone) locomotor = instance_find(locomotorType,0);
			else
			{
				var tInst = instance_create_layer(0,0,"Instances", locomotorType);
				locomotor = tInst;
			}

			Projectile_Locomote(self, locomotor);
			var scanLength = abs(sprite_get_bbox_left(dmg_sprite) - sprite_get_bbox_right(dmg_sprite));

			var movDist = point_distance(x, y, locomotor.x, locomotor.y);


			var movDir = GetDirection(x, y, locomotor.x, locomotor.y, false, true);

			if(movDist > 0)
			{
				//direction = movDir + 90;
			}
		
			var movSeg = round(movDist/scanLength);
			var movVec = GetCoord(movDir);

			var nx = movVec.x;
			var ny = movVec.y;
			nx *= scanLength;
			ny *= scanLength;

			for(var i = 1; i < movSeg; i++)
			{

				var tX = nx * i;
				var tY = ny * i;
				with global.projomotor
				{
					if(place_meeting(x + tX, y + tY, base_Agent))
					{
						var tObj = instance_place(x+tX, y+tY, base_Agent);
					
						if(slavedObj.hp >= 0)
						{
							OnProjHit(slavedObj,tObj);
			
						}else
						{
							break;	
						}
					}
				}
			}

			if(point_distance(x,y,locomotor.x, locomotor.y) > 0)
			{
				direction = point_direction(x,y,locomotor.x, locomotor.y);

				x = locomotor.x;
				y = locomotor.y;

				var tVec2 = GetCoord(direction - 90);

				mx = tVec2.x;
				my = tVec2.y;

			}
		}

	}
}

function dmg_comp_Homing(a_parent, a_drunkMod, a_rotSpeed, a_targetAngle, a_targetTime) : base_DamagerComponent(a_parent) constructor
{
	dmg_comp_TargetTeam = undefined;
	dmg_comp_TurnSpeed = a_rotSpeed;	
	dmg_comp_retargetTimer = 0;
	dmg_comp_retargetTime = a_targetTime;
	dmg_comp_target = undefined;
	dmg_comp_targetingAngle = a_targetAngle;
	dmg_comp_drunkMod = 3;
	
	if(a_drunkMod != undefined) dmg_comp_drunkMod = a_drunkMod;
	
	dmg_comp_Update = function()
	{
		
		dmg_comp_retargetTimer += GetDeltaTime(owner.timeScale);

		if(dmg_comp_retargetTimer >= dmg_comp_retargetTime || CheckExistance(dmg_comp_target) == false)
		{
			//dmg_comp_target = undefined;
			dmg_comp_retargetTimer = 0;
			var tTeam = TEAMID.Other;
			if(dmg_comp_TargetTeam == owner.teamID) tTeam = TEAMID.Same;
			dmg_comp_target = FindTargetInDir(parentDmg.x,parentDmg.y,parentDmg.direction, base_Character, weapon.owner, dmg_comp_targetingAngle, 480,true, tTeam);
		}

		if(CheckExistance(dmg_comp_target) != false)
		{
			var tDir = point_direction(parentDmg.x,parentDmg.y,dmg_comp_target.x,dmg_comp_target.y);
			var tDiff = angle_difference(tDir, parentDmg.direction);
			if(abs(tDiff) > dmg_comp_targetingAngle)
			{
				dmg_comp_target=undefined;
			}else
			{	
				var rotAngle = clamp(tDiff, -dmg_comp_TurnSpeed, dmg_comp_TurnSpeed);
				parentDmg.direction += rotAngle;
			}
		}

		var mVec = GetCoord(parentDmg.direction-90 + (irandom_range(-dmg_comp_drunkMod, dmg_comp_drunkMod)));
		parentDmg.mx = mVec.x;
		parentDmg.my = mVec.y;
	
	}
}

function dmg_comp_OwnerAnchored(a_parent) : base_DamagerComponent(a_parent) constructor
{
	dmg_comp_Update = function()
	{
		parentDmg.x = owner.x;
		parentDmg.y = owner.y;
	}
}