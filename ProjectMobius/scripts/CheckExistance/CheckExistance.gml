function CheckExistance(a_Target) {
	
	if(is_undefined(a_Target))
	{
		return false;	
	}else if(is_struct(a_Target))
	{
		return true;	
	}else if(instance_exists(a_Target))
	{
		return true;	
	}else return false;
	
}
