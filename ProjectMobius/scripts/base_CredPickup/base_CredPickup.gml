// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_CreditPickup() : base_Pickup() constructor{
	
	lifeTime = 10;
	blinkTime = 4;
	
	
	moveSpeed = 2;
	pickupRange = 300;
	
	credAmount = 1;
	func_EndUpdateStep = Credit_EndUpdateStep;
}

function Credit_EndUpdateStep(a_pkup)
{
	with a_pkup
	{
		if(isCollected)
		{
			global.credits += credAmount;
			lifeTimer = lifeTime;
		}
		Pickup_EndUpdate(self);
	}
}