// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function defunc_abl_EquipLoadout() : defunc_base_AltLoadout() constructor
{
	ablName = "Equip Dummy";
	ldt_ActivateAbility = ActivateAltLoadout;	
	abl_Icon = spr_Nanolathe;
	cooldown = 12;
}

function defunc_abl_ResetLoadout() : defunc_base_AltLoadout() constructor
{
	reqRelease = false;
	abl_chargeTime = 0.3;
	ldt_ActivateAbility = DeactivateAltLoadout;	
	abl_Icon = spr_Reset;
}

function defunc_abl_EquipMinigun() : defunc_abl_EquipLoadout() constructor{
	
	ablName = "Minigun";
	cooldown = 20;
	alt_wep1 = new PKG_SinglePack(wep_Minigun);
	modsToApply = [debuff_slowed, new buff_notice(ablName + " Equipped")];
}

function defunc_abl_EquipRPG() : defunc_abl_EquipLoadout() constructor{
	ablName = "RPG";
	cooldown = 20;
	alt_wep1 = new PKG_SinglePack(wep_RPG);
	modsToApply = [debuff_slowed, new buff_notice(ablName + " Equipped")];	
}

function defunc_abl_EquipRatPack() : defunc_abl_EquipLoadout() constructor{
	ablName = "RatPack";
	cooldown = 20;
	alt_wep1 = new PKG_SinglePack(wep_RatPack);
	modsToApply = [debuff_slowed25, new buff_notice(ablName + " Equipped")];	
}