// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function buff_LP_autoHeal() : base_Modifier() constructor{

	name = "LP_AutoHeal"
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			if(subject.hp <= subject.char_maxHP*0.25 && !subject.isIncap)
			{
				if(owner.hp > (owner.char_maxHP*0.1))
				{
					subject.hp = subject.char_maxHP;
					owner.hp -= owner.char_maxHP*0.1;
				}
			}
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}

function buff_LP_TankProtect() : base_Modifier() constructor{

	name = "LP_TankProtect"
	displayName = true;
	stackLimit = 1;
	mod_Update = function()
	{
		if(CheckExistance(subject))
		{
			if(CheckExistance(owner) && !owner.isIncap)
			{
				AttachMod(buff_hitInvul, subject, owner, 1);
			}
		}	
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);
	}
}