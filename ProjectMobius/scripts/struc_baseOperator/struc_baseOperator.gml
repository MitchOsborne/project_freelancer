// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function base_Operator() : base_CharacterComponent() constructor{
	color = c_white;
	
	
	controlMode = CONT_MODE.NONE;
	func_Init = Oper_Init;
	func_PreUpdateStep = Oper_PreUpdateStep;
	func_UpdateStep = Oper_UpdateStep;
	func_EndUpdateStep = Oper_EndUpdateStep;
	func_DrawStep = Oper_DrawStep;
}

function Oper_Init(a_oper)
{
	with a_oper
	{
		return true;	
	}
}

function Oper_PreUpdateStep(a_oper)
{
	with a_oper
	{
		char_comp_PreUpdateStep(self);
		slavedObj.ownerColor = color;
	}
}

function Oper_UpdateStep(a_oper)
{
	with a_oper
	{
		char_comp_UpdateStep(self);
	}	
}

function Oper_EndUpdateStep(a_oper)
{
	with a_oper
	{
		char_comp_EndUpdateStep(self);
	}
}

function Oper_DrawStep(a_oper)
{
	with a_oper
	{
		char_comp_DrawStep(self);
	}
}