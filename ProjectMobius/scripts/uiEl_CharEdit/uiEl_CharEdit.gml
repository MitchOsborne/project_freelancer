// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_uiEl_CharEditAbl(a_PI) : base_defunc_uiElement(a_PI) constructor
{
	defunc_uiEl_text = "Edit Character Loadout"
	defunc_uiEl_sprite = spr_MenuBkg;
	
	defunc_uiEl_OnActivateFunc = function(a_defunc_uiEl)
	{
		var tMenu = new Menu_CharEditAbl(defunc_uiEl_PlayerIndex);
	}
}

function defunc_uiEl_CharEditStat(a_PI) : base_defunc_uiElement(a_PI) constructor
{
	defunc_uiEl_text = "Edit Character Stat Slots"
	defunc_uiEl_sprite = spr_MenuBkg;
	
	defunc_uiEl_OnActivateFunc = function(a_defunc_uiEl)
	{
		var tMenu = new Menu_CharEditStat(defunc_uiEl_PlayerIndex);
		tMenu = UI_Init(tMenu);
		array_push(global.ui_MenuStack, tMenu);
	}
}


function defunc_uiEl_CharEditStatSlot(a_PI, a_index) : base_defunc_uiElement(a_PI) constructor
{
	defunc_uiEl_StatCard = UI_GetCharCard(a_PI);
	defunc_uiEl_StatSlot = defunc_uiEl_StatCard.crd_StatSlotArr[a_index];
	defunc_uiEl_text = string(defunc_uiEl_StatSlot.stat_currLvl);
	defunc_uiEl_tooltipTxt = defunc_uiEl_StatSlot.stat_mainTxt + "-" + defunc_uiEl_StatSlot.stat_altTxt;
	defunc_uiEl_sprite = defunc_uiEl_StatSlot.stat_icon;
	defunc_uiEl_SlotIndex = a_index;
	defunc_uiEl_OnActivateFunc = function(a_defunc_uiEl)
	{
		var tMenu = new Menu_CharSelectStatMod(defunc_uiEl_PlayerIndex, defunc_uiEl_SlotIndex);
		tMenu = UI_Init(tMenu);
		array_push(global.ui_MenuStack, tMenu);
	}
}


function defunc_uiEl_CharSocketMod(a_PI, a_index, a_mod) : base_defunc_uiElement(a_PI) constructor
{
	defunc_uiEl_StatCard = UI_GetCharCard(a_PI);
	defunc_uiEl_StatSlot = defunc_uiEl_StatCard.crd_StatSlotArr[a_index];
	defunc_uiEl_SlotIndex = a_index;
	defunc_uiEl_NewMod = a_mod;
	defunc_uiEl_sprite = defunc_uiEl_NewMod.stat_icon;
	defunc_uiEl_text = string(defunc_uiEl_NewMod.stat_currLvl);
	defunc_uiEl_tooltipTxt = a_mod.stat_mainTxt + "-" + a_mod.stat_altTxt;
	defunc_uiEl_OnActivateFunc = function(a_defunc_uiEl)
	{
		defunc_uiEl_StatCard.crd_StatSlotArr[defunc_uiEl_SlotIndex] = defunc_uiEl_NewMod;
		global.activeUI.ui_CanClose = true;
	}
}