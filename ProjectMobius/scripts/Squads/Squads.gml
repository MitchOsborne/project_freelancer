// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function base_Squad(a_name, a_members) constructor
{
	sqd_Name = "Unnamed Squad";
	sqd_Members = [];
	
	if(a_name != undefined) sqd_Name = a_name;
	if(a_members != undefined) sqd_Members = a_members;
}

global.squadArr = 
[
	new base_Squad("Batman", [enemy_Bat,enemy_Bat,enemy_Bat,enemy_Bat,enemy_Bat,enemy_Bat,enemy_Bat,enemy_Yaki_Gun]),
	new base_Squad("Bandit Team", [enemy_Yaki,enemy_Yaki,enemy_Yaki_Gun, enemy_Nezumi,enemy_Nezumi,enemy_Kitsune,enemy_Kitsune_Oracle]),
	new base_Squad("Wolfpack", [enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,]),
	new base_Squad("Nezumi Assassins", [enemy_Nezumi,enemy_Nezumi,enemy_Nezumi,enemy_Nezumi,enemy_Nezumi,enemy_Nezumi,enemy_Nezumi,]),
	new base_Squad("Man's Best Friend", [enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Yaki_Gun,enemy_Yaki_Gun]),
	new base_Squad("Nature Strikes Back", [enemy_Doggo,enemy_Doggo,enemy_Doggo,enemy_Nezumi,enemy_Nezumi,enemy_Bat,enemy_Bat,enemy_Bat]),
]

global.WorldExpPool = 10;

