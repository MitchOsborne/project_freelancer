// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ai_Default() : base_AI() constructor{
	func_UpdateStep = AI_UpdateDefault;
	ai_atkDelay = 0.5;
	toggleAtk = false;
}

function AI_UpdateDefault(a_ai)
{
	AI_UpdateStep(self);
}


function ai_Adaptive() : base_AI() constructor
{
	func_UpdateStep = AI_UpdateAdaptive;
	ai_atkDelay = 0.5;
	toggleAtk = false; //variable for cycling between releasing input, always set to false
	func_DrawStep = AI_DrawAdaptive;
	chosenAbl = undefined;
}

function ai_FlameTurret() : base_AI() constructor
{
	
		ai_dir = 0;
		func_UpdateStep = AI_UpdateFlameTurret;
	
}

function AI_UpdateFlameTurret(a_ai)
{
	with a_ai
		{
			ai_dir += 45* GetDeltaTime(timeScale);
			slavedObj.char_AC.ac_activeLoadout.ld_slots.ld_wep1.inPressed = true;
		
			var tVec = GetCoord(ai_dir);
			slavedObj.in_lx = tVec.x;
			slavedObj.in_ly = tVec.y;
		}	
}

function AI_UpdateAdaptive(a_ai)
{
	with a_ai
	{
		AI_UpdateStep(self);
		AI_CheckVision(self);
		currentTarget = enemyTarget;
		ai_abl_spTmr += GetDeltaTime(slavedObj.timeScale);
		switch ai_CurrentState
			{
				case AI_STATE.IDLE:
				{
					ai_logic_idle(self);
					break;
				}
				case AI_STATE.SEARCH:
				{
					ai_logic_search(self);
					ai_debugTxt = "Searching...";
				}break;
				case AI_STATE.INVESTIGATE:
				{
					ai_logic_investigate(self);
					ai_debugTxt = "Investigating...";
				}break;
				case AI_STATE.ENGAGE:
				{
					ai_fightTmr = 0;
					if(!ai_prepAttack)
					{
						if(ai_abl_spTmr >= ai_abl_spCD && ai_abl_spEngage != undefined)
						{
							chosenAbl = GetAblFromSlot(ai_abl_spEngage, slavedObj.char_AC.ac_activeLoadout);
							ai_abl_spTmr = 0;
						}else chosenAbl = GetAblFromSlot(ai_abl_engage, slavedObj.char_AC.ac_activeLoadout);
					}
					if(chosenAbl != undefined)
					{
						AI_GetAbilityScores(chosenAbl);
						ai_logic_moveEngage(self);
						ai_logic_engage(self);
					}
					ai_debugTxt = "Engaging";
				}break;
				case AI_STATE.FIGHT:
				{
					ai_fightTmr += GetDeltaTime(slavedObj.timeScale);
					if(ai_fightTmr < ai_fightDelay)
					{
						ai_logic_moveEngage(self);
						ai_debugTxt = "Pre-Fight";
					}
					else
					{
						if(!ai_prepAttack)
						{
							if(ai_abl_spTmr >= ai_abl_spCD && ai_abl_spFight != undefined)
							{
								chosenAbl = GetAblFromSlot(ai_abl_spFight, slavedObj.char_AC.ac_activeLoadout);
								ai_abl_spTmr = 0;
							}else chosenAbl = GetAblFromSlot(ai_abl_fight, slavedObj.char_AC.ac_activeLoadout);
						}
						if(chosenAbl != undefined)
						{
							AI_GetAbilityScores(chosenAbl);
							ai_logic_moveFight(self);
							var didFight = ai_logic_fight(self);
							if(didFight)
							{
								ai_logic_postFight(self);
								ai_fightTmr = -ai_fightDelay;
							}
							
						}
						ai_debugTxt = "Fighting";
					}
				}break;
			
			}
		if(ai_useAlertMode && ai_CurrentState >= AI_STATE.INVESTIGATE) AI_RaiseAlertMode(self);
		
		if(ai_priorityTmr < ai_priorityDelay) 
		{
			ai_priorityTmr += GetDeltaTime(slavedObj.timeScale);
			slavedObj.ai_priorityMod = -1;
		}else slavedObj.ai_priorityMod = 1;
		
		var prepVal = atkTimer/ai_atkDelay;
		if(ai_canEvade && prepVal < 0.5)
		{
			ai_abl_evadeTmr += GetDeltaTime(slavedObj.timeScale);
			if(ai_abl_evadeTmr >= ai_abl_evadeRes)
			{
				var tEvade = GetAblFromSlot(ai_abl_spEvade, slavedObj.char_AC.ac_activeLoadout);
				ai_abl_evadeTmr = 0;
				if(tEvade != undefined) AI_GetAbilityScores(tEvade);
				if(tEvade != undefined && tEvade.ai_CanAttack) AI_UseAbility(self, tEvade);
				else
				{
					tEvade = GetAblFromSlot(ai_abl_evade, slavedObj.char_AC.ac_activeLoadout);
					if (tEvade != undefined)
					{
						AI_GetAbilityScores(tEvade);
						if(tEvade.ai_CanAttack) AI_UseAbility(self, tEvade);
					}
				}
			}	
		}
		if(atkTmrCheck == atkTimer) atkDecay += GetDeltaTime(slavedObj.timeScale);
		else atkDecay = 0;
		if(atkDecay >= 1)
		{
			atkDecay = 0;
			atkTimer = 0;
		}
		atkTmrCheck = atkTimer;
		
	}
}


function AI_DrawAdaptive(a_ai)
{
	with a_ai
	{
		AI_DrawStep(self);
		
		if(!is_undefined(chosenAbl))
		{
			if(global.debug == DEBUG_MODES.AI)
			{
				DrawTextOutline(slavedObj.x, slavedObj.y, 0.5, ai_debugTxt,c_white, c_black, 64,true);
				DrawTextOutline(slavedObj.x, slavedObj.y-4, 0.5, chosenAbl.ablName,c_white, c_black, 64,true);
			}
		}
		
		
	}
}