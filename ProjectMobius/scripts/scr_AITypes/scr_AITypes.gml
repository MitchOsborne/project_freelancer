// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

function ai_BasicAgent() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE);
		}
	}
		ai_canEvade = false;
		ai_useAlertMode = true;
	
		ai_logic_idle = AI_PatrolRoute;
		ai_logic_search = AI_LookAtTarget;
		ai_logic_investigate = AI_InvestigateTarget;
		ai_logic_engage = AI_FightEnemy;
		ai_logic_moveEngage = AI_MoveToEngageRange;
		ai_logic_fight = AI_FightEnemy;
		ai_logic_moveFight = AI_MoveToFightRange;
	
}


function ai_FollowerAgent() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE);
		}
	}
		ai_canEvade = true;
		ai_useAlertMode = true;
	
		ai_logic_idle = AI_FollowPlayer;
		ai_logic_search = AI_LookAtTarget;
		ai_logic_investigate = AI_InvestigateTarget;
		ai_logic_engage = AI_FightEnemy;
		ai_logic_moveEngage = AI_MoveToEngageRange;
		ai_logic_fight = AI_FightEnemy;
		ai_logic_moveFight = AI_MoveToFightRange;
	
}

function ai_AdvRogueAgent() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON2, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.ABILITY1, LOADOUT_SLOT.ABILITY2, LOADOUT_SLOT.UTILITY2);
		}
	}
	ai_canEvade = true;	
	ai_useAlertMode = true;
	
	ai_logic_idle = AI_PatrolRoute;
	ai_logic_search = AI_LookAtTarget;
	ai_logic_investigate = AI_InvestigateTarget;
	ai_logic_engage = AI_FightEnemy;
	ai_logic_moveEngage = AI_EncircleTarget;
	ai_logic_fight = AI_FightEnemy;
	ai_logic_moveFight = AI_MoveToFightRange;
}

function ai_AdvRangedAgent() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.ABILITY1, LOADOUT_SLOT.ABILITY2, LOADOUT_SLOT.UTILITY2);
		}
	}
	ai_canEvade = true;	
	ai_useAlertMode = true;
	
	ai_logic_idle = AI_PatrolRoute;
	ai_logic_search = AI_LookAtTarget;
	ai_logic_investigate = AI_InvestigateTarget;
	ai_logic_engage = AI_FightEnemy;
	ai_logic_moveEngage = AI_MoveToEngageRange;
	ai_logic_fight = AI_FightEnemy;
	ai_logic_moveFight = AI_MoveToFightRange;
}

function ai_AdvMeleeAgent() : ai_Adaptive() constructor
{

	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON2, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.ABILITY1, LOADOUT_SLOT.ABILITY2, LOADOUT_SLOT.NONE);
		}
	}
	ai_canEvade = true;	
	ai_useAlertMode = true;
	
	ai_logic_idle = AI_PatrolRoute;
	ai_logic_search = AI_LookAtTarget;
	ai_logic_investigate = AI_InvestigateTarget;
	ai_logic_engage = AI_FightEnemy;
	ai_logic_moveEngage = AI_MoveToEngageRange;
	ai_logic_fight = AI_FightEnemy;
	ai_logic_moveFight = AI_MoveToFightRange;
}

function ai_Swarm() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE);
		}
	}
	
	ai_canEvade = false;
	ai_fightDelay = 0;
	ai_atkDelay = 0.4;
	ai_logic_idle = AI_InvestigateTarget;
	ai_logic_search = AI_InvestigateTarget;
	ai_logic_investigate = AI_InvestigateTarget;
	ai_logic_engage = AI_InvestigateTarget;
	ai_logic_moveEngage = AI_InvestigateTarget;
	ai_logic_fight = AI_FightEnemy;
	ai_logic_moveFight = AI_InvestigateTarget;
	ai_logic_postFight = AI_InvestigateTarget;
}

function ai_Wildlife() : ai_Adaptive() constructor
{
	func_Init = function(a_ai)
	{
		with a_ai
		{
			AI_BaseInit(self);
			AI_SetAILoadout(self, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.WEAPON1, LOADOUT_SLOT.UTILITY1, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE, LOADOUT_SLOT.NONE);
		}
	}
	ai_canEvade = false;
	ai_fightDelay = 1;
	ai_atkDelay = 0.4;
	ai_logic_idle = AI_Idle;
	ai_logic_search = AI_EncircleTarget;
	ai_logic_investigate = AI_EncircleTarget;
	ai_logic_engage = AI_Idle;
	ai_logic_moveEngage = AI_EncircleTarget;
	ai_logic_fight = AI_FightEnemy;
	ai_logic_moveFight = AI_MoveToFightRange;
	ai_logic_postFight = AI_HitAndRun;
}
