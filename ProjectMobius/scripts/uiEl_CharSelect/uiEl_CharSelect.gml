// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function defunc_base_ElCharSelect(a_PI) : base_defunc_uiElement() constructor
{
	/*
defunc_uiEl_text = "Character Name goes here"
	ui_Sprite = spr_MenuBkg;
	defunc_uiEl_Character = undefined;
	defunc_uiEl_PlayerIndex = a_PI;
	
	defunc_uiEl_OnActivateFunc = function(a_defunc_uiEl)
	{
		if(defunc_uiEl_PlayerIndex == "1" && defunc_uiEl_Character != undefined)
		{
			global.Player1ID = new Player1(defunc_uiEl_Character);
			
			if(global.p1Cont.in_InputType = CONT_MODE.GAMEPAD)
			{
				global.Player1ID.controlMode = global.p1Cont.in_InputType;
				
			}else if(global.p1Cont.in_InputType = CONT_MODE.KB_M)
			{
				global.Player1ID.controlMode = global.p1Cont.in_InputType;
			}
			global.Player1ID.inputSource = global.p1Cont;
	
			global.activeUI.ui_CanClose = true;
			if(global.p2Cont != undefined)
			{
				var tMenu = new Menu_CharSelect(2);
				tMenu = UI_Init(tMenu);
				array_push(global.ui_MenuStack,tMenu);				
			}
		}else if(defunc_uiEl_PlayerIndex == "2" && defunc_uiEl_Character != undefined)
		{
			global.Player2ID = new Player2(defunc_uiEl_Character);
			global.activeUI.ui_CanClose = true;
			
			
			if(global.p2Cont.in_InputType = CONT_MODE.GAMEPAD)
			{
				global.Player2ID.controlMode = global.p2Cont.in_InputType;
				gamepad_set_axis_deadzone(global.p2Cont.in_InputIndex,0.3);
			}else if(global.p2Cont.in_InputType = CONT_MODE.KB_M)
			{
				global.Player2ID.controlMode = global.p2Cont.in_InputType;
			}
			global.Player2ID.inputSource = global.p2Cont;
		}
	}
	*/
}

function defunc_uiEl_CharSelectShepherd(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Shepherd";
	defunc_uiEl_Character = char_Shepherd;
}
function defunc_uiEl_CharSelectKamui(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Kamui";
	defunc_uiEl_Character = char_Kamui;
}
function defunc_uiEl_CharSelectPilot(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Mech Pilot";
	defunc_uiEl_Character = char_MechPilot;
}
function defunc_uiEl_CharSelectDoggo(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Doggo (Unstable)";
	defunc_uiEl_Character = enemy_Doggo;
}
function defunc_uiEl_CharSelectYakiMelee(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Yaki_M (Unstable)";
	defunc_uiEl_Character = enemy_Yaki;
}
function defunc_uiEl_CharSelectYakiGun(a_PI) : defunc_base_ElCharSelect(a_PI) constructor
{
	defunc_uiEl_text = "Yaki_Gun (Unstable)";
	defunc_uiEl_Character = enemy_Yaki_Gun;
}
