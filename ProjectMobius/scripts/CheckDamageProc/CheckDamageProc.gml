function CheckDamageProc(a_Proj, a_Receiver, a_FriendlyFire) {

	var attackHit = true;

	var attackSucceeds = false;


	if(a_FriendlyFire == (a_Proj.teamID == a_Receiver.teamID))
	{
		if(a_Proj.dmg_canDamage == true)
		{
			if(array_find(objectsDamaged,a_Receiver) != -1)
			{
				attackHit = false;	
			}
		}else
		{
			attackHit = false;	
		}

		if(attackHit)
		{
			if(a_Receiver.hp <= 0 || a_Receiver.isPhased)
			{
				attackSucceeds = false;
			}
			else
			{
				attackSucceeds = true;
			}
		}
	}

return attackSucceeds;

}
