function CalcFixedSpread(a_iterator, a_projSpread, a_projCount) {


	if(a_projCount == 1)a_iterator = 0.5;
	var maxAngle = a_projSpread/2;
	var minAngle = -(a_projSpread/2);
	var lerpVal = (a_iterator)/(a_projCount);
	var tRot = lerp(minAngle,maxAngle,lerpVal);

	return tRot;


}

function CalcFixedSpreadPos(a_iterator, a_projSpread, a_projCount)
{
	var a_x = a_projSpread.x;
	var a_y = a_projSpread.y;
	if(a_projCount == 1)a_iterator = 0.5;
	var maxPosX = a_x/2;
	var minPosX = -(a_x/2);
	var maxPosY = a_y/2;
	var minPosY = -(a_y/2);
	var lerpVal = (a_iterator)/(a_projCount);
	var tPos = new Vector2(lerp(minPosX,maxPosX,lerpVal), lerp(minPosY, maxPosY, lerpVal));

	return tPos;	
}
