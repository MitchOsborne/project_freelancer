// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function abl_comp_Input(a_parent) : base_AbilityComponent(a_parent) constructor
{
	array_push(tags,COMP_TAGS.INPUT);	
}

function abl_comp_OnRelease(a_parent) : abl_comp_Input(a_parent) constructor{
	
	
	abl_comp_Update = function()
	{
		if(parentAbility.inPressed == false && parentAbility.wasPressed == true) return true;
		else return false;
	}
}

function abl_comp_AutoFire(a_parent) : abl_comp_Input(a_parent) constructor
{
	abl_comp_Update = function()
	{
		return true;	
	}
}

function abl_comp_OnPress(a_parent) : abl_comp_Input(a_parent) constructor{
	
	
	abl_comp_Update = function()
	{
		if(parentAbility.inPressed == true || parentAbility.forcePressed == true) return true;
		else return false;
	}
}

function abl_comp_ReqRelease(a_parent) : abl_comp_Input(a_parent) constructor
{
	isReleased = false;
	abl_comp_Update = function()
	{
		if(parentAbility.inPressed == false) isReleased = true;
		if(isReleased) return true; 
		else return false;
	}
	
	abl_comp_Init = function()
	{
		parentAbility.ai_reqRelease = true;	
	}
	
	abl_comp_OnActivate = function()
	{
		isReleased = false;	
	}
}

function abl_comp_Charge(a_parent, a_chargeTime) : abl_comp_Input(a_parent) constructor
{
	array_push(tags,COMP_TAGS.CHARGE);	
	chargeTime = 1;
	chargeTimer = 0;
	if(a_chargeTime != undefined) chargeTime = a_chargeTime;
	
	abl_comp_Update = function()
	{
		if(parentAbility.inPressed) chargeTimer += GetDeltaTime(parentAbility.timeScale);
		if(chargeTimer > chargeTime) return false;
		else return true;
	}
	
	abl_comp_OnActivate = function()
	{
		isReleased = false;	
	}
}

function abl_comp_Burst(a_parent, a_BurstCount, a_BurstDelay) : abl_comp_Input(a_parent) constructor
{
	isBursting = false;
	burstCount = 3;
	burstCounter = 0;
	
	burstDelay = 0.5;
	burstTimer = 0;
	
	if(a_BurstCount != undefined) burstCount = a_BurstCount;
	if(a_BurstDelay != undefined) burstDelay = a_BurstDelay;
	
	abl_comp_Update = function()
	{
		
		if(parentAbility.inPressed && burstTimer >= burstDelay)
		{
			isBursting = true;	
		}
		
		if(isBursting)
		{
			parentAbility.forcePressed = true;	
		}else
		{
			parentAbility.forcePressed = false;
			burstTimer += GetDeltaTime(parentAbility.timeScale);	
		}
		
	}
	
	abl_comp_OnActivate = function()
	{
		burstCounter += 1;
		burstTimer = 0;
		
		if(burstCounter >= burstCount)
		{
			isBursting = false;	
		}
	}
}

