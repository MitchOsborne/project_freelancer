function GetGridCell(a_x, a_y) {


	var tX = floor(a_x / global.gridSize);
	var tY = floor(a_y / global.gridSize);

	return new Vector2(tX,tY);


}
