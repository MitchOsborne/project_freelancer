function InitLocomotor(a_loco, a_char) {

	var tInst = instance_create_layer(a_char.x,a_char.y,"Physics",a_loco);

	tInst.slavedObj = a_char;

	return tInst;
}

function MoveLoco(a_loco, a_char)
{
	a_loco.x = a_char.locoOffset.x+a_char.x;
	a_loco.y = a_char.locoOffset.y+a_char.y;	
}
