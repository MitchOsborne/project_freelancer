// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.bp_BlueprintArr = [];

global.ClassCards = 
{
	crd_Freelancer : new crd_Default(),
	crd_Gladiator : new crd_Fighter(),	
	crd_Rogue : new crd_Hunter(),	
}

enum DMG_TYPE
{
	HARD,
	SOFT,
	THERMAL,
	EM,
}

function bp_BaseBlueprint() constructor
{
	bp_Name = "Empty";
	bp_FlexType = "None";
	bp_Frame = abl_Empty;
	bp_Slot = LOADOUT_SLOT.NONE;
	bp_AbilityType = ABILITY_TYPE.NONE;
	bp_reqWeapon = -1;
	bp_Class = global.ClassCards.crd_Freelancer;
	bp_ReqKeywords = [];
	bp_Keywords = [];
	bp_DisplayActive = true;
	bp_StayActive = true;
	
	bp_WepStats = new bp_WeaponStats();
	bp_StatMods = new bp_WeaponStats();
	bp_Archetype = undefined;
	
	bp_Components = [];

	bp_AttackArr = [];
	bp_AttackFin = undefined;

	bp_Icon = spr_error;
	bp_SpriteSet = undefined;
	bp_SFX = undefined;
	
	bp_DMGType = DMG_TYPE.HARD;
	
	//Used to allow paired abilities to be equipped outside the class cards usual restrictions
	bp_wep1Slots = [];
	bp_wep2Slots = [];
	bp_util1Slots = [];
	bp_util2Slots = [];
	bp_abl1Slots = [];
	bp_abl2Slots = [];
	bp_coreSlots = [];
	
	bp_AltLd = undefined;
	
	if(!variable_global_exists("bp_BlueprintArr")) global.bp_BlueprintArr = [];
	array_push(global.bp_BlueprintArr, self);
}

function bp_WeaponStats() constructor
{
	bp_Range = 1; 
	bp_Spread = 1; 
	bp_RPM = 1;
	bp_Ammo = 1;
	bp_ReloadSpd = 1;
	bp_ReloadAmount = 0;
	bp_AutoReload = true;
	bp_EquipTime = 0.5;
	bp_ProjectileSize = 1;
	bp_ProjectileHP = 1;				//Used for projectiles penetrating enemies
	bp_ProjectileBounces = 0;			//Used for projectiles bouncing off walls
	bp_ProjectileCount = 1;				//Not used by melee weapons
	bp_ProjectileType = proj_Bullet;	//Not used by melee weapons, for now
	bp_SpeedCurve = "Default";			//Not used by melee weapons
	bp_ChargeBased = false;				//Used to prevent firing from reseting reload time
	bp_WeightBonus = 1.2;
	
	//Damage Split across types
	bp_DMGType = new DMGReport(0,1,0,0);
	
	bp_DMGScale = 1;
	bp_SPDScale = 0;
	bp_DURScale = 0;
	bp_CDScale = 0;
}

global.bp_Abilities =
{
	//Base Stats
	bp_BaseRifleStats : new bp_WeaponStats(),
	bp_BaseSidearmStats : new bp_WeaponStats(),
	bp_BaseShotgunStats : new bp_WeaponStats(),
	bp_BaseMeleeStats : new bp_WeaponStats(),
	bp_BaseGrenadeStats : new bp_WeaponStats(),
	
	//Class Blueprints
	bp_Class_Freelancer : new bp_BaseBlueprint(),
	bp_Class_Fighter : new bp_BaseBlueprint(),
	bp_Class_Hunter : new bp_BaseBlueprint(),
	
	//Base Abilities
	bp_Empty : new bp_BaseBlueprint(),
	bp_Unequip : new bp_BaseBlueprint(),
	
	//Evades
	bp_Evade : new bp_BaseBlueprint(),
	bp_BlitzEvade : new bp_BaseBlueprint(),
	
	//Self Buffs
	bp_RegenPop : new bp_BaseBlueprint(),
	bp_CritPop : new bp_BaseBlueprint(),
	bp_CombatJunkie : new bp_BaseBlueprint(),
	bp_EquipMosin : new bp_BaseBlueprint(),
	bp_KobaldRegroup : new bp_BaseBlueprint(),
	
	//Sidearms
	bp_M9T : new bp_BaseBlueprint(),
	bp_Rook443 : new bp_BaseBlueprint(),
	bp_Rook443H : new bp_BaseBlueprint(),
	bp_Naga44 : new bp_BaseBlueprint(),
	bp_Naga44H : new bp_BaseBlueprint(),
	bp_Naga44SG : new bp_BaseBlueprint(),
	bp_Storm : new bp_BaseBlueprint(),
	bp_Drake : new bp_BaseBlueprint(),
	
	//Rifles
	bp_RK : new bp_BaseBlueprint(),
	bp_RKL : new bp_BaseBlueprint(),
	bp_RKH : new bp_BaseBlueprint(),
	bp_RKR : new bp_BaseBlueprint(),
	bp_Mosin : new bp_BaseBlueprint(),
	bp_TheEnd : new bp_BaseBlueprint(),
	bp_Straya88 : new bp_BaseBlueprint(),
	bp_DomR : new bp_BaseBlueprint(),
	bp_DragnetRifle : new bp_BaseBlueprint(),
	bp_HarpoonRifle : new bp_BaseBlueprint(),
	
	//Shotguns
	bp_SP45 : new bp_BaseBlueprint(),
	bp_SP45r : new bp_BaseBlueprint(),
	bp_DBSG : new bp_BaseBlueprint(),
	bp_DBSGs : new bp_BaseBlueprint(),
	
	//Melee
	bp_CombatKnife : new bp_BaseBlueprint(),
	bp_Katana : new bp_BaseBlueprint(),
	bp_Shortsword : new bp_BaseBlueprint(),
	bp_NezumiBlade : new bp_BaseBlueprint(),
	bp_ShieldBash : new bp_BaseBlueprint(),
	
	bp_LungeBite : new bp_BaseBlueprint(),
	bp_Bite : new bp_BaseBlueprint(),
	bp_BladeDash : new bp_BaseBlueprint(),
	
	//MeleeAlt
	bp_KnifeThrow : new bp_BaseBlueprint(),
	bp_HatchetThrow : new bp_BaseBlueprint(),
	
	//Grenades
	bp_SpikeTraps : new bp_BaseBlueprint(),
	bp_FragGrenade : new bp_BaseBlueprint(),
	
	//Special
	bp_Soulfire : new bp_BaseBlueprint(),
	bp_SelfDestruct : new bp_BaseBlueprint(),
	bp_BossFlamer : new bp_BaseBlueprint(),
	
	//Enemy Abilities
	bp_YakiRifle : new bp_BaseBlueprint(),
	bp_YakiSword : new bp_BaseBlueprint(),
}
