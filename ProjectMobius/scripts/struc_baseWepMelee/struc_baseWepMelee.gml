// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information


enum PARRY_TYPE
{
	DEFLECT,
	REFLECT,
	BLOCK,
	NONE,
}

function defunc_base_wep_Melee() : base_Ability() constructor{

	AbilityType = ABILITY_TYPE.MELEE;
	wep_parryType = PARRY_TYPE.BLOCK;
	wep_SlotType = SLOT_TYPE.MELEE;
	melee_attackList = ds_list_create();
	melee_comboFinisher = undefined;
	
	melee_currAtk = undefined;
	
	/*
	Multiplier for Attack Speed, 
	e.g: 0.5 will allow for the next attack after waiting for half the 
	current attacks duration to expire
	*/
	melee_atkSpd = 1;
	melee_atkTimer = 0;
	
	melee_comboCount = 3;
	melee_comboDur = 1;
	melee_comboIndex = 0;
	melee_attackIndex = 0;
	
	melee_comboTimer = 0;
	
	speedMod = 1;
	
	reqRelease = true;
		
	range = 20;
	ai_engageRange = 30;
	
	abl_forceStrength = 0;
	abl_stunDur = 0.5;
	
	func_DrawStep = Melee_DrawWeapon;
	func_PreUpdateStep = MeleePreUpdateStep;
	func_UpdateStep = MeleeUpdateStep;
	func_EndUpdateStep = MeleeEndUpdateStep;
	func_InitAbility = MeleeInitWep;
	
	abl_Icon = spr_Slash;

	wep_projectileSpread = 0;
	wep_projectileCount = 1;
	wep_randomSpread = false;
	
	
	melee_forceRef = undefined;
	melee_recoilOnHit = false;
	melee_hasRecoiled = false;
	
	projSizeScale = 1;

	speedMod = 1;
	dmg_speedCurve = animcurve_get_channel(crv_ProjectileSpeed,"Default");
	
	wepProjList = ds_list_create();
	wepNodeList = ds_list_create();
	
	staminaCost = 0;
	
	animIndex = 0;
	animTimer = 0;
	animCtr = 1;
	animType = 0;
	animWep = 0;
	
}


function MeleeInitWep(a_wep)
{
	with a_wep
	{
		atk_InitAttack(self, melee_comboFinisher);
		for(var i = 0; i < ds_list_size(melee_attackList); i += 1)
		{
			atk_InitAttack(self, melee_attackList[|i]);	
		}
		animType = sprite_get_speed_type(weaponSprite);
		animCtr = sprite_get_speed(weaponSprite);
	}
}


function MeleePreUpdateStep(a_weapon)
{
	with a_weapon
	{
		AbilityPreUpdateStep(a_weapon);
		//ai_engageRange = range*0.9;
		
		resourceDisplay = melee_comboCount - melee_comboIndex;
		cdDisplay = melee_comboDur - melee_comboTimer;
		animCtr = sprite_get_speed(weaponSprite);

		if(teamID == TEAMID.Enemy)
		{
			melee_atkTimer -= (GetDeltaTime(timeScale)*owner.diffScaling)/10;
		}else
		{
			melee_atkTimer -= GetDeltaTime(timeScale);
		}

		MeleeComboCheck(self);
		
		
		if(melee_recoilOnHit && !melee_hasRecoiled)
		{
			if(array_length(abl_hitReports) != 0)
			{
				if(melee_forceRef != undefined)
				{
					with melee_forceRef
					{
						forceDirection += 180;
						durTimer = duration - durTimer;
						melee_hasRecoiled = true;
					}
				}
			}
		}
	}
	
}

function MeleeUpdateStep(a_weapon)
{
	with a_weapon
	{
		if (activate)
		{
			Melee_ActivateAttack(self);
		}
		if(melee_currAtk != undefined) atk_UpdateAtk(melee_currAtk);
			
		AbilityUpdateStep(self);
		
	}
	
}

function MeleeEndUpdateStep(a_ability)
{
	with a_ability
	{
		if(didActivate)
		{

			melee_comboIndex += 1;
			if(melee_comboIndex > melee_comboCount) melee_comboIndex = 0;
			melee_attackIndex += 1;
			if(melee_attackIndex > ds_list_size(melee_attackList)) melee_attackIndex = 0;
			melee_comboTimer = 0;
		}
		
		// Stops the index from moving outside the bounds of the projectile list
		if(melee_attackIndex >= ds_list_size(melee_attackList) || melee_attackIndex < 0)
		{
			melee_attackIndex = 0;
		}
		
		AbilityEndUpdateStep(self);
		
		if(didActivate)
		{
			animWep = true;
			animIndex = 1;
		}else if(animIndex >= sprite_get_number(weaponSprite)-1)
		{
			animWep = false;
			animIndex = 0;
		}
		
		if(animWep)
		{
			var animCheck = 0;
			if(animType == spritespeed_framespersecond)
			{
				animTimer += GetDeltaTime(timeScale);	
				animCheck = 1/animCtr;
			}else
			{
				animTimer += 1;
				animCheck = animCtr;
			}
			if(animTimer >= animCheck) 
			{
				animIndex += 1;
				animTimer = 0;
			}
			if(animIndex >= sprite_get_number(weaponSprite)) animIndex = 0;
		}
		
	}
}

function FinisherEndUpdateStep(a_ability)
{
	with a_ability
	{
		Melee_Finisher(self);
		if(didActivate)
		{
			AttachMod(debuff_moveLocked, owner, owner, melee_lungeDur + 0.2);
			AttachLinearForce(owner, owner, melee_lungeDur, owner.direction, melee_lungeForce);
		}
		AbilityEndUpdateStep(self);
	}
}

function Melee_ActivateAttack(a_ability)
{
	with a_ability
	{
		melee_currAtk = undefined;
		
		if(melee_comboFinisher != undefined && melee_comboIndex >= melee_comboCount)
		{
			melee_currAtk = melee_comboFinisher;
		}
		else melee_currAtk = melee_attackList[|melee_attackIndex];
		if(!is_undefined(melee_currAtk) && is_struct(melee_currAtk))
		{
			if(melee_atkTimer <= 0)
			{
				didActivate = true;
				melee_atkTimer = (melee_currAtk.atk_duration * melee_atkSpd);
				melee_comboTimer = 0;
				melee_currAtk.executeAttack = true;
			}
		}
	}
}

function Melee_DrawWeapon(a_ability)
{
	
	with a_ability
	{
		var flip = 1;
		if(owner.direction <= 180) flip = -1;
		var tDir = owner.direction + 90;
		if(animIndex == 0) tDir = 0;
		if(weaponSprite != undefined) draw_sprite_ext(weaponSprite,animIndex,owner.char_offset.x + abl_posOffset.x,owner.char_offset.y + abl_posOffset.y,0.75,0.75,tDir, c_white, 1);
		else draw_sprite(spr_error,0,owner.x, owner.y);
		
		
	}	
}

function Melee_RefundStamina(a_ability)
{
	with a_ability
	{
		owner.staminaStat += staminaCost;
	}
}

function MeleeComboCheck()
{
	if(abl_comp_comboIndex != 0)
		{
			abl_comp_comboTimer += GetDeltaTime(owner.timeScale);
			if(abl_comp_comboTimer >= abl_comp_comboDur)
			{
				abl_comp_comboIndex = 0;
				abl_comp_attackIndex = 0;
				abl_comp_comboTimer = 0;
			}
		}
		
}

