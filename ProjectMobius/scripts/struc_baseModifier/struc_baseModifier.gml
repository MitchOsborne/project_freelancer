// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum STACK_TYPE
{
	NONE,
	SELF,
	OTHER,
	ALL,
	SINGLE,
}

function base_Modifier() constructor{
	
	name = "base_Modifier";
	displayName = true;
	owner = undefined;			//Object that applied the Mod
	subject = undefined;		//Object the Mod is attached to
	canDelete = false;
	
	clearOnExit = false;

	stacks = 1;					//Number of stacks this Mod has
	stackLimit = 1;
	stackType = STACK_TYPE.ALL;		//Whether other objects applying the same Mod cause it to stack
	duration = 1;
	durTimer = 0;
	stackLoss = -1;
	
	mod_DMG = 1;
	mod_SPD = 1;
	mod_HP = 1;
	mod_SHD = 1;
	mod_DUR = 1;
	mod_CD = 1;
	mod_CRIT = 1;
	mod_PROC = 1;

	isDebuff = false;	//Indicates whether it is a Buff or Debuff

	timeScale = 1;
	
	mod_Init = function()
	{
			
	}
	
	mod_PreStep = function()
	{
		if(CheckExistance(subject) && CheckExistance(owner))
		{
			timeScale = subject.timeScale;
			x = subject.x;
			y = subject.y;
			
			stacks = clamp(stacks, 0, stackLimit);
			if(stacks <= 0) mod_Destroy();
		}else
		{
			mod_Destroy();
		}	
	}
	
	mod_Update = function()
	{
			
	}
	
	mod_EndStep = function()
	{
		
		if(durTimer > duration)
		{
			if(stackLoss > 0)
			{
				stacks -= stackLoss;
				durTimer = 0;
			}
			else mod_Destroy();	
		}
		//This is added after so that a modifier attached with a duration of 0 can run for 1 frame before being deleted
		durTimer += GetDeltaTime(timeScale);
		if(durTimer > duration) displayName = false;
	}
	
	mod_Destroy = function()
	{
		Destroy_Mod(self);	
	}
	
	mod_Draw = function()
	{
		
	}
	
	mod_OnHit = function(a_HitReport)
	{
		
	}
	
	mod_OnKill = function(a_HitReport)
	{
		
	}
	
	mod_OnDeath = function(a_HitReport)
	{
		
	}
	
	mod_OnHurt = function(a_HitReport)
	{
		
	}
	
}

function Destroy_Mod(a_Mod)
{
	with a_Mod
	{
		canDelete = true;
	}
}