function GetDeltaTime(a_TimeScale) {
	var retVal = (delta_time/1000000) * (a_TimeScale * global.timeScale);
	if(global.debug) retVal = 0.016666 * (a_TimeScale * global.timeScale);
	return retVal;
}

// Returns DeltaTime unaffected by TimeScale
function GetFixedDeltaTime()
{
	var retVal = (delta_time/1000000); 
	if(global.debug) retVal = 0.016666;
	return retVal;
}

//Returns the difference between the target FPS and the current FPS
function GetSigmaTime(a_TimeScale)
{
	var t = GetFixedDeltaTime();
	var nyuk = 1/(global.targetFPS*t) * (a_TimeScale * global.timeScale);
	return nyuk;
}