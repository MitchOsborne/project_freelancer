// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
global.AgentCollisionClass = [base_PhysWall, base_PhysGap, base_PhysProp];
global.AgentCollisionClassSprinting =  [base_PhysWall, base_PhysGap];
global.AgentCollisionClassDashing =  [base_PhysWall, base_PhysGap];
global.AgentTileCollisionClass = [TILE_PHYS.WALL, TILE_PHYS.GAP];

global.ProjectileCollisionClass = [ base_PhysProp, base_PhysWall, base_PhysBarrier];
global.ProjectileTileCollisionClass= [TILE_PHYS.WALL, TILE_PHYS.BARRIER];