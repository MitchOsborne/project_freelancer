{
  "$GMRoom":"v1",
  "%Name":"t_room_cave_camp",
  "creationCodeFile":"",
  "inheritCode":false,
  "inheritCreationOrder":false,
  "inheritLayers":false,
  "instanceCreationOrder":[
    {"name":"inst_7DB826DC_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_1BE262CA_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_5BA824E2_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_3ED4EDE5_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_343B787B_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_41553A18_1","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_A83CFD3","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_1F0FB26B","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_47D98A9A","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_3B05BAFC","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
    {"name":"inst_6331DC55","path":"rooms/t_room_cave_camp/t_room_cave_camp.yy",},
  ],
  "isDnd":false,
  "layers":[
    {"$GMRInstanceLayer":"","%Name":"hud","depth":-600,"effectEnabled":true,"effectType":null,"gridX":16,"gridY":16,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"hud","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Heat_Map","depth":-500,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_7DB826DC_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7DB826DC_1","objectId":{"name":"HeatRegion","path":"objects/HeatRegion/HeatRegion.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":32.0,"scaleY":24.0,"x":256.0,"y":0.0,},
      ],"layers":[],"name":"Heat_Map","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":false,},
    {"$GMRInstanceLayer":"","%Name":"Persistance","depth":-400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_1BE262CA_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_1BE262CA_1","objectId":{"name":"CameraMarker","path":"objects/CameraMarker/CameraMarker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":384.0,"y":256.0,},
        {"$GMRInstance":"v1","%Name":"inst_5BA824E2_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_5BA824E2_1","objectId":{"name":"Commander","path":"objects/Commander/Commander.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":-160.0,"y":-64.0,},
        {"$GMRInstance":"v1","%Name":"inst_3ED4EDE5_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3ED4EDE5_1","objectId":{"name":"Debugger","path":"objects/Debugger/Debugger.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":-352.0,"y":-64.0,},
        {"$GMRInstance":"v1","%Name":"inst_343B787B_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_343B787B_1","objectId":{"name":"ProjectileManager","path":"objects/ProjectileManager/ProjectileManager.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":-96.0,"y":-64.0,},
      ],"layers":[],"name":"Persistance","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Physics","depth":-300,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Physics","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Text","depth":-200,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Text","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Debug","depth":-100,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_41553A18_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_41553A18_1","objectId":{"name":"entry_default","path":"objects/entry_default/entry_default.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":360.0,"y":240.0,},
        {"$GMRInstance":"v1","%Name":"inst_A83CFD3","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_A83CFD3","objectId":{"name":"event_Marker","path":"objects/event_Marker/event_Marker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":256.0,"y":704.0,},
        {"$GMRInstance":"v1","%Name":"inst_1F0FB26B","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_1F0FB26B","objectId":{"name":"event_Marker","path":"objects/event_Marker/event_Marker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":264.0,"y":10.0,},
        {"$GMRInstance":"v1","%Name":"inst_47D98A9A","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_47D98A9A","objectId":{"name":"event_Marker","path":"objects/event_Marker/event_Marker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":336.0,"y":240.0,},
      ],"layers":[
        {"$GMRInstanceLayer":"","%Name":"Doors","depth":0,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
            {"$GMRInstance":"v1","%Name":"inst_6331DC55","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_6331DC55","objectId":{"name":"tp_doorway","path":"objects/tp_doorway/tp_doorway.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.5,"scaleY":0.75,"x":304.0,"y":112.0,},
            {"$GMRInstance":"v1","%Name":"inst_3B05BAFC","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3B05BAFC","objectId":{"name":"base_entry","path":"objects/base_entry/base_entry.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":328.0,"y":160.0,},
          ],"layers":[],"name":"Doors","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
      ],"name":"Debug","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Lighting","depth":100,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Lighting","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Abilities","depth":200,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Abilities","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Modifiers","depth":300,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Modifiers","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Projectiles","depth":400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Projectiles","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":true,"visible":true,},
    {"$GMRTileLayer":"","%Name":"Tiles_Physics","depth":500,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Physics","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":30,"TileCompressedData":[
          -197,
          0,
          -7,
          2,
          -23,
          0,
          -2,
          2,
          -3,
          0,
          -2,
          2,
          -23,
          0,
          2,
          2,
          1,
          -3,
          0,
          2,
          1,
          2,
          -23,
          0,
          1,
          2,
          -5,
          0,
          1,
          2,
          -17,
          0,
          -5,
          2,
          2,
          0,
          2,
          -5,
          0,
          1,
          2,
          -16,
          0,
          -8,
          2,
          -5,
          0,
          -5,
          2,
          -12,
          0,
          1,
          2,
          -7,
          1,
          -5,
          0,
          -4,
          1,
          -2,
          2,
          -11,
          0,
          1,
          2,
          -16,
          0,
          2,
          1,
          2,
          -11,
          0,
          1,
          2,
          -17,
          0,
          -2,
          2,
          -10,
          0,
          1,
          2,
          -17,
          0,
          2,
          1,
          2,
          -10,
          0,
          1,
          2,
          -18,
          0,
          1,
          2,
          -10,
          0,
          1,
          2,
          -18,
          0,
          1,
          2,
          -10,
          0,
          1,
          2,
          -13,
          1,
          -5,
          0,
          1,
          1,
          -10,
          0,
          1,
          2,
          -19,
          1,
          -900,
          -2147483648,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Physics","path":"tilesets/tSet_Physics/tSet_Physics.yy",},"userdefinedDepth":false,"visible":false,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Props","depth":600,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Props","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":30,"TileCompressedData":[
          -600,
          0,
          -900,
          -2147483648,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Desert_16x16","path":"tilesets/tSet_Desert_16x16/tSet_Desert_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Front","depth":700,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Front","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":30,"TileCompressedData":[
          -559,
          -2147483648,
          -2,
          0,
          -939,
          -2147483648,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Desert_16x16","path":"tilesets/tSet_Desert_16x16/tSet_Desert_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRInstanceLayer":"","%Name":"Instances","depth":800,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Instances","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Mid","depth":900,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Mid","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":30,"TileCompressedData":[
          -6,
          0,
          3,
          64,
          2,
          67,
          -17,
          0,
          3,
          128,
          3,
          131,
          -5,
          0,
          5,
          32,
          33,
          0,
          98,
          99,
          -7,
          0,
          13,
          97,
          98,
          98,
          97,
          98,
          97,
          98,
          98,
          0,
          0,
          128,
          3,
          131,
          -3,
          0,
          7,
          32,
          33,
          0,
          2,
          67,
          3,
          131,
          -5,
          0,
          3,
          2,
          67,
          128,
          -7,
          3,
          15,
          64,
          2,
          160,
          3,
          163,
          35,
          0,
          0,
          64,
          2,
          0,
          98,
          99,
          3,
          131,
          -5,
          0,
          3,
          98,
          99,
          128,
          -7,
          3,
          23,
          96,
          98,
          98,
          66,
          2,
          67,
          0,
          32,
          0,
          0,
          99,
          3,
          131,
          3,
          163,
          0,
          97,
          98,
          98,
          99,
          3,
          131,
          160,
          -7,
          3,
          16,
          128,
          3,
          4,
          96,
          98,
          99,
          0,
          64,
          2,
          67,
          131,
          3,
          131,
          0,
          0,
          99,
          -3,
          3,
          27,
          131,
          3,
          131,
          0,
          0,
          97,
          98,
          98,
          97,
          98,
          0,
          128,
          3,
          4,
          128,
          3,
          4,
          0,
          96,
          98,
          99,
          131,
          3,
          163,
          0,
          67,
          131,
          -3,
          3,
          6,
          131,
          3,
          163,
          0,
          67,
          128,
          -3,
          3,
          18,
          131,
          64,
          160,
          3,
          4,
          128,
          3,
          4,
          0,
          128,
          3,
          131,
          163,
          0,
          0,
          2,
          67,
          131,
          -3,
          3,
          1,
          163,
          -3,
          0,
          7,
          67,
          128,
          129,
          130,
          130,
          131,
          64,
          -3,
          0,
          7,
          160,
          3,
          4,
          0,
          128,
          3,
          131,
          -4,
          0,
          2,
          99,
          163,
          -7,
          0,
          7,
          67,
          160,
          161,
          162,
          162,
          163,
          64,
          -7,
          0,
          17,
          160,
          3,
          163,
          2,
          0,
          0,
          67,
          131,
          0,
          65,
          98,
          97,
          98,
          97,
          98,
          98,
          99,
          -5,
          0,
          9,
          96,
          98,
          97,
          98,
          65,
          0,
          0,
          64,
          2,
          -4,
          0,
          5,
          2,
          67,
          131,
          0,
          67,
          -6,
          3,
          1,
          131,
          -5,
          0,
          1,
          128,
          -3,
          3,
          14,
          96,
          65,
          0,
          96,
          98,
          98,
          0,
          2,
          0,
          98,
          99,
          163,
          0,
          67,
          -6,
          3,
          1,
          131,
          -5,
          0,
          1,
          128,
          -3,
          3,
          14,
          128,
          64,
          0,
          128,
          3,
          4,
          96,
          98,
          99,
          3,
          131,
          0,
          0,
          67,
          -6,
          3,
          1,
          163,
          -5,
          0,
          1,
          160,
          -3,
          3,
          14,
          128,
          96,
          65,
          128,
          3,
          4,
          128,
          3,
          131,
          3,
          131,
          0,
          0,
          67,
          -16,
          0,
          14,
          160,
          128,
          64,
          160,
          3,
          4,
          128,
          3,
          131,
          4,
          163,
          0,
          0,
          67,
          -17,
          0,
          2,
          128,
          64,
          -3,
          0,
          3,
          160,
          3,
          163,
          -4,
          0,
          1,
          67,
          -17,
          0,
          13,
          160,
          96,
          98,
          97,
          98,
          97,
          98,
          97,
          98,
          97,
          98,
          98,
          99,
          -18,
          0,
          1,
          128,
          -10,
          3,
          1,
          131,
          -18,
          0,
          1,
          128,
          -10,
          3,
          2,
          131,
          44,
          -11,
          113,
          1,
          46,
          -5,
          0,
          1,
          160,
          -10,
          3,
          2,
          163,
          82,
          -11,
          0,
          1,
          112,
          -6,
          113,
          -11,
          45,
          1,
          114,
          -8,
          0,
          -6,
          65,
          -19,
          0,
          -11,
          65,
          -12,
          0,
          1,
          480,
          -17,
          65,
          -6,
          0,
          1,
          480,
          -6,
          0,
          -17,
          65,
          1,
          0,
          -29,
          65,
          1,
          0,
          -29,
          65,
          1,
          0,
          -24,
          65,
          -6,
          0,
          -18,
          65,
          -12,
          0,
          -14,
          65,
          -11,
          0,
          5,
          32,
          33,
          33,
          35,
          0,
          -14,
          65,
          -2,
          0,
          3,
          32,
          33,
          35,
          -6,
          0,
          4,
          64,
          2,
          2,
          67,
          -4,
          0,
          -11,
          65,
          -2,
          0,
          3,
          64,
          2,
          67,
          -5,
          0,
          6,
          48,
          96,
          98,
          97,
          99,
          50,
          -3,
          0,
          -11,
          65,
          -2,
          0,
          3,
          96,
          98,
          99,
          -3,
          0,
          8,
          480,
          0,
          80,
          128,
          3,
          3,
          131,
          82,
          -3,
          0,
          -11,
          65,
          16,
          0,
          32,
          128,
          3,
          131,
          0,
          0,
          65,
          65,
          0,
          80,
          128,
          3,
          3,
          131,
          82,
          -3,
          0,
          -11,
          65,
          16,
          0,
          64,
          128,
          3,
          131,
          0,
          0,
          65,
          65,
          0,
          80,
          160,
          3,
          3,
          163,
          82,
          -3,
          0,
          -11,
          65,
          6,
          48,
          96,
          160,
          3,
          163,
          35,
          -4,
          0,
          1,
          112,
          -4,
          113,
          4,
          114,
          0,
          480,
          0,
          -11,
          65,
          6,
          80,
          128,
          3,
          64,
          2,
          67,
          -13,
          0,
          -11,
          65,
          7,
          80,
          128,
          3,
          96,
          98,
          99,
          50,
          -6,
          0,
          -16,
          65,
          8,
          0,
          80,
          160,
          3,
          128,
          3,
          131,
          82,
          -6,
          0,
          -16,
          65,
          10,
          0,
          112,
          113,
          46,
          128,
          3,
          105,
          82,
          0,
          480,
          -4,
          0,
          -16,
          65,
          -3,
          0,
          5,
          80,
          160,
          3,
          137,
          82,
          -6,
          0,
          -16,
          65,
          -3,
          0,
          1,
          112,
          -3,
          113,
          1,
          114,
          -6,
          0,
          -16,
          65,
          -14,
          0,
          -16,
          65,
          -12,
          0,
          -6,
          65,
          -2,
          0,
          -13,
          65,
          -9,
          0,
          -6,
          65,
          -2,
          0,
          -13,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -9,
          0,
          -6,
          65,
          -47,
          0,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Desert_16x16","path":"tilesets/tSet_Desert_16x16/tSet_Desert_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Back","depth":1000,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Back","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":30,"TileCompressedData":[
          -1500,
          257,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Desert_16x16","path":"tilesets/tSet_Desert_16x16/tSet_Desert_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRBackgroundLayer":"","%Name":"Background","animationFPS":15.0,"animationSpeedType":0,"colour":4278190080,"depth":1100,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Background","properties":[],"resourceType":"GMRBackgroundLayer","resourceVersion":"2.0","spriteId":null,"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":false,"x":0,"y":0,},
  ],
  "name":"t_room_cave_camp",
  "parent":{
    "name":"Cave",
    "path":"folders/Rooms/Cave.yy",
  },
  "parentRoom":null,
  "physicsSettings":{
    "inheritPhysicsSettings":false,
    "PhysicsWorld":false,
    "PhysicsWorldGravityX":0.0,
    "PhysicsWorldGravityY":10.0,
    "PhysicsWorldPixToMetres":0.1,
  },
  "resourceType":"GMRoom",
  "resourceVersion":"2.0",
  "roomSettings":{
    "Height":800,
    "inheritRoomSettings":false,
    "persistent":false,
    "Width":480,
  },
  "sequenceId":null,
  "views":[
    {"hborder":32,"hport":700,"hspeed":1,"hview":200,"inherit":false,"objectId":null,"vborder":32,"visible":true,"vspeed":1,"wport":800,"wview":280,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings":{
    "clearDisplayBuffer":true,
    "clearViewBackground":false,
    "enableViews":true,
    "inheritViewSettings":false,
  },
  "volume":1.0,
}