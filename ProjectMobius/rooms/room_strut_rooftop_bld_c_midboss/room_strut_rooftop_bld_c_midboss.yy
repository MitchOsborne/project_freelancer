{
  "$GMRoom":"v1",
  "%Name":"room_strut_rooftop_bld_c_midboss",
  "creationCodeFile":"",
  "inheritCode":false,
  "inheritCreationOrder":false,
  "inheritLayers":false,
  "instanceCreationOrder":[
    {"name":"inst_702D6A69_1_3_4_2_1_2","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_1306D5CF_1_1_1","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_779764B0_1_1_1","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_3AAC65CE_1_1","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_FB337AB_1","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_F240D0F_1","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_101231E4","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_2E3A8660","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_D1AA94E","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_F607B7E","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
    {"name":"inst_3B9BDFB6","path":"rooms/room_strut_rooftop_bld_c_midboss/room_strut_rooftop_bld_c_midboss.yy",},
  ],
  "isDnd":false,
  "layers":[
    {"$GMRInstanceLayer":"","%Name":"hud","depth":-900,"effectEnabled":true,"effectType":null,"gridX":16,"gridY":16,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"hud","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Text","depth":-800,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Text","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Debug","depth":-700,"effectEnabled":true,"effectType":null,"gridX":16,"gridY":16,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[
        {"$GMRInstanceLayer":"","%Name":"Doors","depth":-600,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
            {"$GMRInstance":"v1","%Name":"inst_702D6A69_1_3_4_2_1_2","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_702D6A69_1_3_4_2_1_2","objectId":{"name":"entry_default","path":"objects/entry_default/entry_default.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":568.0,"y":240.0,},
            {"$GMRInstance":"v1","%Name":"inst_1306D5CF_1_1_1","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_1306D5CF_1_1_1","objectId":{"name":"base_entry","path":"objects/base_entry/base_entry.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":240.0,},
            {"$GMRInstance":"v1","%Name":"inst_779764B0_1_1_1","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_779764B0_1_1_1","objectId":{"name":"base_entry","path":"objects/base_entry/base_entry.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":584.0,"y":240.0,},
            {"$GMRInstance":"v1","%Name":"inst_FB337AB_1","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_FB337AB_1","objectId":{"name":"tp_doorway","path":"objects/tp_doorway/tp_doorway.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":0.5,"scaleY":1.0,"x":608.0,"y":224.0,},
            {"$GMRInstance":"v1","%Name":"inst_F240D0F_1","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_F240D0F_1","objectId":{"name":"tp_doorway","path":"objects/tp_doorway/tp_doorway.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":0.5,"scaleY":1.0,"x":16.0,"y":224.0,},
          ],"layers":[],"name":"Doors","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRInstanceLayer":"","%Name":"Patrol","depth":-500,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
            {"$GMRInstance":"v1","%Name":"inst_101231E4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_101231E4","objectId":{"name":"region_EnemySpawn","path":"objects/region_EnemySpawn/region_EnemySpawn.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":0.375,"x":288.0,"y":40.0,},
            {"$GMRInstance":"v1","%Name":"inst_2E3A8660","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_2E3A8660","objectId":{"name":"region_EnemySpawn","path":"objects/region_EnemySpawn/region_EnemySpawn.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.5,"scaleY":0.5,"x":272.0,"y":408.0,},
            {"$GMRInstance":"v1","%Name":"inst_D1AA94E","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_D1AA94E","objectId":{"name":"event_Marker","path":"objects/event_Marker/event_Marker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":320.0,"y":240.0,},
          ],"layers":[],"name":"Patrol","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRInstanceLayer":"","%Name":"Persistence","depth":-400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Persistence","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRInstanceLayer":"","%Name":"Physics","depth":-300,"effectEnabled":true,"effectType":null,"gridX":2,"gridY":2,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Physics","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRTileLayer":"","%Name":"Tiles_Physics","depth":-200,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Physics","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":30,"SerialiseWidth":40,"TileCompressedData":[
              -41,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -8,
              0,
              -10,
              2,
              -2,
              0,
              -11,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -11,
              2,
              -21,
              0,
              -10,
              2,
              -38,
              0,
              -2,
              2,
              -38,
              0,
              -11,
              2,
              -21,
              0,
              -10,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              1,
              2,
              -21,
              0,
              1,
              2,
              -7,
              0,
              -2,
              2,
              -8,
              0,
              -10,
              2,
              -2,
              0,
              -11,
              2,
              -7,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -2,
              2,
              -13,
              0,
              1,
              2,
              -10,
              0,
              1,
              2,
              -13,
              0,
              -41,
              2,
            ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Physics","path":"tilesets/tSet_Physics/tSet_Physics.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
      ],"name":"Debug","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Lighting","depth":-100,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_3AAC65CE_1_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3AAC65CE_1_1","objectId":{"name":"light_AmbCircle","path":"objects/light_AmbCircle/light_AmbCircle.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":8.0,"y":336.0,},
      ],"layers":[],"name":"Lighting","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Front","depth":0,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Front","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":30,"SerialiseWidth":40,"TileCompressedData":[
          -7,
          0,
          -30,
          5,
          -10,
          0,
          -30,
          5,
          -10,
          0,
          -30,
          5,
          -10,
          0,
          -30,
          5,
          -10,
          0,
          -30,
          5,
          -10,
          0,
          -30,
          5,
          -732,
          0,
          -28,
          5,
          -12,
          0,
          -28,
          5,
          -12,
          0,
          -28,
          5,
          -12,
          0,
          -28,
          5,
          -12,
          0,
          -28,
          5,
          -12,
          0,
          -28,
          5,
          -3,
          0,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_16x16","path":"tilesets/tSet_Complex_16x16/tSet_Complex_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRInstanceLayer":"","%Name":"Events","depth":100,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[
        {"$GMRInstanceLayer":"","%Name":"Event_0","depth":200,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
            {"$GMRInstance":"v1","%Name":"inst_F607B7E","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_F607B7E","objectId":{"name":"trig_WaterCurrent","path":"objects/trig_WaterCurrent/trig_WaterCurrent.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":18.0,"scaleY":5.0,"x":160.0,"y":240.0,},
            {"$GMRInstance":"v1","%Name":"inst_3B9BDFB6","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3B9BDFB6","objectId":{"name":"trig_WaterCurrent","path":"objects/trig_WaterCurrent/trig_WaterCurrent.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":-180.0,"scaleX":16.0,"scaleY":5.0,"x":496.0,"y":240.0,},
          ],"layers":[],"name":"Event_0","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
      ],"name":"Events","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Instances","depth":300,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[
        {"$GMRInstanceLayer":"","%Name":"Projectiles","depth":400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Projectiles","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRInstanceLayer":"","%Name":"Interactables","depth":500,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Interactables","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
        {"$GMRInstanceLayer":"","%Name":"Characters","depth":600,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Characters","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
      ],"name":"Instances","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":true,"visible":true,},
    {"$GMRLayer":"","%Name":"Tilemap","depth":700,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[
        {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Mid","depth":800,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Mid","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":30,"SerialiseWidth":40,"TileCompressedData":[
              -290,
              0,
              1,
              100,
              -19,
              101,
              1,
              102,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -10,
              0,
              -10,
              66,
              -19,
              0,
              -9,
              66,
              -2,
              0,
              -10,
              66,
              -19,
              0,
              -9,
              66,
              -11,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              132,
              -19,
              0,
              1,
              134,
              -19,
              0,
              1,
              164,
              -19,
              165,
              1,
              166,
              -289,
              0,
            ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_16x16","path":"tilesets/tSet_Complex_16x16/tSet_Complex_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
        {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Back_Desert","depth":900,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Back_Desert","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":30,"SerialiseWidth":40,"TileCompressedData":[
              -509,
              -2147483648,
              -3,
              0,
              -37,
              -2147483648,
              -4,
              0,
              -34,
              -2147483648,
              -11,
              0,
              -29,
              -2147483648,
              -7,
              0,
              -33,
              -2147483648,
              2,
              0,
              -2147483648,
              -5,
              0,
              -33,
              -2147483648,
              -7,
              0,
              -34,
              -2147483648,
              -7,
              0,
              -33,
              -2147483648,
              -5,
              0,
              2,
              -2147483648,
              0,
              -33,
              -2147483648,
              -2,
              0,
              3,
              -2147483648,
              0,
              0,
              -36,
              -2147483648,
              -4,
              0,
              -37,
              -2147483648,
              -3,
              0,
              -287,
              -2147483648,
            ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Desert_16x16","path":"tilesets/tSet_Desert_16x16/tSet_Desert_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
        {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Back","depth":1000,"effectEnabled":true,"effectType":"none","gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Back","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":30,"SerialiseWidth":40,"TileCompressedData":[
              -259,
              0,
              2,
              177,
              178,
              -29,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -10,
              0,
              -38,
              2,
              -2,
              0,
              -38,
              2,
              -11,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -19,
              0,
              -21,
              2,
              -28,
              0,
              2,
              145,
              146,
              -259,
              0,
            ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_16x16","path":"tilesets/tSet_Complex_16x16/tSet_Complex_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
      ],"name":"Tilemap","properties":[],"resourceType":"GMRLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRBackgroundLayer":"","%Name":"Background","animationFPS":15.0,"animationSpeedType":0,"colour":4278190080,"depth":1100,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Background","properties":[],"resourceType":"GMRBackgroundLayer","resourceVersion":"2.0","spriteId":null,"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":false,"x":0,"y":0,},
  ],
  "name":"room_strut_rooftop_bld_c_midboss",
  "parent":{
    "name":"StrutA",
    "path":"folders/Rooms/StrutA.yy",
  },
  "parentRoom":null,
  "physicsSettings":{
    "inheritPhysicsSettings":false,
    "PhysicsWorld":false,
    "PhysicsWorldGravityX":0.0,
    "PhysicsWorldGravityY":10.0,
    "PhysicsWorldPixToMetres":0.1,
  },
  "resourceType":"GMRoom",
  "resourceVersion":"2.0",
  "roomSettings":{
    "Height":480,
    "inheritRoomSettings":false,
    "persistent":false,
    "Width":640,
  },
  "sequenceId":null,
  "views":[
    {"hborder":32,"hport":700,"hspeed":1,"hview":200,"inherit":false,"objectId":null,"vborder":32,"visible":true,"vspeed":1,"wport":800,"wview":280,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings":{
    "clearDisplayBuffer":true,
    "clearViewBackground":false,
    "enableViews":true,
    "inheritViewSettings":false,
  },
  "volume":1.0,
}