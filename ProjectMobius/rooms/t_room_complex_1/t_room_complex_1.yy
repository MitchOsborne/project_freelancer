{
  "$GMRoom":"v1",
  "%Name":"t_room_complex_1",
  "creationCodeFile":"",
  "inheritCode":false,
  "inheritCreationOrder":false,
  "inheritLayers":false,
  "instanceCreationOrder":[
    {"name":"inst_7DB826DC_1_1_2","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_4EAE5EE_2","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_702D6A69_1","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_75896DD8","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_2EC0278C","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_2701A1C3","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_65B6970","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_239B8221","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_18F36D36","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_2C59DAA4","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_3ABC6CE5","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_44B3D1C1","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_265A522A","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_618375CA","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_7A58F5ED","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_4B2B517D","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_7B60E7E9","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
    {"name":"inst_25162B92","path":"rooms/t_room_complex_1/t_room_complex_1.yy",},
  ],
  "isDnd":false,
  "layers":[
    {"$GMRInstanceLayer":"","%Name":"hud","depth":-600,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"hud","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Heat_Map","depth":-500,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_7DB826DC_1_1_2","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7DB826DC_1_1_2","objectId":{"name":"HeatRegion","path":"objects/HeatRegion/HeatRegion.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":32.0,"scaleY":24.0,"x":256.0,"y":0.0,},
      ],"layers":[],"name":"Heat_Map","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":false,},
    {"$GMRInstanceLayer":"","%Name":"Persistance","depth":-400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Persistance","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Physics","depth":-300,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Physics","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Text","depth":-200,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Text","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Debug","depth":-100,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_4EAE5EE_2","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_4EAE5EE_2","objectId":{"name":"region_EnemySpawn","path":"objects/region_EnemySpawn/region_EnemySpawn.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.25,"scaleY":0.125,"x":80.0,"y":32.0,},
        {"$GMRInstance":"v1","%Name":"inst_702D6A69_1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_702D6A69_1","objectId":{"name":"entry_default","path":"objects/entry_default/entry_default.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":120.0,"y":104.0,},
        {"$GMRInstance":"v1","%Name":"inst_75896DD8","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_75896DD8","objectId":{"name":"region_EnemySpawn","path":"objects/region_EnemySpawn/region_EnemySpawn.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.25,"scaleY":0.125,"x":80.0,"y":176.0,},
        {"$GMRInstance":"v1","%Name":"inst_2EC0278C","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_2EC0278C","objectId":{"name":"region_EnemySpawn","path":"objects/region_EnemySpawn/region_EnemySpawn.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":0.75,"scaleY":0.625,"x":144.0,"y":80.0,},
        {"$GMRInstance":"v1","%Name":"inst_2701A1C3","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_2701A1C3","objectId":{"name":"tp_doorway_left1","path":"objects/tp_doorway_left1/tp_doorway_left1.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":0.75,"scaleY":0.5,"x":-8.0,"y":96.0,},
        {"$GMRInstance":"v1","%Name":"inst_65B6970","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_65B6970","objectId":{"name":"tp_doorway_right1","path":"objects/tp_doorway_right1/tp_doorway_right1.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":0.75,"scaleY":0.5,"x":272.0,"y":96.0,},
        {"$GMRInstance":"v1","%Name":"inst_265A522A","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_265A522A","objectId":{"name":"PatrolMarker","path":"objects/PatrolMarker/PatrolMarker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":152.0,},
        {"$GMRInstance":"v1","%Name":"inst_618375CA","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_618375CA","objectId":{"name":"PatrolMarker_Start","path":"objects/PatrolMarker_Start/PatrolMarker_Start.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":240.0,"y":152.0,},
        {"$GMRInstance":"v1","%Name":"inst_7A58F5ED","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7A58F5ED","objectId":{"name":"PatrolMarker_Start","path":"objects/PatrolMarker_Start/PatrolMarker_Start.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":32.0,},
        {"$GMRInstance":"v1","%Name":"inst_4B2B517D","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_4B2B517D","objectId":{"name":"PatrolMarker","path":"objects/PatrolMarker/PatrolMarker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":256.0,"y":32.0,},
        {"$GMRInstance":"v1","%Name":"inst_7B60E7E9","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_7B60E7E9","objectId":{"name":"PatrolMarker","path":"objects/PatrolMarker/PatrolMarker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":256.0,"y":72.0,},
        {"$GMRInstance":"v1","%Name":"inst_25162B92","colour":4294967295,"frozen":false,"hasCreationCode":true,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_25162B92","objectId":{"name":"PatrolMarker","path":"objects/PatrolMarker/PatrolMarker.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":1.0,"scaleY":1.0,"x":40.0,"y":72.0,},
      ],"layers":[],"name":"Debug","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Lighting","depth":0,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[
        {"$GMRInstance":"v1","%Name":"inst_239B8221","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_239B8221","objectId":{"name":"light_AmbBox","path":"objects/light_AmbBox/light_AmbBox.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":4.375,"scaleY":1.25,"x":70.0,"y":100.0,},
        {"$GMRInstance":"v1","%Name":"inst_18F36D36","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_18F36D36","objectId":{"name":"light_EmergencyLight","path":"objects/light_EmergencyLight/light_EmergencyLight.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.25,"scaleY":1.4000001,"x":144.0,"y":24.0,},
        {"$GMRInstance":"v1","%Name":"inst_2C59DAA4","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_2C59DAA4","objectId":{"name":"light_EmergencyLight","path":"objects/light_EmergencyLight/light_EmergencyLight.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":2.25,"scaleY":1.4000001,"x":144.0,"y":184.0,},
        {"$GMRInstance":"v1","%Name":"inst_3ABC6CE5","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_3ABC6CE5","objectId":{"name":"light_EmergencyLight","path":"objects/light_EmergencyLight/light_EmergencyLight.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":-2.23,"scaleY":1.4000001,"x":144.0,"y":24.0,},
        {"$GMRInstance":"v1","%Name":"inst_44B3D1C1","colour":4294967295,"frozen":false,"hasCreationCode":false,"ignore":false,"imageIndex":0,"imageSpeed":1.0,"inheritCode":false,"inheritedItemId":null,"inheritItemSettings":false,"isDnd":false,"name":"inst_44B3D1C1","objectId":{"name":"light_EmergencyLight","path":"objects/light_EmergencyLight/light_EmergencyLight.yy",},"properties":[],"resourceType":"GMRInstance","resourceVersion":"2.0","rotation":0.0,"scaleX":-2.23,"scaleY":1.4000001,"x":144.0,"y":184.0,},
      ],"layers":[],"name":"Lighting","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Abilities","depth":100,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Abilities","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Modifiers","depth":200,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Modifiers","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":false,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Instances","depth":300,"effectEnabled":true,"effectType":null,"gridX":8,"gridY":8,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Instances","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":true,"visible":true,},
    {"$GMRInstanceLayer":"","%Name":"Projectiles","depth":400,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":true,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"instances":[],"layers":[],"name":"Projectiles","properties":[],"resourceType":"GMRInstanceLayer","resourceVersion":"2.0","userdefinedDepth":true,"visible":true,},
    {"$GMRTileLayer":"","%Name":"Tiles_Physics","depth":500,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Physics","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":25,"SerialiseWidth":35,"TileCompressedData":[
          -73,
          2,
          -30,
          1,
          -5,
          2,
          -30,
          0,
          -5,
          2,
          -30,
          0,
          -5,
          2,
          -27,
          0,
          3,
          2,
          0,
          0,
          -5,
          2,
          -4,
          0,
          -10,
          2,
          7,
          0,
          2,
          2,
          0,
          2,
          2,
          0,
          -7,
          2,
          -2,
          0,
          -5,
          2,
          -4,
          0,
          -10,
          1,
          7,
          0,
          1,
          1,
          0,
          1,
          1,
          0,
          -7,
          1,
          -2,
          0,
          -5,
          2,
          -30,
          0,
          -5,
          2,
          -30,
          0,
          -5,
          2,
          -30,
          0,
          -4,
          2,
          1,
          1,
          -22,
          0,
          -6,
          -2147483648,
          -2,
          0,
          3,
          1,
          2,
          2,
          -24,
          0,
          -6,
          -2147483648,
          -4,
          0,
          1,
          2,
          -24,
          0,
          -6,
          -2147483648,
          -4,
          0,
          -3,
          2,
          -22,
          0,
          -6,
          -2147483648,
          -2,
          0,
          -5,
          2,
          -22,
          0,
          -6,
          -2147483648,
          -2,
          0,
          -5,
          2,
          -4,
          0,
          -25,
          2,
          1,
          0,
          -5,
          2,
          -4,
          0,
          -24,
          1,
          2,
          2,
          -2147483648,
          -5,
          2,
          -28,
          0,
          2,
          2,
          -2147483648,
          -5,
          2,
          -28,
          0,
          2,
          2,
          -2147483648,
          -5,
          2,
          -4,
          0,
          -22,
          2,
          -2,
          0,
          2,
          2,
          -2147483648,
          -5,
          2,
          -4,
          0,
          -22,
          1,
          -2,
          0,
          2,
          1,
          -2147483648,
          -5,
          2,
          -28,
          0,
          -2,
          -2147483648,
          -72,
          2,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Physics","path":"tilesets/tSet_Physics/tSet_Physics.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Props","depth":600,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Props","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":70,"TileCompressedData":[
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -45,
          -2147483648,
          -25,
          0,
          -1795,
          -2147483648,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Plains_8x8_Props","path":"tilesets/tSet_Plains_8x8_Props/tSet_Plains_8x8_Props.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Front","depth":700,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Front","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":50,"SerialiseWidth":70,"TileCompressedData":[
          -213,
          -2147483648,
          30,
          52,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          83,
          82,
          82,
          83,
          -40,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -26,
          -2147483648,
          2,
          60,
          61,
          -41,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          23,
          -9,
          24,
          8,
          25,
          23,
          24,
          25,
          23,
          24,
          25,
          23,
          -5,
          24,
          2,
          76,
          77,
          -41,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          39,
          -9,
          40,
          8,
          41,
          39,
          40,
          41,
          39,
          40,
          41,
          39,
          -5,
          40,
          2,
          92,
          93,
          -41,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          25,
          55,
          56,
          55,
          56,
          55,
          56,
          55,
          56,
          55,
          56,
          57,
          55,
          56,
          57,
          55,
          56,
          57,
          55,
          56,
          55,
          56,
          55,
          56,
          108,
          109,
          -41,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -69,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          23,
          -23,
          24,
          2,
          60,
          61,
          -40,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          39,
          -23,
          40,
          2,
          76,
          77,
          -40,
          -2147483648,
          1,
          68,
          -27,
          -2147483648,
          2,
          76,
          77,
          -40,
          -2147483648,
          1,
          68,
          -27,
          -2147483648,
          2,
          60,
          61,
          -40,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          23,
          -21,
          24,
          4,
          25,
          21,
          76,
          77,
          -40,
          -2147483648,
          1,
          68,
          -3,
          -2147483648,
          1,
          39,
          -21,
          40,
          4,
          41,
          21,
          92,
          93,
          -40,
          -2147483648,
          1,
          68,
          -24,
          -2147483648,
          5,
          56,
          57,
          21,
          108,
          109,
          -40,
          -2147483648,
          1,
          68,
          -1886,
          -2147483648,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_8x8_Props","path":"tilesets/tSet_Complex_8x8_Props/tSet_Complex_8x8_Props.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Mid","depth":800,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Mid","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":25,"SerialiseWidth":35,"TileCompressedData":[
          -875,
          0,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_16x16","path":"tilesets/tSet_Complex_16x16/tSet_Complex_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRTileLayer":"","%Name":"Tiles_Visuals_Back","depth":900,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Tiles_Visuals_Back","properties":[],"resourceType":"GMRTileLayer","resourceVersion":"2.0","tiles":{"SerialiseHeight":25,"SerialiseWidth":35,"TileCompressedData":[
          -72,
          7,
          1,
          20,
          -30,
          34,
          1,
          21,
          -3,
          7,
          32,
          19,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          17,
          -3,
          7,
          32,
          19,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          17,
          -3,
          7,
          32,
          19,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          17,
          -3,
          7,
          32,
          19,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          17,
          -3,
          7,
          32,
          19,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          17,
          -3,
          7,
          32,
          19,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          17,
          -3,
          7,
          32,
          19,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          17,
          -3,
          7,
          207,
          19,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          17,
          7,
          34,
          34,
          35,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          33,
          34,
          118,
          119,
          119,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          118,
          119,
          134,
          135,
          135,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          118,
          119,
          134,
          135,
          2,
          2,
          3,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          134,
          135,
          1,
          2,
          7,
          7,
          19,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          17,
          -3,
          7,
          32,
          19,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          17,
          -3,
          7,
          32,
          19,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          17,
          -3,
          7,
          32,
          19,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          17,
          -3,
          7,
          32,
          19,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          97,
          98,
          99,
          17,
          -3,
          7,
          32,
          19,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          113,
          114,
          115,
          17,
          -3,
          7,
          32,
          19,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          129,
          130,
          131,
          17,
          -3,
          7,
          32,
          19,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          145,
          146,
          147,
          17,
          -3,
          7,
          1,
          36,
          -30,
          2,
          1,
          37,
          -36,
          7,
        ],"TileDataFormat":1,},"tilesetId":{"name":"tSet_Complex_16x16","path":"tilesets/tSet_Complex_16x16/tSet_Complex_16x16.yy",},"userdefinedDepth":false,"visible":true,"x":0,"y":0,},
    {"$GMRBackgroundLayer":"","%Name":"Background","animationFPS":15.0,"animationSpeedType":0,"colour":4278190080,"depth":1000,"effectEnabled":true,"effectType":null,"gridX":32,"gridY":32,"hierarchyFrozen":false,"hspeed":0.0,"htiled":false,"inheritLayerDepth":false,"inheritLayerSettings":false,"inheritSubLayers":true,"inheritVisibility":true,"layers":[],"name":"Background","properties":[],"resourceType":"GMRBackgroundLayer","resourceVersion":"2.0","spriteId":null,"stretch":false,"userdefinedAnimFPS":false,"userdefinedDepth":false,"visible":true,"vspeed":0.0,"vtiled":false,"x":0,"y":0,},
  ],
  "name":"t_room_complex_1",
  "parent":{
    "name":"Complex",
    "path":"folders/Rooms/Complex.yy",
  },
  "parentRoom":null,
  "physicsSettings":{
    "inheritPhysicsSettings":false,
    "PhysicsWorld":false,
    "PhysicsWorldGravityX":0.0,
    "PhysicsWorldGravityY":10.0,
    "PhysicsWorldPixToMetres":0.1,
  },
  "resourceType":"GMRoom",
  "resourceVersion":"2.0",
  "roomSettings":{
    "Height":400,
    "inheritRoomSettings":false,
    "persistent":false,
    "Width":560,
  },
  "sequenceId":null,
  "views":[
    {"hborder":32,"hport":700,"hspeed":1,"hview":200,"inherit":false,"objectId":null,"vborder":32,"visible":true,"vspeed":1,"wport":800,"wview":280,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
    {"hborder":32,"hport":768,"hspeed":-1,"hview":768,"inherit":false,"objectId":null,"vborder":32,"visible":false,"vspeed":-1,"wport":1024,"wview":1024,"xport":0,"xview":0,"yport":0,"yview":0,},
  ],
  "viewSettings":{
    "clearDisplayBuffer":true,
    "clearViewBackground":false,
    "enableViews":true,
    "inheritViewSettings":false,
  },
  "volume":1.0,
}