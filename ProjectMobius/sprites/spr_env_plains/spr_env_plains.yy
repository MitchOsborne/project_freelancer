{
  "$GMSprite":"",
  "%Name":"spr_env_plains",
  "bboxMode":0,
  "bbox_bottom":167,
  "bbox_left":8,
  "bbox_right":539,
  "bbox_top":8,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c8c9682a-27f2-4883-b7c4-2ccd6e361f18","name":"c8c9682a-27f2-4883-b7c4-2ccd6e361f18","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":8,
  "gridY":8,
  "height":176,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"877e3fc6-9d18-4df3-9ca8-b87730e98ad0","blendMode":0,"displayName":"default","isLocked":false,"name":"877e3fc6-9d18-4df3-9ca8-b87730e98ad0","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_env_plains",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"defunc",
    "path":"folders/Sprites/Environment/defunc.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_env_plains",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":1.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_env_plains",
    "playback":1,
    "playbackSpeed":30.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c8c9682a-27f2-4883-b7c4-2ccd6e361f18","path":"sprites/spr_env_plains/spr_env_plains.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c9fc8dd7-8fe9-4be6-9faf-b3544e40ef94","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Environment",
    "path":"texturegroups/txg_Environment",
  },
  "type":0,
  "VTile":false,
  "width":552,
}