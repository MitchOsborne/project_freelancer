{
  "$GMSprite":"",
  "%Name":"spr_Kamui_walk",
  "bboxMode":0,
  "bbox_bottom":29,
  "bbox_left":9,
  "bbox_right":23,
  "bbox_top":3,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"8948db8e-e6aa-4a2c-8dee-6b10862fcb3c","name":"8948db8e-e6aa-4a2c-8dee-6b10862fcb3c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2343179-5377-467c-8218-6f1f4a52de9f","name":"d2343179-5377-467c-8218-6f1f4a52de9f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b14336ca-1ad0-4003-93e0-d63c3ea0c4a1","name":"b14336ca-1ad0-4003-93e0-d63c3ea0c4a1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c14a6e2a-c5cf-4419-86e2-2385bced5475","name":"c14a6e2a-c5cf-4419-86e2-2385bced5475","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6538bc86-f424-4987-a948-1cff9ff28293","name":"6538bc86-f424-4987-a948-1cff9ff28293","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"50ccf833-6a97-4f5f-b8f0-04233a2a00ff","name":"50ccf833-6a97-4f5f-b8f0-04233a2a00ff","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"657fd1c4-3aac-4905-8368-9402abe2d197","name":"657fd1c4-3aac-4905-8368-9402abe2d197","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cd64c388-12a6-4801-9ac0-1a0764b755d1","name":"cd64c388-12a6-4801-9ac0-1a0764b755d1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"dfb0337c-9b32-40ba-85ab-9f07c9f90470","blendMode":0,"displayName":"default","isLocked":false,"name":"dfb0337c-9b32-40ba-85ab-9f07c9f90470","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_Kamui_walk",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Kamui",
    "path":"folders/Sprites/Characters/Kamui.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_Kamui_walk",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_Kamui_walk",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8948db8e-e6aa-4a2c-8dee-6b10862fcb3c","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12877489-2b90-4efa-96f2-d9fcf09de05d","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2343179-5377-467c-8218-6f1f4a52de9f","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"023629a2-295d-4446-a7d8-3fb684edd6e1","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b14336ca-1ad0-4003-93e0-d63c3ea0c4a1","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"beca62aa-1701-46e7-98e6-d61b5939e517","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c14a6e2a-c5cf-4419-86e2-2385bced5475","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"46f322bb-5583-4909-9b86-c69e402723c2","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6538bc86-f424-4987-a948-1cff9ff28293","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c10086a0-eff7-4cb4-9908-a75f9f14b7a8","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"50ccf833-6a97-4f5f-b8f0-04233a2a00ff","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"27843c8e-82cc-4bfd-be52-60396588e33a","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"657fd1c4-3aac-4905-8368-9402abe2d197","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7378b79b-8bfc-49b9-893c-b1cafbd7a721","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cd64c388-12a6-4801-9ac0-1a0764b755d1","path":"sprites/spr_Kamui_walk/spr_Kamui_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5f800896-54cb-4afc-abc8-038c9902f58c","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":17,
    "yorigin":18,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":32,
}