{
  "$GMSprite":"",
  "%Name":"spr_wep_Hands_Atk4",
  "bboxMode":2,
  "bbox_bottom":30,
  "bbox_left":-4,
  "bbox_right":22,
  "bbox_top":0,
  "collisionKind":2,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6e9cb20b-02dc-4310-be62-c2039bffaf4f","name":"6e9cb20b-02dc-4310-be62-c2039bffaf4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f0863717-ebee-4337-afda-29f6b16913df","name":"f0863717-ebee-4337-afda-29f6b16913df","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d1b58a81-8e89-417a-9fe0-33965d333782","name":"d1b58a81-8e89-417a-9fe0-33965d333782","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e5a89367-993b-4007-9c1a-0870d0a1f19d","name":"e5a89367-993b-4007-9c1a-0870d0a1f19d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"259d0229-986b-4ee3-afef-9b1ab84443b4","blendMode":0,"displayName":"Hands","isLocked":false,"name":"259d0229-986b-4ee3-afef-9b1ab84443b4","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"e8bd4d9b-2822-48bd-8ac8-95e1d90a3fd2","blendMode":0,"displayName":"default","isLocked":false,"name":"e8bd4d9b-2822-48bd-8ac8-95e1d90a3fd2","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":false,},
  ],
  "name":"spr_wep_Hands_Atk4",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Hands",
    "path":"folders/Sprites/Wepon/Sprites/Hands.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_wep_Hands_Atk4",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireStart",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"160d1537-56f9-42c3-883a-99acbd6a9da6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireTrigger",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"ea8d7869-6fb8-4cf5-b8db-e7e10da70694","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireCycle",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"536078b8-4b0e-4974-8e4e-aa031ccc6ccf","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireEnd",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"6e6d71eb-784e-4794-a975-e83a62d956cf","IsCreationKey":false,"Key":3.9999,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
      ],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_wep_Hands_Atk4",
    "playback":1,
    "playbackSpeed":12.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6e9cb20b-02dc-4310-be62-c2039bffaf4f","path":"sprites/spr_wep_Hands_Atk4/spr_wep_Hands_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"429b7ee9-645d-448d-9917-0186521ed675","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f0863717-ebee-4337-afda-29f6b16913df","path":"sprites/spr_wep_Hands_Atk4/spr_wep_Hands_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f1076d68-0beb-4441-bd93-369597b17058","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d1b58a81-8e89-417a-9fe0-33965d333782","path":"sprites/spr_wep_Hands_Atk4/spr_wep_Hands_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b8c75663-ddd4-4d48-8049-2d625ffa9b0b","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e5a89367-993b-4007-9c1a-0870d0a1f19d","path":"sprites/spr_wep_Hands_Atk4/spr_wep_Hands_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"58adf87c-ee74-4338-92e7-d3b6d2243715","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":16,
  },
  "swatchColours":[
    4278190335,4278255615,4278255360,4294967040,4294901760,4294902015,4294967269,4293717228,4293059298,4292335575,
    4291677645,4290230199,4287993237,4280556782,4278252287,4283540992,4293963264,4287770926,4287365357,4287203721,
    4286414205,4285558896,4284703587,4283782485,4281742902,4278190080,4286158839,4286688762,4287219453,4288280831,
    4288405444,4288468131,4288465538,4291349882,4294430829,4292454269,4291466115,4290675079,4290743485,4290943732,
    4288518390,4283395315,4283862775,4284329979,4285068799,4285781164,4285973884,4286101564,4290034460,4294164224,
    4291529796,4289289312,4289290373,4289291432,4289359601,4286410226,4280556782,4280444402,4280128760,4278252287,
    4282369933,4283086137,4283540992,4288522496,4293963264,4290540032,4289423360,4289090560,4287770926,4287704422,
    4287571858,4287365357,4284159214,4279176094,4279058848,4278870691,4278231211,4281367321,
  ],
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Attacks",
    "path":"texturegroups/txg_Attacks",
  },
  "type":0,
  "VTile":false,
  "width":32,
}