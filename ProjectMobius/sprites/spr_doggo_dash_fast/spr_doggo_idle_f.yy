{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 69,
  "bbox_top": 1,
  "bbox_bottom": 50,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 70,
  "height": 57,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c7dca9b2-0e3e-4c72-9afc-323615251c9c","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c7dca9b2-0e3e-4c72-9afc-323615251c9c","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"c7dca9b2-0e3e-4c72-9afc-323615251c9c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5fa191eb-5a3a-486f-a37b-d7018681643b","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5fa191eb-5a3a-486f-a37b-d7018681643b","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"5fa191eb-5a3a-486f-a37b-d7018681643b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c1e2afb9-b8b8-4e11-8acc-9d25e85b28ac","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c1e2afb9-b8b8-4e11-8acc-9d25e85b28ac","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"c1e2afb9-b8b8-4e11-8acc-9d25e85b28ac","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aeacc662-9420-4d9d-b63f-030a41f9345a","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aeacc662-9420-4d9d-b63f-030a41f9345a","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"aeacc662-9420-4d9d-b63f-030a41f9345a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c44d4f1e-b259-4ce6-8f83-42108a974d8f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c44d4f1e-b259-4ce6-8f83-42108a974d8f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","name":"c44d4f1e-b259-4ce6-8f83-42108a974d8f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9f1dee13-8388-46f1-a5b7-e0422adb13e7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c7dca9b2-0e3e-4c72-9afc-323615251c9c","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3d66f746-2ff1-457f-8274-f4f509cbd69c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5fa191eb-5a3a-486f-a37b-d7018681643b","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"880a3869-c3ff-4ab5-9c29-de456eaec02d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c1e2afb9-b8b8-4e11-8acc-9d25e85b28ac","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"994ded3a-b540-4295-bb5d-f8f27196ab9a","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aeacc662-9420-4d9d-b63f-030a41f9345a","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e452cc0-6ce1-488c-926a-c5c6b1ca40aa","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c44d4f1e-b259-4ce6-8f83-42108a974d8f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 35,
    "yorigin": 28,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_doggo_idle_f","path":"sprites/spr_doggo_idle_f/spr_doggo_idle_f.yy",},
    "resourceVersion": "1.3",
    "name": "spr_doggo_idle_f",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Doggo",
    "path": "folders/Sprites/Characters/Enemy/Doggo.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_doggo_idle_f",
  "tags": [],
  "resourceType": "GMSprite",
}