{
  "$GMSprite":"",
  "%Name":"spr_mech_idle_f",
  "bboxMode":0,
  "bbox_bottom":72,
  "bbox_left":2,
  "bbox_right":86,
  "bbox_top":1,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"08967ad4-d31e-4fe2-93d9-f3281547eef8","name":"08967ad4-d31e-4fe2-93d9-f3281547eef8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cb07b940-e020-4b08-a8ca-249e9d91c21a","name":"cb07b940-e020-4b08-a8ca-249e9d91c21a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ecf2838d-b621-4332-ab01-6494e2de3443","name":"ecf2838d-b621-4332-ab01-6494e2de3443","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a6dd3293-6e69-4328-a6a5-0f1393054661","name":"a6dd3293-6e69-4328-a6a5-0f1393054661","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5b062f16-f0ff-468a-a22e-37bd58804220","name":"5b062f16-f0ff-468a-a22e-37bd58804220","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b63276af-f895-43dd-90b2-489585139727","name":"b63276af-f895-43dd-90b2-489585139727","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":82,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"40d9ce02-ed70-44c9-a5bd-027a6e3ff1e1","blendMode":0,"displayName":"default","isLocked":false,"name":"40d9ce02-ed70-44c9-a5bd-027a6e3ff1e1","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_mech_idle_f",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Mech",
    "path":"folders/Sprites/Characters/MechPilot/Mech.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_mech_idle_f",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_mech_idle_f",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"08967ad4-d31e-4fe2-93d9-f3281547eef8","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cec8bc13-1312-4787-9a1b-9b9fb8897281","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cb07b940-e020-4b08-a8ca-249e9d91c21a","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"963d3bcb-41b8-4f7b-b8d3-a53fd1da6da7","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ecf2838d-b621-4332-ab01-6494e2de3443","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0b1abbf8-f110-4b84-879d-9fe1ccf48120","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a6dd3293-6e69-4328-a6a5-0f1393054661","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"520bc320-61b3-482c-9b82-98340faa9b89","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5b062f16-f0ff-468a-a22e-37bd58804220","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"349e4679-5bf0-442b-978d-1a43acc39f38","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b63276af-f895-43dd-90b2-489585139727","path":"sprites/spr_mech_idle_f/spr_mech_idle_f.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e77b4726-101f-4c56-afb5-0c362ff5f79d","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":46,
    "yorigin":41,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":93,
}