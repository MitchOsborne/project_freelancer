{
  "$GMSprite":"",
  "%Name":"spr_wep_flash_rifle",
  "bboxMode":0,
  "bbox_bottom":6,
  "bbox_left":0,
  "bbox_right":18,
  "bbox_top":2,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"0d7e7a3e-ffe5-4e23-8726-224659be3efb","name":"0d7e7a3e-ffe5-4e23-8726-224659be3efb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9ad7129e-dcb2-424b-b28b-93833950b851","name":"9ad7129e-dcb2-424b-b28b-93833950b851","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a9863941-05b7-4f14-be3d-02f74fd5d5fa","name":"a9863941-05b7-4f14-be3d-02f74fd5d5fa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d09d4324-8e78-4801-a035-cf5ae8043db4","name":"d09d4324-8e78-4801-a035-cf5ae8043db4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":9,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"311b5c71-e738-43aa-9ae4-a93f1754d95c","blendMode":0,"displayName":"default","isLocked":false,"name":"311b5c71-e738-43aa-9ae4-a93f1754d95c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_wep_flash_rifle",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"sheets",
    "path":"folders/Sprites/Wepon/Sprites/sheets.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_wep_flash_rifle",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_wep_flash_rifle",
    "playback":1,
    "playbackSpeed":30.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0d7e7a3e-ffe5-4e23-8726-224659be3efb","path":"sprites/spr_wep_flash_rifle/spr_wep_flash_rifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"da3a87c0-f6f7-42d0-a468-d15fee01c98d","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9ad7129e-dcb2-424b-b28b-93833950b851","path":"sprites/spr_wep_flash_rifle/spr_wep_flash_rifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"06606d09-8481-4f02-a495-12449b6ca809","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a9863941-05b7-4f14-be3d-02f74fd5d5fa","path":"sprites/spr_wep_flash_rifle/spr_wep_flash_rifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"91db8e3d-8801-448a-bde9-aa7c30b2995a","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d09d4324-8e78-4801-a035-cf5ae8043db4","path":"sprites/spr_wep_flash_rifle/spr_wep_flash_rifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fa0b3e34-d0ff-4fae-8c54-1b203bd2c7a5","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":5,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Weapons",
    "path":"texturegroups/txg_Weapons",
  },
  "type":0,
  "VTile":false,
  "width":27,
}