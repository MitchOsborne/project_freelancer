{
  "$GMSprite":"",
  "%Name":"spr_wep_RK47_fire",
  "bboxMode":0,
  "bbox_bottom":11,
  "bbox_left":3,
  "bbox_right":21,
  "bbox_top":5,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c86cb970-a7d3-4f33-9333-0af61c715541","name":"c86cb970-a7d3-4f33-9333-0af61c715541","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e1e2122d-aac1-4b9e-8874-d3eeaed46e4f","name":"e1e2122d-aac1-4b9e-8874-d3eeaed46e4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a0758f7e-c7fb-4912-887d-c5dde396520a","name":"a0758f7e-c7fb-4912-887d-c5dde396520a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bd14c1ab-ff93-4d8f-83cb-170a2fb75341","name":"bd14c1ab-ff93-4d8f-83cb-170a2fb75341","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b3dc27a0-ccca-41e8-9cf0-d1606f198091","name":"b3dc27a0-ccca-41e8-9cf0-d1606f198091","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"d538edb5-4fe4-487f-a610-8fd42c8529e9","blendMode":0,"displayName":"Hands","isLocked":false,"name":"d538edb5-4fe4-487f-a610-8fd42c8529e9","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"83a00399-e25c-44b9-ba70-b782ada73aa5","blendMode":0,"displayName":"default","isLocked":false,"name":"83a00399-e25c-44b9-ba70-b782ada73aa5","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_wep_RK47_fire",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"RKBase",
    "path":"folders/Sprites/Wepon/Sprites/Rifles/RKBase.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_wep_RK47_fire",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireStart",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"573965ed-6c1a-4de2-803f-21c484907c9f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireTrigger",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"463497a8-61eb-44dc-9f41-cbd4b3562ac1","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireCycle",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"2ff70e51-b8d7-405f-a0df-a56d665c9b1b","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireEnd",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"aab9cda1-7f5d-425a-b88a-6b8e006185f7","IsCreationKey":false,"Key":4.9999,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
      ],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":5.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_wep_RK47_fire",
    "playback":1,
    "playbackSpeed":30.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c86cb970-a7d3-4f33-9333-0af61c715541","path":"sprites/spr_wep_RK47_fire/spr_wep_RK47_fire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3a8f3961-4de0-47a4-be59-a142108df402","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e1e2122d-aac1-4b9e-8874-d3eeaed46e4f","path":"sprites/spr_wep_RK47_fire/spr_wep_RK47_fire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c7d58092-c195-49bd-a705-569ca5ef287a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a0758f7e-c7fb-4912-887d-c5dde396520a","path":"sprites/spr_wep_RK47_fire/spr_wep_RK47_fire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4c826731-6fe3-421b-9f3e-4ad4e321d11b","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bd14c1ab-ff93-4d8f-83cb-170a2fb75341","path":"sprites/spr_wep_RK47_fire/spr_wep_RK47_fire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"733c1ec3-1fea-4aee-b6e6-410dfb36f4d2","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b3dc27a0-ccca-41e8-9cf0-d1606f198091","path":"sprites/spr_wep_RK47_fire/spr_wep_RK47_fire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"20005809-ec65-4157-939b-aee53750b227","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Weapons",
    "path":"texturegroups/txg_Weapons",
  },
  "type":0,
  "VTile":false,
  "width":32,
}