{
  "$GMSprite":"",
  "%Name":"spr_wep_Shield_atkM",
  "bboxMode":0,
  "bbox_bottom":24,
  "bbox_left":9,
  "bbox_right":30,
  "bbox_top":6,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"887cdcfd-3a51-4e6c-9400-f4a9a9d2a645","name":"887cdcfd-3a51-4e6c-9400-f4a9a9d2a645","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a1bf8eb6-1d27-4df5-a50d-0f689d891ec0","name":"a1bf8eb6-1d27-4df5-a50d-0f689d891ec0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"17c2b8b1-3a40-41e4-9ad1-4c54260cf448","name":"17c2b8b1-3a40-41e4-9ad1-4c54260cf448","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1d7c8cfa-f364-4f31-9bf7-271717c035da","name":"1d7c8cfa-f364-4f31-9bf7-271717c035da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5b510361-3ec8-468e-9f31-f5855feef583","name":"5b510361-3ec8-468e-9f31-f5855feef583","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"407b0cc7-16f4-45c3-a090-5463030530bf","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"407b0cc7-16f4-45c3-a090-5463030530bf","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"17953dc4-34b9-412f-af26-92a44c232294","blendMode":0,"displayName":"default","isLocked":false,"name":"17953dc4-34b9-412f-af26-92a44c232294","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_wep_Shield_atkM",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Shield",
    "path":"folders/Sprites/Wepon/Sprites/Shield.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_wep_Shield_atkM",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":5.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_wep_Shield_atkM",
    "playback":1,
    "playbackSpeed":12.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"887cdcfd-3a51-4e6c-9400-f4a9a9d2a645","path":"sprites/spr_wep_Shield_atkM/spr_wep_Shield_atkM.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c05cc33d-8f1c-4d50-9718-bd2cc46a4190","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a1bf8eb6-1d27-4df5-a50d-0f689d891ec0","path":"sprites/spr_wep_Shield_atkM/spr_wep_Shield_atkM.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f293053c-0ac7-4193-8830-10c9f07f0209","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"17c2b8b1-3a40-41e4-9ad1-4c54260cf448","path":"sprites/spr_wep_Shield_atkM/spr_wep_Shield_atkM.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"351b062a-3858-474d-9d55-0c2f625cd3c3","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1d7c8cfa-f364-4f31-9bf7-271717c035da","path":"sprites/spr_wep_Shield_atkM/spr_wep_Shield_atkM.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d8627ef5-ade2-48b5-9503-8b7f87f4113f","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5b510361-3ec8-468e-9f31-f5855feef583","path":"sprites/spr_wep_Shield_atkM/spr_wep_Shield_atkM.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f8f16fcc-86be-4783-9f52-cba283222f28","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":14,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Weapons",
    "path":"texturegroups/txg_Weapons",
  },
  "type":0,
  "VTile":false,
  "width":32,
}