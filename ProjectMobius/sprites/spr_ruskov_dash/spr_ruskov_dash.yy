{
  "$GMSprite":"",
  "%Name":"spr_Ruskov_dash",
  "bboxMode":0,
  "bbox_bottom":16,
  "bbox_left":3,
  "bbox_right":14,
  "bbox_top":2,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"4c29113e-59a7-425a-af14-335fe3aff85b","name":"4c29113e-59a7-425a-af14-335fe3aff85b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"48637035-8c94-49cf-b3dc-f7d7802afcf3","name":"48637035-8c94-49cf-b3dc-f7d7802afcf3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"62e0372a-7e0c-4b84-9d9d-1a05fef21c01","name":"62e0372a-7e0c-4b84-9d9d-1a05fef21c01","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"98366a19-b311-43e2-a32a-0126838f3080","name":"98366a19-b311-43e2-a32a-0126838f3080","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":18,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"a31203d0-8dd5-4d2a-a1af-db0dc2556832","blendMode":0,"displayName":"default","isLocked":false,"name":"a31203d0-8dd5-4d2a-a1af-db0dc2556832","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_Ruskov_dash",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"Ruskov",
    "path":"folders/Sprites/Characters/Shops/Ruskov.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_Ruskov_dash",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_Ruskov_dash",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4c29113e-59a7-425a-af14-335fe3aff85b","path":"sprites/spr_Ruskov_dash/spr_Ruskov_dash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"24605552-ae64-4c1a-8a45-b581be88eba1","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"48637035-8c94-49cf-b3dc-f7d7802afcf3","path":"sprites/spr_Ruskov_dash/spr_Ruskov_dash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b89dba12-a0c6-4ede-be64-cbdc4414c2b6","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"62e0372a-7e0c-4b84-9d9d-1a05fef21c01","path":"sprites/spr_Ruskov_dash/spr_Ruskov_dash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1b8a59db-f292-4dcd-ae65-0a0639bb2a26","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"98366a19-b311-43e2-a32a-0126838f3080","path":"sprites/spr_Ruskov_dash/spr_Ruskov_dash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ffe882fc-8376-41c6-b07c-618e2075b840","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":16,
}