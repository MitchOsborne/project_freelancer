{
  "$GMSprite":"",
  "%Name":"spr_KO_1",
  "bboxMode":0,
  "bbox_bottom":5,
  "bbox_left":1,
  "bbox_right":7,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"06fe231b-eaf1-4fc6-acdf-e89e951bf538","name":"06fe231b-eaf1-4fc6-acdf-e89e951bf538","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"52184b06-8df8-41bd-bba9-667cb2a1dac5","name":"52184b06-8df8-41bd-bba9-667cb2a1dac5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"08f6b4fa-7537-47ba-a876-8ab04f2ec872","name":"08f6b4fa-7537-47ba-a876-8ab04f2ec872","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c855f6ee-a561-43e4-b2c7-a7dde7e08fb1","name":"c855f6ee-a561-43e4-b2c7-a7dde7e08fb1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a6acfebd-e349-4e16-93aa-fb87f3f4fced","name":"a6acfebd-e349-4e16-93aa-fb87f3f4fced","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"81c217a7-3868-4968-9fb9-55950f19786b","name":"81c217a7-3868-4968-9fb9-55950f19786b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"066f1933-e209-4d2d-a1bd-125494ca9d87","name":"066f1933-e209-4d2d-a1bd-125494ca9d87","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"17d058d6-d410-410b-a343-ad8bd06d2b00","name":"17d058d6-d410-410b-a343-ad8bd06d2b00","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"07acaa62-a49f-4e93-8fa2-9e619ec025fe","name":"07acaa62-a49f-4e93-8fa2-9e619ec025fe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e80bae67-cb62-4fff-bc15-688b85fd5c12","name":"e80bae67-cb62-4fff-bc15-688b85fd5c12","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fb8622c2-1e70-47e2-9657-060148789a94","name":"fb8622c2-1e70-47e2-9657-060148789a94","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"12402406-bbb9-43ff-88ef-f26833d84574","name":"12402406-bbb9-43ff-88ef-f26833d84574","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":8,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"c947e58d-d07a-4e2f-8a75-e417c7b4427f","blendMode":0,"displayName":"default","isLocked":false,"name":"c947e58d-d07a-4e2f-8a75-e417c7b4427f","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_KO_1",
  "nineSlice":null,
  "origin":7,
  "parent":{
    "name":"Effects",
    "path":"folders/Sprites/Particles/Effects.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_KO_1",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":12.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_KO_1",
    "playback":1,
    "playbackSpeed":12.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"06fe231b-eaf1-4fc6-acdf-e89e951bf538","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"213611dc-1495-424a-b868-70b6a05ec66c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"52184b06-8df8-41bd-bba9-667cb2a1dac5","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6371d09c-7d2d-4ace-8852-058966c3525f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"08f6b4fa-7537-47ba-a876-8ab04f2ec872","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3d3a43ca-acbe-4e28-bb9e-1c0679d2e3d2","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c855f6ee-a561-43e4-b2c7-a7dde7e08fb1","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"25d99206-b325-4e0f-b6e9-78b41ae15845","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a6acfebd-e349-4e16-93aa-fb87f3f4fced","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"80691456-26e4-42ca-b857-094032cd542d","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"81c217a7-3868-4968-9fb9-55950f19786b","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"47336968-2d61-47ad-9194-a16ceb39f556","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"066f1933-e209-4d2d-a1bd-125494ca9d87","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"434a0b8e-99be-487a-8801-3905ab2a40de","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"17d058d6-d410-410b-a343-ad8bd06d2b00","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0e811743-8932-4268-8b74-3bcc4571a26e","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"07acaa62-a49f-4e93-8fa2-9e619ec025fe","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d5969790-f76c-4aff-ad5f-c346671b4270","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e80bae67-cb62-4fff-bc15-688b85fd5c12","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0dcd1429-f81e-4a39-b67b-7def26b79eae","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fb8622c2-1e70-47e2-9657-060148789a94","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"63d505ce-b11f-425f-968c-150bdd5655ae","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"12402406-bbb9-43ff-88ef-f26833d84574","path":"sprites/spr_KO_1/spr_KO_1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cb472d45-9ddc-4331-a35d-60e5da592388","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":4,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Icons",
    "path":"texturegroups/txg_Icons",
  },
  "type":0,
  "VTile":false,
  "width":8,
}