{
  "$GMSprite":"",
  "%Name":"spr_KitsuneSamurai_walk",
  "bboxMode":0,
  "bbox_bottom":29,
  "bbox_left":8,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"a36f59ab-5e69-4db2-bbee-88dcbe676949","name":"a36f59ab-5e69-4db2-bbee-88dcbe676949","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"61a9fbf8-6cf9-4ecb-9e67-0a7ca9fa0673","name":"61a9fbf8-6cf9-4ecb-9e67-0a7ca9fa0673","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"35e0501a-6f67-44cd-8605-fcd01ad2333c","name":"35e0501a-6f67-44cd-8605-fcd01ad2333c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9d70d859-6713-45f3-a02d-2963aba4b41a","name":"9d70d859-6713-45f3-a02d-2963aba4b41a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2a12e0b3-f2d2-4bb6-9dbd-bb80484194b1","name":"2a12e0b3-f2d2-4bb6-9dbd-bb80484194b1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"198d0f5b-6c16-42ef-aaa9-0f9bb9966c24","name":"198d0f5b-6c16-42ef-aaa9-0f9bb9966c24","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3a08c327-1bfa-4f91-9ffc-f2f866a08b08","name":"3a08c327-1bfa-4f91-9ffc-f2f866a08b08","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2daff10a-c8bc-460a-80f7-a2174d971420","name":"2daff10a-c8bc-460a-80f7-a2174d971420","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":36,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"851949ed-d188-4b46-aaa3-e8521df17180","blendMode":0,"displayName":"default","isLocked":false,"name":"851949ed-d188-4b46-aaa3-e8521df17180","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_KitsuneSamurai_walk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Samurai",
    "path":"folders/Sprites/Characters/Enemy/Kitsune/Samurai.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_KitsuneSamurai_walk",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_KitsuneSamurai_walk",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a36f59ab-5e69-4db2-bbee-88dcbe676949","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"838b1622-01be-4f3e-b27f-569295e2aaca","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"61a9fbf8-6cf9-4ecb-9e67-0a7ca9fa0673","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea8c8176-988a-4ca2-91b9-bd8fb56847c9","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"35e0501a-6f67-44cd-8605-fcd01ad2333c","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6db8956e-6697-4874-82da-f099be2c5e00","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9d70d859-6713-45f3-a02d-2963aba4b41a","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"093f33c0-5790-44e2-9bd5-caf978870cdd","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2a12e0b3-f2d2-4bb6-9dbd-bb80484194b1","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"29204e99-ce5c-4b94-9af8-a812a4403691","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"198d0f5b-6c16-42ef-aaa9-0f9bb9966c24","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"18ab0378-51f4-447c-9890-30fc78582127","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3a08c327-1bfa-4f91-9ffc-f2f866a08b08","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4dfc053d-0dc3-414c-8a5f-04b1f1fb4b9b","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2daff10a-c8bc-460a-80f7-a2174d971420","path":"sprites/spr_KitsuneSamurai_walk/spr_KitsuneSamurai_walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7a30325e-8502-4831-9c19-56a065a8f50b","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":18,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":32,
}