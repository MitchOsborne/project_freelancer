{
  "$GMSprite":"",
  "%Name":"spr_Kamui_walk_old",
  "bboxMode":0,
  "bbox_bottom":28,
  "bbox_left":6,
  "bbox_right":20,
  "bbox_top":5,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"907bb087-d9b7-482f-b2e5-2259c94ef3b3","name":"907bb087-d9b7-482f-b2e5-2259c94ef3b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"033bc982-4afc-4a0d-a0f1-1fb41036ae62","name":"033bc982-4afc-4a0d-a0f1-1fb41036ae62","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d560aead-5f60-40b2-9bba-06f8388ee9d3","name":"d560aead-5f60-40b2-9bba-06f8388ee9d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"684d18cd-b519-4a76-867e-f62ea3147701","name":"684d18cd-b519-4a76-867e-f62ea3147701","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"879a6b56-c20f-4409-a1fd-1571cc480dad","name":"879a6b56-c20f-4409-a1fd-1571cc480dad","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b8f7821a-f4ba-4c73-bde5-14a5b31df85d","name":"b8f7821a-f4ba-4c73-bde5-14a5b31df85d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"55890caf-c703-4ca4-9a14-3330644e375f","name":"55890caf-c703-4ca4-9a14-3330644e375f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ad2e8a38-0d57-4a7f-b46f-8c87ac721c7f","name":"ad2e8a38-0d57-4a7f-b46f-8c87ac721c7f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"deb5a519-b2fc-410e-b861-bc1dd87ca1f4","blendMode":0,"displayName":"default","isLocked":false,"name":"deb5a519-b2fc-410e-b861-bc1dd87ca1f4","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_Kamui_walk_old",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"old",
    "path":"folders/Sprites/Characters/Kamui/old.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_Kamui_walk_old",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_Kamui_walk_old",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"907bb087-d9b7-482f-b2e5-2259c94ef3b3","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0bb5892a-e57e-444e-a091-f0526fa6ca45","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"033bc982-4afc-4a0d-a0f1-1fb41036ae62","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d116d9b4-3d62-4db6-969d-e8a137a339c1","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d560aead-5f60-40b2-9bba-06f8388ee9d3","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af49b2ef-2fab-4b63-8385-1d826495db80","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"684d18cd-b519-4a76-867e-f62ea3147701","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"df6c2790-67e3-445d-b0bd-2a00af7be54c","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"879a6b56-c20f-4409-a1fd-1571cc480dad","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af1fb3d0-e83a-4ac5-85e1-87c2caa70d51","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b8f7821a-f4ba-4c73-bde5-14a5b31df85d","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"990e4500-0c31-4dd5-b96d-562f6c7cb964","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"55890caf-c703-4ca4-9a14-3330644e375f","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"acfe753b-e844-4622-b3a5-a206b7816880","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ad2e8a38-0d57-4a7f-b46f-8c87ac721c7f","path":"sprites/spr_Kamui_walk_old/spr_Kamui_walk_old.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"50cfae19-1f1a-464e-8414-10722678be22","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":19,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":32,
}