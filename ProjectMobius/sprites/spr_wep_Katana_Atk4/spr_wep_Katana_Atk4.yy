{
  "$GMSprite":"",
  "%Name":"spr_wep_Katana_Atk4",
  "bboxMode":0,
  "bbox_bottom":24,
  "bbox_left":2,
  "bbox_right":30,
  "bbox_top":8,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"5c106b73-b5c4-4f98-9828-c3578fff6653","name":"5c106b73-b5c4-4f98-9828-c3578fff6653","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8c4d9a83-99b9-4f04-8ca7-6e518ffa7dab","name":"8c4d9a83-99b9-4f04-8ca7-6e518ffa7dab","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"984fabb5-517b-4ac7-80f4-0fb65eccc709","name":"984fabb5-517b-4ac7-80f4-0fb65eccc709","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dd554905-ee58-4a28-9cfb-b4f45f7609e2","name":"dd554905-ee58-4a28-9cfb-b4f45f7609e2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7b46b2b5-2134-407e-a71c-9415a138b1f8","name":"7b46b2b5-2134-407e-a71c-9415a138b1f8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8dde757a-80f0-4e8f-a77d-542e851d8da7","name":"8dde757a-80f0-4e8f-a77d-542e851d8da7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"77872776-b2a5-496d-9f3b-275921b830fc","name":"77872776-b2a5-496d-9f3b-275921b830fc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e443b5c8-c56a-44ab-a526-b65dc1bb5278","name":"e443b5c8-c56a-44ab-a526-b65dc1bb5278","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e0e18f34-ba33-47c1-a6ff-43627a974258","name":"e0e18f34-ba33-47c1-a6ff-43627a974258","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5c5bcea5-2205-430d-95b3-d2f76ad8c08c","blendMode":0,"displayName":"Hands","isLocked":false,"name":"5c5bcea5-2205-430d-95b3-d2f76ad8c08c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"f5eb978d-1ecc-48ab-a149-49a1c0c265cd","blendMode":0,"displayName":"Layer 2","isLocked":false,"name":"f5eb978d-1ecc-48ab-a149-49a1c0c265cd","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":false,},
    {"$GMImageLayer":"","%Name":"ddeee100-909c-4a41-b3b5-a57c3c288796","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"ddeee100-909c-4a41-b3b5-a57c3c288796","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"17953dc4-34b9-412f-af26-92a44c232294","blendMode":0,"displayName":"default","isLocked":false,"name":"17953dc4-34b9-412f-af26-92a44c232294","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_wep_Katana_Atk4",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Katana",
    "path":"folders/Sprites/Wepon/Sprites/Melee/Katana.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_wep_Katana_Atk4",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireStart",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"e5f78404-0313-4e25-908d-0288260e21b2","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireTrigger",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"439475ab-433d-453e-847e-c93141b7767e","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireCycle",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"214339f9-c641-4041-bd68-a2a9ec6a95d9","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
        {"$Keyframe<MessageEventKeyframe>":"","Channels":{
            "0":{"$MessageEventKeyframe":"","Events":[
                "FireEnd",
              ],"resourceType":"MessageEventKeyframe","resourceVersion":"2.0",},
          },"Disabled":false,"id":"3a619c8a-a69f-48c0-a55c-c45636747226","IsCreationKey":false,"Key":8.9999,"Length":1.0,"resourceType":"Keyframe<MessageEventKeyframe>","resourceVersion":"2.0","Stretch":false,},
      ],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_wep_Katana_Atk4",
    "playback":1,
    "playbackSpeed":15.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5c106b73-b5c4-4f98-9828-c3578fff6653","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9acb1b02-681f-43a4-934a-e342db40b9b1","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8c4d9a83-99b9-4f04-8ca7-6e518ffa7dab","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4ca94182-3141-43f7-8179-97626d64d957","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"984fabb5-517b-4ac7-80f4-0fb65eccc709","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bf55bf3e-44f0-4201-91ff-1c7a14a4e341","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dd554905-ee58-4a28-9cfb-b4f45f7609e2","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"45e3ab4d-115e-4f07-a6bd-3200b7ea5ea8","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7b46b2b5-2134-407e-a71c-9415a138b1f8","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e97e4190-66e0-4525-a914-b3578c6c5592","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8dde757a-80f0-4e8f-a77d-542e851d8da7","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"794261c1-a63d-4cf4-a672-d4a002c1d559","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"77872776-b2a5-496d-9f3b-275921b830fc","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"619aaf0d-4960-40ed-94bc-fc9ae964594b","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e443b5c8-c56a-44ab-a526-b65dc1bb5278","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e7bdb3cd-079b-4ace-9a5c-a0e17d0c4500","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e0e18f34-ba33-47c1-a6ff-43627a974258","path":"sprites/spr_wep_Katana_Atk4/spr_wep_Katana_Atk4.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a1b3e6da-317c-4fc2-9502-37b56573893d","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":10,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Weapons",
    "path":"texturegroups/txg_Weapons",
  },
  "type":0,
  "VTile":false,
  "width":32,
}