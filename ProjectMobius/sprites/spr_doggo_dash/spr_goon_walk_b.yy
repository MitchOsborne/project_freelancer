{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 1,
  "bbox_bottom": 53,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 56,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fc16cbae-8ef0-4352-b52d-72f9fba96623","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc16cbae-8ef0-4352-b52d-72f9fba96623","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"fc16cbae-8ef0-4352-b52d-72f9fba96623","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5d0d75c6-00ec-4cdd-9b72-5c1b58a367e2","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5d0d75c6-00ec-4cdd-9b72-5c1b58a367e2","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"5d0d75c6-00ec-4cdd-9b72-5c1b58a367e2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c3e39147-44b3-4de2-827e-972825c5671b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c3e39147-44b3-4de2-827e-972825c5671b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"c3e39147-44b3-4de2-827e-972825c5671b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1aaba3cc-e21b-400b-b1c0-d960fe203497","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1aaba3cc-e21b-400b-b1c0-d960fe203497","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"1aaba3cc-e21b-400b-b1c0-d960fe203497","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"da9c2118-488e-4155-bbb9-fd7a622b3d49","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"da9c2118-488e-4155-bbb9-fd7a622b3d49","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"da9c2118-488e-4155-bbb9-fd7a622b3d49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b8391833-1f11-41f7-820c-b8c9d90e9f7b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8391833-1f11-41f7-820c-b8c9d90e9f7b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"LayerId":{"name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","name":"b8391833-1f11-41f7-820c-b8c9d90e9f7b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"32ed520e-95c5-4825-81ce-ce37363cf5fb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc16cbae-8ef0-4352-b52d-72f9fba96623","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d248191d-5779-4512-9ee7-8524b011d6dd","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5d0d75c6-00ec-4cdd-9b72-5c1b58a367e2","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c144000a-fd7b-459e-8015-7e2687701b2a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c3e39147-44b3-4de2-827e-972825c5671b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e304797f-9b8c-4642-b8e3-02fddc80c4e5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1aaba3cc-e21b-400b-b1c0-d960fe203497","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc13f925-5a91-44dd-8404-b5d1336a35b1","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"da9c2118-488e-4155-bbb9-fd7a622b3d49","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a46dd441-683f-4fcb-8cc2-92cc7f52ee38","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8391833-1f11-41f7-820c-b8c9d90e9f7b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 28,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_goon_walk_b","path":"sprites/spr_goon_walk_b/spr_goon_walk_b.yy",},
    "resourceVersion": "1.3",
    "name": "spr_goon_walk_b",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"36b1df08-39e4-4bd3-8d64-dceba1f2debd","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Goon",
    "path": "folders/Sprites/Characters/Enemy/Goon.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_goon_walk_b",
  "tags": [],
  "resourceType": "GMSprite",
}