{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 6,
  "bbox_top": 3,
  "bbox_bottom": 6,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 10,
  "height": 10,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1e3ba6ba-3700-436c-83d7-2482bacbd484","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1e3ba6ba-3700-436c-83d7-2482bacbd484","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":{"name":"5112f8a5-396b-42d9-97ea-e62759df6103","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"1e3ba6ba-3700-436c-83d7-2482bacbd484","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f7a0f1fd-8b6f-4326-a415-6cc7d5c408e5","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f7a0f1fd-8b6f-4326-a415-6cc7d5c408e5","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":{"name":"5112f8a5-396b-42d9-97ea-e62759df6103","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"f7a0f1fd-8b6f-4326-a415-6cc7d5c408e5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"284450de-c6fe-42c1-bea3-ae07f71c9241","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"284450de-c6fe-42c1-bea3-ae07f71c9241","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":{"name":"5112f8a5-396b-42d9-97ea-e62759df6103","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"284450de-c6fe-42c1-bea3-ae07f71c9241","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0896dc1b-6eb6-47cf-908c-eb40582db5bb","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0896dc1b-6eb6-47cf-908c-eb40582db5bb","path":"sprites/spr_bCred/spr_bCred.yy",},"LayerId":{"name":"5112f8a5-396b-42d9-97ea-e62759df6103","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","name":"0896dc1b-6eb6-47cf-908c-eb40582db5bb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3c072555-73f4-4f69-8eec-66e0768c1652","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1e3ba6ba-3700-436c-83d7-2482bacbd484","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e56230a-6282-45c1-84b7-11ac17d4ea08","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f7a0f1fd-8b6f-4326-a415-6cc7d5c408e5","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00270cc0-2ede-41c8-af9c-77eb378987ae","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"284450de-c6fe-42c1-bea3-ae07f71c9241","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e9759eb2-6832-4e00-9f94-26c8a12910e7","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0896dc1b-6eb6-47cf-908c-eb40582db5bb","path":"sprites/spr_bCred/spr_bCred.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_bCred","path":"sprites/spr_bCred/spr_bCred.yy",},
    "resourceVersion": "1.3",
    "name": "spr_bCred",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5112f8a5-396b-42d9-97ea-e62759df6103","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_bCred",
  "tags": [],
  "resourceType": "GMSprite",
}