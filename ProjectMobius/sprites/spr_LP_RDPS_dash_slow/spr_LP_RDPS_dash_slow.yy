{
  "$GMSprite":"",
  "%Name":"spr_LP_RDPS_dash_slow",
  "bboxMode":0,
  "bbox_bottom":14,
  "bbox_left":0,
  "bbox_right":12,
  "bbox_top":2,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"b6689874-548e-4be0-a015-45434efc8049","name":"b6689874-548e-4be0-a015-45434efc8049","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ac6e2e6e-4d90-47f7-a160-4db08adf5b55","name":"ac6e2e6e-4d90-47f7-a160-4db08adf5b55","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9d3a68bb-d01d-4576-9456-320f05b7598b","name":"9d3a68bb-d01d-4576-9456-320f05b7598b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ad2551a3-7330-485a-819a-b564507aeced","name":"ad2551a3-7330-485a-819a-b564507aeced","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"0a23542a-f9b9-4fae-a800-7a8c39910a51","blendMode":0,"displayName":"default","isLocked":false,"name":"0a23542a-f9b9-4fae-a800-7a8c39910a51","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_LP_RDPS_dash_slow",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"RangedDPS",
    "path":"folders/Sprites/Characters/Enemy/Yaki/Old/LightParty/RangedDPS.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_LP_RDPS_dash_slow",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_LP_RDPS_dash_slow",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b6689874-548e-4be0-a015-45434efc8049","path":"sprites/spr_LP_RDPS_dash_slow/spr_LP_RDPS_dash_slow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"50267250-7d4a-410e-9cbb-84c8a81b3631","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ac6e2e6e-4d90-47f7-a160-4db08adf5b55","path":"sprites/spr_LP_RDPS_dash_slow/spr_LP_RDPS_dash_slow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1d0f9de9-9504-4d2c-9395-538e820c299d","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9d3a68bb-d01d-4576-9456-320f05b7598b","path":"sprites/spr_LP_RDPS_dash_slow/spr_LP_RDPS_dash_slow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fea6846a-9dc4-4c0f-b1f3-dbb494139e5a","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ad2551a3-7330-485a-819a-b564507aeced","path":"sprites/spr_LP_RDPS_dash_slow/spr_LP_RDPS_dash_slow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3c690ca2-8227-49ee-97d3-4576ac82f13e","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":7,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":14,
}