{
  "$GMSprite":"",
  "%Name":"spr_KitsuneSamurai_incap",
  "bboxMode":0,
  "bbox_bottom":29,
  "bbox_left":2,
  "bbox_right":29,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"89e4b2f3-734a-45c2-8c3e-355c5b09c253","name":"89e4b2f3-734a-45c2-8c3e-355c5b09c253","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c2672c10-3a76-4c18-9e15-0d90fff407b0","name":"c2672c10-3a76-4c18-9e15-0d90fff407b0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7187b084-9026-4434-8c87-6c972ba68229","name":"7187b084-9026-4434-8c87-6c972ba68229","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5d0a552-08cc-4aa9-be4d-73bc2dac69a2","name":"c5d0a552-08cc-4aa9-be4d-73bc2dac69a2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"deaef86d-3b7d-4539-98de-254dc47e5d0c","name":"deaef86d-3b7d-4539-98de-254dc47e5d0c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"189aa6d2-6ae4-4327-8279-5f6376589a3b","name":"189aa6d2-6ae4-4327-8279-5f6376589a3b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2bdad7b-bf4d-4b33-80cc-541a6acc32c0","name":"d2bdad7b-bf4d-4b33-80cc-541a6acc32c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1177878c-88da-411b-885a-2da6a6697c9b","name":"1177878c-88da-411b-885a-2da6a6697c9b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"aac42bc2-0b91-44e2-88ce-dd09bd8455b8","name":"aac42bc2-0b91-44e2-88ce-dd09bd8455b8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":36,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"21948640-2410-49dc-a11e-1a587f811e12","blendMode":0,"displayName":"default","isLocked":false,"name":"21948640-2410-49dc-a11e-1a587f811e12","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"spr_KitsuneSamurai_incap",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Samurai",
    "path":"folders/Sprites/Characters/Enemy/Kitsune/Samurai.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"spr_KitsuneSamurai_incap",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"spr_KitsuneSamurai_incap",
    "playback":1,
    "playbackSpeed":10.0,
    "playbackSpeedType":0,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"89e4b2f3-734a-45c2-8c3e-355c5b09c253","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5679fef7-562b-4b5b-8d7e-fb3c634fbd0c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c2672c10-3a76-4c18-9e15-0d90fff407b0","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c00a0fad-e73d-4e7b-9a1f-650f38286b89","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7187b084-9026-4434-8c87-6c972ba68229","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aa2573de-30f7-4fb6-b167-6405751db859","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5d0a552-08cc-4aa9-be4d-73bc2dac69a2","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"18a2ac47-4df1-4a74-bfdf-9a23c17c190e","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"deaef86d-3b7d-4539-98de-254dc47e5d0c","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3227e7a5-5c38-4c93-bb71-a6525849145e","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"189aa6d2-6ae4-4327-8279-5f6376589a3b","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"57b8860f-d905-4f53-bd4e-8e2760612ac0","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2bdad7b-bf4d-4b33-80cc-541a6acc32c0","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7aba67d8-6f47-4c35-8166-0304343b7d25","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1177878c-88da-411b-885a-2da6a6697c9b","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f35ffbf9-9856-4bab-8f99-d596530868e2","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"aac42bc2-0b91-44e2-88ce-dd09bd8455b8","path":"sprites/spr_KitsuneSamurai_incap/spr_KitsuneSamurai_incap.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"445d0601-e65f-4e02-b5af-8cd6e50ca671","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":18,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"txg_Characters",
    "path":"texturegroups/txg_Characters",
  },
  "type":0,
  "VTile":false,
  "width":32,
}