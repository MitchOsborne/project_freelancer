{
  "$GMTileSet":"v1",
  "%Name":"tSet_Desert_16x16",
  "autoTileSets":[
    {"$GMAutoTileSet":"","%Name":"autotile_1","closed_edge":false,"name":"autotile_1","resourceType":"GMAutoTileSet","resourceVersion":"2.0","tiles":[
        17,
        43,
        40,
        2,
        11,
        35,
        1,
        1,
        8,
        1,
        32,
        1,
        10,
        1,
        1,
        2,
      ],},
    {"$GMAutoTileSet":"","%Name":"autotile_2","closed_edge":false,"name":"autotile_2","resourceType":"GMAutoTileSet","resourceVersion":"2.0","tiles":[
        22,
        37,
        36,
        2,
        21,
        17,
        6,
        1,
        20,
        6,
        19,
        3,
        34,
        33,
        35,
        6,
      ],},
  ],
  "macroPageTiles":{
    "SerialiseHeight":32,
    "SerialiseWidth":18,
    "TileCompressedData":[
      -9,
      0,
      1,
      136,
      -17,
      0,
      1,
      136,
      -12,
      0,
      6,
      32,
      33,
      35,
      0,
      0,
      168,
      -4,
      0,
      4,
      97,
      98,
      97,
      98,
      -4,
      0,
      3,
      64,
      2,
      67,
      -15,
      0,
      3,
      96,
      98,
      99,
      -15,
      0,
      8,
      128,
      3,
      131,
      0,
      0,
      128,
      0,
      131,
      -10,
      0,
      12,
      128,
      3,
      131,
      0,
      0,
      128,
      0,
      131,
      0,
      128,
      0,
      131,
      -6,
      0,
      12,
      160,
      3,
      163,
      0,
      0,
      160,
      0,
      163,
      0,
      160,
      0,
      163,
      -30,
      0,
      2,
      142,
      139,
      -16,
      0,
      2,
      126,
      123,
      -384,
      0,
    ],
    "TileDataFormat":1,
  },
  "name":"tSet_Desert_16x16",
  "out_columns":32,
  "out_tilehborder":2,
  "out_tilevborder":2,
  "parent":{
    "name":"Tile Sets",
    "path":"folders/Tile Sets.yy",
  },
  "resourceType":"GMTileSet",
  "resourceVersion":"2.0",
  "spriteId":{
    "name":"spr_env_desert_16x16_1bit",
    "path":"sprites/spr_env_desert_16x16_1bit/spr_env_desert_16x16_1bit.yy",
  },
  "spriteNoExport":true,
  "textureGroupId":{
    "name":"txg_Environment",
    "path":"texturegroups/txg_Environment",
  },
  "tileAnimationFrames":[
    {"$GMTileAnimation":"","%Name":"animation_1","frames":[
        224,
        227,
        230,
        233,
      ],"name":"animation_1","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_2","frames":[
        256,
        259,
        262,
        265,
      ],"name":"animation_2","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_3","frames":[
        288,
        291,
        294,
        297,
      ],"name":"animation_3","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_4","frames":[
        289,
        292,
        295,
        298,
      ],"name":"animation_4","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_5","frames":[
        290,
        293,
        296,
        299,
      ],"name":"animation_5","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_6","frames":[
        258,
        261,
        264,
        267,
      ],"name":"animation_6","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_7","frames":[
        226,
        229,
        232,
        235,
      ],"name":"animation_7","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_8","frames":[
        225,
        228,
        231,
        234,
      ],"name":"animation_8","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_10","frames":[
        236,
        237,
        238,
        239,
      ],"name":"animation_10","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_11","frames":[
        268,
        269,
        270,
        271,
      ],"name":"animation_11","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_12","frames":[
        332,
        333,
        334,
        335,
      ],"name":"animation_12","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_13","frames":[
        352,
        355,
        358,
        361,
      ],"name":"animation_13","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_14","frames":[
        384,
        387,
        390,
        393,
      ],"name":"animation_14","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_15","frames":[
        416,
        419,
        422,
        425,
      ],"name":"animation_15","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_16","frames":[
        417,
        420,
        423,
        426,
      ],"name":"animation_16","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_17","frames":[
        418,
        421,
        424,
        427,
      ],"name":"animation_17","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_18","frames":[
        386,
        389,
        392,
        395,
      ],"name":"animation_18","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_19","frames":[
        354,
        357,
        360,
        363,
      ],"name":"animation_19","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_20","frames":[
        353,
        356,
        359,
        362,
      ],"name":"animation_20","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_21","frames":[
        448,
        449,
        450,
        451,
        452,
        453,
        454,
        455,
      ],"name":"animation_21","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
    {"$GMTileAnimation":"","%Name":"animation_22","frames":[
        480,
        481,
        482,
        483,
        484,
        485,
        486,
        487,
      ],"name":"animation_22","resourceType":"GMTileAnimation","resourceVersion":"2.0",},
  ],
  "tileAnimationSpeed":3.0,
  "tileHeight":16,
  "tilehsep":0,
  "tilevsep":0,
  "tileWidth":16,
  "tilexoff":0,
  "tileyoff":0,
  "tile_count":1024,
}