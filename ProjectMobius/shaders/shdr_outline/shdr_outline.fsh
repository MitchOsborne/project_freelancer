//
// Simple passthrough fragment shader
// Shout out to Shaun Spalding and "flyingsaucerinvasion" for the outline and colour logic
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float pixelH;
uniform float pixelW;
uniform vec3 outlineColor;

void main()
{
	vec2 offsetX;
	offsetX.x = pixelW;
	vec2 offsetY;
	offsetY.y = pixelH;
	
	//Gets the base alpha of the current cell
	float baseAlpha = texture2D(gm_BaseTexture, v_vTexcoord).a;
	
	if(baseAlpha == 0.0)
	{
		float alpha = baseAlpha;
	
		//Adds the alpha of the adjucent cells to the current alpha variable
		alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + offsetX).a);
		alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - offsetX).a);
		alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + offsetY).a);
		alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - offsetY).a);
		//alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + offsetX + offsetY).a);
		//alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord + offsetX - offsetY).a);
		//alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - offsetX + offsetY).a);
		//alpha += ceil(texture2D(gm_BaseTexture, v_vTexcoord - offsetX - offsetY).a);
	
	    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
		gl_FragColor.a = alpha;
		gl_FragColor.rgb = mix(gl_FragColor.rgb, outlineColor, float(baseAlpha == 0.0));
	}
}
