// Shout out to IDoZ_ for this shader code
// https://www.reddit.com/r/gamemaker/comments/91v8xv/vibrant_lighting_through_a_super_simple_shader/
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform sampler2D lighting;
uniform sampler2D gameview;
uniform float lightStr;
void main()
{
    vec4 light = texture2D(lighting, v_vTexcoord);
    vec4 main = texture2D(gameview, v_vTexcoord);
    vec4 finalcol;
	vec4 lightcol;

	lightcol.r = light.r * (lightStr+1.);
	lightcol.b = light.b * (lightStr+1.);
	lightcol.g = light.g * (lightStr+1.);


	finalcol.r = mix(main.r,main.r*lightcol.r,light.r);
	finalcol.g = mix(main.g,main.g*lightcol.g,light.g);
	finalcol.b = mix(main.b,main.b*lightcol.b,light.b);
	finalcol.a = 1.;


	finalcol.r = mix(finalcol.r,0.,1.-light.r);
	finalcol.g = mix(finalcol.g,0.,1.-light.g);
	finalcol.b = mix(finalcol.b,0.,1.-light.b);
            
    gl_FragColor = finalcol;
} 
