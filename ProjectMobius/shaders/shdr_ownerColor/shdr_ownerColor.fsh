//
// Simple passthrough fragment shader
// Shout out to Shaun Spalding and "flyingsaucerinvasion" for the outline and colour logic
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform vec3 ownerColor;

void main()
{
	
	//Gets the base alpha of the current cell
	float tRed = texture2D(gm_BaseTexture, v_vTexcoord).r;
	float tBlue = texture2D(gm_BaseTexture, v_vTexcoord).b;
	float tGreen = texture2D(gm_BaseTexture, v_vTexcoord).g;
	gl_FragColor = v_vColour * texture2D(gm_BaseTexture, v_vTexcoord);
	if((tRed == 1.0) && (tBlue == 1.0) && (tGreen == 0.0))
	{
		gl_FragColor.rgb = mix(gl_FragColor.rgb, ownerColor, gl_FragColor.a);
	}
}
