{
  "$GMAnimCurve":"",
  "%Name":"crv_Misc",
  "channels":[
    {"$GMAnimCurveChannel":"","%Name":"BloodParticle","colour":4290799884,"name":"BloodParticle","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":1.0,},
        {"th0":-0.027329976,"th1":0.007556674,"tv0":0.0,"tv1":0.0,"x":0.1,"y":1.0,},
        {"th0":-0.007556674,"th1":0.16511336,"tv0":0.0,"tv1":0.0,"x":0.2,"y":0.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":0.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"Linear","colour":4281083598,"name":"Linear","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":1.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
  ],
  "function":1,
  "name":"crv_Misc",
  "parent":{
    "name":"Animation Curves",
    "path":"folders/Animation Curves.yy",
  },
  "resourceType":"GMAnimCurve",
  "resourceVersion":"2.0",
}