{
  "$GMAnimCurve":"",
  "%Name":"crv_StatGrowths",
  "channels":[
    {"$GMAnimCurveChannel":"","%Name":"Dmg","colour":4290799884,"name":"Dmg","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":0.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"HPMed","colour":4281083598,"name":"HPMed","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"HPFast","colour":4279025727,"name":"HPFast","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":-0.040000003,"th1":0.16000001,"tv0":0.0,"tv1":0.0,"x":0.2,"y":0.6,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":0.8,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"HPSlow","colour":4279234797,"name":"HPSlow","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.3,"y":0.15,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.2,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"AtkSpd","colour":4279015665,"name":"AtkSpd","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":1.0,"th1":-1.0,"tv0":0.0,"tv1":0.0,"x":0.4,"y":0.6,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":1.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"AblDur","colour":4293197869,"name":"AblDur","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":1.0,"th1":-1.0,"tv0":0.0,"tv1":0.0,"x":0.4,"y":1.7,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":2.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
    {"$GMAnimCurveChannel":"","%Name":"AblCD","colour":4293544448,"name":"AblCD","points":[
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":0.0,"y":0.0,},
        {"th0":1.0,"th1":-1.0,"tv0":0.0,"tv1":0.0,"x":0.4,"y":1.7,},
        {"th0":-0.1,"th1":0.1,"tv0":0.0,"tv1":0.0,"x":1.0,"y":2.0,},
      ],"resourceType":"GMAnimCurveChannel","resourceVersion":"2.0","visible":true,},
  ],
  "function":1,
  "name":"crv_StatGrowths",
  "parent":{
    "name":"Animation Curves",
    "path":"folders/Animation Curves.yy",
  },
  "resourceType":"GMAnimCurve",
  "resourceVersion":"2.0",
}